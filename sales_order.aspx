﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master.Master" AutoEventWireup="true" CodeBehind="sales_order.aspx.cs" Inherits="InvyLap.Sales.sales_order" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(InIEvent);
        function InIEvent() {
            $("#toolbar").show();

            $("#left").addClass("forced-hide").css("display", "none");

            $("#main").css("margin-left", "0");

            $("div.scrollable").css("width", "480px");

            var checkbox = document.getElementById('<%= cb_useOfDeliveryDatePerItem.ClientID %>');
            var rdp_dateOfDelivery = $find('<%= rdp_dateOfDelivery.ClientID %>');
            rdp_dateOfDelivery.set_enabled(!(checkbox.checked));

            var currencyCB = document.getElementById('<%= cb_currencyPerItem.ClientID %>');
            var currencyDDL = $find('<%= rcb_currency.ClientID %>');
            currencyDDL.set_enabled(!(currencyCB.checked));

        }

        function ShowBox(msg) {
            document.getElementById("errorMSG").innerText = msg;
            document.getElementById("alert").click();
        }

        var HashCode = "<%=ConfigurationManager.AppSettings["HashCode"].ToString()%>";

        function OnSelectedIndexChanged(sender, eventArgs) {
            var item = eventArgs.get_item();
            var startIndex = item.get_value().indexOf(HashCode);
            var rcbID = sender.get_id();
            var txtID = document.getElementById(rcbID.replace("rcb_", "txt_"));
            txtID.value = item.get_value().substring(startIndex + HashCode.length);
        }

        function OnCodeChanged(txt, field) {
            var rcb_requestedBy = $find(field);

            var items = rcb_requestedBy.get_items();
            for (i = 0; i < items.get_count() ; i++) {
                var item = items.getItem(i);
                var itemValue = item.get_value();
                var startIndex = itemValue.indexOf(HashCode);
                var empCode = itemValue.substring(startIndex + HashCode.length);
                if (empCode == txt) {
                    item.select();
                    break;
                } else {
                    rcb_requestedBy.clearSelection();
                }
            }
        }

        function reflect2(text) {
            count = text.value.length;

            text.value = text.value.substr(0, 450);

            if (count < 450) {
                var charLeft = 450 - count;
                var txt = charLeft;
                document.getElementById("lbl_maxError2").innerHTML = "<font color=green>" + txt + "/450</font>";
            }
            else {
                document.getElementById("lbl_maxError2").innerHTML = "<font color=red>0/450</font>";
                return false;
            }
        }

        function reflect3(text) {
            count = text.value.length;

            text.value = text.value.substr(0, 450);

            var grid = $find("<%=RadGrid_Items.ClientID %>");
            var MasterTable = grid.get_masterTableView();

            var index = text.getAttribute('key');

            var review = MasterTable.get_dataItems()[index].findElement("lbl_maxError3");

            if (count < 450) {
                var charLeft = 450 - count;
                var txt = charLeft;
                review.innerHTML = "<font color=green>" + txt + "/450</font>";
            }
            else {
                review.innerHTML = "<font color=red>0/450</font>";
                return false;
            }
        }

        function reflect4(text) {
            count = text.value.length;

            text.value = text.value.substr(0, 450);


            var grid = $find("<%=radGrid_charges.ClientID %>");
            var MasterTable = grid.get_masterTableView();

            var index = text.getAttribute('key');

            var review = MasterTable.get_dataItems()[index].findElement("lbl_maxError4");

            if (count < 450) {
                var charLeft = 450 - count;
                var txt = charLeft;
                review.innerHTML = "<font color=green>" + txt + "/450</font>";
            }
            else {
                review.innerHTML = "<font color=red>0/450</font>";
                return false;
            }
        }

        function reflect5(text) {
            count = text.value.length;

            text.value = text.value.substr(0, 450);

            var grid = $find("<%=radGrid_tax.ClientID %>");
            var MasterTable = grid.get_masterTableView();

            var index = text.getAttribute('key');

            var review = MasterTable.get_dataItems()[index].findElement("lbl_maxError5");

            if (count < 450) {
                var charLeft = 450 - count;
                var txt = charLeft;
                review.innerHTML = "<font color=green>" + txt + "/450</font>";
            }
            else {
                review.innerHTML = "<font color=red>0/450</font>";
                return false;
            }
        }

        function reflect6(text) {
            count = text.value.length;

            text.value = text.value.substr(0, 450);

            var grid = $find("<%=radGrid_discount.ClientID %>");
            var MasterTable = grid.get_masterTableView();

            var index = text.getAttribute('key');

            var review = MasterTable.get_dataItems()[index].findElement("lbl_maxError6");

            if (count < 450) {
                var charLeft = 450 - count;
                var txt = charLeft;
                review.innerHTML = "<font color=green>" + txt + "/450</font>";
            }
            else {
                review.innerHTML = "<font color=red>0/450</font>";
                return false;
            }
        }

        function validationFailed(radAsyncUpload, args) {
            var $row = $(args.get_row());
            var erorMessage = getErrorMessage(radAsyncUpload, args);
            var span = createError(erorMessage);
            $row.addClass("ruError");
            $row.append(span);
        }

        function getErrorMessage(sender, args) {
            var fileExtention = args.get_fileName().substring(args.get_fileName().lastIndexOf('.') + 1, args.get_fileName().length);
            if (args.get_fileName().lastIndexOf('.') != -1) {//this checks if the extension is correct
                if (sender.get_allowedFileExtensions().indexOf(fileExtention) == -1) {
                    return ("<asp:Literal runat='server' Text='<%$Resources:Lang, this_file_type_is_not_supported %>' />");
                }
                else {
                    return ("<asp:Literal runat='server' Text='<%$Resources:Lang, this_file_exceeds_the_maximum_allowed_size %>' />");
                }
            }
            else {
                return ("<asp:Literal runat='server' Text='<<%$Resources:Lang, not_correct_extension %>' />");
            }
        }

        function createError(erorMessage) {
            var input = '<br /><span class="ruErrorMessage">' + erorMessage + ' </span>';
            return input;
        }

        function nodeClicking(sender, args) {
            var tree = sender.get_id();

            var comboBox = $find(tree.replace("rtv_", "rcb_"));

            var txt = document.getElementById(tree.replace("rtv_", "txt_"));

            var mainAddress = document.getElementById("lbl_customerBillAddress");

            var shipAddress = document.getElementById("lbl_customerShipAddress");

            var hf_mainAddress = document.getElementById("hf_customerBillAddress");

            var hf_shipAddress = document.getElementById("hf_customerShipAddress");

            var node = args.get_node()

            comboBox.set_text(node.get_text());
            comboBox.set_value(node.get_value());

            comboBox.trackChanges();
            comboBox.get_items().getItem(0).set_text(node.get_text());
            comboBox.get_items().getItem(0).set_value(node.get_value());
            comboBox.commitChanges();

            var startIndex = node.get_value().indexOf(HashCode);
            txt.value = node.get_value().substring(startIndex + HashCode.length);

            var vendorID = node.get_value().substring(0, startIndex);

            GetAddress(vendorID, shipAddress, 2, hf_shipAddress);

            GetAddress(vendorID, mainAddress, 1, hf_mainAddress);

            comboBox.hideDropDown();
        }

        function nodeClicking2(sender, args) {
            var comboBox = $find("<%= rcb_customer.ClientID %>");

            var node = args.get_node()
            if (node.get_nodes().get_count() > 0) {
                comboBox.set_value("<%= Resources.Lang.choose_account %>");
                comboBox.set_text("");
            } else {
                comboBox.set_text(node.get_text());
                comboBox.set_value(node.get_value());

                comboBox.trackChanges();
                comboBox.get_items().getItem(0).set_text(node.get_text());
                comboBox.get_items().getItem(0).set_value(node.get_value());
                comboBox.commitChanges();

                comboBox.hideDropDown();
            }
        }

        function GetAddress(id, control, type, hf) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: '<%=ResolveUrl("~/cntries.asmx/GetVendorAddress") %>',
                data: '{"vendorID": "' + id + '", "type": "' + type + '" }',
                processData: false,
                dataType: "json",
                success: function (response) {
                    if (response.d == "") {
                        control.innerText = '<%= Resources.Lang.no_address_available %>';
                        hf.value = "";
                    } else {
                        //alert(response.d);
                        var startIndex = response.d.indexOf(HashCode);
                        control.innerText = response.d.substring(startIndex + HashCode.length);
                        hf.value = response.d.substring(0, startIndex);
                    }
                },
                error: function (a, b, c) {
                    control.innerText = '<%= Resources.Lang.no_address_available %>';
                    hf.value = "";
                }
            });
            }

            function TreeCodeChanged(txt, comboControl, treeControl) {
                var combo = $find(comboControl);

                var tree = $find(treeControl);

                var mainAddress = document.getElementById("lbl_customerBillAddress");

                var shipAddress = document.getElementById("lbl_customerShipAddress");

                var hf_mainAddress = document.getElementById("hf_customerBillAddress");

                var hf_shipAddress = document.getElementById("hf_customerShipAddress");

                mainAddress.innerText = "";
                shipAddress.innerText = "";

                var nodes = tree.get_allNodes(); shipAddress

                for (var i = 0; i < nodes.length; i++) {
                    if (nodes[i].get_nodes() != null) {
                        nodes[i].collapse();

                        var node = nodes[i];

                        var startIndex = node.get_value().indexOf(HashCode);

                        var empCode = node.get_value().substring(startIndex + HashCode.length);

                        var id = node.get_value().substring(0, startIndex);

                        if (empCode == txt) {
                            var node2 = node.get_parent();

                            while (node2 != null) {
                                if (node2.expand) {
                                    node2.expand();
                                }

                                node2 = node2.get_parent();
                            }

                            node.expand();
                            node.set_expanded(true);
                            node.select();

                            combo.set_value(node.get_value());
                            combo.set_text(node.get_text());

                            combo.trackChanges();
                            combo.get_items().getItem(0).set_text(node.get_text());
                            combo.get_items().getItem(0).set_value(node.get_value());
                            combo.commitChanges();

                            GetAddress(id, shipAddress, 2, hf_shipAddress);

                            GetAddress(id, mainAddress, 1, hf_mainAddress);

                            break;
                        } else {
                            combo.clearSelection();
                        }
                    }
                }
            }

            function openDeliveryDate(checkbox) {
                var rdp_dateOfDelivery = $find('<%= rdp_dateOfDelivery.ClientID %>');
                rdp_dateOfDelivery.set_enabled(!(checkbox.checked));
                HideColumn(checkbox.checked, 12);
            }

            function openCurrency(checkbox) {
                var rcb_currency = $find('<%= rcb_currency.ClientID %>');
                rcb_currency.set_enabled(!(checkbox.checked));
                HideColumn(checkbox.checked, 11);
            }

            function HideColumn(checked, column) {
                var radGrid = $find("<%= RadGrid_Items.ClientID %>");

                if (checked) {
                    radGrid.get_masterTableView().showColumn(column);
                }
                else {
                    radGrid.get_masterTableView().hideColumn(column);
                }

                radGrid.get_element().style.width = "99%"
            }

            function ShowHideColumns(sender, eventArgs) {
                var cb_useCode = document.getElementById('<%= cb_useCode.ClientID %>');
                var cb_useCategory = document.getElementById('<%= cb_useCategory.ClientID %>');
                var cb_useDescription = document.getElementById('<%= cb_useDescription.ClientID %>');
                var cb_useDiscountPerItem = document.getElementById('<%= cb_useDiscountPerItem.ClientID %>');
                var cb_summary = document.getElementById('<%= cb_summary.ClientID %>');
                var cb_useCountryOFOrgin = document.getElementById('<%= cb_useCountryOFOrgin.ClientID %>');

                var checkbox = document.getElementById('<%= cb_useOfDeliveryDatePerItem.ClientID %>');

                var cb_currencyPerItem = document.getElementById('<%= cb_currencyPerItem.ClientID %>');

                if (cb_currencyPerItem.checked) {
                    sender.get_masterTableView().showColumn(11);
                }
                else {
                    sender.get_masterTableView().hideColumn(11);
                }

                if (checkbox.checked) {
                    sender.get_masterTableView().showColumn(12);
                }
                else {
                    sender.get_masterTableView().hideColumn(12);
                }

                if (cb_useCode.checked) {
                    sender.get_masterTableView().showColumn(1);
                } else {
                    sender.get_masterTableView().hideColumn(1);
                }

                if (cb_useCategory.checked) {
                    sender.get_masterTableView().showColumn(2);
                } else {
                    sender.get_masterTableView().hideColumn(2);
                }

                if (cb_useDescription.checked) {
                    sender.get_masterTableView().showColumn(4);
                } else {
                    sender.get_masterTableView().hideColumn(4);
                }

                if (cb_useDiscountPerItem.checked) {
                    sender.get_masterTableView().showColumn(9);
                } else {
                    sender.get_masterTableView().hideColumn(9);
                }

                if (cb_summary.checked) {
                    sender.get_masterTableView().showColumn(6);
                } else {
                    sender.get_masterTableView().hideColumn(6);
                }

                if (cb_useCountryOFOrgin.checked) {
                    sender.get_masterTableView().showColumn(13);
                } else {
                    sender.get_masterTableView().hideColumn(13);
                }

                sender.get_element().style.width = "99%"
            }

            function UseColumns(value, index) {
                var radGrid = $find("<%= RadGrid_Items.ClientID %>");

                if (value) {
                    radGrid.get_masterTableView().showColumn(index);
                }
                else {
                    radGrid.get_masterTableView().hideColumn(index);
                }

                radGrid.get_element().style.width = "99%"
            }

            function CheckValidation() {
                var txt = document.getElementById('<%= txt_isValidItems.ClientID %>');
                if (!Page_ClientValidate('save')) {
                    return false;
                } else if (txt.value != "") {
                    document.getElementById("errorMSG").innerText = txt.value;
                    document.getElementById("alert").click();
                    return false;
                } else {
                    return true;
                }
            }
    </script>

    <style>
        .widy {
            height: 256px;
        }

        @media screen and (-webkit-min-device-pixel-ratio:0) {
            .widy {
                height: 262px;
            }
        }

        #hf_vendorAddressID, #hf_addressTypeID, #txt_isValidItems {
            /*display: none;*/
        }

        #hf_customerBillAddress, #hf_customerShipAddress, #txt_isValidItems {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="box-title">
        <h3>
            <i class="icon-reorder"></i>
            <asp:Label ID="Label8" runat="server" Text="<%$Resources:Lang, sales_order %>"></asp:Label>
        </h3>
    </div>
    <asp:Panel ID="pnl_overAll" runat="server">
        <div class="row-fluid">
            <div class="span6">
                <div class="box box-color box-bordered">
                    <div class="box-title" style="margin-top: 0">
                        <ul class="tabs tabs-left" style='float: <%= Resources.Lang.style_float %>'>
                            <li class="active" style='float: <%= Resources.Lang.style_float %>'>
                                <a href="#tab_salesOrderData" data-toggle="tab">
                                    <asp:Label runat="server" ID="Label19" Text="<%$ Resources:Lang, sales_order_Data %>" /></a>
                            </li>
                            <li>
                                <a href="#tab_message" data-toggle="tab">
                                    <asp:Label runat="server" ID="Label3" Text="<%$ Resources:Lang, notes_and_message %>" /></a>
                            </li>
                            <li>
                                <a href="#tab_upload" data-toggle="tab">
                                    <asp:Label runat="server" ID="Label5" Text="<%$ Resources:Lang, upload_documents %>" /></a>
                            </li>

                        </ul>
                        <div class="actions" style='float: <%= Resources.Lang.style_float_reverse %>'>
                            <a href="#" class="btn btn-mini content-slideUp" style="display: block"><i class="icon-angle-up"></i></a>
                        </div>
                    </div>
                    <div class="box-content" id="messageBox" style="height: 400px;">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_salesOrderData">
                                <table cellpadding="4">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lbl_salesOrderCodeLabel" runat="server" Text="<%$Resources:Lang, s_O_No %>" />:
                                        </td>
                                        <td>
                                            <asp:Label ID="lbl_salesOrderCodeValue" runat="server" Text="" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lbl_paperBasedSalesOrderNumber" runat="server" Text="<%$Resources:Lang, paper_based_sono %>" />:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txt_paperBasedSalesOrderNumber" runat="server" Width="215" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <asp:CheckBox ID="cb_hold" runat="server" Text="<%$Resources:Lang, Hold %>" Checked="false" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lbl_requestedBy" runat="server" Text="<%$Resources:Lang, Requested_By %>" />:
                                        </td>
                                        <td>
                                            <telerik:RadComboBox ID="rcb_requestedBy" Width="230" Filter="Contains" runat="server" Skin="Metro" EmptyMessage="<%$Resources:Lang, Select_Employee %>" OnClientSelectedIndexChanged="OnSelectedIndexChanged" ClientIDMode="Static">
                                            </telerik:RadComboBox>
                                            <asp:RequiredFieldValidator ForeColor="Red" ErrorMessage="<%$Resources:Lang, please_select_requestedby_employee %>" ValidationGroup="save" ID="RequiredFieldValidator3" runat="server" Text="*" Display="Dynamic" ControlToValidate="rcb_requestedBy" />
                                            <asp:TextBox ClientIDMode="Static" ID="txt_requestedBy" runat="server" Width="150" onchange="OnCodeChanged(this.value, 'rcb_requestedBy')" onkeyup="OnCodeChanged(this.value,'rcb_requestedBy')" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lbl_date" runat="server" Text="<%$Resources:Lang, Date %>" />:
                                        </td>
                                        <td>
                                            <telerik:RadDatePicker ID="rdp_date" runat="server" Width="235" Height="28" Skin="Metro"></telerik:RadDatePicker>
                                            <asp:RequiredFieldValidator ForeColor="Red" ValidationGroup="save" ErrorMessage="<%$Resources:Lang, please_select_requested_date %>" ID="RequiredFieldValidator1" runat="server" Text="*" Display="Dynamic" ControlToValidate="rdp_date" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="tab-pane" id="tab_message">
                                <table cellpadding="5">
                                    <tr>
                                        <td valign="top">
                                            <asp:Label ID="lbl_note" runat="server" Text="<%$ Resources:Lang, Note %>" />:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txt_note" runat="server" Width="350" TextMode="MultiLine" Height="80" onkeypress="return reflect(this);" onkeyup="return reflect(this);" onchange="return reflect(this);" />
                                            <span id="lbl_maxError"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <hr />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lbl_messageTitle" runat="server" Text="<%$ Resources:Lang, Message_Title %>" />:
                                        </td>
                                        <td>
                                            <telerik:RadComboBox ID="rcb_messageTitle" Width="365" Filter="Contains" runat="server" Skin="Metro" EmptyMessage="<%$ Resources:Lang, select_message_title %>">
                                            </telerik:RadComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <asp:Label ID="lbl_message" runat="server" Text="<%$ Resources:Lang, Message_Body %>" />:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txt_message" runat="server" Width="350" TextMode="MultiLine" Height="80" onkeypress="return reflect2(this);" onkeyup="return reflect2(this);" onchange="return reflect2(this);" />
                                            <span id="lbl_maxError2"></span>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="tab-pane" id="tab_upload">
                                <table>
                                    <tr>
                                        <td valign="top">
                                            <asp:Label ID="lbl_poCopy" runat="server" Text="<%$ Resources:Lang, so_copy %>" />:
                                        </td>
                                        <td>
                                            <div class="scrollable" style="border: 0" data-height="300" data-start="bottom" data-visible="true">
                                                <telerik:RadAsyncUpload UploadedFilesRendering="BelowFileInput" OnClientValidationFailed="validationFailed" Width="320" runat="server" ID="rau_uploadedFiles" MaxFileSize="102400" AllowedFileExtensions="jpg,png,gif,bmp,doc,docx,xlsx,xls,pdf,txt" MultipleFileSelection="Automatic" />
                                                <div style="font-size: 10px; color: #b4b4b4">
                                                    Max size:100Kb - Allowed:jpg,png,gif,bmp,doc,docx,xls,xlsx,pdf,txt
                                                </div>
                                                <div>
                                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                        <ContentTemplate>
                                                            <telerik:RadGrid ID="rgv_attachments" runat="server" AutoGenerateColumns="False" Skin="Metro" EnableEmbeddedSkins="true" CellSpacing="0" GridLines="None" OnDeleteCommand="rgv_attachments_DeleteCommand">
                                                                <MasterTableView DataKeyNames="OrderTemplateAttachmentId" Dir="<%$Resources:Lang, grid_direction %>" NoMasterRecordsText="<%$Resources:Lang, no_records %>">
                                                                    <Columns>
                                                                        <telerik:GridTemplateColumn DataField="AttachmentName" HeaderText="<%$Resources:Lang, files %>" UniqueName="AttachmentName">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lbl_attachmentName" runat="server" Text='<%# Eval("AttachmentName") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>

                                                                        <telerik:GridButtonColumn ConfirmText="<%$Resources:Lang, confirm_delete %>" ButtonType="ImageButton"
                                                                            CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                                                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                                                                        </telerik:GridButtonColumn>
                                                                    </Columns>
                                                                </MasterTableView>
                                                            </telerik:RadGrid>
                                                            <asp:UpdateProgress ID="UpdateProgress5" runat="server" AssociatedUpdatePanelID="UpdatePanel4">
                                                                <ProgressTemplate>
                                                                    <div class="loader">
                                                                        <img src="../App_Themes/Default/img/ajax-loader.gif" />
                                                                    </div>
                                                                </ProgressTemplate>
                                                            </asp:UpdateProgress>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="span6">
                <div class="box box-color box-bordered">
                    <div class="box-title" style="margin-top: 0">

                        <ul class="tabs tabs-left" style='float: <%= Resources.Lang.style_float %>'>
                            <li class="active" style='float: <%= Resources.Lang.style_float %>'>
                                <a href="#tab_customerData" data-toggle="tab">
                                    <asp:Label runat="server" ID="Label4" Text="<%$ Resources:Lang, customer_data %>" /></a>
                            </li>
                            <li>
                                <a href="#tab_paymentData" data-toggle="tab">
                                    <asp:Label runat="server" ID="Label20" Text="<%$ Resources:Lang, Payment_Data %>" /></a>
                            </li>
                            <li>
                                <a href="#tab_sales" data-toggle="tab">
                                    <asp:Label runat="server" ID="Label1" Text="<%$ Resources:Lang, sales %>" /></a>
                            </li>
                            <li>
                                <a href="#tab_others" data-toggle="tab">
                                    <asp:Label runat="server" ID="Label6" Text="<%$ Resources:Lang, others %>" /></a>
                            </li>
                        </ul>
                        <div class="actions" style='float: <%= Resources.Lang.style_float_reverse %>'>
                            <a href="#" class="btn btn-mini content-slideUp" style="display: block"><i class="icon-angle-up"></i></a>
                        </div>
                    </div>
                    <div class="box-content" id="Div1" style="height: 400px;">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_customerData">
                                <div style="position: relative">
                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                        <ContentTemplate>
                                            <table cellpadding="5">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lbl_customer" runat="server" Text="<%$ Resources:Lang, customer %>" />:
                                                    </td>
                                                    <td>

                                                        <telerik:RadDropDownTree Width="230" ID="rcb_customer" runat="server" DataFieldID="CustomerVendorDataId" DataFieldParentID="ParentCustomerVendorId"
                                                            DataTextField="CustomerVendorName" DataValueField="CustomerVendorCode" Skin="Metro" DefaultMessage="<%$ Resources:Lang, select_customer %>" AutoPostBack="True" OnEntryAdded="rcb_customer_EntryAdded">
                                                        </telerik:RadDropDownTree>

                                                        <asp:RequiredFieldValidator ForeColor="Red" ValidationGroup="save" ID="RequiredFieldValidator4" runat="server" Text="*" Display="Dynamic" ControlToValidate="rcb_customer" ErrorMessage="<%$Resources:Lang, please_select_customer %>"/>
                                                        <asp:TextBox ClientIDMode="AutoID" ID="txt_customer" runat="server" Width="150" OnTextChanged="txt_customer_TextChanged" AutoPostBack="true" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" style="text-align: right">
                                                        <asp:LinkButton ID="lb_findQuotation" runat="server" Text="<%$Resources:Lang, find_quotation %>" OnClick="lb_findQuotation_Click"></asp:LinkButton>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lbl_shipVia" runat="server" Text="<%$ Resources:Lang, Ship_Via %>" />:
                                                    </td>
                                                    <td>
                                                        <asp:RadioButtonList ID="rbl_shipVia" runat="server" RepeatDirection="Horizontal">
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                        <asp:Label ID="lbl_shipmentAddressLabel" runat="server" Text="<%$ Resources:Lang, ship_to_address %>" />:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="lbl_customerShipAddress" CssClass="stop_resize" Enabled="false" ClientIDMode="Static" TextMode="MultiLine" Width="382" Height="100" runat="server"></asp:TextBox>

                                                        <asp:TextBox ID="hf_customerShipAddress" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>
                                                        <asp:LinkButton ID="lb_selectAnotherShipmentAddress" runat="server" OnClick="lb_selectAnotherShipmentAddress_Click" Text="<%$Resources:Lang, select_another_address %>"></asp:LinkButton>
                                                        &nbsp;&nbsp;
                                                    <asp:LinkButton ID="lb_editShipmentAddress" runat="server" OnClick="lb_editShipmentAddress_Click" Text="<%$Resources:Lang, Edit %>"></asp:LinkButton>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td valign="top">
                                                        <asp:Label ID="lbl_billAddressLabel" runat="server" Text="<%$ Resources:Lang, bill_to_address %>" />:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="lbl_customerBillAddress" CssClass="stop_resize" ClientIDMode="Static" Enabled="false" TextMode="MultiLine" Width="382" Height="100" runat="server"></asp:TextBox>

                                                        <asp:TextBox ID="hf_customerBillAddress" ClientIDMode="Static" runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>
                                                        <asp:LinkButton ID="lb_selectAnotherBillAddress" runat="server" OnClick="lb_selectAnotherBillAddress_Click" Text="<%$Resources:Lang, select_another_address %>"></asp:LinkButton>
                                                        &nbsp;&nbsp;
                                                    <asp:LinkButton ID="lb_editBillAddress" runat="server" OnClick="lb_editBillAddress_Click" Text="<%$Resources:Lang, Edit %>"></asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:LinkButton Text="" ID="lnkFake3" runat="server" />
                                            <asp:LinkButton Text="" ID="lnkFake4" runat="server" />
                                            <asp:LinkButton Text="" ID="lnkFake5" runat="server" />
                                            <asp:ModalPopupExtender ID="mpe_customerEdit" runat="server" PopupControlID="pnl_customerEdit" TargetControlID="lnkFake3"
                                                CancelControlID="btn_cancel_Customer" BackgroundCssClass="modalBackground">
                                            </asp:ModalPopupExtender>
                                            <asp:ModalPopupExtender ID="mpe_customerSelectAddress" runat="server" PopupControlID="pnl_selectCustomerShipmentAddress" TargetControlID="lnkFake4"
                                                CancelControlID="btn_closeCustomerPopup" BackgroundCssClass="modalBackground">
                                            </asp:ModalPopupExtender>
                                            <asp:ModalPopupExtender ID="mpe_validCustomerQuotation" runat="server" PopupControlID="pnl_validCustomerQuotation" TargetControlID="lnkFake5"
                                                CancelControlID="btn_closeValidCustomerQuotation" BackgroundCssClass="modalBackground">
                                            </asp:ModalPopupExtender>
                                            <asp:Panel ID="pnl_validCustomerQuotation" runat="server" CssClass="modalPopup" Style="display: none">

                                                <div class="header">
                                                    <asp:Label ID="Label15" runat="server" Text="<%$Resources:Lang, valid_customer_quotation %>" />
                                                    <asp:Label ID="Label22" runat="server" Text=" / " />
                                                    <asp:Label ID="Label23" runat="server" Text="<%$Resources:Lang, purchasing_order %>" />
                                                </div>
                                                <div class="body">
                                                    <div class="alert alert-info" style="margin-bottom: 0">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="Label21" runat="server" Text="Show" />:
                                                                </td>
                                                                <td>
                                                                    <asp:RadioButtonList ID="rbl_showItems" runat="server" OnSelectedIndexChanged="rbl_showItems_SelectedIndexChanged" AutoPostBack="true" RepeatDirection="Horizontal">
                                                                    </asp:RadioButtonList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <telerik:RadGrid ID="radGrid_validCustomerQuotations" runat="server" AllowPaging="True" AllowSorting="True" GridLines="None" Skin="Metro"
                                                        AutoGenerateColumns="False" CellSpacing="0" OnNeedDataSource="radGrid_validCustomerQuotations_NeedDataSource" OnItemDataBound="radGrid_validCustomerQuotations_ItemDataBound">
                                                        <MasterTableView CommandItemDisplay="None" DataKeyNames="QuotationID" Dir="<%$Resources:Lang, grid_direction %>" NoMasterRecordsText="<%$Resources:Lang, no_records %>">
                                                            <Columns>
                                                                <telerik:GridBoundColumn Visible="False" DataField="QuotationID" DataType="System.Int32" HeaderText="QuotationID" ReadOnly="True" SortExpression="QuotationID" UniqueName="QuotationID"></telerik:GridBoundColumn>

                                                                <telerik:GridTemplateColumn DataField="QuotationOrder" HeaderText="#" SortExpression="QuotationOrder" UniqueName="QuotationOrder">
                                                                    <ItemTemplate>
                                                                        <asp:HiddenField ID="hf_color" runat="server" Value='<%# Eval("StatusColor") %>' />
                                                                        <asp:HiddenField ID="hf_statusID" runat="server" Value='<%# Eval("StatusID") %>' />
                                                                        <asp:HiddenField ID="hf_validTillDate" runat="server" Value='<%# Eval("ValidTillDate") %>' />
                                                                        <asp:Label ID="lbl_QuotationOrder" runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>

                                                                <telerik:GridTemplateColumn DataField="QuotationCode" HeaderText="<%$Resources:Lang, code %>" SortExpression="QuotationCode" UniqueName="QuotationCode">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_QuotationCode" runat="server" Text='<%# Eval("QuotationCode") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>

                                                                <telerik:GridTemplateColumn DataField="DocumentTypeID" HeaderText="<%$Resources:Lang, document_type %>" SortExpression="DocumentTypeID" UniqueName="DocumentTypeID">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_DocumentTypeID" runat="server" Text='<%# (Eval("DocumentTypeID").ToString() == "1") ? Resources.Lang.purchasing_order : Resources.Lang.customer_quotation %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>

                                                                <telerik:GridTemplateColumn DataField="PaperQuotationNo" HeaderText="<%$Resources:Lang, Paper_Based_Q %>" SortExpression="PaperQuotationNo" UniqueName="PaperQuotationNo">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_PaperQuotationNo" runat="server" Text='<%# Eval("PaperQuotationNo") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>

                                                                <telerik:GridTemplateColumn DataField="RequestDate" HeaderText="<%$Resources:Lang, Request_Date %>" SortExpression="RequestDate" UniqueName="RequestDate">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_RequestDate" runat="server" Text='<%# Eval("RequestDate", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>

                                                                <telerik:GridTemplateColumn DataField="ValidTillDate" HeaderText="<%$Resources:Lang, valid_till_date %>" SortExpression="ValidTillDate" UniqueName="ValidTillDate">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_ValidTillDate" runat="server" Text='<%# Eval("ValidTillDate", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>

                                                                <telerik:GridTemplateColumn DataField="TotalAmount" HeaderText="<%$Resources:Lang, Total_Amount %>" SortExpression="TotalAmount" UniqueName="TotalAmount">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_TotalAmount" runat="server" Text='<%# Eval("TotalAmount", "{0:0.00}") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>

                                                                <telerik:GridTemplateColumn>
                                                                    <ItemStyle Width="40" />
                                                                    <ItemTemplate>
                                                                        <asp:Button ID="btn_select" runat="server" Text="<%$Resources:Lang, select %>" OnClick="btn_select_Click" />
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>

                                                            </Columns>
                                                        </MasterTableView>
                                                    </telerik:RadGrid>

                                                    <br />
                                                    <div style="text-align: center">
                                                        <asp:Button ID="btn_closeValidCustomerQuotation" runat="server" Text="<%$Resources:Lang, Close %>" />
                                                    </div>
                                                </div>

                                            </asp:Panel>
                                            <asp:Panel ID="pnl_customerEdit" runat="server" CssClass="modalPopup" Style="display: none">
                                                <div class="header">
                                                    <asp:Label ID="lbl_cEditTitle" runat="server" Text="<%$Resources:Lang, edit_address %>" />
                                                </div>
                                                <div class="body">
                                                    <div class="first-panel" style='float: <%= Resources.Lang.style_float %>'>
                                                        <table cellpadding="5">
                                                            <tr>
                                                                <td>
                                                                    <asp:Label runat="server" ID="Lbl_Country_Customer" Text="<%$ Resources:Lang, Country %>"></asp:Label>:</td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddl_country_Customer" runat="server" Width="250px"></asp:DropDownList>
                                                                    <asp:CascadingDropDown ID="Cascad_cntries_Customer" runat="server" TargetControlID="ddl_country_Customer" PromptText='<%$ Resources:Lang, Select_Country %>' ServicePath="~/cntries.asmx" ServiceMethod="GetCountries" Category="CountryId" LoadingText="Loading..." Enabled="True"></asp:CascadingDropDown>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label runat="server" ID="Lbl_City_Customer" Text="<%$ Resources:Lang, City %>"></asp:Label>:</td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddl_City_Customer" runat="server" Width="250px"></asp:DropDownList>
                                                                    <asp:CascadingDropDown ID="Cascad_City_Customer" runat="server" TargetControlID="ddl_City_Customer" PromptText='<%$ Resources:Lang, Select_City %>'
                                                                        ParentControlID="ddl_country_Customer" ServicePath="~/cntries.asmx" ServiceMethod="GetCities" Category="CityId" LoadingText="Loading..." Enabled="True">
                                                                    </asp:CascadingDropDown>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label runat="server" ID="Lbl_Governorate_Customer" Text="<%$ Resources:Lang, Governorate %>"></asp:Label>:</td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddl_Governorate_Customer" runat="server" Width="250px"></asp:DropDownList>
                                                                    <asp:CascadingDropDown ID="Cascad_Governorate_Customer" runat="server" TargetControlID="ddl_Governorate_Customer" PromptText='<%$ Resources:Lang, Select_Governorate %>'
                                                                        ParentControlID="ddl_City_Customer" ServicePath="~/cntries.asmx" ServiceMethod="GetGovernrs" Category="GovernorateId" LoadingText="Loading..." Enabled="True">
                                                                    </asp:CascadingDropDown>

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label runat="server" ID="Lbl_Neighborhood_Customer" Text="<%$ Resources:Lang, Neighborhood %>"></asp:Label>:</td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddl_Neighborhood_Customer" runat="server" Width="250px"></asp:DropDownList>
                                                                    <asp:CascadingDropDown ID="Cascad_Neighborhood_Customer" runat="server" TargetControlID="ddl_Neighborhood_Customer" PromptText='<%$ Resources:Lang, Select_Neighborhood %>'
                                                                        ParentControlID="ddl_Governorate_Customer" ServicePath="~/cntries.asmx" ServiceMethod="GetNeighbor" Category="NeighborhoodId" LoadingText="Loading..." Enabled="True">
                                                                    </asp:CascadingDropDown>
                                                                </td>

                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div class="second-panel" style='float: <%= Resources.Lang.style_float_reverse %>'>
                                                        <table cellpadding="5">
                                                            <tr>
                                                                <td valign="top">
                                                                    <asp:Label runat="server" ID="lbl_Address_Customer" Text="<%$ Resources:Lang, Address %>"></asp:Label>:</td>
                                                                <td>
                                                                    <asp:TextBox ID="txt_Address_Customer" runat="server" Width="250px" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label runat="server" ID="Lbl_Building_Customer" Text="<%$ Resources:Lang, Building_Number %>"></asp:Label>:</td>
                                                                <td>
                                                                    <asp:TextBox ID="TXT_Building_Customer" runat="server" Width="250"></asp:TextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label runat="server" ID="Lbl_Zip_Code_Customer" Text="<%$ Resources:Lang, Zip_Code %>"></asp:Label>:</td>
                                                                <td>
                                                                    <asp:TextBox ID="TXT_Zip_Code_Customer" runat="server" Width="250"></asp:TextBox></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="footer" align="right">
                                                    <asp:Button ID="btn_save_Customer" runat="server" CssClass="buttonStyle" Text="<%$ Resources:Lang, save_and_apply %>" OnClick="btn_save_Customer_Click" />

                                                    <asp:Button ID="btn_saveNew_Customer" runat="server" CssClass="buttonStyle" Text="<%$ Resources:Lang, save_for_this_po %>" OnClick="btn_saveNew_Customer_Click" />

                                                    <asp:Button ID="btn_cancel_Customer" runat="server" CssClass="buttonStyle" Text="<%$ Resources:Lang, Close %>" />
                                                </div>
                                            </asp:Panel>
                                            <!-- Customer select address popup -->
                                            <asp:Panel ID="pnl_selectCustomerShipmentAddress" runat="server" CssClass="modalPopup" Style="display: none">
                                                <div class="box box-bordered box-color lightgrey" style="margin: 5px;">
                                                    <div class="box-title">
                                                        <h3>
                                                            <asp:Label ID="lbl_tableCustomerShipAddessesTitle" runat="server" Text=""></asp:Label></h3>
                                                    </div>
                                                    <div class="box-content nopadding">
                                                        <div class="tabs-container">
                                                            <ul class="tabs tabs-inline tabs-left" runat="server" id="ul_customerShipmentAddressTitle">
                                                            </ul>
                                                        </div>
                                                        <div class="tab-content padding tab-content-inline" runat="server" id="div_customerShipmentAddressTitle">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="footer" align="right">
                                                    <asp:Button ID="btn_closeCustomerShipPopup" runat="server" CssClass="buttonStyle" Text="Close" />
                                                </div>
                                            </asp:Panel>
                                            <asp:Panel ID="pnl_selectCustomerMainAddress" runat="server" CssClass="modalPopup" Style="display: none">
                                                <div class="box box-bordered box-color lightgrey" style="margin: 5px;">
                                                    <div class="box-title">
                                                        <h3>
                                                            <asp:Label ID="lbl_tableCustomerMainAddessesTitle" runat="server" Text=""></asp:Label></h3>
                                                    </div>
                                                    <div class="box-content nopadding">
                                                        <div class="tabs-container">
                                                            <ul class="tabs tabs-inline tabs-left" runat="server" id="ul_customerMainAddressTitle">
                                                            </ul>
                                                        </div>
                                                        <div class="tab-content padding tab-content-inline" runat="server" id="div_customerMainAddressTitle">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="footer" align="right">
                                                    <asp:Button ID="btn_closeCustomerMainPopup" runat="server" CssClass="buttonStyle" Text="Close" />
                                                </div>
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <asp:UpdateProgress ID="UpdateProgress6" runat="server" AssociatedUpdatePanelID="UpdatePanel3">
                                        <ProgressTemplate>
                                            <div class="loader">
                                                <img src="../App_Themes/Default/img/ajax-loader.gif" />
                                            </div>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab_paymentData">
                                <div style="position: relative">
                                    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                        <ContentTemplate>
                                            <table cellpadding="5">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lbl_currency" runat="server" Text="<%$ Resources:Lang, currency %>" />:
                                                    </td>
                                                    <td>
                                                        <telerik:RadComboBox ID="rcb_currency" ClientIDMode="AutoID" Width="200" Filter="Contains" runat="server" Skin="Metro" EmptyMessage="<%$ Resources:Lang, select_currency %>" OnSelectedIndexChanged="rcb_currency_SelectedIndexChanged" AutoPostBack="true">
                                                        </telerik:RadComboBox>
                                                        <asp:RequiredFieldValidator ForeColor="Red" ValidationGroup="save" ID="RequiredFieldValidator5" runat="server" Text="*" Display="Dynamic" ControlToValidate="rcb_currency"  ErrorMessage="<%$Resources:Lang, please_select_currency %>"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>
                                                        <asp:CheckBox ID="cb_currencyPerItem" onclick="openCurrency(this)" runat="server" Text="<%$ Resources:Lang, currency_per_item %>" OnCheckedChanged="cb_currencyPerItem_CheckedChanged" AutoPostBack="true" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lbl_noneCurrencyCost" runat="server" Text="<%$ Resources:Lang, None_Inventory_Cost %>" />:
                                                    </td>
                                                    <td>
                                                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                                            <ContentTemplate>
                                                                <telerik:RadNumericTextBox ShowSpinButtons="true" IncrementSettings-InterceptArrowKeys="true"
                                                                    IncrementSettings-InterceptMouseWheel="true" runat="server" AutoPostBack="true"
                                                                    ID="numeric_noneInventoryCost" Width="85px" Value="0" Min="0" OnTextChanged="numeric_noneCurrencyCost_TextChanged">
                                                                </telerik:RadNumericTextBox>
                                                                <asp:RequiredFieldValidator ForeColor="Red" ValidationGroup="save" ID="RequiredFieldValidator7" runat="server" Text="*" Display="Dynamic" ControlToValidate="numeric_noneInventoryCost" ErrorMessage="<%$Resources:Lang, please_insert_noneInventoryCost %>"/>
                                                                <asp:CheckBox ID="cb_PercentageNoneInventoryCost" runat="server" Text="<%$ Resources:Lang, Percentage %>" AutoPostBack="true" OnCheckedChanged="numeric_noneCurrencyCost_TextChanged" />
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Lang, deduction %>" />:
                                                    </td>
                                                    <td>
                                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                            <ContentTemplate>
                                                                <telerik:RadNumericTextBox ShowSpinButtons="true" IncrementSettings-InterceptArrowKeys="true"
                                                                    IncrementSettings-InterceptMouseWheel="true" runat="server" AutoPostBack="true"
                                                                    ID="numeric_deductiont" Width="85px" Value="0" Min="0" OnTextChanged="numeric_noneCurrencyCost_TextChanged">
                                                                </telerik:RadNumericTextBox>
                                                                <asp:RequiredFieldValidator ForeColor="Red" ValidationGroup="save" ID="RequiredFieldValidator2" runat="server" Text="*" Display="Dynamic" ControlToValidate="numeric_deductiont" ErrorMessage="<%$Resources:Lang, please_insert_deduction %>"/>
                                                                <asp:CheckBox ID="cb_deductionPercentage" runat="server" Text="<%$ Resources:Lang, Percentage %>" AutoPostBack="true" OnCheckedChanged="numeric_noneCurrencyCost_TextChanged" />
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lbl_paymentMethod" runat="server" Text="<%$ Resources:Lang, payment_method %>" />:
                                                    </td>
                                                    <td>
                                                        <telerik:RadComboBox ID="rcb_paymentMethod" ClientIDMode="Static" Width="200" Filter="Contains" runat="server" Skin="Metro" EmptyMessage="<%$ Resources:Lang, select_payment_method %>" OnClientSelectedIndexChanged="OnSelectedIndexChanged">
                                                        </telerik:RadComboBox>
                                                        <asp:RequiredFieldValidator ForeColor="Red" ValidationGroup="save" ID="RequiredFieldValidator8" runat="server" Text="*" Display="Dynamic" ControlToValidate="rcb_paymentMethod" ErrorMessage="<%$Resources:Lang, please_select_payment_method %>"/>
                                                        <asp:TextBox ClientIDMode="Static" ID="txt_paymentMethod" runat="server" Width="150" onchange="OnCodeChanged(this.value, 'rcb_paymentMethod')" onkeyup="OnCodeChanged(this.value,'rcb_paymentMethod')" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lbl_paymentTerm" runat="server" Text="<%$ Resources:Lang, payment_term %>" />:
                                                    </td>
                                                    <td>
                                                        <telerik:RadComboBox ClientIDMode="Static" ID="rcb_paymentTerm" Width="200" Filter="Contains" runat="server" Skin="Metro" EmptyMessage="<%$ Resources:Lang, select_payment_term %>" OnClientSelectedIndexChanged="OnSelectedIndexChanged">
                                                        </telerik:RadComboBox>
                                                        <asp:RequiredFieldValidator ForeColor="Red" ValidationGroup="save" ID="RequiredFieldValidator9" runat="server" Text="*" Display="Dynamic" ControlToValidate="rcb_paymentTerm" ErrorMessage="<%$Resources:Lang, please_select_payment_term %>"/>
                                                        <asp:TextBox ClientIDMode="Static" ID="txt_paymentTerm" runat="server" Width="150" onchange="OnCodeChanged(this.value, 'rcb_paymentTerm')" onkeyup="OnCodeChanged(this.value,'rcb_paymentTerm')" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lbl_dateOfDelivery" runat="server" Text="<%$ Resources:Lang, Delivery_Date %>" />:
                                                    </td>
                                                    <td>
                                                        <telerik:RadDatePicker ID="rdp_dateOfDelivery" Enabled="true" runat="server" Width="205" Height="28" Skin="Metro" SelectedDate="<%# DateTime.Today.Date  %>"></telerik:RadDatePicker>
                                                        <asp:RequiredFieldValidator ForeColor="Red" ValidationGroup="save" ID="RequiredFieldValidator10" runat="server" Text="*" Display="Dynamic" ControlToValidate="rdp_dateOfDelivery" ErrorMessage="<%$Resources:Lang, please_select_delivery_date %>"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>
                                                        <asp:CheckBox ID="cb_useOfDeliveryDatePerItem" onclick="openDeliveryDate(this)" runat="server" Checked="false" Text="<%$ Resources:Lang, use_of_delivery_date_peritem %>" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lbl_paymentDate" runat="server" Text="<%$ Resources:Lang, payment_date %>" />:
                                                    </td>
                                                    <td>
                                                        <telerik:RadDatePicker ID="rdp_paymentDate" runat="server" Width="205" Height="28" Skin="Metro"></telerik:RadDatePicker>
                                                        <asp:RequiredFieldValidator ForeColor="Red" ValidationGroup="save" ID="RequiredFieldValidator11" runat="server" Text="*" Display="Dynamic" ControlToValidate="rdp_paymentDate" ErrorMessage="<%$Resources:Lang, please_select_payment_date %>"/>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <asp:UpdateProgress ID="UpdateProgress7" runat="server" AssociatedUpdatePanelID="UpdatePanel7">
                                        <ProgressTemplate>
                                            <div class="loader">
                                                <img src="../App_Themes/Default/img/ajax-loader.gif" />
                                            </div>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab_sales">
                                <div style="position: relative">
                                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                        <ContentTemplate>
                                            <div>
                                                <asp:LinkButton ID="lb_addNewSalesPerson" runat="server" Text="<%$Resources:Lang, add %>" ValidationGroup="salesPerson" OnClick="lb_addNewSalesPerson_Click" />
                                            </div>

                                            <telerik:RadGrid ID="radGrid_salesPerson" runat="server" AllowPaging="true" AllowSorting="false" GridLines="None" Skin="Metro"
                                                AutoGenerateColumns="False" CellSpacing="0"
                                                OnItemDataBound="radGrid_salesPerson_ItemDataBound"
                                                OnDeleteCommand="radGrid_salesPerson_DeleteCommand"
                                                OnNeedDataSource="radGrid_salesPerson_NeedDataSource" PageSize="8">
                                                <MasterTableView CommandItemDisplay="None" DataKeyNames="ModifiedUserId" Dir="<%$Resources:Lang, grid_direction %>" NoMasterRecordsText="<%$Resources:Lang, no_records %>">
                                                    <Columns>
                                                        <telerik:GridBoundColumn Visible="False" DataField="SalesPersonId" DataType="System.Int32" HeaderText="SalesPersonId" ReadOnly="True" SortExpression="SalesPersonId" UniqueName="SalesPersonId"></telerik:GridBoundColumn>

                                                        <telerik:GridTemplateColumn DataField="ModifiedUserId" HeaderText="#" SortExpression="ModifiedUserId" UniqueName="ModifiedUserId">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_OrderIndex" runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>

                                                        <telerik:GridTemplateColumn DataField="SalesPersonCode" HeaderText="<%$Resources:Lang, Sales_Person_code %>" SortExpression="SalesPersonCode" UniqueName="SalesPersonCode">
                                                            <ItemTemplate>
                                                                <asp:TextBox AutoPostBack="true" ID="txt_SalesPersonCode" runat="server" Text='<%# Eval("SalesPersonCode") %>' Width="150px" OnTextChanged="txt_SalesPersonCode_TextChanged"></asp:TextBox>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>

                                                        <telerik:GridTemplateColumn DataField="SalesPersonId" HeaderText="<%$Resources:Lang, Sales_Person_Name %>" SortExpression="SalesPersonId" UniqueName="SalesPersonId">
                                                            <ItemTemplate>
                                                                <asp:HiddenField ID="hf_SalesPersonId" runat="server" Value='<%#Eval("SalesPersonId") %>' />
                                                                <telerik:RadComboBox AutoPostBack="true" ID="rcb_salesPerson" Filter="Contains" runat="server" Skin="Metro" EmptyMessage="<%$Resources:Lang, select_from_list %>" OnSelectedIndexChanged="rcb_salesPerson_SelectedIndexChanged">
                                                                </telerik:RadComboBox>
                                                                <asp:RequiredFieldValidator ForeColor="Red" ID="rfv_salesPerson" runat="server" Text="*" Display="Dynamic" ControlToValidate="rcb_salesPerson" ValidationGroup="salesPerson" />
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>

                                                        <telerik:GridTemplateColumn DataField="IsPrecentageCommission" HeaderText="<%$Resources:Lang, commision %>" SortExpression="IsPrecentageCommission" UniqueName="IsPrecentageCommission">
                                                            <ItemTemplate>
                                                                <asp:Label Width="100" ID="lbl_Commission" runat="server" Text='<%# (bool.Parse(Eval("IsPrecentageCommission").ToString()) == false) ? Eval("FixedAmountCommission" , "{0:0.00}") :  Eval("PrecentageCommission", "{0:0.00}") + " %" %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>

                                                        <telerik:GridButtonColumn ConfirmText="Delete This?" ButtonType="ImageButton"
                                                            CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                                                        </telerik:GridButtonColumn>
                                                    </Columns>
                                                </MasterTableView>
                                            </telerik:RadGrid>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <asp:UpdateProgress ID="UpdateProgress8" runat="server" AssociatedUpdatePanelID="UpdatePanel6">
                                        <ProgressTemplate>
                                            <div class="loader">
                                                <img src="../App_Themes/Default/img/ajax-loader.gif" />
                                            </div>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab_others">
                                <div class="scrollable" style="border: 0" data-height="350" data-start="bottom" data-visible="true">
                                    <asp:Panel ID="panel_customFields" runat="server">
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <div class="row-fluid">
            <div class="span12">
                <div class="box box-color box-bordered">
                    <div class="box-title" style="margin-top: 0">
                        <ul class="tabs tabs-left" style='float: <%= Resources.Lang.style_float %>'>
                            <li class="active" style='float: <%= Resources.Lang.style_float %>'>
                                <a href="#tab_item" data-toggle="tab">
                                    <asp:Label runat="server" ID="Label7" Text="<%$ Resources:Lang, item %>" /></a>
                            </li>
                            <li>
                                <a href="#tab_charge" data-toggle="tab">
                                    <asp:Label runat="server" ID="Label9" Text="<%$ Resources:Lang, charges %>" /></a>
                            </li>
                            <li>
                                <a href="#tab_tax" data-toggle="tab">
                                    <asp:Label runat="server" ID="Label10" Text="<%$ Resources:Lang, tax %>" /></a>
                            </li>
                            <li>
                                <a href="#tab_discount" data-toggle="tab">
                                    <asp:Label runat="server" ID="Label11" Text="<%$ Resources:Lang, discount %>" /></a>
                            </li>

                        </ul>

                    </div>
                    <div class="box-content" id="Div2" style="padding-top: 0">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_item">
                                <div style="position: relative">
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <br />
                                            <div class="alert alert-info" style="margin-bottom: 0">
                                                <table cellpadding="5" style="font-weight: bold">
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="cb_useCode" runat="server" Text="<%$ Resources:Lang, use_code %>" onclick="UseColumns(this.checked, '1')" />
                                                        </td>
                                                        <td>
                                                            <asp:CheckBox ID="cb_useCategory" runat="server" Text="<%$ Resources:Lang, use_category %>" onclick="UseColumns(this.checked, '2')" />
                                                        </td>
                                                        <td>
                                                            <asp:CheckBox ID="cb_summary" Checked="true" runat="server" Text="<%$ Resources:Lang, Show_Summary %>" onclick="UseColumns(this.checked, '7')" />
                                                        </td>
                                                        <td>
                                                            <asp:CheckBox ID="cb_useDescription" runat="server" Text="<%$ Resources:Lang, use_description %>" onclick="UseColumns(this.checked, '4')" />
                                                        </td>
                                                        <td>
                                                            <asp:CheckBox ID="cb_useDiscountPerItem" runat="server" Text="<%$ Resources:Lang, use_discount_per_item %>" onclick="UseColumns(this.checked, '9')" />
                                                        </td>
                                                        <td>
                                                            <asp:CheckBox ID="cb_useCountryOFOrgin" runat="server" Text="<%$ Resources:Lang, use_country_of_orgin %>" onclick="UseColumns(this.checked, '13')" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="box-title" style="margin-top: 0; background-color: #666 !important; border-color: #666 !important">
                                                <h3 style='float: <%= Resources.Lang.style_float %>'><i class="icon-ok"></i>
                                                    <asp:Label ID="Label12" runat="server" Text="<%$ Resources:Lang, selected_items %>" />&nbsp;&nbsp;&nbsp;</h3>

                                                <div class="actions" style='float: <%= Resources.Lang.style_float_reverse %>'>
                                                    <asp:LinkButton ID="lb_addNewItem" runat="server" CssClass="btn" OnClick="lb_addNewItem_Click">
                                                        <i class="icon-plus-sign"></i>&nbsp;<asp:Label ID="Label13" runat="server" Text="<%$ Resources:Lang, add_item %>" />
                                                    </asp:LinkButton><a href="#modal-4" role="button" id="alert" class="btn" data-toggle="modal"></a>
                                                </div>
                                            </div>
                                            <div class="box-content nopadding" style="border-color: #666 !important">
                                                <div style="overflow: scroll">
                                                    <telerik:RadGrid ID="RadGrid_Items" runat="server" AllowPaging="True" AllowSorting="true" GridLines="None" Skin="Metro"
                                                        AutoGenerateColumns="False" CellSpacing="0"
                                                        OnItemDataBound="RadGrid_Items_ItemDataBound"
                                                        OnDeleteCommand="RadGrid_Items_DeleteCommand"
                                                        OnNeedDataSource="RadGrid_Items_NeedDataSource">
                                                        <ClientSettings>
                                                            <ClientEvents OnGridCreated="ShowHideColumns" />
                                                        </ClientSettings>
                                                        <MasterTableView CommandItemDisplay="None" DataKeyNames="itemOrderID" Dir="<%$Resources:Lang, grid_direction %>" NoMasterRecordsText="<%$Resources:Lang, no_records %>">
                                                            <Columns>
                                                                <telerik:GridBoundColumn Visible="False" DataField="itemOrderID" DataType="System.Int32" HeaderText="itemOrderID" ReadOnly="True" SortExpression="itemOrderID" UniqueName="itemOrderID"></telerik:GridBoundColumn>

                                                                <telerik:GridTemplateColumn DataField="itemOrderIndex" HeaderText="#" SortExpression="itemOrderIndex" UniqueName="itemOrderIndex">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_itemOrderIndex" runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn DataField="itemCode" HeaderText="<%$Resources:Lang, Item_Code %>" SortExpression="itemCode" UniqueName="itemCode">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox AutoPostBack="true" ID="txt_itemCode" runat="server" Text='<%# Eval("itemCode") %>' Width="150px" OnTextChanged="txt_itemCode_TextChanged"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn DataField="itemCategoryID" HeaderText="<%$Resources:Lang, item_category %>" SortExpression="itemCategoryID" UniqueName="itemCategoryID">
                                                                    <ItemTemplate>
                                                                        <asp:HiddenField ID="hf_itemCategoryID" runat="server" Value='<%#Bind("itemCategoryID") %>' />
                                                                        <telerik:RadComboBox AutoPostBack="true" ID="rcb_itemCategory" Filter="Contains" runat="server" Skin="Metro" EmptyMessage="<%$Resources:Lang, select_category %>" OnSelectedIndexChanged="rcb_itemCategory_SelectedIndexChanged">
                                                                        </telerik:RadComboBox>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>

                                                                <telerik:GridTemplateColumn DataField="itemID" HeaderText="<%$Resources:Lang, Item %>" SortExpression="itemID" UniqueName="itemID">
                                                                    <ItemTemplate>
                                                                        <asp:HiddenField ID="hf_itemID" runat="server" Value='<%#Eval("itemID") %>' />
                                                                        <telerik:RadComboBox AutoPostBack="true" ID="rcb_items" Filter="Contains" runat="server" Skin="Metro" EmptyMessage="<%$Resources:Lang, select_from_list %>" OnSelectedIndexChanged="rcb_items_SelectedIndexChanged">
                                                                        </telerik:RadComboBox>
                                                                        <%--<asp:RequiredFieldValidator ForeColor="Red" ID="rfv_items" runat="server" Text="*" Display="Dynamic" ControlToValidate="rcb_items" />--%>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>

                                                                <telerik:GridTemplateColumn DataField="Description" HeaderText="<%$Resources:Lang, description %>" SortExpression="Description" UniqueName="Description">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox CssClass="stop_resize" AutoPostBack="true" TextMode="MultiLine" Height="40" ID="txt_description" runat="server" Text='<%# Eval("description") %>' Width="200px" OnTextChanged="txt_description_TextChanged" onkeypress="return reflect3(this);" onkeyup="return reflect3(this);" onchange="return reflect3(this);"></asp:TextBox>
                                                                        <asp:Label ID="lbl_maxError3" runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>

                                                                <telerik:GridTemplateColumn DataField="UOMID" HeaderText="<%$Resources:Lang, Units_measur %>" SortExpression="UOMID" UniqueName="UOMID">
                                                                    <ItemTemplate>
                                                                        <asp:HiddenField ID="hf_baseUOMID" runat="server" Value='<%#Eval("baseUOMID") %>' />
                                                                        <asp:HiddenField ID="hf_salesUOMID" runat="server" Value='<%#Eval("salesUOMID") %>' />
                                                                        <asp:HiddenField ID="hf_fixedSalesUOMID" runat="server" Value='<%#Eval("salesUOMID") %>' />
                                                                        <telerik:RadComboBox ID="rcb_UOM" AutoPostBack="true" OnSelectedIndexChanged="rcb_UOM_SelectedIndexChanged" Filter="Contains" runat="server" Skin="Metro" EmptyMessage="<%$Resources:Lang, select_unit %>">
                                                                        </telerik:RadComboBox>
                                                                        <%--<asp:RequiredFieldValidator ForeColor="Red" ID="rfv_UOM" runat="server" Text="*" Display="Dynamic" ControlToValidate="rcb_UOM" />--%>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>

                                                                <telerik:GridTemplateColumn DataField="quantity" HeaderText="<%$Resources:Lang, quantity %>" SortExpression="quantity" UniqueName="quantity">
                                                                    <ItemTemplate>
                                                                        <telerik:RadNumericTextBox ShowSpinButtons="true" IncrementSettings-InterceptArrowKeys="true" AutoPostBack="true"
                                                                            IncrementSettings-InterceptMouseWheel="true" runat="server" OnTextChanged="numeric_quantity_TextChanged"
                                                                            ID="numeric_quantity" Width="80px" Min="0" Height="28" Value='<%# (Eval("quantity") == null)? 0 : double.Parse(Eval("quantity").ToString()) %>'>
                                                                            <NumberFormat AllowRounding="false" DecimalDigits="6" />
                                                                        </telerik:RadNumericTextBox>
                                                                        <%--<asp:RequiredFieldValidator ForeColor="Red" ID="rfv_quantity" runat="server" Text="*" Display="Dynamic" ControlToValidate="numeric_quantity" />--%>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>

                                                                <telerik:GridTemplateColumn DataField="quantitySummary" HeaderText="<%$Resources:Lang, quantity_summary %>" SortExpression="quantitySummary" UniqueName="quantitySummary">
                                                                    <ItemTemplate>
                                                                        <asp:Label Width="100" ID="lbl_quantitySummary" runat="server" Text='<%# Eval("quantitySummary") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn DataField="unitPrice" HeaderText="<%$Resources:Lang, Unit_Price %>" SortExpression="unitPrice" UniqueName="unitPrice">
                                                                    <ItemTemplate>
                                                                        <asp:HiddenField ID="hf_fixedUnitPrice" runat="server" Value='<%# (Eval("unitPrice") == null)? 0 : double.Parse(Eval("unitPrice").ToString()) %>' />
                                                                        <telerik:RadNumericTextBox ShowSpinButtons="true" IncrementSettings-InterceptArrowKeys="true"
                                                                            IncrementSettings-InterceptMouseWheel="true" runat="server" AutoPostBack="true" OnTextChanged="numeric_unitPrice_TextChanged"
                                                                            ID="numeric_unitPrice" Width="80px" Min="0" Height="28" Value='<%# (Eval("unitPrice") == null)? 0 : double.Parse(Eval("unitPrice").ToString()) %>'>
                                                                            <NumberFormat AllowRounding="false" DecimalDigits="6" />
                                                                        </telerik:RadNumericTextBox>
                                                                        <%--<asp:RequiredFieldValidator ForeColor="Red" ID="rfv_unitPrice" runat="server" Text="*" Display="Dynamic" ControlToValidate="numeric_unitPrice" />--%>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>

                                                                <telerik:GridTemplateColumn DataField="discountPerItem" HeaderText="<%$Resources:Lang, discount_per_item %>" SortExpression="discountPerItem" UniqueName="discountPerItem">
                                                                    <ItemTemplate>
                                                                        <asp:HiddenField ID="hf_discountPerItem" runat="server" Value='<%# (Eval("discountPerItem") == null)? 0 : double.Parse(Eval("discountPerItem").ToString()) %>' />
                                                                        <telerik:RadNumericTextBox ShowSpinButtons="true" IncrementSettings-InterceptArrowKeys="true"
                                                                            IncrementSettings-InterceptMouseWheel="true" runat="server" AutoPostBack="true" OnTextChanged="numeric_discountPerItem_TextChanged"
                                                                            ID="numeric_discountPerItem" Width="80px" Max='<%# (Eval("unitPrice") == null)? 0 : double.Parse(Eval("unitPrice").ToString()) %>' Min="0" Height="28" Value='<%# (Eval("discountPerItem") == null)? 0 : double.Parse(Eval("discountPerItem").ToString()) %>'>
                                                                            <NumberFormat AllowRounding="false" DecimalDigits="6" />
                                                                        </telerik:RadNumericTextBox>

                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>

                                                                <telerik:GridTemplateColumn DataField="totalAmount" HeaderText="<%$Resources:Lang, Total_Amount %>" SortExpression="totalAmount" UniqueName="totalAmount">
                                                                    <ItemTemplate>
                                                                        <telerik:RadNumericTextBox ShowSpinButtons="true" IncrementSettings-InterceptArrowKeys="true"
                                                                            IncrementSettings-InterceptMouseWheel="true" runat="server" AutoPostBack="true" OnTextChanged="numeric_totalAmount_TextChanged"
                                                                            ID="numeric_totalAmount" Width="80px" Min="0" Height="28" Value='<%# (Eval("totalAmount") == null)? 0 : double.Parse(Eval("totalAmount").ToString()) %>'>
                                                                            <NumberFormat AllowRounding="false" DecimalDigits="6" />
                                                                        </telerik:RadNumericTextBox>
                                                                        <%--<asp:RequiredFieldValidator ForeColor="Red" ID="rfv_totalAmount" runat="server" Text="*" Display="Dynamic" ControlToValidate="numeric_totalAmount" />--%>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>

                                                                <telerik:GridTemplateColumn DataField="currency" HeaderText="<%$Resources:Lang, currency %>" SortExpression="currency" UniqueName="currency">
                                                                    <ItemTemplate>
                                                                        <asp:HiddenField ID="hf_itemCurrency" runat="server" Value='<%#Eval("currency") %>' />
                                                                        <telerik:RadComboBox AutoPostBack="true" OnSelectedIndexChanged="rcb_itemCurrency_SelectedIndexChanged" ID="rcb_itemCurrency" Filter="Contains" runat="server" Skin="Metro" EmptyMessage="<%$Resources:Lang, select_currency %>">
                                                                        </telerik:RadComboBox>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>

                                                                <telerik:GridTemplateColumn DataField="deliveryDate" HeaderText="<%$Resources:Lang, Delivery_Date %>" SortExpression="deliveryDate" UniqueName="deliveryDate">
                                                                    <ItemTemplate>
                                                                        <telerik:RadDatePicker ID="rdp_itemDeliveryDate" AutoPostBack="true" OnSelectedDateChanged="rdp_itemDeliveryDate_SelectedDateChanged" runat="server" Width="100" Height="28" Skin="Metro" SelectedDate='<%# (Eval("deliveryDate") == null) ? DateTime.Now : Eval("deliveryDate") %>'></telerik:RadDatePicker>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>

                                                                <telerik:GridTemplateColumn DataField="classID" HeaderText="<%$Resources:Lang, Country %>" SortExpression="classID" UniqueName="classID">
                                                                    <ItemTemplate>
                                                                        <asp:HiddenField ID="hf_classID" runat="server" Value='<%#Eval("classID") %>' />
                                                                        <telerik:RadComboBox AutoPostBack="true" OnSelectedIndexChanged="rcb_classes_SelectedIndexChanged" ID="rcb_classes" Filter="Contains" runat="server" Skin="Metro" EmptyMessage="<%$Resources:Lang, Select_Country %>">
                                                                        </telerik:RadComboBox>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>

                                                                <telerik:GridButtonColumn ConfirmText="Delete This?" ButtonType="ImageButton"
                                                                    CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                                                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                                                                </telerik:GridButtonColumn>
                                                            </Columns>
                                                        </MasterTableView>
                                                    </telerik:RadGrid>
                                                    <asp:TextBox ID="txt_isValidItems" runat="server" ClientIDMode="Static"></asp:TextBox>
                                                </div>
                                            </div>


                                            <script type="text/javascript">
                                                Sys.WebForms.PageRequestManager.getInstance().add_endRequest(InIEvent);
                                            </script>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <asp:UpdateProgress ID="UpdateProgress9" runat="server" AssociatedUpdatePanelID="UpdatePanel2">
                                        <ProgressTemplate>
                                            <div class="loader">
                                                <img src="../App_Themes/Default/img/ajax-loader.gif" style="padding-top: 130px" />
                                            </div>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab_charge" style="padding-top: 20px;">
                                <div style="position: relative">
                                    <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                        <ContentTemplate>
                                            <div class="box-title" style="margin-top: 0; background-color: #666 !important; border-color: #666 !important">
                                                <h3 style='float: <%= Resources.Lang.style_float %>'><i class="icon-ok"></i>
                                                    <asp:Label ID="Label24" runat="server" Text="<%$ Resources:Lang, charges %>" />&nbsp;&nbsp;&nbsp;</h3>

                                                <div class="actions" style='float: <%= Resources.Lang.style_float_reverse %>'>
                                                    <asp:LinkButton ID="lb_addNewCharge" ValidationGroup="addCharge" runat="server" CssClass="btn" OnClick="lb_addNewCharge_Click">
                                                        <i class="icon-plus-sign"></i>&nbsp;<asp:Label ID="Label25" runat="server" Text="<%$ Resources:Lang, add_item %>" />
                                                    </asp:LinkButton><a href="#modal-4" role="button" id="a1" class="btn" data-toggle="modal"></a>
                                                </div>
                                            </div>
                                            <div class="box-content nopadding" style="border-color: #666 !important">
                                                <div style="overflow: scroll">
                                                    <telerik:RadGrid ID="radGrid_charges" runat="server" AllowPaging="True" AllowSorting="true" GridLines="None" Skin="Metro"
                                                        AutoGenerateColumns="False" CellSpacing="0"
                                                        OnItemDataBound="radGrid_charges_ItemDataBound"
                                                        OnDeleteCommand="radGrid_charges_DeleteCommand"
                                                        OnNeedDataSource="radGrid_charges_NeedDataSource">
                                                        <MasterTableView CommandItemDisplay="None" DataKeyNames="itemOrderID" Dir="<%$Resources:Lang, grid_direction %>" NoMasterRecordsText="<%$Resources:Lang, no_records %>">
                                                            <Columns>
                                                                <telerik:GridBoundColumn Visible="False" DataField="itemOrderID" DataType="System.Int32" HeaderText="itemOrderID" ReadOnly="True" SortExpression="itemOrderID" UniqueName="itemOrderID"></telerik:GridBoundColumn>

                                                                <telerik:GridTemplateColumn DataField="itemOrderIndex" HeaderText="#" SortExpression="itemOrderIndex" UniqueName="itemOrderIndex">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_itemOrderIndex" runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>

                                                                <telerik:GridTemplateColumn DataField="itemCode" HeaderText="<%$Resources:Lang, code %>" SortExpression="itemCode" UniqueName="itemCode">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox AutoPostBack="true" ID="txt_ChargesCode" runat="server" Text='<%# Eval("itemCode") %>' Width="150px" OnTextChanged="txt_ChargesCode_TextChanged"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>

                                                                <telerik:GridTemplateColumn DataField="itemCategoryID" HeaderText="<%$Resources:Lang, charge_type %>" SortExpression="itemCategoryID" UniqueName="itemCategoryID">
                                                                    <ItemTemplate>
                                                                        <asp:HiddenField ID="hf_ChargesTypeId" runat="server" Value='<%#Bind("itemCategoryID") %>' />
                                                                        <telerik:RadComboBox AutoPostBack="true" ID="rcb_ChargesType" Filter="Contains" runat="server" Skin="Metro" EmptyMessage="<%$Resources:Lang, select_from_list %>" OnSelectedIndexChanged="rcb_ChargesType_SelectedIndexChanged">
                                                                        </telerik:RadComboBox>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>

                                                                <telerik:GridTemplateColumn DataField="itemID" HeaderText="<%$Resources:Lang, charges %>" SortExpression="itemID" UniqueName="itemID">
                                                                    <ItemTemplate>
                                                                        <asp:HiddenField ID="hf_ChargeId" runat="server" Value='<%#Eval("itemID") %>' />
                                                                        <telerik:RadComboBox AutoPostBack="true" ID="rcb_Charge" Filter="Contains" runat="server" Skin="Metro" EmptyMessage="<%$Resources:Lang, select_from_list %>" OnSelectedIndexChanged="rcb_Charge_SelectedIndexChanged">
                                                                        </telerik:RadComboBox>
                                                                        <asp:RequiredFieldValidator ForeColor="Red" ID="rfv_Charge" runat="server" ValidationGroup="addCharge" Text="*" Display="Dynamic" ControlToValidate="rcb_Charge" />
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>

                                                                <telerik:GridTemplateColumn DataField="Description" HeaderText="<%$Resources:Lang, description %>" SortExpression="Description" UniqueName="Description">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox CssClass="stop_resize" AutoPostBack="true" TextMode="MultiLine" Height="40" ID="txt_chargeDescription" runat="server" Text='<%# Eval("description") %>' Width="200px" OnTextChanged="txt_chargeDescription_TextChanged" onkeypress="return reflect4(this);" onkeyup="return reflect4(this);" onchange="return reflect4(this);"></asp:TextBox>
                                                                        <asp:Label ID="lbl_maxError4" runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>

                                                                <telerik:GridTemplateColumn DataField="quantitySummary" HeaderText="<%$Resources:Lang, amount %>" SortExpression="quantitySummary" UniqueName="quantitySummary">
                                                                    <ItemTemplate>
                                                                        <asp:Label Width="100" ID="lbl_quantitySummary" runat="server" Text='<%# Eval("quantitySummary") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>

                                                                <telerik:GridButtonColumn ConfirmText="Delete This?" ButtonType="ImageButton"
                                                                    CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                                                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                                                                </telerik:GridButtonColumn>
                                                            </Columns>
                                                        </MasterTableView>
                                                    </telerik:RadGrid>
                                                    <%--<asp:TextBox ID="TextBox1" runat="server" ClientIDMode="Static"></asp:TextBox>--%>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <asp:UpdateProgress ID="UpdateProgress10" runat="server" AssociatedUpdatePanelID="UpdatePanel8">
                                        <ProgressTemplate>
                                            <div class="loader">
                                                <img src="../App_Themes/Default/img/ajax-loader.gif" style="padding-top: 50px" />
                                            </div>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab_tax" style="padding-top: 20px;">
                                <div style="position: relative">
                                    <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                        <ContentTemplate>
                                            <div class="box-title" style="margin-top: 0; background-color: #666 !important; border-color: #666 !important">
                                                <h3 style='float: <%= Resources.Lang.style_float %>'><i class="icon-ok"></i>
                                                    <asp:Label ID="Label16" runat="server" Text="<%$ Resources:Lang, tax %>" />&nbsp;&nbsp;&nbsp;</h3>

                                                <div class="actions" style='float: <%= Resources.Lang.style_float_reverse %>'>
                                                    <asp:LinkButton ID="lb_addNewTax" ValidationGroup="addTax" runat="server" CssClass="btn" OnClick="lb_addNewTax_Click">
                                                        <i class="icon-plus-sign"></i>&nbsp;<asp:Label ID="Label31" runat="server" Text="<%$ Resources:Lang, add_item %>" />
                                                    </asp:LinkButton><a href="#modal-4" role="button" id="a2" class="btn" data-toggle="modal"></a>
                                                </div>
                                            </div>
                                            <div class="box-content nopadding" style="border-color: #666 !important">
                                                <div style="overflow: scroll">
                                                    <telerik:RadGrid ID="radGrid_tax" runat="server" AllowPaging="True" AllowSorting="true" GridLines="None" Skin="Metro"
                                                        AutoGenerateColumns="False" CellSpacing="0"
                                                        OnItemDataBound="radGrid_tax_ItemDataBound"
                                                        OnDeleteCommand="radGrid_tax_DeleteCommand"
                                                        OnNeedDataSource="radGrid_tax_NeedDataSource">
                                                        <MasterTableView CommandItemDisplay="None" DataKeyNames="itemOrderID" Dir="<%$Resources:Lang, grid_direction %>" NoMasterRecordsText="<%$Resources:Lang, no_records %>">
                                                            <Columns>
                                                                <telerik:GridBoundColumn Visible="False" DataField="itemOrderID" DataType="System.Int32" HeaderText="itemOrderID" ReadOnly="True" SortExpression="itemOrderID" UniqueName="itemOrderID"></telerik:GridBoundColumn>

                                                                <telerik:GridTemplateColumn DataField="itemOrderIndex" HeaderText="#" SortExpression="itemOrderIndex" UniqueName="itemOrderIndex">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_itemOrderIndex" runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>

                                                                <telerik:GridTemplateColumn DataField="itemCode" HeaderText="<%$Resources:Lang, code %>" SortExpression="itemCode" UniqueName="itemCode">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox AutoPostBack="true" ID="txt_taxCode" runat="server" Text='<%# Eval("itemCode") %>' Width="150px" OnTextChanged="txt_taxCode_TextChanged"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>

                                                                <telerik:GridTemplateColumn DataField="itemCategoryID" HeaderText="<%$Resources:Lang, tax_type %>" SortExpression="itemCategoryID" UniqueName="itemCategoryID">
                                                                    <ItemTemplate>
                                                                        <asp:HiddenField ID="hf_taxTypeId" runat="server" Value='<%#Bind("itemCategoryID") %>' />
                                                                        <telerik:RadComboBox AutoPostBack="true" ID="rcb_taxType" Filter="Contains" runat="server" Skin="Metro" EmptyMessage="<%$Resources:Lang, select_from_list %>" OnSelectedIndexChanged="rcb_taxType_SelectedIndexChanged">
                                                                        </telerik:RadComboBox>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>

                                                                <telerik:GridTemplateColumn DataField="itemID" HeaderText="<%$Resources:Lang, tax_item %>" SortExpression="itemID" UniqueName="itemID">
                                                                    <ItemTemplate>
                                                                        <asp:HiddenField ID="hf_taxId" runat="server" Value='<%#Eval("itemID") %>' />
                                                                        <telerik:RadComboBox AutoPostBack="true" ID="rcb_tax" Filter="Contains" runat="server" Skin="Metro" EmptyMessage="<%$Resources:Lang, select_from_list %>" OnSelectedIndexChanged="rcb_tax_SelectedIndexChanged">
                                                                        </telerik:RadComboBox>
                                                                        <asp:RequiredFieldValidator ForeColor="Red" ID="rfv_tax" runat="server" ValidationGroup="addTax" Text="*" Display="Dynamic" ControlToValidate="rcb_tax" />
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>

                                                                <telerik:GridTemplateColumn DataField="Description" HeaderText="<%$Resources:Lang, description %>" SortExpression="Description" UniqueName="Description">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox CssClass="stop_resize" AutoPostBack="true" TextMode="MultiLine" Height="40" ID="txt_taxDescription" runat="server" Text='<%# Eval("description") %>' Width="200px" OnTextChanged="txt_taxDescription_TextChanged" onkeypress="return reflect5(this);" onkeyup="return reflect5(this);" onchange="return reflect5(this);"></asp:TextBox>
                                                                        <asp:Label ID="lbl_maxError5" runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>

                                                                <telerik:GridTemplateColumn DataField="quantitySummary" HeaderText="<%$Resources:Lang, amount %>" SortExpression="quantitySummary" UniqueName="quantitySummary">
                                                                    <ItemTemplate>
                                                                        <asp:Label Width="100" ID="lbl_quantitySummary" runat="server" Text='<%# Eval("quantitySummary") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>

                                                                <telerik:GridButtonColumn ConfirmText="Delete This?" ButtonType="ImageButton"
                                                                    CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                                                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                                                                </telerik:GridButtonColumn>
                                                            </Columns>
                                                        </MasterTableView>
                                                    </telerik:RadGrid>
                                                    <%--<asp:TextBox ID="TextBox1" runat="server" ClientIDMode="Static"></asp:TextBox>--%>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <asp:UpdateProgress ID="UpdateProgress11" runat="server" AssociatedUpdatePanelID="UpdatePanel9">
                                        <ProgressTemplate>
                                            <div class="loader">
                                                <img src="../App_Themes/Default/img/ajax-loader.gif" style="padding-top: 50px" />
                                            </div>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab_discount" style="padding-top: 20px;">
                                <div style="position: relative">
                                    <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                                        <ContentTemplate>
                                            <div class="box-title" style="margin-top: 0; background-color: #666 !important; border-color: #666 !important">
                                                <h3 style='float: <%= Resources.Lang.style_float %>'><i class="icon-ok"></i>
                                                    <asp:Label ID="Label32" runat="server" Text="<%$ Resources:Lang, discount %>" />&nbsp;&nbsp;&nbsp;</h3>

                                                <div class="actions" style='float: <%= Resources.Lang.style_float_reverse %>'>
                                                    <asp:LinkButton ID="lb_addNewDiscount" ValidationGroup="addDiscount" runat="server" CssClass="btn" OnClick="lb_addNewDiscount_Click">
                                                        <i class="icon-plus-sign"></i>&nbsp;<asp:Label ID="Label33" runat="server" Text="<%$ Resources:Lang, add_item %>" />
                                                    </asp:LinkButton><a href="#modal-4" role="button" id="a3" class="btn" data-toggle="modal"></a>
                                                </div>
                                            </div>
                                            <div class="box-content nopadding" style="border-color: #666 !important">
                                                <div style="overflow: scroll">
                                                    <telerik:RadGrid ID="radGrid_discount" runat="server" AllowPaging="True" AllowSorting="true" GridLines="None" Skin="Metro"
                                                        AutoGenerateColumns="False" CellSpacing="0"
                                                        OnItemDataBound="radGrid_discount_ItemDataBound"
                                                        OnDeleteCommand="radGrid_discount_DeleteCommand"
                                                        OnNeedDataSource="radGrid_discount_NeedDataSource">
                                                        <MasterTableView CommandItemDisplay="None" DataKeyNames="itemOrderID" Dir="<%$Resources:Lang, grid_direction %>" NoMasterRecordsText="<%$Resources:Lang, no_records %>">
                                                            <Columns>
                                                                <telerik:GridBoundColumn Visible="False" DataField="itemOrderID" DataType="System.Int32" HeaderText="itemOrderID" ReadOnly="True" SortExpression="itemOrderID" UniqueName="itemOrderID"></telerik:GridBoundColumn>

                                                                <telerik:GridTemplateColumn DataField="itemOrderIndex" HeaderText="#" SortExpression="itemOrderIndex" UniqueName="itemOrderIndex">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbl_itemOrderIndex" runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>

                                                                <telerik:GridTemplateColumn DataField="itemCode" HeaderText="<%$Resources:Lang, code %>" SortExpression="itemCode" UniqueName="itemCode">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox AutoPostBack="true" ID="txt_discountCode" runat="server" Text='<%# Eval("itemCode") %>' Width="150px" OnTextChanged="txt_discountCode_TextChanged"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>

                                                                <telerik:GridTemplateColumn DataField="itemCategoryID" HeaderText="<%$Resources:Lang, Discount_Type %>" SortExpression="itemCategoryID" UniqueName="itemCategoryID">
                                                                    <ItemTemplate>
                                                                        <asp:HiddenField ID="hf_discountTypeId" runat="server" Value='<%#Bind("itemCategoryID") %>' />
                                                                        <telerik:RadComboBox AutoPostBack="true" ID="rcb_discountType" Filter="Contains" runat="server" Skin="Metro" EmptyMessage="<%$Resources:Lang, select_from_list %>" OnSelectedIndexChanged="rcb_discountType_SelectedIndexChanged">
                                                                        </telerik:RadComboBox>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>

                                                                <telerik:GridTemplateColumn DataField="itemID" HeaderText="<%$Resources:Lang, discount %>" SortExpression="itemID" UniqueName="itemID">
                                                                    <ItemTemplate>
                                                                        <asp:HiddenField ID="hf_discountId" runat="server" Value='<%#Eval("itemID") %>' />
                                                                        <telerik:RadComboBox AutoPostBack="true" ID="rcb_discount" Filter="Contains" runat="server" Skin="Metro" EmptyMessage="<%$Resources:Lang, select_from_list %>" OnSelectedIndexChanged="rcb_discount_SelectedIndexChanged">
                                                                        </telerik:RadComboBox>
                                                                        <asp:RequiredFieldValidator ForeColor="Red" ID="rfv_discount" runat="server" ValidationGroup="addDiscount" Text="*" Display="Dynamic" ControlToValidate="rcb_discount" />
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>

                                                                <telerik:GridTemplateColumn DataField="Description" HeaderText="<%$Resources:Lang, description %>" SortExpression="Description" UniqueName="Description">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox CssClass="stop_resize" AutoPostBack="true" TextMode="MultiLine" Height="40" ID="txt_discountDescription" runat="server" Text='<%# Eval("description") %>' Width="200px" OnTextChanged="txt_discountDescription_TextChanged" onkeypress="return reflect6(this);" onkeyup="return reflect6(this);" onchange="return reflect6(this);"></asp:TextBox>
                                                                        <asp:Label ID="lbl_maxError6" runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>

                                                                <telerik:GridTemplateColumn DataField="quantitySummary" HeaderText="<%$Resources:Lang, amount %>" SortExpression="quantitySummary" UniqueName="quantitySummary">
                                                                    <ItemTemplate>
                                                                        <asp:Label Width="100" ID="lbl_quantitySummary" runat="server" Text='<%# Eval("quantitySummary") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>

                                                                <telerik:GridButtonColumn ConfirmText="Delete This?" ButtonType="ImageButton"
                                                                    CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                                                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                                                                </telerik:GridButtonColumn>
                                                            </Columns>
                                                        </MasterTableView>
                                                    </telerik:RadGrid>
                                                    <%--<asp:TextBox ID="TextBox1" runat="server" ClientIDMode="Static"></asp:TextBox>--%>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <asp:UpdateProgress ID="UpdateProgress12" runat="server" AssociatedUpdatePanelID="UpdatePanel10">
                                        <ProgressTemplate>
                                            <div class="loader">
                                                <img src="../App_Themes/Default/img/ajax-loader.gif" style="padding-top: 50px" />
                                            </div>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="position: relative">
                    <asp:UpdatePanel runat="server" ID="updatePanel">
                        <ContentTemplate>
                            <div style='text-align: <%= Resources.Lang.style_float_reverse %>'>
                                <div class="alert alert-success" style="font-weight: bold">
                                    <asp:Label ID="Label17" runat="server" Text="<%$Resources:Lang, Total_Amount %>" />:
                                <asp:Label ID="lbl_itemsTotal" runat="server" />
                                    <br />
                                    <asp:Label ID="Label26" runat="server" Text="<%$Resources:Lang, deduction %>" />:
                                <asp:Label ID="lbl_totalDeduction" runat="server" />
                                    <br />
                                    <asp:Label ID="Label27" runat="server" Text="<%$Resources:Lang, Total_Tax %>" />:
                                <asp:Label ID="lbl_totalTax" runat="server" />
                                    <br />
                                    <asp:Label ID="lbl_salesTax1Label" runat="server" Visible="false" />
                                    <asp:Label ID="lbl_salesTax1Value" runat="server" Visible="false" />

                                    <asp:Label ID="lbl_salesTax2Label" runat="server" Visible="false" />
                                    <asp:Label ID="lbl_salesTax2Value" runat="server" Visible="false" />

                                    <asp:Label ID="lbl_salesTax3Label" runat="server" Visible="false" />
                                    <asp:Label ID="lbl_salesTax3Value" runat="server" Visible="false" />

                                    <asp:Label ID="Label28" runat="server" Text="<%$Resources:Lang, total_charge %>" />:
                                <asp:Label ID="lbl_totalCharges" runat="server" />
                                    <br />
                                    <asp:Label ID="Label29" runat="server" Text="<%$Resources:Lang, total_DISCOUNT %>" />:
                                <asp:Label ID="lbl_totalDiscount" runat="server" />
                                    <br />
                                    <asp:Label ID="Label30" runat="server" Text="<%$Resources:Lang, None_Inventory_Cost %>" />:
                                <asp:Label ID="lbl_totalNoneInventoryCost" runat="server" />
                                    <br />
                                    <asp:Label ID="Label18" runat="server" Text="----------------------------------------" />
                                    <br />
                                    <asp:Label ID="Label14" runat="server" Text="<%$Resources:Lang, total %>" />:
                                <asp:Label ID="lbl_totalAmount" runat="server" />
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdateProgress ID="UpdateProgress13" runat="server" AssociatedUpdatePanelID="updatePanel">
                        <ProgressTemplate>
                            <div class="loader">
                                <img src="../App_Themes/Default/img/ajax-loader.gif" style="padding-top: 40px" />
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </div>
                <div id="modal-4" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
                    <div class="modal-header">
                        <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 id="myModalLabel"><br /></h3>--%>
                    </div>
                    <div class="modal-body">
                        <p id="errorMSG">
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" data-dismiss="modal">Ok</button>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <div class="leftside">
        <!-- all things in floating left side -->
        <%--<asp:UpdatePanel ID="UpdatePanel5" runat="server">
            <ContentTemplate>--%>
        <ul id="social">
            <li>
                <asp:ImageButton OnClientClick="return CheckValidation();" CssClass="saveNew" ID="btn_saveAndNewOrder" runat="server" ValidationGroup="save" OnClick="btn_saveAndNewOrder_Click" ImageUrl="~/App_Themes/Default/img/save_and_new.png" />
                <span>
                    <asp:Label ID="Label34" runat="server" Text="<%$ Resources:Lang, save_addnew %>" />
                </span>
            </li>
            <li>
                <%--<asp:Button ID="btn_saveAndClose" runat="server" ValidationGroup="save" Text="<%$Resources:Lang, save_close %>" CssClass="buttonStyle" OnClick="btn_saveAndClose_Click" />--%>
                <asp:ImageButton OnClientClick="return CheckValidation();" CssClass="saveClose" ID="btn_saveAndClose" runat="server" ValidationGroup="save" OnClick="btn_saveAndClose_Click" ImageUrl="~/App_Themes/Default/img/save_and_close.png" />
                <span>
                    <asp:Label ID="Label35" runat="server" Text="<%$ Resources:Lang, save_close %>" />
                </span>
            </li>
            <li>
                <%--<asp:Button ID="btn_addAndCopy" runat="server" ValidationGroup="save" Text="<%$Resources:Lang, save_and_copy %>" CssClass="buttonStyle" OnClick="btn_addAndCopy_Click" />--%>
                <asp:ImageButton OnClientClick="return CheckValidation();" CssClass="savePrint" ID="btn_addAndCopy" runat="server" ValidationGroup="save" OnClick="btn_addAndCopy_Click" ImageUrl="~/App_Themes/Default/img/save_and_print.png" />
                <span>
                    <asp:Label ID="Label36" runat="server" Text="<%$ Resources:Lang, save_and_copy %>" />
                </span>
            </li>
            <li>
                <%--<asp:Button ID="btn_cancelOrder" runat="server" Text="<%$Resources:Lang, btn_Cancel %>" CssClass="buttonStyle" OnClick="btn_cancelOrder_Click" />--%>
                <asp:ImageButton CssClass="cancel" ID="btn_cancelOrder" runat="server" OnClick="btn_cancelOrder_Click" ImageUrl="~/App_Themes/Default/img/Cancel-icon.png" />
                <span>
                    <asp:Label ID="Label37" runat="server" Text="<%$ Resources:Lang, btn_Cancel %>" />
                </span>
            </li>
        </ul>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="List" ValidationGroup="save" ShowMessageBox="true" ShowSummary="false" ForeColor="Red" />

        <%--</ContentTemplate>
        </asp:UpdatePanel>--%>
    </div>
    <div class="rightside">
        <!-- all things in floating left side -->
        <span class="downarr">
            <!-- hide button -->
            <a href="#"></a></span><span class="menu_title"><a class="menutit" href="#">quick menu</a>
                <!-- quick menu title -->
            </span>
        <div class="quickmenu">
            <ul>
                <!-- quick menu list -->
                <asp:Repeater ID="rpt_relatedLink" runat="server">
                    <ItemTemplate>
                        <li>
                            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# Eval("key") %>'>
                                <asp:Label ID="Label15" runat="server" Text='<%# Eval("value") %>' />
                            </asp:HyperLink></li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
        </div>
    </div>
</asp:Content>
