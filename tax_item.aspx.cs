﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace InvyLap.Setup
{
    public partial class tax_item : System.Web.UI.Page
    {
        Boolean status;
        List<int> foundError = new List<int>();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                Privilage();
                LoadDLL();
                if (Session["lang"].ToString() != "E")
                    ImageButton3.Visible = false;
                List<BL.Setup.Tree> accounts = BL.Setup.cls_AccountType.AccountTree();
                RadTreeView tree1 = (RadTreeView)RadComboBox1.Items[0].FindControl("RadTreeView1");
                tree1.DataSource = accounts;
                tree1.DataBind();
                bool? status = BL.Setup.cls_ItemCategory.CheckModule(2);
                tr_account.Visible=(bool)status;
            }
        }
        public void Privilage()
        {
            List<KeyValuePair<int, bool>> functions = new List<KeyValuePair<int, bool>>();
            BL.Administration.cls_groups.GetPagePrivilage(28, out functions, int.Parse(Session["UserID"].ToString()));
            if (functions.Count > 0)
            {
                try
                {
                    if (functions.Where(a => a.Key == 6).SingleOrDefault().Value == false)
                    {
                        Response.Redirect("../Welcome/index.aspx");
                    }
                    RadGrid_Items.MasterTableView.GetColumn("DeleteColumn").Display = functions.Where(a => a.Key == 9).SingleOrDefault().Value;
                    RadGrid_Items.MasterTableView.GetColumn("Edit").Display = functions.Where(a => a.Key == 8).SingleOrDefault().Value;
                    lb_add.Visible = functions.Where(a => a.Key == 7).SingleOrDefault().Value;                    
                    RadTabStrip1.Tabs[1].Visible = functions.Where(a => a.Key == 7).SingleOrDefault().Value;
                    //if (functions.Where(a => a.Key == 7).SingleOrDefault().Value == false)
                    //{
                    //    RadGrid_Items.MasterTableView.CommandItemDisplay = GridCommandItemDisplay.None;
                    //}
                    //else
                    //{
                    //    RadGrid_Items.MasterTableView.CommandItemDisplay = GridCommandItemDisplay.Top;
                    //}
                }
                catch { }
            }
            else
            {
                Response.Redirect("../Welcome/index.aspx");
            }
        }
        protected override void InitializeCulture()
        {
            Settings.ChangeLang();

        }

        protected void rb_percentage_CheckedChanged(object sender, EventArgs e)
        {
            if (rb_percentage.Checked)
            {
                Numeric1.Enabled = true;
                Numeric2.Enabled = false;
                Numeric2.Text = "0.00";
                ddl_currency.Enabled = false;
            }
            else
            {
                Numeric1.Enabled = false;
                Numeric2.Enabled = true;
                Numeric1.Text = "0.00";
                ddl_currency.Enabled = true;
            }
        }

        protected void rb_amount_CheckedChanged(object sender, EventArgs e)
        {
            if (rb_amount.Checked)
            {
                Numeric1.Enabled = false;
                Numeric2.Enabled = true;
                Numeric1.Text = "0.00";
                ddl_currency.Enabled = true;
            }
            else
            {
                Numeric1.Enabled = true;
                Numeric2.Enabled = false;
                Numeric2.Text = "0.00";
                ddl_currency.Enabled = false;
            }
        }

        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                LoadDLL();
            }
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            if (lbl_error.Text == "")
            {
                string prec = "0.00";
                string amount = "0.00";

                int? typeID = null;
                int? accountID = null;

                if (ddl_type.SelectedValue != "")
                {
                    typeID = int.Parse(ddl_type.SelectedValue);
                }

                if (RadComboBox1.SelectedValue != "")
                {
                    accountID = int.Parse(RadComboBox1.SelectedValue);
                }

                if (Numeric1.Text != "")
                {
                    prec = Numeric1.Text;
                }

                if (Numeric2.Text != "")
                {
                    amount = Numeric2.Text;
                }

                if (ViewState["id"] == null)
                {
                    status = BL.Setup.cls_Tax.AddNewTaxItem(typeID, txt_name.Text, txt_description.Text, rb_percentage.Checked,
                                               decimal.Parse(prec), decimal.Parse(amount), int.Parse(ddl_currency.SelectedValue),
                                               accountID, cb_isActive.Checked, int.Parse(Session["UserID"].ToString()), out foundError);

                    UI.ShowSuccessMessage1(this, foundError, status);

                    RadGrid_Items.CurrentPageIndex = RadGrid_Items.PageCount - 1;
                    RadGrid_Items.Rebind();
                    Clear();
                    
                }
                else
                {
                    status = BL.Setup.cls_Tax.UpdateTaxItem(int.Parse(ViewState["id"].ToString()), typeID, txt_name.Text, txt_description.Text, rb_percentage.Checked,
                                               decimal.Parse(prec), decimal.Parse(amount), int.Parse(ddl_currency.SelectedValue),
                                               accountID, cb_isActive.Checked, int.Parse(Session["UserID"].ToString()), out foundError);

                    UI.ShowSuccessMessage1(this, foundError, status);
                    Clear();
                }
            }

        }

        private void Clear()
        {
            LoadDLL();
            lbl_code.Text = "";
            txt_name.Text = "";
            txt_description.Text = "";
            cb_isActive.Checked = true;
            rb_percentage.Checked = true;
            Numeric1.Text = "0.00";
            Numeric1.Enabled = true;
            rb_amount.Checked = false;
            Numeric2.Text = "0.00";
            Numeric2.Enabled = false;
            ddl_currency.DataBind();
            ddl_currency.Enabled = false;
            RadTreeView tree = (RadTreeView)RadComboBox1.Items[0].FindControl("RadTreeView1");
            tree.DataBind();
            tree.CollapseAllNodes();
            tree.UnselectAllNodes();
            RadComboBox1.ClearSelection();
            RadComboBox1.Text = string.Empty;
            RadGrid_Items.DataBind();
            ViewState["id"] = null;
            RadTabStrip1.SelectedIndex = 0;
            RadMultiPage1.SelectedIndex = 0;
        }

        protected void btn_cancel_Click(object sender, EventArgs e)
        {
            Clear();
        }

        protected void RadGrid_Items_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            GridDataItem item = (GridDataItem)e.Item;

            string ID = item.GetDataKeyValue("TaxItemId").ToString();

            status = BL.Setup.cls_Tax.DeleteTaxItem(int.Parse(ID), int.Parse(Session["UserID"].ToString()), out foundError);


            UI.ShowSuccessMessage1(this, foundError, status);
        }

        protected void txt_name_TextChanged(object sender, EventArgs e)
        {

            if (ViewState["id"] == null)
            {
                status = BL.Setup.cls_Tax.IsValidTaxItem(txt_name.Text, out foundError);
                List<string> errorMessages = BL.Configuration.cls_AppConfiguration.GetErrors(Session["lang"].ToString(), foundError);
                string success = "";
                foreach (string x in errorMessages)
                {
                    success += x + '\n';
                }

                if (status)
                {
                    lbl_error.Text = "";
                }
                else
                {
                    lbl_error.Text = success;
                }
            }
            else
            {
                status = BL.Setup.cls_Tax.IsValidUpdateTaxItem(int.Parse(ViewState["id"].ToString()), txt_name.Text, out foundError);
                List<string> errorMessages = BL.Configuration.cls_AppConfiguration.GetErrors(Session["lang"].ToString(), foundError);
                string success = "";
                foreach (string x in errorMessages)
                {
                    success += x + '\n';
                }

                if (status)
                {
                    lbl_error.Text = "";
                }
                else
                {
                    lbl_error.Text = success;
                }
            }
        }
            
        protected void lb_add_Click(object sender, EventArgs e)
        {
            Clear();
            RadTabStrip1.SelectedIndex = 1;
            RadMultiPage1.SelectedIndex = 1;
        }

        private void LoadDLL()
        {
            ddl_type.Items.Clear();

            Dictionary<int, string> types = BL.Setup.cls_Tax.GetTaxItemTypeList();
            ddl_type.DataSource = types;
            ddl_type.DataBind();

            ddl_type.Items.Insert(0, new ListItem(Resources.Lang.select_type, ""));
        }

        protected void RadTreeView1_NodeDataBound(object sender, RadTreeNodeEventArgs e)
        {
            if (e.Node.Nodes.Count > 0)
            {
                e.Node.CssClass = "dis";
            }
        }

        protected void RadGrid_Items_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridDataItem gridItem = (GridDataItem)RadGrid_Items.SelectedItems[0];
            string dataKey = gridItem.GetDataKeyValue("TaxItemId").ToString();

            ViewState["id"] = dataKey;

            RadTabStrip1.SelectedIndex = 1;
            RadMultiPage1.SelectedIndex = 1;

            DAL.Setup_TaxItem item = BL.Setup.cls_Tax.GetTaxItem(int.Parse(ViewState["id"].ToString()));

            ddl_type.SelectedValue = item.TaxItemTypeId.ToString();
            txt_name.Text = item.TaxItemTitle;
            lbl_code.Text = item.TaxItemcode;
            txt_description.Text = item.Description;
            cb_isActive.Checked = bool.Parse(item.IsActive.ToString());
            if (item.IsPrecentage == true)
            {
                rb_percentage.Checked = bool.Parse(item.IsPrecentage.ToString());
                Numeric1.Text = item.Precentage.ToString();
            }
            else
            {
                rb_percentage.Checked = false;
                Numeric1.Enabled = false;
                rb_amount.Checked = true;
                Numeric2.Enabled = true;
                ddl_currency.Enabled = true;
                Numeric2.Text = item.FixedAmount.ToString();
                ddl_currency.SelectedValue = item.CurrencyId.ToString();
            }

            RadTreeView tree = (RadTreeView)RadComboBox1.Items[0].FindControl("RadTreeView1");
            RadTreeNode tn = new RadTreeNode();
            tn = tree.FindNodeByValue(item.AccountId.ToString());
            tn.Expanded = true;
            tn.ExpandParentNodes();
            tn.Selected = true;
            RadComboBox1.SelectedValue = tn.Value;
            RadComboBox1.Text = tn.Text;
        }


        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {

            UI.Excel_ex(RadGrid_Items, Label1.Text);
        }

        bool isPdfExport;


        protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
        {
            isPdfExport = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "starScript", "hidefilter();", true);
            RadGrid_Items.Columns[0].Visible = false;
            RadGrid_Items.Columns[9].Visible = false;
            UI.pdf_ex(RadGrid_Items, Label1);

            ScriptManager.RegisterStartupScript(this, this.GetType(), "starScript", "showfilter();", true);


        }
       protected void RadGrid_Items_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (isPdfExport)
                UI.FormatGridItem(e.Item);
        }
    }
}