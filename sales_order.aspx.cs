﻿using BL.Purchasing;
using BL.Static;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace InvyLap.Sales
{
    public partial class sales_order : System.Web.UI.Page
    {
        Boolean status;

        List<int> foundError = new List<int>();

        public int? saleOrderID
        {
            get
            {
                int i = 0;
                if (Request.QueryString["salesOrderID"] != null && int.TryParse(Request.QueryString["salesOrderID"], out i))
                {
                    return i;
                }
                else
                {
                    return null;
                }
            }
        }

        public int? quotationID
        {
            get
            {
                int i = 0;
                if (Request.QueryString["quotationID"] != null && int.TryParse(Request.QueryString["quotationID"], out i))
                {
                    return i;
                }
                else
                {
                    return null;
                }
            }
        }

        public int? purchasingOrderID
        {
            get
            {
                int i = 0;
                if (Request.QueryString["purshacingOrderID"] != null && int.TryParse(Request.QueryString["purshacingOrderID"], out i))
                {
                    return i;
                }
                else
                {
                    return null;
                }
            }
        }

        public Dictionary<int, object> customFields
        {
            get { return (Dictionary<int, object>)Session["orderControls"]; }
            set { Session["orderControls"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // ArrayList dataList = BL.Sales.cls_SalesOrder.GetItemCustomUnitPrice(35, 22,40,25,25);

        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Dictionary<string, string> employeeList = BL.Administration.cls_appUser.getActiveUserListByCode2();
                rcb_requestedBy.DataSource = employeeList;
                rcb_requestedBy.DataTextField = "Value";
                rcb_requestedBy.DataValueField = "Key";
                rcb_requestedBy.DataBind();

                lbl_salesOrderCodeValue.Text = BL.Setup.cls_Setup.GetEntityNextCode("Sales Order");

                var user = employeeList.Where(a => a.Key.StartsWith(Session["UserID"].ToString() + BL.Static.cls_StaticData.HashCode)).SingleOrDefault().Key;
                rcb_requestedBy.SelectedValue = user;
                txt_requestedBy.Text = BL.Static.cls_StaticData.DecodingCode(user);

                Dictionary<int, string> messageTitleList = BL.Setup.cls_MessageTitle.GetMessageTitlelist(Session["lang"].ToString());
                rcb_messageTitle.DataSource = messageTitleList;
                rcb_messageTitle.DataTextField = "Value";
                rcb_messageTitle.DataValueField = "Key";
                rcb_messageTitle.DataBind();

                List<DAL.Data_CustomerVendorData> customerList = BL.Data.cls_customer_vendor.GetAllVendors(false);
                //RadTreeView tree2 = (RadTreeView)rcb_customer.Items[0].FindControl("rtv_customer");
                rcb_customer.DataSource = customerList;
                rcb_customer.DataBind();

                Dictionary<int, string> shipmentTypeList = BL.Setup.cls_ShipmentMethod.GetShipmentMethodListWithoutAlias();
                rbl_shipVia.DataSource = shipmentTypeList;
                rbl_shipVia.DataTextField = "Value";
                rbl_shipVia.DataValueField = "Key";
                rbl_shipVia.DataBind();
                rbl_shipVia.SelectedIndex = 0;

                Dictionary<int, string> currencyList = BL.Static.cls_StaticData.GetCurrencyList(Session["lang"].ToString());
                rcb_currency.DataSource = currencyList;
                rcb_currency.DataTextField = "Value";
                rcb_currency.DataValueField = "Key";
                rcb_currency.DataBind();

                int defaultCurrencyID = BL.Static.cls_StaticData.GetDefaultCurrencyId();

                if (defaultCurrencyID != 0)
                {
                    rcb_currency.SelectedValue = defaultCurrencyID.ToString();
                }

                Dictionary<string, string> paymentMethodList = BL.Setup.cls_PaymentMethod.GetPaymentMethodList2();
                rcb_paymentMethod.DataSource = paymentMethodList;
                rcb_paymentMethod.DataTextField = "Value";
                rcb_paymentMethod.DataValueField = "Key";
                rcb_paymentMethod.DataBind();

                Dictionary<string, string> paymentTermList = BL.Setup.cls_PaymentTerm.GetPaymentTermList2();
                rcb_paymentTerm.DataSource = paymentTermList;
                rcb_paymentTerm.DataTextField = "Value";
                rcb_paymentTerm.DataValueField = "Key";
                rcb_paymentTerm.DataBind();

                rbl_showItems.Items.Add(new ListItem() { Text = Resources.Lang.All, Value = "", Selected = true });
                rbl_showItems.Items.Add(new ListItem() { Text = Resources.Lang.purchasing_order, Value = "1" });
                rbl_showItems.Items.Add(new ListItem() { Text = Resources.Lang.customer_quotation_code, Value = "2" });

                List<OrderedItems> list = new List<OrderedItems>();
                List<OrderedItems> chargeList = new List<OrderedItems>();
                List<OrderedItems> taxList = new List<OrderedItems>();
                List<OrderedItems> discountList = new List<OrderedItems>();
                List<DAL.Setup_SalesPerson> salesPersonList = new List<DAL.Setup_SalesPerson>();

                if (saleOrderID != null)
                {
                    int? id = saleOrderID;

                    LoadSalesOrderData(employeeList, customerList, null, paymentMethodList, paymentTermList, list,chargeList,taxList,discountList,salesPersonList, id, 3);
                }
                else if (quotationID != null)
                {
                    BL.Quotation.QuotationList record = BL.Quotation.cls_QuotationList.GetReceivedQuotation(quotationID, 1);

                    LoadQuotationOrder(record, list);
                }
                else if(purchasingOrderID != null)
                {
                    DAL.Flow_OrderTemplate purchasingOrder = BL.Purchasing.cls_PurchasingOrder.SelectPurchasingOrder(purchasingOrderID, 1);

                    LoadPurchasingOrder(purchasingOrder);
                }
                else
                {
                    list.Add(new OrderedItems()
                    {
                        itemOrderID = -1,
                        itemOrderIndex = list.Count + 1,
                        itemCategoryID = null,
                        itemID = null,
                        customerID = null,
                        classID = null,
                        baseUOMID = null,
                        salesUOMID = null,
                        fixedSalesUOMID = null,
                        isDeleted = false,
                        totalAmount = 0
                    });
                }

                Dictionary<string, decimal> salesTaxes = BL.Configuration.cls_GeneralSetting.GetSalesTaxes();

                int order = 1;

                foreach (var tax in salesTaxes)
                {
                    if (order == 1)
                    {
                        lbl_salesTax1Label.Visible = true;
                        lbl_salesTax1Label.Text = tax.Key + ":";

                        lbl_salesTax1Value.Visible = true;
                        lbl_salesTax1Value.Text = "0.00 <br/>";

                        order++;
                    }
                    else if (order == 2)
                    {
                        lbl_salesTax2Label.Visible = true;
                        lbl_salesTax2Label.Text = tax.Key + ":";

                        lbl_salesTax2Value.Visible = true;
                        lbl_salesTax2Value.Text = "0.00 <br/>";

                        order++;
                    }
                    else if (order == 3)
                    {
                        lbl_salesTax3Label.Visible = true;
                        lbl_salesTax3Label.Text = tax.Key + ":";

                        lbl_salesTax3Value.Visible = true;
                        lbl_salesTax3Value.Text = "0.00 <br/>";
                    }
                }

                ViewState["salesTaxes"] = salesTaxes;
                ViewState["itemSalesTaxes"] = salesTaxes;

                ViewState["itemsList"] = list;
                ViewState["itemOrderID"] = 0;
                LoadGridItems(list);

                ViewState["chargesList"] = chargeList;
                ViewState["chargeOrderID"] = 0;
                LoadChargesGrid(chargeList);

                ViewState["taxList"] = taxList;
                ViewState["taxOrderID"] = 0;
                LoadTaxGrid(taxList);

                ViewState["discountList"] = discountList;
                ViewState["discountOrderID"] = 0;
                LoadDiscountGrid(discountList);

                ViewState["salesPerson"] = salesPersonList;
                ViewState["salesPersonOrder"] = 0;
                LoadSalesPersonGrid(salesPersonList);

                CalculateTotalAmount(); 
            }
        }

        private void LoadSalesOrderData(Dictionary<string, string> employeeList, List<DAL.Data_CustomerVendorData> customerList, RadTreeView tree2,
            Dictionary<string, string> paymentMethodList, Dictionary<string, string> paymentTermList, List<OrderedItems> list,
            List<OrderedItems> chargeList,List<OrderedItems> taxList,List<OrderedItems> discountList,List<DAL.Setup_SalesPerson> salesPersonList, int? id, int orderTypeID)
        {
            DAL.Flow_OrderTemplate order = BL.Sales.cls_SalesOrder.SelectSalesOrder(id, orderTypeID);

            if (order != null)
            {
                if (saleOrderID != null)
                {
                    lbl_salesOrderCodeValue.Text = order.TemplateCode;
                    txt_paperBasedSalesOrderNumber.Text = order.PaperTemplateCode;
                }

                cb_hold.Checked = order.IsHold;

                if (order.RequestedByEmployeeId != null)
                {
                    var requestedBy = employeeList.Where(a => a.Key.StartsWith(order.RequestedByEmployeeId.ToString() + BL.Static.cls_StaticData.HashCode)).SingleOrDefault().Key;
                    rcb_requestedBy.SelectedValue = requestedBy;
                    txt_requestedBy.Text = BL.Static.cls_StaticData.DecodingCode(requestedBy);
                }

                if (order.RequestDateTime != null)
                {
                    rdp_date.SelectedDate = order.RequestDateTime;
                }

                txt_note.Text = order.Notes;
                rcb_messageTitle.SelectedValue = order.MessageTitleId.ToString();
                txt_message.Text = order.Message;

                List<DAL.Flow_OrderTemplateAttachment> attachmentList = order.Flow_OrderTemplateAttachment.Where(a => a.IsDeleted == false).ToList();
                rgv_attachments.DataSource = attachmentList;
                rgv_attachments.DataBind();
                ViewState["orderAttachments"] = attachmentList;

                if (order.CustomerId != null)
                {
                    var customer = customerList.Where(a => a.CustomerVendorCode.StartsWith(order.CustomerId.ToString() + BL.Static.cls_StaticData.HashCode)).SingleOrDefault().CustomerVendorCode;
                    //RadTreeNode tn2 = new RadTreeNode();
                    //tn2 = tree2.FindNodeByValue(customer);
                    //tn2.Expanded = true;
                    //tn2.ExpandParentNodes();
                    //tn2.Selected = true;
                    rcb_customer.SelectedValue = customer;

                    txt_customer.Text = BL.Static.cls_StaticData.DecodingCode(customer);
                    //hassed Code Here
                    lbl_customerBillAddress.Text = BL.Data.cls_CustomerVendorAddress.BuildAddress(Session["lang"].ToString(), "", order.Setup_Address, 1);
                    hf_customerBillAddress.Text = order.BillingAddressId.ToString();

                    lbl_customerShipAddress.Text = BL.Data.cls_CustomerVendorAddress.BuildAddress(Session["lang"].ToString(), "", order.Setup_Address2, 2);
                    hf_customerShipAddress.Text = order.ShipmentAddressId.ToString();
                }

                rbl_shipVia.SelectedValue = order.ShipmentMethodId.ToString();

                rcb_currency.SelectedValue = order.CurrencyId.ToString();

                cb_currencyPerItem.Checked = order.IsCurrencyPerItem;

                if (order.IsCurrencyPerItem)
                {
                    rcb_currency.Enabled = false;
                }

                if (order.NoneInventoryCost != null)
                {
                    numeric_noneInventoryCost.Value = double.Parse(order.NoneInventoryCost.ToString());
                }

                if (order.IsPersentageNonInventoryCost != null)
                {
                    cb_PercentageNoneInventoryCost.Checked = order.IsPersentageNonInventoryCost;
                }

                if (order.Deduction != null)
                {
                    numeric_deductiont.Value = double.Parse(order.Deduction.ToString());
                }

                if (order.IsDeductionPercentage != null)
                {
                    cb_deductionPercentage.Checked = order.IsDeductionPercentage;
                }

                if (order.PaymentMethodId != null)
                {
                    var paymentMethod = paymentMethodList.Where(a => a.Key.StartsWith(order.PaymentMethodId.ToString() + BL.Static.cls_StaticData.HashCode)).SingleOrDefault().Key;
                    rcb_paymentMethod.SelectedValue = paymentMethod;
                    txt_paymentMethod.Text = BL.Static.cls_StaticData.DecodingCode(paymentMethod);
                }


                if (order.PaymentTearmId != null)
                {
                    var paymentTerm = paymentTermList.Where(a => a.Key.StartsWith(order.PaymentTearmId.ToString() + BL.Static.cls_StaticData.HashCode)).SingleOrDefault().Key;
                    rcb_paymentTerm.SelectedValue = paymentTerm;
                    txt_paymentTerm.Text = BL.Static.cls_StaticData.DecodingCode(paymentTerm);
                }

                if (order.DateOfDelivery != null)
                {
                    if (order.DateOfDelivery < DateTime.Today.Date)
                    {
                        rdp_dateOfDelivery.SelectedDate = DateTime.Today.Date;
                    }
                    else
                    {
                        rdp_dateOfDelivery.SelectedDate = order.DateOfDelivery;
                    }
                }

                if (order.IsDeliveryDatePerItem != null)
                {
                    cb_useOfDeliveryDatePerItem.Checked = (bool)order.IsDeliveryDatePerItem;

                }


                if ((bool)order.IsDeliveryDatePerItem)
                {
                    rdp_dateOfDelivery.Enabled = false;
                }

                if (order.PaymentDate != null)
                {
                    rdp_paymentDate.SelectedDate = order.PaymentDate;
                }

                double? totalItems = 0;

                if (order.Flow_OrderTemplateItem != null)
                {
                    foreach (var item in order.Flow_OrderTemplateItem.Where(a => a.IsDeleted == false).ToList())
                    {
                        list.Add(new OrderedItems()
                        {
                            itemOrderID = item.OrderTemplateItemId,
                            itemOrderIndex = list.Count + 1,
                            itemCode = item.Data_ItemData.ItemCode,
                            itemCategoryID = item.Data_ItemData.ItemCategoryId,
                            itemID = item.ItemId,
                            salesUOMID = item.ItemUOMId,
                            quantity = (double)item.ItemQuantity,
                            quantitySummary = BL.Purchasing.cls_PurchasingOrder.GetQuqntitySummary(),
                            unitPrice = (item.UnitPrice == null) ? 0 : (double?)item.UnitPrice,
                            totalAmount = (item.UnitPrice == null) ? 0 : (double?)(item.ItemQuantity * item.UnitPrice),
                            deliveryDate = item.DeliveryDate,
                            classID = item.CountryOfOrignID,
                            Description = item.Description,
                            discountPerItem = (double)item.Discount,
                            isDeleted = false,
                            currency=item.CurrencyId
                        });

                        totalItems += (item.UnitPrice == null) ? 0 : (double?)(item.ItemQuantity * item.UnitPrice);
                    }
                }

                if (order.Flow_OrderTemplateCharg != null)
                {
                    foreach (var item in order.Flow_OrderTemplateCharg.Where(a => a.IsDeleted == false).ToList())
                    {
                        chargeList.Add(new OrderedItems()
                        {
                            itemOrderID = item.OrderTemplateChargId,
                            itemCode = item.Setup_Charge.ChargesCode,
                            itemCategoryID = item.Setup_Charge.ChargesTypeId,
                            itemID = item.ChargeId,
                            Description = item.Description,
                            totalAmount = ((bool)item.Setup_Charge.IsPrecentage) ? ((double)item.Setup_Charge.Precentage / 100) * totalItems : (double)item.Setup_Charge.FixedAmount,
                            quantitySummary = ((bool)item.Setup_Charge.IsPrecentage) ? string.Format("{0:0.00}", item.Setup_Charge.Precentage) + " % (" + ((double)item.Setup_Charge.Precentage / 100) * totalItems + ")" : "(" + string.Format("{0:0.00}", item.Setup_Charge.FixedAmount) + ")",
                            fixedUnitPrice = ((bool)item.Setup_Charge.IsPrecentage) ? (double)item.Setup_Charge.Precentage : (double)item.Setup_Charge.FixedAmount,
                            isDeleted = false,
                            isPercentage = (bool)item.Setup_Charge.IsPrecentage
                        });
                    }
                }

                if (order.Flow_OrderTemplateTax != null)
                {
                    foreach (var item in order.Flow_OrderTemplateTax.Where(a => a.IsDeleted == false).ToList())
                    {
                        taxList.Add(new OrderedItems()
                        {
                            itemOrderID = item.OrderTemplateTaxId,
                            itemCode = item.Setup_TaxItem.TaxItemcode,
                            itemCategoryID = item.Setup_TaxItem.TaxItemTypeId,
                            itemID = item.TaxId,
                            Description = item.Description,
                            totalAmount = ((bool)item.Setup_TaxItem.IsPrecentage) ? ((double)item.Setup_TaxItem.Precentage / 100) * totalItems : (double)item.Setup_TaxItem.FixedAmount,
                            quantitySummary = ((bool)item.Setup_TaxItem.IsPrecentage) ? string.Format("{0:0.00}", item.Setup_TaxItem.Precentage) + " % (" + ((double)item.Setup_TaxItem.Precentage / 100) * totalItems + ")" : "(" + string.Format("{0:0.00}", item.Setup_TaxItem.FixedAmount) + ")",
                            fixedUnitPrice = ((bool)item.Setup_TaxItem.IsPrecentage) ? (double)item.Setup_TaxItem.Precentage : (double)item.Setup_TaxItem.FixedAmount,
                            isDeleted = false,
                            isPercentage = (bool)item.Setup_TaxItem.IsPrecentage
                        });
                    }
                }

                if (order.Flow_OrderTemplateDiscount != null)
                {
                    foreach (var item in order.Flow_OrderTemplateDiscount.Where(a => a.IsDeleted == false).ToList())
                    {
                        discountList.Add(new OrderedItems()
                        {
                            itemOrderID = item.OrderTemplateDiscountId,
                            itemCode = item.Setup_Discount.DiscountCode,
                            itemCategoryID = item.Setup_Discount.DiscountTypeId,
                            itemID = item.DiscountId,
                            Description = item.Description,
                            totalAmount = ((bool)item.Setup_Discount.IsPrecentage) ? ((double)item.Setup_Discount.Precentage / 100) * totalItems : (double)item.Setup_Discount.FixedAmount,
                            quantitySummary = ((bool)item.Setup_Discount.IsPrecentage) ? string.Format("{0:0.00}", item.Setup_Discount.Precentage) + " % (" + ((double)item.Setup_Discount.Precentage / 100) * totalItems + ")" : "(" + string.Format("{0:0.00}", item.Setup_Discount.FixedAmount) + ")",
                            fixedUnitPrice = ((bool)item.Setup_Discount.IsPrecentage) ? (double)item.Setup_Discount.Precentage : (double)item.Setup_Discount.FixedAmount,
                            isDeleted = false,
                            isPercentage = (bool)item.Setup_Discount.IsPrecentage
                        });
                    }
                }

                if (order.Flow_OrderTemplateSalesPerson != null)
                {
                    foreach (var item in order.Flow_OrderTemplateSalesPerson.Where(a => a.IsDeleted == false).ToList())
                    {
                        salesPersonList.Add(new DAL.Setup_SalesPerson()
                        {
                            ModifiedUserId = item.OrderTemplateSalesPersonId,
                            SalesPersonId = (int)item.SalesPersonId,
                            SalesPersonName = item.Setup_SalesPerson.SalesPersonName,
                            SalesPersonCode = item.Setup_SalesPerson.SalesPersonCode,
                            IsPrecentageCommission = item.Setup_SalesPerson.IsPrecentageCommission,
                            FixedAmountCommission = item.Setup_SalesPerson.FixedAmountCommission,
                            PrecentageCommission = item.Setup_SalesPerson.PrecentageCommission,
                            IsDeleted = false
                        });
                    }
                }

                Dictionary<int, object> controlsList = new Dictionary<int, object>();

                foreach (var field in customFields)
                {
                    foreach (var recivedField in order.Flow_OrderTemplateCustomField)
                    {
                        if (field.Key == recivedField.CustomFieldId)
                        {
                            SetFieldData(field.Value, recivedField.Setup_CustomField.FieldName, recivedField.CustomFieldValue, recivedField.OrderTemplateCustomFieldId.ToString(), controlsList);
                            break;
                        }
                    }
                }

                Session.Remove("orderControls");
                Session["orderControls"] = controlsList;
            }
        }

        private void SetFieldData(object field, string name, string value, string id, Dictionary<int, object> controlsList)
        {
            if (field is TextBox)
            {
                TextBox f = (TextBox)panel_customFields.FindControl("ctrl_" + name.Replace(" ", ""));
                f.Text = value;
                f.Attributes["rowID"] = id;
                controlsList.Add(int.Parse(id), f);
            }
            else if (field is RadDatePicker)
            {
                RadDatePicker f = (RadDatePicker)panel_customFields.FindControl("ctrl_" + name.Replace(" ", ""));
                if (value == "")
                {
                    f.SelectedDate = null;
                }
                else
                {
                    f.SelectedDate = DateTime.Parse(value);
                }
                f.Attributes["rowID"] = id;
                controlsList.Add(int.Parse(id), f);
            }
            else if (field is RadNumericTextBox)
            {
                RadNumericTextBox f = (RadNumericTextBox)panel_customFields.FindControl("ctrl_" + name.Replace(" ", ""));
                f.Text = value;
                f.Attributes["rowID"] = id;
                controlsList.Add(int.Parse(id), f);
            }
            else if (field is CheckBox)
            {
                CheckBox f = (CheckBox)panel_customFields.FindControl("ctrl_" + name.Replace(" ", ""));
                f.Checked = bool.Parse(value);
                f.Attributes["rowID"] = id;
                controlsList.Add(int.Parse(id), f);
            }


        }

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            if (rcb_customer.SelectedValue != "")
            {
                this.createAddressList(rcb_customer, 2, div_customerShipmentAddressTitle, ul_customerShipmentAddressTitle, lbl_tableCustomerShipAddessesTitle, "customerShipment", Resources.Lang.ship_address);

                this.createAddressList(rcb_customer, 1, div_customerMainAddressTitle, ul_customerMainAddressTitle, lbl_tableCustomerMainAddessesTitle, "customerMain", Resources.Lang.ship_address);
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            GenerateDynamicControls();
        }

        protected override void InitializeCulture()
        {
            Settings.ChangeLang();
        }

        #region 'Custom Fields'
        //getting all custom fields of the sales order
        private void GenerateDynamicControls()
        {
            List<DAL.Setup_CustomField> customFields = BL.Data.cls_ItemData.GetCustomFieldByEntityName("Sales Order");

            HtmlTable table = new HtmlTable();
            table.CellPadding = 5;

            Dictionary<int, object> controlsList = new Dictionary<int, object>();

            int customFieldID = 0;

            foreach (var field in customFields)
            {
                if (field.Static_SupportedDataType.SupportedDataTypeId == 1)
                {
                    customFieldID = customFieldID - 1;

                    TextBox txt = new TextBox();
                    txt.ID = "ctrl_" + field.FieldName.Replace(" ", "");
                    txt.Width = 200;
                    txt.Text = field.DefaultValue;
                    txt.Attributes.Add("rowID", customFieldID.ToString());
                    CreateTableControls(table, txt, field.FieldName);
                    controlsList.Add(field.CustomFieldId, txt);
                }
                else if (field.Static_SupportedDataType.SupportedDataTypeId == 2)
                {
                    customFieldID = customFieldID - 1;

                    RadDatePicker datePicker = new RadDatePicker();
                    datePicker.ID = "ctrl_" + field.FieldName.Replace(" ", "");
                    datePicker.Width = 220;
                    datePicker.Height = 28;
                    if (field.DefaultValue != "")
                    {
                        datePicker.SelectedDate = DateTime.Parse(field.DefaultValue);
                    }
                    datePicker.Attributes.Add("rowID", customFieldID.ToString());
                    CreateTableControls(table, datePicker, field.FieldName);
                    controlsList.Add(field.CustomFieldId, datePicker);
                }
                else if (field.Static_SupportedDataType.SupportedDataTypeId == 3)
                {
                    customFieldID = customFieldID - 1;

                    RadNumericTextBox ntxt = new RadNumericTextBox();
                    ntxt.ID = "ctrl_" + field.FieldName.Replace(" ", "");
                    ntxt.Width = 220;
                    ntxt.Height = 28;
                    ntxt.IncrementSettings.InterceptArrowKeys = true;
                    ntxt.IncrementSettings.InterceptMouseWheel = true;
                    ntxt.MinValue = 0;
                    if (field.DefaultValue == "")
                    {
                        ntxt.Value = 0;
                    }
                    else
                    {
                        ntxt.Text = field.DefaultValue;
                    }
                    ntxt.ShowSpinButtons = true;
                    ntxt.Attributes.Add("rowID", customFieldID.ToString());
                    CreateTableControls(table, ntxt, field.FieldName);
                    controlsList.Add(field.CustomFieldId, ntxt);
                }
                else if (field.Static_SupportedDataType.SupportedDataTypeId == 4)
                {
                    customFieldID = customFieldID - 1;

                    CheckBox cb = new CheckBox();
                    cb.ID = "ctrl_" + field.FieldName.Replace(" ", "");

                    if (field.DefaultValue == "")
                    {
                        cb.Checked = false;
                    }
                    else
                    {
                        cb.Checked = true;
                    }

                    cb.Attributes.Add("rowID", customFieldID.ToString());
                    CreateTableControls(table, cb, field.FieldName);
                    controlsList.Add(field.CustomFieldId, cb);
                }

            }


            panel_customFields.Controls.Add(table);

            Session["orderControls"] = controlsList;
        }

        //creating table rows and columns to place and arrange the custom fields
        private static void CreateTableControls(HtmlTable table, object control, string fieldName)
        {
            HtmlTableRow row = new HtmlTableRow();
            for (int i = 0; i < 2; i++)
            {
                HtmlTableCell cell = new HtmlTableCell();
                if (i == 0)
                {
                    cell.Controls.Add(new LiteralControl(fieldName + ":"));
                }
                else
                {
                    if (control is TextBox)
                    {
                        cell.Controls.Add((TextBox)control);
                    }
                    else if (control is RadNumericTextBox)
                    {
                        cell.Controls.Add((RadNumericTextBox)control);
                    }
                    else if (control is RadDatePicker)
                    {
                        cell.Controls.Add((RadDatePicker)control);
                    }
                    else
                    {
                        cell.Controls.Add((CheckBox)control);
                    }
                }

                row.Cells.Add(cell);
            }
            table.Rows.Add(row);
        }
        #endregion   

        #region 'Shipment and Bill addresses'

        protected void lb_editShipmentAddress_Click(object sender, EventArgs e)
        {
            editAddress(hf_customerShipAddress.Text, 2);
            ViewState["typeID"] = 2;
        }

        protected void lb_editBillAddress_Click(object sender, EventArgs e)
        {
            editAddress(hf_customerBillAddress.Text, 1);

            ViewState["typeID"] = 1;
        }

        //Event to show the popup to edit customer shipment address or add new customer shipment address
        protected void editAddress(string addressID, int type)
        {
            clearCustomerPopupAddressControls();

            if (addressID != "")
            {
                DAL.Setup_Address address = BL.Data.cls_CustomerVendorAddress.GetAddressByID(int.Parse(addressID));

                txt_Address_Customer.Text = address.AddressLine;
                TXT_Building_Customer.Text = address.BuildingNumber;
                TXT_Zip_Code_Customer.Text = address.ZipCode;
                Cascad_cntries_Customer.SelectedValue = address.CountryId.ToString();
                Cascad_City_Customer.SelectedValue = address.CityId.ToString();
                Cascad_Governorate_Customer.SelectedValue = address.GovernorateId.ToString();
                Cascad_Neighborhood_Customer.SelectedValue = address.NeighborhoodId.ToString();

                btn_save_Customer.Enabled = true;
            }
            else if (ViewState["newCustomerMainAddress"] != null && type == 1)
            {
                DAL.Setup_Address address = (DAL.Setup_Address)ViewState["newCustomerMainAddress"];

                txt_Address_Customer.Text = address.AddressLine;
                TXT_Building_Customer.Text = address.BuildingNumber;
                TXT_Zip_Code_Customer.Text = address.ZipCode;
                Cascad_cntries_Customer.SelectedValue = address.CountryId.ToString();
                Cascad_City_Customer.SelectedValue = address.CityId.ToString();
                Cascad_Governorate_Customer.SelectedValue = address.GovernorateId.ToString();
                Cascad_Neighborhood_Customer.SelectedValue = address.NeighborhoodId.ToString();

                btn_save_Customer.Enabled = false;
            }
            else if (ViewState["newCustomerShipAddress"] != null && type == 2)
            {
                DAL.Setup_Address address = (DAL.Setup_Address)ViewState["newCustomerShipAddress"];

                txt_Address_Customer.Text = address.AddressLine;
                TXT_Building_Customer.Text = address.BuildingNumber;
                TXT_Zip_Code_Customer.Text = address.ZipCode;
                Cascad_cntries_Customer.SelectedValue = address.CountryId.ToString();
                Cascad_City_Customer.SelectedValue = address.CityId.ToString();
                Cascad_Governorate_Customer.SelectedValue = address.GovernorateId.ToString();
                Cascad_Neighborhood_Customer.SelectedValue = address.NeighborhoodId.ToString();

                btn_save_Customer.Enabled = false;
            }
            else
            {
                btn_save_Customer.Enabled = false;
            }

            mpe_customerEdit.Show();
        }

        protected void lb_selectAnotherShipmentAddress_Click(object sender, EventArgs e)
        {
            if (rcb_customer.SelectedValue != "")
            {
                ViewState["typeID"] = 2;

                mpe_customerSelectAddress.PopupControlID = "pnl_selectCustomerShipmentAddress";
                mpe_customerSelectAddress.CancelControlID = "btn_closeCustomerShipPopup";
                mpe_customerSelectAddress.Show();
            }
        }

        protected void lb_selectAnotherBillAddress_Click(object sender, EventArgs e)
        {
            if (rcb_customer.SelectedValue != "")
            {
                ViewState["typeID"] = 1;

                mpe_customerSelectAddress.PopupControlID = "pnl_selectCustomerMainAddress";
                mpe_customerSelectAddress.CancelControlID = "btn_closeCustomerMainPopup";
                mpe_customerSelectAddress.Show();
            }
        }

        //Event to save the updated customer shipment address
        protected void btn_save_Customer_Click(object sender, EventArgs e)
        {
            int typeID = (int)ViewState["typeID"];

            if (hf_customerShipAddress.Text != "" && typeID == 2)
            {
                int id = int.Parse(hf_customerShipAddress.Text);
                BL.Setup.cls_Address.UpdateAddress(id, int.Parse(ddl_country_Customer.SelectedValue), int.Parse(ddl_City_Customer.SelectedValue),
                    int.Parse(ddl_Governorate_Customer.SelectedValue), int.Parse(ddl_Neighborhood_Customer.SelectedValue), txt_Address_Customer.Text, TXT_Building_Customer.Text,
                    TXT_Zip_Code_Customer.Text, int.Parse(Session["UserID"].ToString()));

                int customerID = BL.Static.cls_StaticData.DecodingID(rcb_customer.SelectedValue);

                DAL.Data_CustomerVendorAddress item = BL.Data.cls_CustomerVendorAddress.GetAddress(customerID, id);

                string result = BL.Data.cls_CustomerVendorAddress.BuildAddress(Session["lang"].ToString(), "", item);

                lbl_customerShipAddress.Text = BL.Static.cls_StaticData.DecodingCode(result);
            }
            else if (hf_customerBillAddress.Text != "" && typeID == 1)
            {
                int id = int.Parse(hf_customerBillAddress.Text);
                BL.Setup.cls_Address.UpdateAddress(id, int.Parse(ddl_country_Customer.SelectedValue), int.Parse(ddl_City_Customer.SelectedValue),
                    int.Parse(ddl_Governorate_Customer.SelectedValue), int.Parse(ddl_Neighborhood_Customer.SelectedValue), txt_Address_Customer.Text, TXT_Building_Customer.Text,
                    TXT_Zip_Code_Customer.Text, int.Parse(Session["UserID"].ToString()));

                int customerID = BL.Static.cls_StaticData.DecodingID(rcb_customer.SelectedValue);

                DAL.Data_CustomerVendorAddress item = BL.Data.cls_CustomerVendorAddress.GetAddress(customerID, id);

                string result = BL.Data.cls_CustomerVendorAddress.BuildAddress(Session["lang"].ToString(), "", item);

                lbl_customerBillAddress.Text = BL.Static.cls_StaticData.DecodingCode(result);
            }
        }

        //Event to save the new customer shipment address
        protected void btn_saveNew_Customer_Click(object sender, EventArgs e)
        {
            int? countryID = null;
            int? cityID = null;
            int? governorateID = null;
            int? neighborhoodID = null;

            int typeID = (int)ViewState["typeID"];

            DAL.Setup_Address newAddress = new DAL.Setup_Address()
            {
                CountryId = (ddl_country_Customer.SelectedValue == "") ? countryID : int.Parse(ddl_country_Customer.SelectedValue),
                CityId = (ddl_City_Customer.SelectedValue == "") ? cityID : int.Parse(ddl_City_Customer.SelectedValue),
                GovernorateId = (ddl_Governorate_Customer.SelectedValue == "") ? governorateID : int.Parse(ddl_Governorate_Customer.SelectedValue),
                NeighborhoodId = (ddl_Neighborhood_Customer.SelectedValue == "") ? neighborhoodID : int.Parse(ddl_Neighborhood_Customer.SelectedValue),
                AddressLine = txt_Address_Customer.Text,
                BuildingNumber = TXT_Building_Customer.Text,
                ZipCode = TXT_Zip_Code_Customer.Text,
            };

            if (newAddress.CountryId == null && newAddress.CityId == null && newAddress.GovernorateId == null &&
                newAddress.NeighborhoodId == null && newAddress.AddressLine == "" && newAddress.BuildingNumber == "" && newAddress.ZipCode == "")
            {
                if (typeID == 1)
                {
                    lbl_customerBillAddress.Text = "";
                    ViewState["newCustomerMainAddress"] = null;
                }
                else
                {
                    lbl_customerShipAddress.Text = "";
                    ViewState["newCustomerShipAddress"] = null;
                }
                
            }
            else
            {
                if (typeID == 1)
                {
                    ViewState["newCustomerMainAddress"] = newAddress;
                    lbl_customerBillAddress.Text = BL.Data.cls_CustomerVendorAddress.BuildAddress(Session["lang"].ToString(), "", newAddress, 1);

                    hf_customerBillAddress.Text = "";
                }
                else
                {
                    ViewState["newCustomerShipAddress"] = newAddress;
                    lbl_customerShipAddress.Text = BL.Data.cls_CustomerVendorAddress.BuildAddress(Session["lang"].ToString(), "", newAddress, 2);

                    hf_customerShipAddress.Text = "";
                }
                
            }
        }

        //Event to select another customer shipment address
        protected void select_CustomerAddress_Click(object sender, EventArgs e)
        {
            LinkButton select = (LinkButton)sender;
            int typeID = (int)ViewState["typeID"];
            if (typeID == 1)
            {
                hf_customerBillAddress.Text = select.Attributes["itemID"];
                lbl_customerBillAddress.Text = select.Attributes["itemData"];
            }
            else
            {
                hf_customerShipAddress.Text = select.Attributes["itemID"];
                lbl_customerShipAddress.Text = select.Attributes["itemData"];
            }
            
        }

        //Populate the tab menue with list of vendor main addresses or customer shipment addresses to select from them
        private void createAddressList(RadDropDownTree rcb, int addressType, HtmlGenericControl divContent, HtmlGenericControl ulTitle, Label tableTitle, string prefix, string linkTitle)
        {
            tableTitle.Text = rcb.SelectedText + Resources.Lang.Address;

            int vendorID = BL.Static.cls_StaticData.DecodingID(rcb.SelectedValue);

            List<string> addressList = BL.Data.cls_CustomerVendorAddress.GetcustomerVendorAddressListWithDefault(vendorID, Session["lang"].ToString(), addressType);

            if (addressList.Count == 0)
            {
                HtmlGenericControl div = new HtmlGenericControl("div");
                div.InnerText = Resources.Lang.no_address_available;
                divContent.Controls.Add(div);
            }

            for (int i = 0; i < addressList.Count; i++)
            {
                HtmlGenericControl li = new HtmlGenericControl("li");
                HtmlGenericControl anchor = new HtmlGenericControl("a");
                HtmlGenericControl div = new HtmlGenericControl("div");

                LinkButton select = new LinkButton();
                select.ID = "lb_select_" + prefix + i;

                select.Click += new EventHandler(select_CustomerAddress_Click);
                
                select.Text = Resources.Lang.select;
                select.Attributes.Add("itemID", BL.Static.cls_StaticData.DecodingID(addressList[i]).ToString());
                select.Attributes.Add("itemData", BL.Static.cls_StaticData.DecodingCode(addressList[i]));

                anchor.Attributes.Add("href", "#tab_" + prefix + i);
                anchor.Attributes.Add("data-toggle", "tab");
                anchor.InnerText = linkTitle + "(" + (i + 1) + ")";
                li.Controls.Add(anchor);

                div.Attributes.Add("id", "tab_" + prefix + i);
                div.InnerText = BL.Static.cls_StaticData.DecodingCode(addressList[i]);
                div.InnerHtml += "<br /><br />";
                div.Controls.Add(select);

                if (i == 0)
                {
                    li.Attributes.Add("class", "active");
                    div.Attributes.Add("class", "tab-pane active");
                }
                else
                {
                    div.Attributes.Add("class", "tab-pane");
                }

                ulTitle.Controls.Add(li);
                divContent.Controls.Add(div);
            }
        }

        //Clear the customer popup controls when edit the address
        private void clearCustomerPopupAddressControls()
        {
            txt_Address_Customer.Text = "";
            TXT_Building_Customer.Text = "";
            TXT_Zip_Code_Customer.Text = "";
            Cascad_cntries_Customer.SelectedValue = "";
            Cascad_City_Customer.SelectedValue = "";
            Cascad_Governorate_Customer.SelectedValue = "";
            Cascad_Neighborhood_Customer.SelectedValue = "";
        }

        #endregion     

        #region 'items grid events and operations'

        //Binding RadcomboBoxes inside the gridview
        protected void RadGrid_Items_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item.ItemType == GridItemType.AlternatingItem || e.Item.ItemType == GridItemType.Item)
            {
                GridDataItem item = (GridDataItem)e.Item;

                Label lbl_itemOrderIndex = e.Item.FindControl("lbl_itemOrderIndex") as Label;
                lbl_itemOrderIndex.Text = (e.Item.ItemIndex + 1).ToString();

                TextBox txt_description = e.Item.FindControl("txt_description") as TextBox;
                txt_description.Attributes.Add("key", e.Item.ItemIndex.ToString());

                //Binding RadComboBox of item categories and selecting its value
                RadComboBox rcb_itemCategory = (RadComboBox)item.FindControl("rcb_itemCategory");
                HiddenField hf_itemCategoryID = (HiddenField)item.FindControl("hf_itemCategoryID");
                Dictionary<int, string> itemCategory = BL.Setup.cls_ItemCategory.GetItemCategoryList();
                rcb_itemCategory.DataSource = itemCategory;
                rcb_itemCategory.DataTextField = "Value";
                rcb_itemCategory.DataValueField = "Key";
                rcb_itemCategory.SelectedValue = hf_itemCategoryID.Value;
                rcb_itemCategory.DataBind();

                rcb_itemCategory.Items.Insert(0, new RadComboBoxItem(Resources.Lang.select_all, ""));

                //Binding RadComboBox of items and selecting its value
                RadComboBox rcb_items = (RadComboBox)item.FindControl("rcb_items");
                HiddenField hf_itemID = (HiddenField)item.FindControl("hf_itemID");
                int? categoryID = null;
                Dictionary<int, string> items = BL.Data.cls_ItemData.GetItemsList((hf_itemCategoryID.Value == "") ? categoryID : int.Parse(hf_itemCategoryID.Value), null);
                rcb_items.DataSource = items;
                rcb_items.DataTextField = "Value";
                rcb_items.DataValueField = "Key";
                rcb_items.SelectedValue = hf_itemID.Value;
                rcb_items.DataBind();

                //Binding RadComboBox of UOM and selecting its value
                RadComboBox rcb_UOM = (RadComboBox)item.FindControl("rcb_UOM");
                HiddenField hf_baseUOMID = (HiddenField)item.FindControl("hf_baseUOMID");
                HiddenField hf_salesUOMID = (HiddenField)item.FindControl("hf_salesUOMID");
                int? baseUOMID = null;
                ArrayList UOMs = new ArrayList();
                UOMs = BL.Setup.cls_UOM.GetUOMEquivalentByUnitId((hf_baseUOMID.Value == "") ? baseUOMID : int.Parse(hf_baseUOMID.Value), UOMs);
                rcb_UOM.DataSource = UOMs;
                rcb_UOM.DataTextField = "Text";
                rcb_UOM.DataValueField = "Value";
                rcb_UOM.SelectedValue = hf_salesUOMID.Value;
                rcb_UOM.DataBind();


                //Binding RadComboBox of countries and selecting its value
                RadComboBox rcb_classes = (RadComboBox)item.FindControl("rcb_classes");
                HiddenField hf_classID = (HiddenField)item.FindControl("hf_classID");
                Dictionary<int, string> countries = BL.Static.cls_StaticData.GetCountrieslist(Session["lang"].ToString());
                rcb_classes.DataSource = countries;
                rcb_classes.DataTextField = "Value";
                rcb_classes.DataValueField = "Key";
                rcb_classes.SelectedValue = hf_classID.Value;
                rcb_classes.DataBind();

                int defaultCurrencyID = BL.Static.cls_StaticData.GetDefaultCurrencyId();

                if (defaultCurrencyID != 0)
                {
                    rcb_currency.SelectedValue = defaultCurrencyID.ToString();
                }

                RadComboBox rcb_itemCurrency = (RadComboBox)item.FindControl("rcb_itemCurrency");
                HiddenField hf_itemCurrency = (HiddenField)item.FindControl("hf_itemCurrency");
                Dictionary<int, string> currencyList = BL.Static.cls_StaticData.GetCurrencyList(Session["lang"].ToString());
                rcb_itemCurrency.DataSource = currencyList;
                rcb_itemCurrency.DataTextField = "Value";
                rcb_itemCurrency.DataValueField = "Key";
                rcb_itemCurrency.SelectedValue = (hf_itemCurrency.Value == "") ? ((defaultCurrencyID == 0) ? "" : defaultCurrencyID.ToString()) : hf_itemCurrency.Value;
                rcb_itemCurrency.DataBind();


                //RadNumericTextBox numeric_discountPerItem = (RadNumericTextBox)item.FindControl("numeric_discountPerItem");
                //numeric_discountPerItem.Max = 5;
            }
        }

        //Deleting row from the selected items list or set its IsDeleted = true
        protected void RadGrid_Items_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            GridDataItem item = (GridDataItem)e.Item;

            string ID = item.GetDataKeyValue("itemOrderID").ToString();

            List<OrderedItems> list = (List<OrderedItems>)ViewState["itemsList"];

            OrderedItems row = list.Where(a => a.itemOrderID == int.Parse(ID)).SingleOrDefault();

            if (row.itemOrderID > 0)
            {
                row.isDeleted = true;
            }
            else
            {
                list.Remove(row);
            }

            LoadGridItems(list);
            
            ViewState["itemsList"] = list;
            CalculateTotalAmount();
            txt_isValidItems.Text = CheckItemsGridEveryFieldChange(list);
        }

        //Binding the selected items list to the RadGrid at condition IsDeleted = false
        private void LoadGridItems(List<OrderedItems> list)
        {
            RadGrid_Items.DataSource = list.Where(a => a.isDeleted == false).ToList();
            RadGrid_Items.DataBind();
        }

        protected void RadGrid_Items_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            List<OrderedItems> list = (List<OrderedItems>)ViewState["itemsList"];

            if (list != null)
            {
                RadGrid_Items.DataSource = list.Where(a => a.isDeleted == false).ToList();
            }
        }

        private string CheckItemsGridEveryFieldChange(List<OrderedItems> list)
        {
            string msg = "";

            if (list.Count > 0)
            {
                if (list.Where(a => a.itemID == null).Any() || list.Where(a => a.salesUOMID == null).Any() || list.Where(a => a.quantity == null).Any()
                || list.Where(a => a.unitPrice == null).Any() || list.Where(a => a.totalAmount == null).Any())
                {
                    msg = Resources.Lang.Please_fill_grid;
                }
                else
                {
                    msg = "";
                }
            }
            else
            {
                msg = Resources.Lang.please_add_item;
            }

            return msg;
        }

        //Adding new empty row to the items grid
        protected void lb_addNewItem_Click(object sender, EventArgs e)
        {
            List<OrderedItems> list = (List<OrderedItems>)ViewState["itemsList"];

            if (CheckItemsGrid())
            {
                int id = (int)ViewState["itemOrderID"] - 2;

                list.Add(new OrderedItems()
                {
                    itemOrderID = id,
                    itemOrderIndex = list.Count + 1,
                    itemCategoryID = null,
                    itemID = null,
                    customerID = null,
                    classID = null,
                    baseUOMID = null,
                    salesUOMID = null,
                    isDeleted = false
                });

                LoadGridItems(list);

                ViewState["itemsList"] = list;
                ViewState["itemOrderID"] = id;
                txt_isValidItems.Text = CheckItemsGridEveryFieldChange(list);
            }
        }

        private bool CheckItemsGrid()
        {
            bool isValid = false;

            List<OrderedItems> list = (List<OrderedItems>)ViewState["itemsList"];

            if (list.Count > 0)
            {
                if (list.Where(a => a.itemID == null).Any() || list.Where(a => a.salesUOMID == null).Any() || list.Where(a => a.quantity == null).Any()
                || list.Where(a => a.unitPrice == null).Any() || list.Where(a => a.totalAmount == null).Any())
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "starScript", "ShowBox('" + Resources.Lang.Please_fill_grid + "');", true);
                }
                else
                {
                    isValid = true;
                }
            }
            else
            {
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "starScript", "ShowBox('" + Resources.Lang.please_add_item + "');", true);
                isValid = true;
            }

            return isValid;
        }

        #endregion

        #region 'Changing the values in the items grid fields and saving them in the ordered item list'

        //Get the row where the values are changing
        private void GetItemID(GridDataItem item, out List<OrderedItems> list, out OrderedItems row)
        {
            string ID = item.GetDataKeyValue("itemOrderID").ToString();

            list = (List<OrderedItems>)ViewState["itemsList"];

            row = list.Where(a => a.itemOrderID == int.Parse(ID)).SingleOrDefault();
        }

        //Changing the code in the items grid
        protected void txt_itemCode_TextChanged(object sender, EventArgs e)
        {
            GridDataItem gridItem;


            TextBox txt_itemCode = (sender as TextBox);
            gridItem = txt_itemCode.NamingContainer as GridDataItem;

            List<OrderedItems> list;
            OrderedItems row;

            GetItemID(gridItem, out list, out row);

            DAL.Data_ItemData item = BL.Data.cls_ItemData.GetanItemsByCode2(txt_itemCode.Text);

            if (item != null)
            {
                row.itemCode = txt_itemCode.Text;
                row.itemCategoryID = item.ItemCategoryId;
                row.itemID = item.ItemDataId;
                row.baseUOMID = item.BaseUOMId;
                if (item.SalesUOMId == null)
                {
                    row.salesUOMID = item.BaseUOMId;
                    row.fixedSalesUOMID = item.BaseUOMId;
                }
                else
                {
                    row.salesUOMID = item.SalesUOMId;
                    row.fixedSalesUOMID = item.SalesUOMId;
                }
                row.unitPrice = (double)item.SalesPrice;
                row.fixedUnitPrice =(double)item.SalesPrice;
                row.hasSalesTax = item.IsSalesTaxIncluded;
                row.quantity = 0;
                row.totalAmount = 0;
                row.discountPerItem = 0;
                row.Description = "";
                row.quantitySummary = BL.Purchasing.cls_PurchasingOrder.GetQuqntitySummary();
                row.tax1 = 0;
                row.tax2 = 0;
                row.tax3 = 0;

                int id = (int)ViewState["itemOrderID"] - 2;

                list.Add(new OrderedItems()
                {
                    itemOrderID = id,
                    itemOrderIndex = list.Count + 1,
                    itemCategoryID = null,
                    itemID = null,
                    customerID = null,
                    classID = null,
                    baseUOMID = null,
                    salesUOMID = null,
                    isDeleted = false
                });

                ViewState["itemOrderID"] = id;
            }
            else
            {
                row.itemCode = txt_itemCode.Text;
                row.itemCategoryID = null;
                row.itemID = null;
                row.baseUOMID = null;
                row.salesUOMID = null;
                row.fixedSalesUOMID = null;
                row.unitPrice = null;
                row.fixedUnitPrice = null;
                row.quantity = 0;
                row.totalAmount = 0;
                row.discountPerItem = 0;
                row.quantitySummary = "";
                row.Description = "";
                row.tax1 = 0;
                row.tax2 = 0;
                row.hasSalesTax = false;
                row.tax3 = 0;
            }

            LoadGridItems(list);
            ViewState["itemsList"] = list;

            CalculateTotalAmount();

            txt_isValidItems.Text = CheckItemsGridEveryFieldChange(list);
        }

        //Changning the category RadComboBox in the items grid
        protected void rcb_itemCategory_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            GridDataItem gridItem;

            RadComboBox rcb_itemCategory = (RadComboBox)sender;

            gridItem = rcb_itemCategory.NamingContainer as GridDataItem;

            List<OrderedItems> list;
            OrderedItems row;

            GetItemID(gridItem, out list, out row);

            int? selectedCategoryID = null;

            row.itemCategoryID = (e.Value == "") ? selectedCategoryID : int.Parse(e.Value);
            row.itemCode = null;
            row.itemID = null;
            row.baseUOMID = null;
            row.salesUOMID = null;
            row.fixedSalesUOMID = null;
            row.unitPrice = null;
            row.fixedUnitPrice = null;
            row.quantity = 0;
            row.totalAmount = 0;
            row.discountPerItem = 0;
            row.quantitySummary = "";
            row.Description = "";
            row.hasSalesTax = false;

            LoadGridItems(list);
            ViewState["itemsList"] = list;

            CalculateTotalAmount();
            txt_isValidItems.Text = CheckItemsGridEveryFieldChange(list);
        }

        //Changning the items RadComboBox in the items grid 
        protected void rcb_items_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            //System.Threading.Thread.Sleep(10000);

            GridDataItem gridItem;

            RadComboBox rcb_items = (RadComboBox)sender;

            gridItem = rcb_items.NamingContainer as GridDataItem;

            List<OrderedItems> list;
            OrderedItems row;

            GetItemID(gridItem, out list, out row);

            var item = BL.Data.cls_ItemData.GetItem(int.Parse(e.Value));

            row.itemCode = item.ItemCode;
            row.itemCategoryID = item.ItemCategoryId;
            row.itemID = item.ItemDataId;
            row.baseUOMID = item.BaseUOMId;
            row.salesUOMID = item.SalesUOMId;
            row.fixedSalesUOMID = item.SalesUOMId;
            //row.unitPrice = (double)item.SalesPrice;
            row.fixedUnitPrice = (double)item.SalesPrice;
            row.quantity = 0;
            row.quantitySummary = BL.Purchasing.cls_PurchasingOrder.GetQuqntitySummary();
            row.totalAmount = 0;
            row.discountPerItem = 0;
            row.Description = "";
            row.tax1 = 0;
            row.tax2 = 0;
            row.tax3 = 0;
            row.hasSalesTax = item.IsSalesTaxIncluded;

            decimal? customePrice = CalculateItemUnitPrice((int)row.itemID, (int)row.salesUOMID, decimal.Parse(row.quantity.ToString()), (row.currency == null) ? int.Parse(rcb_currency.SelectedValue) : (int)row.currency);

            row.unitPrice = (double)customePrice;

            LoadGridItems(list);
            ViewState["itemsList"] = list;

            CalculateTotalAmount();
            txt_isValidItems.Text = CheckItemsGridEveryFieldChange(list);
        }

        protected void txt_description_TextChanged(object sender, EventArgs e)
        {
            GridDataItem gridItem;

            TextBox txt_description = (sender as TextBox);
            gridItem = txt_description.NamingContainer as GridDataItem;

            List<OrderedItems> list;
            OrderedItems row;

            GetItemID(gridItem, out list, out row);

            row.Description = txt_description.Text;

            LoadGridItems(list);
            ViewState["itemsList"] = list;
        }

        protected void numeric_discountPerItem_TextChanged(object sender, EventArgs e)
        {
            RadNumericTextBox numeric_discountPerItem = (RadNumericTextBox)sender;
            GridDataItem gridItem;
            gridItem = numeric_discountPerItem.NamingContainer as GridDataItem;

            List<OrderedItems> list;
            OrderedItems row;

            GetItemID(gridItem, out list, out row);

            double discountPerItem = double.Parse(numeric_discountPerItem.Value.ToString());

            row.discountPerItem = discountPerItem;
            row.totalAmount = (row.unitPrice - discountPerItem) * row.quantity;

            CalculateSalesTaxPerItem(row);

            LoadGridItems(list);
            ViewState["itemsList"] = list;

            CalculateTotalAmount();
            txt_isValidItems.Text = CheckItemsGridEveryFieldChange(list);
        }

        //Changning the quantity in the items grid
        protected void numeric_quantity_TextChanged(object sender, EventArgs e)
        {
            RadNumericTextBox numeric_quantity = (RadNumericTextBox)sender;
            GridDataItem gridItem;
            gridItem = numeric_quantity.NamingContainer as GridDataItem;

            List<OrderedItems> list;
            OrderedItems row;

            GetItemID(gridItem, out list, out row);

            double quantity = double.Parse(numeric_quantity.Value.ToString());

            decimal? customePrice = CalculateItemUnitPrice((int)row.itemID, (int)row.salesUOMID, decimal.Parse(numeric_quantity.Value.ToString()), (row.currency == null) ? int.Parse(rcb_currency.SelectedValue) : (int)row.currency);

            row.unitPrice = (double)customePrice;
            row.totalAmount = quantity * (row.unitPrice - row.discountPerItem);
            row.quantity = quantity;

            CalculateSalesTaxPerItem(row);

            LoadGridItems(list);
            ViewState["itemsList"] = list;

            CalculateTotalAmount();
            txt_isValidItems.Text = CheckItemsGridEveryFieldChange(list);
        }

        private void CalculateSalesTaxPerItem(OrderedItems row)
        {
            Dictionary<string, decimal> salesTaxes = (Dictionary<string, decimal>)ViewState["salesTaxes"];

            int order = 1;

            if (row.hasSalesTax)
            {
                foreach (var tax in salesTaxes)
                {
                    if (order == 1)
                    {
                        row.tax1 = ((decimal)(row.unitPrice - ((row.discountPerItem == null) ? 0 : row.discountPerItem)) * (decimal)row.quantity) * (tax.Value / 100);

                        order++;
                    }
                    else if (order == 2)
                    {
                        row.tax2 = ((decimal)(row.unitPrice - ((row.discountPerItem == null) ? 0 : row.discountPerItem)) * (decimal)row.quantity) * (tax.Value / 100);

                        order++;
                    }
                    else if (order == 3)
                    {
                        row.tax3 = ((decimal)(row.unitPrice - ((row.discountPerItem == null) ? 0 : row.discountPerItem)) * (decimal)row.quantity) * (tax.Value / 100);
                    }
                }
            }
        }

        //Changning the unit price in the items grid
        protected void numeric_unitPrice_TextChanged(object sender, EventArgs e)
        {
            RadNumericTextBox numeric_unitPrice = (RadNumericTextBox)sender;
            GridDataItem gridItem;
            gridItem = numeric_unitPrice.NamingContainer as GridDataItem;

            List<OrderedItems> list;
            OrderedItems row;

            GetItemID(gridItem, out list, out row);

            double unitPrice = double.Parse(numeric_unitPrice.Value.ToString());

            row.totalAmount = (unitPrice - row.discountPerItem) * row.quantity;
            row.unitPrice = unitPrice;

            CalculateSalesTaxPerItem(row);

            LoadGridItems(list);
            ViewState["itemsList"] = list;

            CalculateTotalAmount();
            txt_isValidItems.Text = CheckItemsGridEveryFieldChange(list);
        }

        //Changning the total amount in the items grid
        protected void numeric_totalAmount_TextChanged(object sender, EventArgs e)
        {
            RadNumericTextBox numeric_totalAmount = (RadNumericTextBox)sender;
            GridDataItem gridItem;
            gridItem = numeric_totalAmount.NamingContainer as GridDataItem;

            List<OrderedItems> list;
            OrderedItems row;

            GetItemID(gridItem, out list, out row);

            double totalAmount = double.Parse(numeric_totalAmount.Value.ToString());

            row.unitPrice = (totalAmount / row.quantity) + row.discountPerItem;
            row.totalAmount = totalAmount;

            CalculateSalesTaxPerItem(row);

            LoadGridItems(list);
            ViewState["itemsList"] = list;

            CalculateTotalAmount();
            txt_isValidItems.Text = CheckItemsGridEveryFieldChange(list);
        }

        //Changning the UOM RadComboBox in the items grid
        protected void rcb_UOM_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            RadComboBox rcb_UOM = (RadComboBox)sender;
            GridDataItem gridItem;
            gridItem = rcb_UOM.NamingContainer as GridDataItem;

            HiddenField hf_fixedSalesUOMID = (HiddenField)gridItem.FindControl("hf_fixedSalesUOMID");
            HiddenField hf_fixedUnitPrice = (HiddenField)gridItem.FindControl("hf_fixedUnitPrice");

            List<OrderedItems> list;
            OrderedItems row;

            GetItemID(gridItem, out list, out row);

            row.salesUOMID = int.Parse(rcb_UOM.SelectedValue);

            decimal? customePrice = CalculateItemUnitPrice((int)row.itemID, (int)row.salesUOMID, decimal.Parse(row.quantity.ToString()), (row.currency == null) ? int.Parse(rcb_currency.SelectedValue) : (int)row.currency);

            row.unitPrice = (double)customePrice;

            row.totalAmount = row.quantity * row.unitPrice;

            

            CalculateSalesTaxPerItem(row);

            LoadGridItems(list);
            ViewState["itemsList"] = list;
            CalculateTotalAmount();
            txt_isValidItems.Text = CheckItemsGridEveryFieldChange(list);
        }

        protected void rcb_itemCurrency_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            RadComboBox rcb_itemCurrency = (RadComboBox)sender;
            GridDataItem gridItem;
            gridItem = rcb_itemCurrency.NamingContainer as GridDataItem;

            List<OrderedItems> list;
            OrderedItems row;

            GetItemID(gridItem, out list, out row);

            row.currency = int.Parse(rcb_itemCurrency.SelectedValue);

            decimal? customePrice = CalculateItemUnitPrice((int)row.itemID, (int)row.salesUOMID, decimal.Parse(row.quantity.ToString()), (row.currency == null) ? int.Parse(rcb_currency.SelectedValue) : (int)row.currency);

            row.unitPrice = (double)customePrice;

            CalculateSalesTaxPerItem(row);

            LoadGridItems(list);
            ViewState["itemsList"] = list;
            txt_isValidItems.Text = CheckItemsGridEveryFieldChange(list);
        }

        //Changning the delivery date in the items grid
        protected void rdp_itemDeliveryDate_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            RadDatePicker rdp_itemDeliveryDate = (RadDatePicker)sender;
            GridDataItem gridItem;
            gridItem = rdp_itemDeliveryDate.NamingContainer as GridDataItem;

            List<OrderedItems> list;
            OrderedItems row;

            GetItemID(gridItem, out list, out row);

            row.deliveryDate = rdp_itemDeliveryDate.SelectedDate;

            LoadGridItems(list);
            ViewState["itemsList"] = list;
            txt_isValidItems.Text = CheckItemsGridEveryFieldChange(list);
        }

        //Changning the class RadComboBox in the items grid
        protected void rcb_classes_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            RadComboBox rcb_classes = (RadComboBox)sender;
            GridDataItem gridItem;
            gridItem = rcb_classes.NamingContainer as GridDataItem;

            List<OrderedItems> list;
            OrderedItems row;

            GetItemID(gridItem, out list, out row);

            row.classID = int.Parse(rcb_classes.SelectedValue);

            LoadGridItems(list);
            ViewState["itemsList"] = list;
            txt_isValidItems.Text = CheckItemsGridEveryFieldChange(list);
        }

        #endregion

        #region 'valid customer quotation & purchasing order' 

        protected void lb_findQuotation_Click(object sender, EventArgs e)
        {
            int id = BL.Static.cls_StaticData.DecodingID(rcb_customer.SelectedValue);
            List<BL.Quotation.QuotationList> list = BL.Quotation.cls_QuotationList.GetValidCustomerQuotationList(id);
            radGrid_validCustomerQuotations.DataSource = list;
            radGrid_validCustomerQuotations.DataBind();
            ViewState["customerQuotationOrderList"] = list;
            mpe_validCustomerQuotation.Show();
        }

        protected void rbl_showItems_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<BL.Quotation.QuotationList> list = (List<BL.Quotation.QuotationList>)ViewState["customerQuotationOrderList"];
            int? _type = null;
            bool _typeSucceed = BL.Static.NullableInt.TryParse(rbl_showItems.SelectedValue, out _type);
            radGrid_validCustomerQuotations.DataSource = list.Where(a => (((_typeSucceed) ? a.DocumentTypeID == _type : rbl_showItems.SelectedValue == ""))).ToList();
            radGrid_validCustomerQuotations.DataBind();
            mpe_validCustomerQuotation.Show();
        }

        protected void radGrid_validCustomerQuotations_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (ViewState["customerQuotationOrderList"] != null)
            {
                List<BL.Quotation.QuotationList> list = (List<BL.Quotation.QuotationList>)ViewState["customerQuotationOrderList"];
                int? _type = null;
                bool _typeSucceed = BL.Static.NullableInt.TryParse(rbl_showItems.SelectedValue, out _type);
                radGrid_validCustomerQuotations.DataSource = list.Where(a => (((_typeSucceed) ? a.DocumentTypeID == _type : rbl_showItems.SelectedValue == ""))).ToList();
                mpe_validCustomerQuotation.Show();
            }
        }

        protected void radGrid_validCustomerQuotations_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item.ItemType == GridItemType.AlternatingItem || e.Item.ItemType == GridItemType.Item)
            {
                Label lbl_QuotationOrder = e.Item.FindControl("lbl_QuotationOrder") as Label;
                lbl_QuotationOrder.Text = (e.Item.ItemIndex + 1).ToString();
            }
        }

        protected void btn_select_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridDataItem item = (GridDataItem)btn.Parent.Parent;
            string id = item.GetDataKeyValue("QuotationID").ToString();

            List<BL.Quotation.QuotationList> list = (List<BL.Quotation.QuotationList>)ViewState["customerQuotationOrderList"];

            BL.Quotation.QuotationList record = list.Where(a => a.QuotationID == int.Parse(id)).SingleOrDefault();

           

            LoadQuotation(record);

        }

        private void LoadQuotation(BL.Quotation.QuotationList record)
        {
            rbl_shipVia.SelectedValue = record.ShipVia.ToString();

            rcb_currency.SelectedValue = record.Currency.ToString();

            if (record.NoneInventoryCost != null)
            {
                numeric_noneInventoryCost.Value = double.Parse(record.NoneInventoryCost.ToString());
            }

            if (record.IsPercentageNoneInventoryCost != null)
            {
                cb_PercentageNoneInventoryCost.Checked = (bool)record.IsPercentageNoneInventoryCost;
            }

            if (record.PaymentMethod != null)
            {
                Dictionary<string, string> paymentMethodList = BL.Setup.cls_PaymentMethod.GetPaymentMethodList2();
                var paymentMethod = paymentMethodList.Where(a => a.Key.StartsWith(record.PaymentMethod.ToString() + BL.Static.cls_StaticData.HashCode)).SingleOrDefault().Key;
                rcb_paymentMethod.SelectedValue = paymentMethod;
                txt_paymentMethod.Text = BL.Static.cls_StaticData.DecodingCode(paymentMethod);
            }


            if (record.PaymentTerm != null)
            {
                Dictionary<string, string> paymentTermList = BL.Setup.cls_PaymentTerm.GetPaymentTermList2();
                var paymentTerm = paymentTermList.Where(a => a.Key.StartsWith(record.PaymentTerm.ToString() + BL.Static.cls_StaticData.HashCode)).SingleOrDefault().Key;
                rcb_paymentTerm.SelectedValue = paymentTerm;
                txt_paymentTerm.Text = BL.Static.cls_StaticData.DecodingCode(paymentTerm);
            }

            if (record.DeliveryDate != null)
            {
                if (record.DeliveryDate < DateTime.Today.Date)
                {
                    rdp_dateOfDelivery.SelectedDate = DateTime.Today.Date;
                }
                else
                {
                    rdp_dateOfDelivery.SelectedDate = record.DeliveryDate;
                }
            }

            cb_useOfDeliveryDatePerItem.Checked = (bool)record.IsDeliveryDatePerItem;

            if (record.PaymentDate != null)
            {
                rdp_paymentDate.SelectedDate = record.PaymentDate;
            }

            ViewState["itemsList"] = record.SelectedItems;

            CalculateUintPriceOfAllItems();

            CalculateTotalAmount();

            LoadGridItems(record.SelectedItems);
        }

        private void LoadQuotationOrder(BL.Quotation.QuotationList record, List<OrderedItems> list)
        {
            rbl_shipVia.SelectedValue = record.ShipVia.ToString();

            rcb_currency.SelectedValue = record.Currency.ToString();

            if (record.NoneInventoryCost != null)
            {
                numeric_noneInventoryCost.Value = double.Parse(record.NoneInventoryCost.ToString());
            }

            if (record.IsPercentageNoneInventoryCost != null)
            {
                cb_PercentageNoneInventoryCost.Checked = (bool)record.IsPercentageNoneInventoryCost;
            }

            if (record.PaymentMethod != null)
            {
                Dictionary<string, string> paymentMethodList = BL.Setup.cls_PaymentMethod.GetPaymentMethodList2();
                var paymentMethod = paymentMethodList.Where(a => a.Key.StartsWith(record.PaymentMethod.ToString() + BL.Static.cls_StaticData.HashCode)).SingleOrDefault().Key;
                rcb_paymentMethod.SelectedValue = paymentMethod;
                txt_paymentMethod.Text = BL.Static.cls_StaticData.DecodingCode(paymentMethod);
            }


            if (record.PaymentTerm != null)
            {
                Dictionary<string, string> paymentTermList = BL.Setup.cls_PaymentTerm.GetPaymentTermList2();
                var paymentTerm = paymentTermList.Where(a => a.Key.StartsWith(record.PaymentTerm.ToString() + BL.Static.cls_StaticData.HashCode)).SingleOrDefault().Key;
                rcb_paymentTerm.SelectedValue = paymentTerm;
                txt_paymentTerm.Text = BL.Static.cls_StaticData.DecodingCode(paymentTerm);
            }

            if (record.DeliveryDate != null)
            {
                if (record.DeliveryDate < DateTime.Today.Date)
                {
                    rdp_dateOfDelivery.SelectedDate = DateTime.Today.Date;
                }
                else
                {
                    rdp_dateOfDelivery.SelectedDate = record.DeliveryDate;
                }
            }

            cb_useOfDeliveryDatePerItem.Checked = (bool)record.IsDeliveryDatePerItem;

            if (record.PaymentDate != null)
            {
                rdp_paymentDate.SelectedDate = record.PaymentDate;
            }

            foreach (var i in record.SelectedItems)
            {
                list.Add(new OrderedItems()
                {
                    itemOrderID = i.itemOrderID,
                    itemCode = i.itemCode,
                    itemCategoryID = i.itemCategoryID,
                    itemID = i.itemID,
                    salesUOMID = i.salesUOMID,
                    quantity = i.quantity,
                    quantitySummary = i.quantitySummary,
                    unitPrice = i.unitPrice,
                    totalAmount = i.totalAmount,
                    deliveryDate = i.deliveryDate,
                });
            }

            CalculateUintPriceOfAllItems();

        }

        private void LoadPurchasingOrder(DAL.Flow_OrderTemplate record)
        {
            List<OrderedItems> itemsList = new List<OrderedItems>();

            rbl_shipVia.SelectedValue = record.ShipmentMethodId.ToString();

            rcb_currency.SelectedValue = record.CurrencyId.ToString();

            if (record.NoneInventoryCost != null)
            {
                numeric_noneInventoryCost.Value = double.Parse(record.NoneInventoryCost.ToString());
            }

            if (record.IsPersentageNonInventoryCost != null)
            {
                cb_PercentageNoneInventoryCost.Checked = (bool)record.IsPersentageNonInventoryCost;
            }

            if (record.PaymentMethodId != null)
            {
                Dictionary<string, string> paymentMethodList = BL.Setup.cls_PaymentMethod.GetPaymentMethodList2();
                var paymentMethod = paymentMethodList.Where(a => a.Key.StartsWith(record.PaymentMethodId.ToString() + BL.Static.cls_StaticData.HashCode)).SingleOrDefault().Key;
                rcb_paymentMethod.SelectedValue = paymentMethod;
                txt_paymentMethod.Text = BL.Static.cls_StaticData.DecodingCode(paymentMethod);
            }


            if (record.PaymentTearmId != null)
            {
                Dictionary<string, string> paymentTermList = BL.Setup.cls_PaymentTerm.GetPaymentTermList2();
                var paymentTerm = paymentTermList.Where(a => a.Key.StartsWith(record.PaymentTearmId.ToString() + BL.Static.cls_StaticData.HashCode)).SingleOrDefault().Key;
                rcb_paymentTerm.SelectedValue = paymentTerm;
                txt_paymentTerm.Text = BL.Static.cls_StaticData.DecodingCode(paymentTerm);
            }

            if (record.DateOfDelivery != null)
            {
                if (record.DateOfDelivery < DateTime.Today.Date)
                {
                    rdp_dateOfDelivery.SelectedDate = DateTime.Today.Date;
                }
                else
                {
                    rdp_dateOfDelivery.SelectedDate = record.DateOfDelivery;
                }
            }

            cb_useOfDeliveryDatePerItem.Checked = (bool)record.IsDeliveryDatePerItem;

            if (record.PaymentDate != null)
            {
                rdp_paymentDate.SelectedDate = record.PaymentDate;
            }

            if (record.Flow_OrderTemplateItem != null)
            {
                foreach (var item in record.Flow_OrderTemplateItem.Where(a => a.IsDeleted == false).ToList())
                {
                    itemsList.Add(new OrderedItems()
                    {
                        itemOrderID = item.OrderTemplateItemId,
                        itemOrderIndex = itemsList.Count + 1,
                        itemCode = item.Data_ItemData.ItemCode,
                        itemCategoryID = item.Data_ItemData.ItemCategoryId,
                        itemID = item.ItemId,
                        salesUOMID = item.ItemUOMId,
                        quantity = (double)item.ItemQuantity,
                        quantitySummary = BL.Purchasing.cls_PurchasingOrder.GetQuqntitySummary(),
                        unitPrice = (item.UnitPrice == null) ? 0 : (double?)item.UnitPrice,
                        totalAmount = (item.UnitPrice == null) ? 0 : (double?)(item.ItemQuantity * item.UnitPrice),
                        deliveryDate = item.DeliveryDate,
                    });
                }
            }

            ViewState["itemsList"] = itemsList;

            CalculateUintPriceOfAllItems();

            CalculateTotalAmount();

            LoadGridItems(itemsList);
        }

        #endregion

        #region 'Sales pesron'

        protected void radGrid_salesPerson_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item.ItemType == GridItemType.AlternatingItem || e.Item.ItemType == GridItemType.Item)
            {
                Label lbl_OrderIndex = e.Item.FindControl("lbl_OrderIndex") as Label;
                lbl_OrderIndex.Text = (e.Item.ItemIndex + 1).ToString();

                GridDataItem item = (GridDataItem)e.Item;

                //Binding RadComboBox of items and selecting its value
                RadComboBox rcb_salesPerson = (RadComboBox)item.FindControl("rcb_salesPerson");
                HiddenField hf_SalesPersonId = (HiddenField)item.FindControl("hf_SalesPersonId");
                Dictionary<int, string> items = BL.Setup.cls_SalesPerson.GetSalesPerson(true);
                rcb_salesPerson.DataSource = items;
                rcb_salesPerson.DataTextField = "Value";
                rcb_salesPerson.DataValueField = "Key";
                rcb_salesPerson.SelectedValue = (hf_SalesPersonId.Value != "-1") ? hf_SalesPersonId.Value : null;
                rcb_salesPerson.DataBind();
            }
        }

        protected void radGrid_salesPerson_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            GridDataItem item = (GridDataItem)e.Item;

            string ID = item.GetDataKeyValue("ModifiedUserId").ToString();

            List<DAL.Setup_SalesPerson> salesPersonList = (List<DAL.Setup_SalesPerson>)ViewState["salesPerson"];

            DAL.Setup_SalesPerson row = salesPersonList.Where(a => a.ModifiedUserId == int.Parse(ID)).SingleOrDefault();

            if (row.ModifiedUserId > 0)
            {
                row.IsDeleted = true;
            }
            else
            {
                salesPersonList.Remove(row);
            }

            LoadSalesPersonGrid(salesPersonList);

            ViewState["salesPerson"] = salesPersonList;
        }

        protected void radGrid_salesPerson_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            List<DAL.Setup_SalesPerson> salesPersonList = (List<DAL.Setup_SalesPerson>)ViewState["salesPerson"];
            radGrid_salesPerson.DataSource = salesPersonList;
        }

        protected void lb_addNewSalesPerson_Click(object sender, EventArgs e)
        {
            List<DAL.Setup_SalesPerson> salesPersonList;

            if (ViewState["salesPerson"] == null)
            {
                salesPersonList = new List<DAL.Setup_SalesPerson>();
            }else
            {
                salesPersonList = (List<DAL.Setup_SalesPerson>)ViewState["salesPerson"];
            }

            int order = 0;

            if (ViewState["salesPersonOrder"] != null)
            {
                order = (int)ViewState["salesPersonOrder"];
            }

            order = order - 1;

            salesPersonList.Add(new DAL.Setup_SalesPerson()
            {
                SalesPersonId = -1,
                SalesPersonCode = null,
                SalesPersonName = "",
                IsPrecentageCommission = false,
                FixedAmountCommission = null,
                PrecentageCommission = null,
                ModifiedUserId = order,
                IsDeleted = false
            });

            LoadSalesPersonGrid(salesPersonList);

            ViewState["salesPerson"] = salesPersonList;
            ViewState["salesPersonOrder"] = order;
        }

        private void LoadSalesPersonGrid(List<DAL.Setup_SalesPerson> salesPersonList)
        {
            radGrid_salesPerson.DataSource = salesPersonList.Where(a=>a.IsDeleted == false).ToList();
            radGrid_salesPerson.DataBind();
        }

        protected void txt_SalesPersonCode_TextChanged(object sender, EventArgs e)
        {
            GridDataItem gridItem;

            TextBox txt_SalesPersonCode = (sender as TextBox);
            gridItem = txt_SalesPersonCode.NamingContainer as GridDataItem;

            List<DAL.Setup_SalesPerson> list;
            DAL.Setup_SalesPerson row;

            GetSalesPersonID(gridItem, out list, out row);

            DAL.Setup_SalesPerson salesPerson = BL.Setup.cls_SalesPerson.GetSalesPerson(txt_SalesPersonCode.Text);

            row.SalesPersonCode = txt_SalesPersonCode.Text;

            if (salesPerson != null)
            {
                row.SalesPersonId = salesPerson.SalesPersonId;
                row.IsPrecentageCommission = salesPerson.IsPrecentageCommission;
                row.FixedAmountCommission = salesPerson.FixedAmountCommission;
                row.PrecentageCommission = salesPerson.PrecentageCommission;
            }
            else
            {
                row.SalesPersonId = -1;
                row.IsPrecentageCommission = false;
                row.FixedAmountCommission = null;
                row.PrecentageCommission = null;
            }

            radGrid_salesPerson.DataSource = list;
            radGrid_salesPerson.DataBind();

            ViewState["salesPerson"] = list;
        }

        protected void rcb_salesPerson_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            GridDataItem gridItem;

            RadComboBox rcb_salesPerson = (sender as RadComboBox);
            gridItem = rcb_salesPerson.NamingContainer as GridDataItem;

            List<DAL.Setup_SalesPerson> list;
            DAL.Setup_SalesPerson row;

            GetSalesPersonID(gridItem, out list, out row);

            DAL.Setup_SalesPerson salesPerson = BL.Setup.cls_SalesPerson.GetSalesPerson(int.Parse(e.Value));

            row.SalesPersonId = int.Parse(e.Value);

            if (salesPerson != null)
            {
                row.SalesPersonCode = salesPerson.SalesPersonCode;
                row.IsPrecentageCommission = salesPerson.IsPrecentageCommission;
                row.FixedAmountCommission = salesPerson.FixedAmountCommission;
                row.PrecentageCommission = salesPerson.PrecentageCommission;
            }
            else
            {
                row.SalesPersonCode = "";
                row.IsPrecentageCommission = false;
                row.FixedAmountCommission = null;
                row.PrecentageCommission = null;
            }

            radGrid_salesPerson.DataSource = list;
            radGrid_salesPerson.DataBind();

            ViewState["salesPerson"] = list;
        }

        //Get the row where the values are changing
        private void GetSalesPersonID(GridDataItem item, out List<DAL.Setup_SalesPerson> list, out DAL.Setup_SalesPerson row)
        {
            string ID = item.GetDataKeyValue("ModifiedUserId").ToString();

            list = (List<DAL.Setup_SalesPerson>)ViewState["salesPerson"];

            row = list.Where(a => a.ModifiedUserId == int.Parse(ID)).SingleOrDefault();
        }

        #endregion

        #region 'Charges Grid and operations'

        protected void radGrid_charges_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item.ItemType == GridItemType.AlternatingItem || e.Item.ItemType == GridItemType.Item)
            {
                GridDataItem item = (GridDataItem)e.Item;

                Label lbl_itemOrderIndex = e.Item.FindControl("lbl_itemOrderIndex") as Label;
                lbl_itemOrderIndex.Text = (e.Item.ItemIndex + 1).ToString();

                TextBox txt_chargeDescription = e.Item.FindControl("txt_chargeDescription") as TextBox;
                txt_chargeDescription.Attributes.Add("key", e.Item.ItemIndex.ToString());

                RadComboBox rcb_ChargesType = (RadComboBox)item.FindControl("rcb_ChargesType");
                HiddenField hf_ChargesTypeId = (HiddenField)item.FindControl("hf_ChargesTypeId");
                Dictionary<int, string> chargeTypeList = BL.Setup.cls_Charge.GetChargeTypeList();
                rcb_ChargesType.DataSource = chargeTypeList;
                rcb_ChargesType.DataTextField = "Value";
                rcb_ChargesType.DataValueField = "Key";
                rcb_ChargesType.SelectedValue = hf_ChargesTypeId.Value;
                rcb_ChargesType.DataBind();

                rcb_ChargesType.Items.Insert(0, new RadComboBoxItem(Resources.Lang.select_all, ""));

                RadComboBox rcb_Charge = (RadComboBox)item.FindControl("rcb_Charge");
                HiddenField hf_ChargeId = (HiddenField)item.FindControl("hf_ChargeId");
                int? typeID = null;
                Dictionary<int, string> chargeList = BL.Setup.cls_Charge.GetChargeList((hf_ChargesTypeId.Value == "") ? typeID : int.Parse(hf_ChargesTypeId.Value));
                rcb_Charge.DataSource = chargeList;
                rcb_Charge.DataTextField = "Value";
                rcb_Charge.DataValueField = "Key";
                rcb_Charge.SelectedValue = hf_ChargeId.Value;
                rcb_Charge.DataBind();
            }
        }

        protected void radGrid_charges_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            GridDataItem item = (GridDataItem)e.Item;

            string ID = item.GetDataKeyValue("itemOrderID").ToString();

            List<OrderedItems> chargesList = (List<OrderedItems>)ViewState["chargesList"];

            OrderedItems row = chargesList.Where(a => a.itemOrderID == int.Parse(ID)).SingleOrDefault();

            if (row.itemOrderID > 0)
            {
                row.isDeleted = true;
            }
            else
            {
                chargesList.Remove(row);
            }

            LoadChargesGrid(chargesList);

            ViewState["chargesList"] = chargesList;

            CalculateTotalAmount();
        }

        protected void radGrid_charges_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            List<OrderedItems> list = (List<OrderedItems>)ViewState["chargesList"];

            if (list != null)
            {
                radGrid_charges.DataSource = list.Where(a => a.isDeleted == false).ToList();
            }
        }

        private void GetChargeID(GridDataItem item, out List<OrderedItems> list, out OrderedItems row)
        {
            string ID = item.GetDataKeyValue("itemOrderID").ToString();

            list = (List<OrderedItems>)ViewState["chargesList"];

            row = list.Where(a => a.itemOrderID == int.Parse(ID)).SingleOrDefault();
        }

        protected void txt_ChargesCode_TextChanged(object sender, EventArgs e)
        {
            GridDataItem gridItem;

            TextBox txt_ChargesCode = (sender as TextBox);
            gridItem = txt_ChargesCode.NamingContainer as GridDataItem;

            List<OrderedItems> list;
            OrderedItems row;

            GetChargeID(gridItem, out list, out row);

            DAL.Setup_Charge item = BL.Setup.cls_Charge.GetCharge(txt_ChargesCode.Text);

            if (item != null)
            {
                row.itemCode = txt_ChargesCode.Text;
                row.itemCategoryID = item.ChargesTypeId;
                row.itemID = item.ChargeId;
                row.Description = "";

                double? totalItem = GetTotalAmountOfItems();
                row.isPercentage = item.IsPrecentage;
                if (totalItem != null)
                {
                    if (item.IsPrecentage == true)
                    {
                        row.totalAmount = ((double)item.Precentage / 100) * totalItem;
                        row.quantitySummary = string.Format("{0:0.00}", item.Precentage) + " % (" + row.totalAmount + ")";
                        row.fixedUnitPrice = (double)item.Precentage;
                    }
                    else
                    {
                        row.totalAmount = (double)item.FixedAmount;
                        row.fixedUnitPrice = (double)item.FixedAmount;
                        row.quantitySummary = "(" + string.Format("{0:0.00}", row.totalAmount) + ")";
                    }
                }
                else
                {
                    row.quantitySummary = "(0)";
                }
            }
            else
            {
                row.itemCode = txt_ChargesCode.Text;
                row.itemCategoryID = null;
                row.itemID = null;
                row.quantitySummary = "";
                row.Description = "";
                row.quantitySummary = "";
                row.isPercentage = null;
                row.fixedUnitPrice = null;
            }

            LoadChargesGrid(list);

            ViewState["chargesList"] = list;

            CalculateTotalAmount();

            //txt_isValidItems.Text = CheckItemsGridEveryFieldChange(list);
        }

        protected void rcb_ChargesType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            GridDataItem gridItem;

            RadComboBox rcb_ChargesType = (RadComboBox)sender;

            gridItem = rcb_ChargesType.NamingContainer as GridDataItem;

            List<OrderedItems> list;
            OrderedItems row;

            GetChargeID(gridItem, out list, out row);

            int? selectedCategoryID = null;

            row.itemCategoryID = (e.Value == "") ? selectedCategoryID : int.Parse(e.Value);
            row.itemCode = null;
            row.itemID = null;
            row.quantitySummary = "";
            row.Description = "";
            row.totalAmount = 0;
            row.isPercentage = null;
            row.fixedUnitPrice = null;

            LoadChargesGrid(list);
            ViewState["chargesList"] = list;
            CalculateTotalAmount();
        }

        protected void rcb_Charge_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            GridDataItem gridItem;

            RadComboBox rcb_Charge = (RadComboBox)sender;

            gridItem = rcb_Charge.NamingContainer as GridDataItem;

            List<OrderedItems> list;
            OrderedItems row;

            GetChargeID(gridItem, out list, out row);

            DAL.Setup_Charge item = BL.Setup.cls_Charge.GetCharge(int.Parse(rcb_Charge.SelectedValue));

            row.itemCode = item.ChargesCode;
            row.itemCategoryID = item.ChargesTypeId;
            row.itemID = item.ChargeId;
            row.Description = "";

            double? totalItem = GetTotalAmountOfItems();
            row.isPercentage = item.IsPrecentage;
            if (totalItem != null)
            {
                if (item.IsPrecentage == true)
                {
                    row.totalAmount = ((double)item.Precentage / 100) * totalItem;
                    row.quantitySummary = string.Format("{0:0.00}", item.Precentage) + " % (" + row.totalAmount + ")";
                    row.fixedUnitPrice = (double)item.Precentage;
                }
                else
                {
                    row.totalAmount = (double)item.FixedAmount;
                    row.fixedUnitPrice = (double)item.FixedAmount;
                    row.quantitySummary = "(" + string.Format("{0:0.00}", row.totalAmount) + ")";
                }
            }
            else
            {
                row.quantitySummary = "(0)";
            }

            LoadChargesGrid(list);
            
            ViewState["chargesList"] = list;

            CalculateTotalAmount();
        }

        protected void txt_chargeDescription_TextChanged(object sender, EventArgs e)
        {
            GridDataItem gridItem;

            TextBox txt_chargeDescription = (TextBox)sender;

            gridItem = txt_chargeDescription.NamingContainer as GridDataItem;

            List<OrderedItems> list;
            OrderedItems row;

            GetChargeID(gridItem, out list, out row);

            row.Description = txt_chargeDescription.Text;

            LoadChargesGrid(list);
            ViewState["chargesList"] = list;
        }

        protected void lb_addNewCharge_Click(object sender, EventArgs e)
        {

            List<OrderedItems> chargesList;

            if (ViewState["chargesList"] == null)
            {
                chargesList = new List<OrderedItems>();
            }
            else
            {
                chargesList = (List<OrderedItems>)ViewState["chargesList"];
            }

            int id = (int)ViewState["chargeOrderID"] - 1;

            chargesList.Add(new OrderedItems()
            {
                itemOrderID = id,
                isDeleted = false
            });

            LoadChargesGrid(chargesList);

            ViewState["chargesList"] = chargesList;
            ViewState["chargeOrderID"] = id;
        }

        private void LoadChargesGrid(List<OrderedItems> chargesList)
        {
            radGrid_charges.DataSource = chargesList.Where(a=> a.isDeleted == false).ToList();
            radGrid_charges.DataBind();
        }

        #endregion

        #region 'Tax Grid and operations'

        protected void radGrid_tax_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item.ItemType == GridItemType.AlternatingItem || e.Item.ItemType == GridItemType.Item)
            {
                GridDataItem item = (GridDataItem)e.Item;

                Label lbl_itemOrderIndex = e.Item.FindControl("lbl_itemOrderIndex") as Label;
                lbl_itemOrderIndex.Text = (e.Item.ItemIndex + 1).ToString();

                TextBox txt_taxDescription = e.Item.FindControl("txt_taxDescription") as TextBox;
                txt_taxDescription.Attributes.Add("key", e.Item.ItemIndex.ToString());

                RadComboBox rcb_taxType = (RadComboBox)item.FindControl("rcb_taxType");
                HiddenField hf_taxTypeId = (HiddenField)item.FindControl("hf_taxTypeId");
                Dictionary<int, string> taxTypeList = BL.Setup.cls_Tax.GetTaxItemTypeList();
                rcb_taxType.DataSource = taxTypeList;
                rcb_taxType.DataTextField = "Value";
                rcb_taxType.DataValueField = "Key";
                rcb_taxType.SelectedValue = hf_taxTypeId.Value;
                rcb_taxType.DataBind();

                rcb_taxType.Items.Insert(0, new RadComboBoxItem(Resources.Lang.select_all, ""));

                RadComboBox rcb_tax = (RadComboBox)item.FindControl("rcb_tax");
                HiddenField hf_taxId = (HiddenField)item.FindControl("hf_taxId");
                int? typeID = null;
                Dictionary<int, string> chargeList = BL.Setup.cls_Tax.GetTaxItemList((hf_taxTypeId.Value == "") ? typeID : int.Parse(hf_taxTypeId.Value));
                rcb_tax.DataSource = chargeList;
                rcb_tax.DataTextField = "Value";
                rcb_tax.DataValueField = "Key";
                rcb_tax.SelectedValue = hf_taxId.Value;
                rcb_tax.DataBind();
            }
        }

        protected void radGrid_tax_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            GridDataItem item = (GridDataItem)e.Item;

            string ID = item.GetDataKeyValue("itemOrderID").ToString();

            List<OrderedItems> taxList = (List<OrderedItems>)ViewState["taxList"];

            OrderedItems row = taxList.Where(a => a.itemOrderID == int.Parse(ID)).SingleOrDefault();

            if (row.itemOrderID > 0)
            {
                row.isDeleted = true;
            }
            else
            {
                taxList.Remove(row);
            }

            LoadTaxGrid(taxList);

            ViewState["taxList"] = taxList;

            CalculateTotalAmount();
        }

        protected void radGrid_tax_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            List<OrderedItems> list = (List<OrderedItems>)ViewState["taxList"];

            if (list != null)
            {
                radGrid_tax.DataSource = list.Where(a => a.isDeleted == false).ToList();
            }
        }

        private void GetTaxID(GridDataItem item, out List<OrderedItems> list, out OrderedItems row)
        {
            string ID = item.GetDataKeyValue("itemOrderID").ToString();

            list = (List<OrderedItems>)ViewState["taxList"];

            row = list.Where(a => a.itemOrderID == int.Parse(ID)).SingleOrDefault();
        }

        protected void txt_taxCode_TextChanged(object sender, EventArgs e)
        {
            GridDataItem gridItem;

            TextBox txt_taxCode = (sender as TextBox);
            gridItem = txt_taxCode.NamingContainer as GridDataItem;

            List<OrderedItems> list;
            OrderedItems row;

            GetTaxID(gridItem, out list, out row);

            DAL.Setup_TaxItem item = BL.Setup.cls_Tax.GetTaxItem(txt_taxCode.Text);

            if (item != null)
            {
                row.itemCode = txt_taxCode.Text;
                row.itemCategoryID = item.TaxItemTypeId;
                row.itemID = item.TaxItemId;
                row.Description = "";

                double? totalItem = GetTotalAmountOfItems();
                row.isPercentage = item.IsPrecentage;
                if (totalItem != null)
                {
                    if (item.IsPrecentage == true)
                    {
                        row.totalAmount = ((double)item.Precentage / 100) * totalItem;
                        row.quantitySummary = string.Format("{0:0.00}", item.Precentage) + " % (" + row.totalAmount + ")";
                        row.fixedUnitPrice = (double)item.Precentage;
                    }
                    else
                    {
                        row.totalAmount = (double)item.FixedAmount;
                        row.fixedUnitPrice = (double)item.FixedAmount;
                        row.quantitySummary = "(" + string.Format("{0:0.00}", row.totalAmount) + ")";
                    }
                }
                else
                {
                    row.quantitySummary = "(0)";
                }
            }
            else
            {
                row.itemCode = txt_taxCode.Text;
                row.itemCategoryID = null;
                row.itemID = null;
                row.quantitySummary = "";
                row.Description = "";
                row.quantitySummary = "";
                row.isPercentage = null;
                row.fixedUnitPrice = null;
            }

            LoadTaxGrid(list);

            ViewState["taxList"] = list;

            CalculateTotalAmount();
        }

        protected void rcb_taxType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            GridDataItem gridItem;

            RadComboBox rcb_taxType = (RadComboBox)sender;

            gridItem = rcb_taxType.NamingContainer as GridDataItem;

            List<OrderedItems> list;
            OrderedItems row;

            GetTaxID(gridItem, out list, out row);

            int? selectedCategoryID = null;

            row.itemCategoryID = (e.Value == "") ? selectedCategoryID : int.Parse(e.Value);
            row.itemCode = null;
            row.itemID = null;
            row.quantitySummary = "";
            row.Description = "";
            row.totalAmount = 0;
            row.isPercentage = null;
            row.fixedUnitPrice = null;

            LoadTaxGrid(list);
            ViewState["taxList"] = list;
            CalculateTotalAmount();
        }

        protected void rcb_tax_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            GridDataItem gridItem;

            RadComboBox rcb_tax = (RadComboBox)sender;

            gridItem = rcb_tax.NamingContainer as GridDataItem;

            List<OrderedItems> list;
            OrderedItems row;

            GetTaxID(gridItem, out list, out row);

            DAL.Setup_TaxItem item = BL.Setup.cls_Tax.GetTaxItem(int.Parse(rcb_tax.SelectedValue));

            row.itemCode = item.TaxItemcode;
            row.itemCategoryID = item.TaxItemTypeId;
            row.itemID = item.TaxItemId;
            row.Description = "";

            double? totalItem = GetTotalAmountOfItems();
            row.isPercentage = item.IsPrecentage;
            if (totalItem != null)
            {
                if (item.IsPrecentage == true)
                {
                    row.totalAmount = ((double)item.Precentage / 100) * totalItem;
                    row.quantitySummary = string.Format("{0:0.00}", item.Precentage) + " % (" + row.totalAmount + ")";
                    row.fixedUnitPrice = (double)item.Precentage;
                }
                else
                {
                    row.totalAmount = (double)item.FixedAmount;
                    row.fixedUnitPrice = (double)item.FixedAmount;
                    row.quantitySummary = "(" + string.Format("{0:0.00}", row.totalAmount) + ")";
                }
            }
            else
            {
                row.quantitySummary = "(0)";
            }

            LoadTaxGrid(list);
            
            ViewState["taxList"] = list;

            CalculateTotalAmount();
        }

        protected void txt_taxDescription_TextChanged(object sender, EventArgs e)
        {
            GridDataItem gridItem;

            TextBox txt_taxDescription = (TextBox)sender;

            gridItem = txt_taxDescription.NamingContainer as GridDataItem;

            List<OrderedItems> list;
            OrderedItems row;

            GetTaxID(gridItem, out list, out row);

            row.Description = txt_taxDescription.Text;

            LoadTaxGrid(list);
            ViewState["taxList"] = list;
        }

        protected void lb_addNewTax_Click(object sender, EventArgs e)
        {
            List<OrderedItems> taxList;

            if (ViewState["taxList"] == null)
            {
                taxList = new List<OrderedItems>();
            }
            else
            {
                taxList = (List<OrderedItems>)ViewState["taxList"];
            }

            int id = (int)ViewState["taxOrderID"] - 1;

            taxList.Add(new OrderedItems()
            {
                itemOrderID = id,
                isDeleted = false
            });

            LoadTaxGrid(taxList);

            ViewState["taxList"] = taxList;
            ViewState["taxOrderID"] = id;
        }

        private void LoadTaxGrid(List<OrderedItems> taxList)
        {
            radGrid_tax.DataSource = taxList;
            radGrid_tax.DataBind();
        }

        #endregion

        #region 'Discount Grid and operations'

        private void GetDiscountID(GridDataItem item, out List<OrderedItems> list, out OrderedItems row)
        {
            string ID = item.GetDataKeyValue("itemOrderID").ToString();

            list = (List<OrderedItems>)ViewState["discountList"];

            row = list.Where(a => a.itemOrderID == int.Parse(ID)).SingleOrDefault();
        }

        protected void radGrid_discount_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item.ItemType == GridItemType.AlternatingItem || e.Item.ItemType == GridItemType.Item)
            {
                GridDataItem item = (GridDataItem)e.Item;

                Label lbl_itemOrderIndex = e.Item.FindControl("lbl_itemOrderIndex") as Label;
                lbl_itemOrderIndex.Text = (e.Item.ItemIndex + 1).ToString();

                TextBox txt_discountDescription = e.Item.FindControl("txt_discountDescription") as TextBox;
                txt_discountDescription.Attributes.Add("key", e.Item.ItemIndex.ToString());

                RadComboBox rcb_discountType = (RadComboBox)item.FindControl("rcb_discountType");
                HiddenField hf_discountTypeId = (HiddenField)item.FindControl("hf_discountTypeId");
                Dictionary<int, string> discountTypeList = BL.Setup.cls_Discount.GetDiscountTypeList();
                rcb_discountType.DataSource = discountTypeList;
                rcb_discountType.DataTextField = "Value";
                rcb_discountType.DataValueField = "Key";
                rcb_discountType.SelectedValue = hf_discountTypeId.Value;
                rcb_discountType.DataBind();

                rcb_discountType.Items.Insert(0, new RadComboBoxItem(Resources.Lang.select_all, ""));

                RadComboBox rcb_discount = (RadComboBox)item.FindControl("rcb_discount");
                HiddenField hf_discountId = (HiddenField)item.FindControl("hf_discountId");
                int? typeID = null;
                Dictionary<int, string> chargeList = BL.Setup.cls_Discount.GetDiscountList((hf_discountTypeId.Value == "") ? typeID : int.Parse(hf_discountTypeId.Value));
                rcb_discount.DataSource = chargeList;
                rcb_discount.DataTextField = "Value";
                rcb_discount.DataValueField = "Key";
                rcb_discount.SelectedValue = hf_discountId.Value;
                rcb_discount.DataBind();
            }
        }

        protected void radGrid_discount_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            GridDataItem item = (GridDataItem)e.Item;

            string ID = item.GetDataKeyValue("itemOrderID").ToString();

            List<OrderedItems> discountList = (List<OrderedItems>)ViewState["discountList"];

            OrderedItems row = discountList.Where(a => a.itemOrderID == int.Parse(ID)).SingleOrDefault();

            if (row.itemOrderID > 0)
            {
                row.isDeleted = true;
            }
            else
            {
                discountList.Remove(row);
            }

            LoadDiscountGrid(discountList);

            ViewState["discountList"] = discountList;

            CalculateTotalAmount();
        }

        protected void radGrid_discount_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            List<OrderedItems> list = (List<OrderedItems>)ViewState["discountList"];

            if (list != null)
            {
                radGrid_discount.DataSource = list.Where(a => a.isDeleted == false).ToList();
            }
        }

        protected void txt_discountCode_TextChanged(object sender, EventArgs e)
        {
            GridDataItem gridItem;

            TextBox txt_discountCode = (sender as TextBox);
            gridItem = txt_discountCode.NamingContainer as GridDataItem;

            List<OrderedItems> list;
            OrderedItems row;

            GetDiscountID(gridItem, out list, out row);

            DAL.Setup_Discount item = BL.Setup.cls_Discount.GetDiscount(txt_discountCode.Text);

            if (item != null)
            {
                row.itemCode = txt_discountCode.Text;
                row.itemCategoryID = item.DiscountTypeId;
                row.itemID = item.DiscountId;
                row.Description = "";

                double? totalItem = GetTotalAmountOfItems();
                row.isPercentage = item.IsPrecentage;
                if (totalItem != null)
                {
                    if (item.IsPrecentage == true)
                    {
                        row.totalAmount = ((double)item.Precentage / 100) * totalItem;
                        row.quantitySummary = string.Format("{0:0.00}", item.Precentage) + " % (" + row.totalAmount + ")";
                        row.fixedUnitPrice = (double)item.Precentage;
                    }
                    else
                    {
                        row.totalAmount = (double)item.FixedAmount;
                        row.fixedUnitPrice = (double)item.FixedAmount;
                        row.quantitySummary = "(" + string.Format("{0:0.00}", row.totalAmount) + ")";
                    }
                }
                else
                {
                    row.quantitySummary = "(0)";
                }
            }
            else
            {
                row.itemCode = txt_discountCode.Text;
                row.itemCategoryID = null;
                row.itemID = null;
                row.quantitySummary = "";
                row.Description = "";
                row.quantitySummary = "";
                row.isPercentage = null;
                row.fixedUnitPrice = null;
            }

            LoadDiscountGrid(list);

            ViewState["discountList"] = list;

            CalculateTotalAmount();
        }

        protected void rcb_discountType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            GridDataItem gridItem;

            RadComboBox rcb_discountType = (RadComboBox)sender;

            gridItem = rcb_discountType.NamingContainer as GridDataItem;

            List<OrderedItems> list;
            OrderedItems row;

            GetDiscountID(gridItem, out list, out row);

            int? selectedCategoryID = null;

            row.itemCategoryID = (e.Value == "") ? selectedCategoryID : int.Parse(e.Value);
            row.itemCode = null;
            row.itemID = null;
            row.quantitySummary = "";
            row.Description = "";
            row.totalAmount = 0;
            row.isPercentage = null;
            row.fixedUnitPrice = null;

            LoadDiscountGrid(list);
            ViewState["discountList"] = list;
            CalculateTotalAmount();
        }

        protected void rcb_discount_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            GridDataItem gridItem;

            RadComboBox rcb_discount = (RadComboBox)sender;

            gridItem = rcb_discount.NamingContainer as GridDataItem;

            List<OrderedItems> list;
            OrderedItems row;

            GetDiscountID(gridItem, out list, out row);

            DAL.Setup_Discount item = BL.Setup.cls_Discount.GetDiscount(int.Parse(rcb_discount.SelectedValue));

            row.itemCode = item.DiscountCode;
            row.itemCategoryID = item.DiscountTypeId;
            row.itemID = item.DiscountId;
            row.Description = "";

            double? totalItem = GetTotalAmountOfItems();
            row.isPercentage = item.IsPrecentage;
            if (totalItem != null)
            {
                if (item.IsPrecentage == true)
                {
                    row.totalAmount = ((double)item.Precentage / 100) * totalItem;
                    row.quantitySummary = string.Format("{0:0.00}", item.Precentage) + " % (" + row.totalAmount + ")";
                    row.fixedUnitPrice = (double)item.Precentage;
                }
                else
                {
                    row.totalAmount = (double)item.FixedAmount;
                    row.quantitySummary = "(" + string.Format("{0:0.00}", row.totalAmount) + ")";
                    row.fixedUnitPrice = (double)item.FixedAmount;
                }
            }
            else
            {
                row.quantitySummary = "(0)";
            }

            LoadDiscountGrid(list);
            
            ViewState["discountList"] = list;

            CalculateTotalAmount();
        }

        protected void txt_discountDescription_TextChanged(object sender, EventArgs e)
        {
            GridDataItem gridItem;

            TextBox txt_discountDescription = (TextBox)sender;

            gridItem = txt_discountDescription.NamingContainer as GridDataItem;

            List<OrderedItems> list;
            OrderedItems row;

            GetDiscountID(gridItem, out list, out row);

            row.Description = txt_discountDescription.Text;

            LoadDiscountGrid(list);
            ViewState["discountList"] = list;
        }

        protected void lb_addNewDiscount_Click(object sender, EventArgs e)
        {
            List<OrderedItems> discountList;

            if (ViewState["discountList"] == null)
            {
                discountList = new List<OrderedItems>();
            }
            else
            {
                discountList = (List<OrderedItems>)ViewState["discountList"];
            }

            int id = (int)ViewState["discountOrderID"] - 1;

            discountList.Add(new OrderedItems()
            {
                itemOrderID = id,
                isDeleted = false
            });

            LoadDiscountGrid(discountList);

            ViewState["discountList"] = discountList;
            ViewState["discountOrderID"] = id;
        }

        private void LoadDiscountGrid(List<OrderedItems> discountList)
        {
            radGrid_discount.DataSource = discountList.Where(a => a.isDeleted == false).ToList();
            radGrid_discount.DataBind();
        }

        #endregion

        protected void rgv_attachments_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            GridDataItem item = (GridDataItem)e.Item;

            string ID = item.GetDataKeyValue("OrderTemplateAttachmentId").ToString();

            List<DAL.Flow_OrderTemplateAttachment> attachments = (List<DAL.Flow_OrderTemplateAttachment>)ViewState["orderAttachments"];

            DAL.Flow_OrderTemplateAttachment row = attachments.Where(a => a.OrderTemplateAttachmentId == int.Parse(ID)).SingleOrDefault();
            if (row.OrderTemplateAttachmentId > 0)
            {
                row.IsDeleted = true;
            }
            else
            {
                attachments.Remove(row);
            }

            rgv_attachments.DataSource = attachments.Where(a => a.IsDeleted == false).ToList();
            rgv_attachments.DataBind();
            Session["OrderTemplateAttachmentId"] = attachments;
        }

        protected void numeric_noneCurrencyCost_TextChanged(object sender, EventArgs e)
        {
            CalculateTotalAmount();
        }

        private double? GetTotalAmountOfItems()
        {
            List<OrderedItems> list = (List<OrderedItems>)ViewState["itemsList"];

            return list.Where(a => a.isDeleted == false).Sum(a => a.totalAmount);
        }

        private void CalculateTotalAmount()
        {
            List<OrderedItems> orderedItemsList = (List<OrderedItems>)ViewState["itemsList"];
            var total = orderedItemsList.Where(a => a.isDeleted == false).Sum(a => a.totalAmount);

            double? totalCharge = 0;
            double? totalTax = 0;
            double? totalDiscount = 0;

            decimal totalTax1 = orderedItemsList.Where(a => a.isDeleted == false).Sum(a => a.tax1);
            decimal totalTax2 = orderedItemsList.Where(a => a.isDeleted == false).Sum(a => a.tax2);
            decimal totalTax3 = orderedItemsList.Where(a => a.isDeleted == false).Sum(a => a.tax3);

            lbl_salesTax1Value.Text = string.Format("{0:0.00}", totalTax1) + "<br/>";
            lbl_salesTax2Value.Text = string.Format("{0:0.00}", totalTax2) + "<br/>";
            lbl_salesTax3Value.Text = string.Format("{0:0.00}", totalTax3) + "<br/>";

            if (ViewState["chargesList"] != null)
            {
                List<OrderedItems> charges = (List<OrderedItems>)ViewState["chargesList"];
                charges = RecalculateChargeTaxDiscount(charges, total);
                totalCharge = charges.Where(a => a.isDeleted == false).Sum(a => a.totalAmount);
                lbl_totalCharges.Text = string.Format("{0:0.00}", totalCharge);

                LoadChargesGrid(charges);
                ViewState["chargesList"] = charges;
            }
            else
            {
                lbl_totalCharges.Text = "0.00";
            }

            if (ViewState["taxList"] != null)
            {
                List<OrderedItems> tax = (List<OrderedItems>)ViewState["taxList"];
                tax = RecalculateChargeTaxDiscount(tax, total);
                totalTax = tax.Where(a => a.isDeleted == false).Sum(a => a.totalAmount);
                lbl_totalTax.Text = string.Format("{0:0.00}", totalTax);

                LoadTaxGrid(tax);
                ViewState["taxList"] = tax;
            }
            else
            {
                lbl_totalTax.Text = "0.00";
            }

            if (ViewState["discountList"] != null)
            {
                List<OrderedItems> discount = (List<OrderedItems>)ViewState["discountList"];
                discount = RecalculateChargeTaxDiscount(discount, total);
                totalDiscount = discount.Where(a => a.isDeleted == false).Sum(a => a.totalAmount);
                lbl_totalDiscount.Text = string.Format("{0:0.00}", totalDiscount);

                LoadDiscountGrid(discount);
                ViewState["discountList"] = discount;
            }
            else
            {
                lbl_totalDiscount.Text = "0.00";
            }

            double? noneInventoryCost = null;

            if (cb_PercentageNoneInventoryCost.Checked)
            {
                noneInventoryCost = ((numeric_noneInventoryCost.Value / 100) * total);      
            }
            else
            {
                noneInventoryCost = numeric_noneInventoryCost.Value;
            }

            lbl_totalNoneInventoryCost.Text = string.Format("{0:0.00}", noneInventoryCost);

            double? deduction = null;

            if (cb_deductionPercentage.Checked)
            {
                deduction = ((numeric_deductiont.Value / 100) * total);
            }
            else
            {
                deduction = numeric_deductiont.Value;
            }

            lbl_totalDeduction.Text = string.Format("{0:0.00}", deduction);

            double? overAll = total + noneInventoryCost + totalCharge + totalTax + (((double?)totalTax1 == null) ? 0 : (double)totalTax1) + (((double?)totalTax2 == null) ? 0 : (double)totalTax2) + (((double?)totalTax3 == null) ? 0 : (double)totalTax3) - totalDiscount - deduction;           

            lbl_itemsTotal.Text = string.Format("{0:0.00}", total);

            lbl_totalAmount.Text = string.Format("{0:0.00}", overAll) + "<br/>";

            if (overAll < 0)
            {
                overAll = overAll * -1;
                lbl_totalAmount.Text += "- ";
            }

            BL.Static.cls_NumberToText ccc = new BL.Static.cls_NumberToText();

            if (overAll != 0)
            {
                if (HttpContext.Current.Session["lang"].ToString() == "Arabic")
                {
                    BL.Static.cls_NumberToArabic ToArabic;

                    string num = (decimal.Round((decimal)overAll, 2, MidpointRounding.AwayFromZero)).ToString();
                    string[] numArray = num.Split('.');

                    if (numArray.Count() > 1)
                    {
                        num = (numArray[1].Length > 1) ? numArray[1].Substring(0, 2) : numArray[1].Substring(0, 1) + "0";
                    }
                    else
                    {
                        num = "00";
                    }

                    if (num == "00")
                    {
                        ToArabic = new BL.Static.cls_NumberToArabic(decimal.Round((decimal)overAll, 0).ToString());
                    }
                    else
                    {
                        ToArabic = new BL.Static.cls_NumberToArabic((numArray[1].Length > 1) ? decimal.Round((decimal)overAll, 2).ToString() : decimal.Round((decimal)overAll, 2).ToString() + "0");
                    }

                    lbl_totalAmount.Text +=  ToArabic.GetNumberAr() + " فقط";
                }
                else
                {
                    string[] no = decimal.Round((decimal)overAll, 2, MidpointRounding.AwayFromZero).ToString().Split('.');
                    string num = "";
                    if (no.Count() > 1)
                    {
                        num = ccc.NumToText(no[0], no[1]);
                    }
                    else
                    {
                        num = ccc.NumToText(no[0], "00");
                    }

                    lbl_totalAmount.Text += num;
                }
            }

        }

        private List<OrderedItems> RecalculateChargeTaxDiscount(List<OrderedItems> list, double? total)
        {
            foreach (OrderedItems row in list)
            {
                if (total != null && row.fixedUnitPrice != null && row.isPercentage != null)
                {
                    if (row.isPercentage == true)
                    {
                        row.totalAmount = ((double)row.fixedUnitPrice / 100) * total;
                        row.quantitySummary = string.Format("{0:0.00}", row.fixedUnitPrice) + " % (" + row.totalAmount + ")";
                    }
                    else
                    {
                        row.totalAmount = (double)row.fixedUnitPrice;
                        row.quantitySummary = "(" + string.Format("{0:0.00}", row.totalAmount) + ")";
                    }
                 }
                else
                {
                    row.quantitySummary = "(0)";
                }
            }

            return list;
        }

        protected void btn_saveAndNewOrder_Click(object sender, ImageClickEventArgs e)
        {
            SaveOrder();

            if (status)
            {
                pnl_overAll.Enabled = false;
                Response.AddHeader("REFRESH", "5;URL=sales_order.aspx");
            }
        }

        protected void btn_saveAndClose_Click(object sender, ImageClickEventArgs e)
        {
            SaveOrder();

            if (status)
            {
                pnl_overAll.Enabled = false;
                Response.AddHeader("REFRESH", "5;URL=SalesOrder_List.aspx");
            }
        }

        protected void btn_addAndCopy_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void btn_cancelOrder_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/sales/SalesOrder_List.aspx");
        }

        private int SaveOrder()
        {
            int newSalesOrderID = 0;

            int? _requestedByID = null;
            NullableInt.TryParse(cls_StaticData.DecodingID(rcb_requestedBy.SelectedValue).ToString(), out _requestedByID);

            DateTime? _requestDate = null;
            NullableDateTime.TryParse(rdp_date.SelectedDate.ToString(), out _requestDate);

            int? _vendorID = null;

            int? _vendorAddressID = null;

            int? _customerID = null;
            NullableInt.TryParse(cls_StaticData.DecodingID(rcb_customer.SelectedValue).ToString(), out _customerID);

            int? _customerAddressID = null;

            int? _shipmentAddressID = null;
            NullableInt.TryParse(hf_customerShipAddress.Text, out _shipmentAddressID);
            DAL.Setup_Address newShipmentAddress = (DAL.Setup_Address)ViewState["newCustomerShipAddress"];

            int? _billAddressID = null;
            NullableInt.TryParse(hf_customerBillAddress.Text, out _billAddressID);
            DAL.Setup_Address newBillAddress = (DAL.Setup_Address)ViewState["newCustomerMainAddress"];

            int? _shipmentMethodID = null;
            NullableInt.TryParse(rbl_shipVia.SelectedValue, out _shipmentMethodID);

            int? _currencyID = null;
            if (!cb_currencyPerItem.Checked)
            {
                NullableInt.TryParse(rcb_currency.SelectedValue, out _currencyID);
            }

            int? _taxItemID = null;

            decimal _noneInventoryCost = 0;
            decimal.TryParse(numeric_noneInventoryCost.Text, out _noneInventoryCost);

            decimal _deduction = 0;
            decimal.TryParse(numeric_deductiont.Text, out _deduction);

            int? _paymentMethodID = null;
            NullableInt.TryParse(cls_StaticData.DecodingID(rcb_paymentMethod.SelectedValue).ToString(), out _paymentMethodID);

            int? _paymentTermID = null;
            NullableInt.TryParse(cls_StaticData.DecodingID(rcb_paymentTerm.SelectedValue).ToString(), out _paymentTermID);

            DateTime? _dateOfDelivery = null;
            if (!cb_useOfDeliveryDatePerItem.Checked)
            {
                NullableDateTime.TryParse(rdp_dateOfDelivery.SelectedDate.ToString(), out _dateOfDelivery);
            }

            DateTime? _paymentDate = null;
            NullableDateTime.TryParse(rdp_paymentDate.SelectedDate.ToString(), out _paymentDate);

            int? _messageID = null;
            NullableInt.TryParse(rcb_messageTitle.SelectedValue, out _messageID);

            List<OrderedItems> orderedItemsList = new List<OrderedItems>();

            if (ViewState["itemsList"] != null)
            {
                orderedItemsList = (List<OrderedItems>)ViewState["itemsList"];
            }

            List<OrderedItems> chargeList = new List<OrderedItems>();

            if (ViewState["chargesList"] != null)
            {
                chargeList = (List<OrderedItems>)ViewState["chargesList"];
            }

            List<OrderedItems> taxList = new List<OrderedItems>();

            if (ViewState["taxList"] != null)
            {
                taxList = (List<OrderedItems>)ViewState["taxList"];
            }

            List<OrderedItems> discountList = new List<OrderedItems>();

            if (ViewState["discountList"] != null)
            {
                discountList = (List<OrderedItems>)ViewState["discountList"];
            }

            List<DAL.Setup_SalesPerson> salesPersonList = new List<DAL.Setup_SalesPerson>();

            if (ViewState["salesPerson"] != null)
            {
                salesPersonList = (List<DAL.Setup_SalesPerson>)ViewState["salesPerson"];
            }

            List<DAL.Flow_OrderTemplateAttachment> attachments = new List<DAL.Flow_OrderTemplateAttachment>();

            if (ViewState["orderAttachments"] != null)
            {
                attachments = (List<DAL.Flow_OrderTemplateAttachment>)ViewState["orderAttachments"];
            }

            List<BL.Data.ItemCustomFieldData> customFielsList = new List<BL.Data.ItemCustomFieldData>();

            if (customFields != null)
            {
                foreach (var field in customFields)
                {
                    if (field.Value is TextBox)
                    {
                        customFielsList.Add(new BL.Data.ItemCustomFieldData()
                        {

                            CustomFieldValue = ((TextBox)field.Value).Text,
                            CustomFieldId = field.Key,
                            ItemCustomFieldId = int.Parse(((TextBox)field.Value).Attributes["rowID"])
                        });
                    }
                    else if (field.Value is RadDatePicker)
                    {
                        customFielsList.Add(new BL.Data.ItemCustomFieldData()
                        {
                            CustomFieldValue = ((RadDatePicker)field.Value).SelectedDate.ToString(),
                            CustomFieldId = field.Key,
                            ItemCustomFieldId = int.Parse(((RadDatePicker)field.Value).Attributes["rowID"])
                        });
                    }
                    else if (field.Value is RadNumericTextBox)
                    {
                        customFielsList.Add(new BL.Data.ItemCustomFieldData()
                        {
                            CustomFieldValue = ((RadNumericTextBox)field.Value).Value.ToString(),
                            CustomFieldId = field.Key,
                            ItemCustomFieldId = int.Parse(((RadNumericTextBox)field.Value).Attributes["rowID"])
                        });
                    }
                    else if (field.Value is CheckBox)
                    {
                        customFielsList.Add(new BL.Data.ItemCustomFieldData()
                        {
                            CustomFieldValue = ((CheckBox)field.Value).Checked.ToString(),
                            CustomFieldId = field.Key,
                            ItemCustomFieldId = int.Parse(((CheckBox)field.Value).Attributes["rowID"])
                        });
                    }
                }
            }

            if (rau_uploadedFiles.UploadedFiles.Count > 0)
            {
                for (int i = 0; i < rau_uploadedFiles.UploadedFiles.Count; i++)
                {
                    UploadedFile file = rau_uploadedFiles.UploadedFiles[i];
                    string Path = string.Format("../Uploads/{0}", file.GetName().Substring(0, file.GetName().IndexOf(".")) + DateTime.Now.ToShortDateString().Replace("/", "").Replace(" ", "") + DateTime.Now.ToLongTimeString().Replace(":", "").Replace(" ", "") + file.GetExtension());
                    string name = file.GetName();
                    var filePath = Server.MapPath(Path);
                    file.SaveAs(filePath);
                    attachments.Add(new DAL.Flow_OrderTemplateAttachment()
                    {
                        OrderTemplateAttachmentId = -(1 + i),
                        AttachmentName = name,
                        AttachmentPath = Path,
                        IsDeleted = false
                    });
                }
            }


            if (saleOrderID == null)
            {
                status = BL.Sales.cls_SalesOrder.AddSalesOrder(3, lbl_salesOrderCodeValue.Text, txt_paperBasedSalesOrderNumber.Text,
                    cb_hold.Checked, _requestedByID, _requestDate, null, _vendorID, _vendorAddressID, _customerID,_shipmentAddressID,
                    _billAddressID, _customerAddressID, _shipmentMethodID, _currencyID,cb_currencyPerItem.Checked,_deduction,cb_deductionPercentage.Checked,
                    _taxItemID, _noneInventoryCost,cb_PercentageNoneInventoryCost.Checked, _paymentMethodID, _paymentTermID, _dateOfDelivery,
                    cb_useOfDeliveryDatePerItem.Checked, _paymentDate, _messageID, txt_message.Text, txt_note.Text, attachments,
                    orderedItemsList, chargeList, taxList, discountList, salesPersonList, customFielsList, newShipmentAddress,newBillAddress,
                    int.Parse(Session["UserID"].ToString()), out foundError, out newSalesOrderID);

            }
            else
            {
                status = BL.Sales.cls_SalesOrder.UpdateSalesOrder(saleOrderID, txt_paperBasedSalesOrderNumber.Text,
                    cb_hold.Checked, _requestedByID, _requestDate, null, _vendorID, _vendorAddressID, _customerID, _shipmentAddressID,
                    _billAddressID, _customerAddressID, _shipmentMethodID, _currencyID, cb_currencyPerItem.Checked, _deduction, cb_deductionPercentage.Checked,
                    _taxItemID, _noneInventoryCost, cb_PercentageNoneInventoryCost.Checked, _paymentMethodID, _paymentTermID, _dateOfDelivery,
                    cb_useOfDeliveryDatePerItem.Checked, _paymentDate, _messageID, txt_message.Text, txt_note.Text, attachments,
                    orderedItemsList, chargeList, taxList, discountList, salesPersonList, customFielsList, newShipmentAddress, newBillAddress,
                    int.Parse(Session["UserID"].ToString()), out foundError);

                newSalesOrderID = (int)saleOrderID;
            }

            UI.ShowSuccessMessage1(this, foundError, status);
            return newSalesOrderID;
        }

        private decimal? CalculateItemUnitPrice(int itemID, int uomID, decimal quantity, int currencyID)
        {
            decimal? value = 0;

            if (quantity != 0 && rcb_customer.SelectedValue != "")
            {
                ArrayList itemsValues = BL.Sales.cls_SalesOrder.GetItemCustomUnitPrice(cls_StaticData.DecodingID(rcb_customer.SelectedValue), itemID, quantity, uomID, null);

                if (currencyID != int.Parse(itemsValues[1].ToString()))
                {
                    value = BL.Sales.cls_SalesOrder.GetItemUnitPriceByDifferentCurrency(int.Parse(itemsValues[1].ToString()), currencyID, decimal.Parse(itemsValues[0].ToString()));
                }
                else
                {
                    value = decimal.Parse(itemsValues[0].ToString());
                }
            }

            return value;
        }

        protected void CalculateUintPriceOfAllItems()
        {
            if (ViewState["itemsList"] != null)
            {
                List<OrderedItems> list = (List<OrderedItems>)ViewState["itemsList"];

                foreach (var row in list)
                {
                    if (row.itemID != null)
                    {
                        decimal? customePrice = CalculateItemUnitPrice((int)row.itemID, (int)row.salesUOMID, decimal.Parse(row.quantity.ToString()), (row.currency == null) ? int.Parse(rcb_currency.SelectedValue) : (int)row.currency);

                        row.unitPrice = (double)customePrice;

                        row.totalAmount = row.unitPrice * row.quantity;

                        CalculateSalesTaxPerItem(row);
                    }
                }

                ViewState["itemsList"] = list;
                LoadGridItems(list);
            }
        }

        protected void txt_customer_TextChanged(object sender, EventArgs e)
        {
            string code = BL.Data.cls_customer_vendor.GetCustomerID(txt_customer.Text).ToString();

            if (code != "")
            {
                rcb_customer.SelectedValue = code + BL.Static.cls_StaticData.HashCode + txt_customer.Text;

                GetCustomerDefaultDiscount(int.Parse(code));

                GetCustomerAddresses();
            }
            else
            {
                rcb_customer.DataBind();

                lbl_customerBillAddress.Text = "";

                hf_customerBillAddress.Text = "";

                lbl_customerShipAddress.Text = "";

                hf_customerShipAddress.Text = "";
            }

           

            CalculateUintPriceOfAllItems();

            CalculateTotalAmount();
        }

        protected void rcb_currency_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            CalculateUintPriceOfAllItems();

            CalculateTotalAmount();
        }

        protected void cb_currencyPerItem_CheckedChanged(object sender, EventArgs e)
        {
            CalculateUintPriceOfAllItems();

            CalculateTotalAmount();
        }

        protected void rcb_customer_EntryAdded(object sender, DropDownTreeEntryEventArgs e)
        {
            txt_customer.Text = BL.Static.cls_StaticData.DecodingCode(rcb_customer.SelectedValue);

            GetCustomerAddresses();

            GetCustomerDefaultDiscount(BL.Static.cls_StaticData.DecodingID(rcb_customer.SelectedValue));

            CalculateUintPriceOfAllItems();

            CalculateTotalAmount();
        }

        private void GetCustomerAddresses()
        {
            int startIndex = BL.Static.cls_StaticData.HashCode.Count();

            string mainAddress = BL.Data.cls_CustomerVendorAddress.GetcustomerVendorAddress(BL.Static.cls_StaticData.DecodingID(rcb_customer.SelectedValue), Session["lang"].ToString(), 1);
            string shipAddress = BL.Data.cls_CustomerVendorAddress.GetcustomerVendorAddress(BL.Static.cls_StaticData.DecodingID(rcb_customer.SelectedValue), Session["lang"].ToString(), 2);

            if (mainAddress == "")
            {
                lbl_customerBillAddress.Text = Resources.Lang.no_address_available;

                hf_customerBillAddress.Text = "";
            }
            else
            {
                lbl_customerBillAddress.Text = BL.Static.cls_StaticData.DecodingCode(mainAddress);

                hf_customerBillAddress.Text = BL.Static.cls_StaticData.DecodingID(mainAddress).ToString();
            }

            if (shipAddress == "")
            {
                lbl_customerShipAddress.Text = Resources.Lang.no_address_available;

                hf_customerShipAddress.Text = "";
            }
            else
            {
                lbl_customerShipAddress.Text = BL.Static.cls_StaticData.DecodingCode(shipAddress);

                hf_customerShipAddress.Text = BL.Static.cls_StaticData.DecodingID(shipAddress).ToString();
            }
        }

        private void GetCustomerDefaultDiscount(int customerID)
        {
            DAL.Setup_Discount discount = BL.Data.cls_CustomerVendorData.CustomerVendorDiscount(customerID);

            List<OrderedItems> discountList = new List<OrderedItems>();

            if (discount != null)
            {
                OrderedItems row = new OrderedItems();

                row.itemCode = discount.DiscountCode;
                row.itemCategoryID = discount.DiscountTypeId;
                row.itemID = discount.DiscountId;
                row.Description = "";

                double? totalItem = GetTotalAmountOfItems();
                row.isPercentage = discount.IsPrecentage;

                if (totalItem != null)
                {
                    if (discount.IsPrecentage == true)
                    {
                        row.totalAmount = ((double)discount.Precentage / 100) * totalItem;
                        row.quantitySummary = string.Format("{0:0.00}", discount.Precentage) + " % (" + row.totalAmount + ")";
                        row.fixedUnitPrice = (double)discount.Precentage;
                    }
                    else
                    {
                        row.totalAmount = (double)discount.FixedAmount;
                        row.fixedUnitPrice = (double)discount.FixedAmount;
                        row.quantitySummary = "(" + string.Format("{0:0.00}", row.totalAmount) + ")";
                    }
                }
                else
                {
                    row.quantitySummary = "(0)";
                }

                discountList.Add(row);
            }

            

            LoadDiscountGrid(discountList);

            ViewState["discountList"] = discountList;
        }
    }
}