﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="tax_item_type.aspx.cs" Inherits="InvyLap.Setup.tax_item_type" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <script type="text/javascript">
        function CloseAndRebind(args) {
            GetRadWindow().BrowserWindow.refreshGrid(args);
            GetRadWindow().close();
        }

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

            return oWindow;
        }

        function CancelEdit() {
            GetRadWindow().close();
        }
        </script>

    <style>
        .msg {
            padding: 20px;
            display: block;
            background-color: #e8f5e5;
            font-size: 15px;
            font-weight: bold;
            margin: 10px 0;
        }

        .buttonStyle {
border:1px solid #1e72c0; -webkit-border-radius: 3px; -moz-border-radius: 3px;border-radius: 3px;font-size:12px; padding: 3px 7px; text-decoration:none; display:inline-block;text-shadow: -1px -1px 0 rgba(0,0,0,0.3);font-weight:bold; color: #FFFFFF;
 background-color: #368EE0; background-image: -webkit-gradient(linear, left top, left bottom, from(#368EE0), to(#368EE0));
 background-image: -webkit-linear-gradient(top, #368EE0, #368EE0);
 background-image: -moz-linear-gradient(top, #368EE0, #368EE0);
 background-image: -ms-linear-gradient(top, #368EE0, #368EE0);
 background-image: -o-linear-gradient(top, #368EE0, #368EE0);
 background-image: linear-gradient(to bottom, #368EE0, #368EE0);filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#368EE0, endColorstr=#368EE0);
 margin:2px 0;
}

.buttonStyle:hover {
   border:1px solid #185a98;
 background-color: #1e74c5; background-image: -webkit-gradient(linear, left top, left bottom, from(#1e74c5), to(#1e74c5));
 background-image: -webkit-linear-gradient(top, #1e74c5, #1e74c5);
 background-image: -moz-linear-gradient(top, #1e74c5, #1e74c5);
 background-image: -ms-linear-gradient(top, #1e74c5, #1e74c5);
 background-image: -o-linear-gradient(top, #1e74c5, #1e74c5);
 background-image: linear-gradient(to bottom, #1e74c5, #1e74c5);filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#1e74c5, endColorstr=#1e74c5);}
    </style>
</head>
<body dir="<%=Resources.Lang.dir %>" style="font-family:'<%=Resources.Lang.font %>'">
    <form id="form1" runat="server">
        <div>

            <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
            </telerik:RadScriptManager>

            <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                <AjaxSettings>
                    <telerik:AjaxSetting AjaxControlID="RadGrid_Items">
                        <UpdatedControls>
                            <telerik:AjaxUpdatedControl ControlID="RadGrid_Items" LoadingPanelID="RadAjaxLoadingPanel1" />
                        </UpdatedControls>
                    </telerik:AjaxSetting>
                </AjaxSettings>
            </telerik:RadAjaxManager>
            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                <ContentTemplate>
                    <asp:Timer ID="Timer1" runat="server" Interval="6000" Enabled="false" OnTick="Timer1_Tick"></asp:Timer>
                    <asp:Label ID="lbl_msg" runat="server" CssClass="msg" Text="" Visible="false" />
                </ContentTemplate>
            </asp:UpdatePanel>
            <telerik:RadGrid ID="RadGrid_Items" runat="server" AllowFilteringByColumn="True" AllowPaging="True" AllowSorting="True" GridLines="None" OnInsertCommand="RadGrid_Items_InsertCommand" Skin="Metro" DataSourceID="ObjectDataSource1" OnUpdateCommand="RadGrid_Items_UpdateCommand" AutoGenerateColumns="False" CellSpacing="0" OnDeleteCommand="RadGrid_Items_DeleteCommand" Width="100%">
                <MasterTableView CommandItemSettings-ShowRefreshButton="false" OverrideDataSourceControlSorting="true" CommandItemDisplay="Top" DataKeyNames="TaxItemTypeId" DataSourceID="ObjectDataSource1" ShowFooter="True" Dir="<%$Resources:Lang, grid_direction %>">
                    <CommandItemSettings AddNewRecordText="<%$Resources:Lang, add %>"/>
                    <Columns>

                        <telerik:GridEditCommandColumn ButtonType="ImageButton">
                            <HeaderStyle Width="20px" />
                            <ItemStyle CssClass="MyImageButton" />
                        </telerik:GridEditCommandColumn>

                        <telerik:GridBoundColumn Visible="False" DataField="TaxItemTypeId" DataType="System.Int32" HeaderText="TaxItemTypeId" ReadOnly="True" SortExpression="TaxItemTypeId" UniqueName="TaxItemTypeId">
                        </telerik:GridBoundColumn>

                        <telerik:GridTemplateColumn DataField="TaxItemTypeName" HeaderText="<%$Resources:Lang, name %>" SortExpression="TaxItemTypeName" UniqueName="TaxItemTypeName">
                            <ItemTemplate>
                                <asp:Label ID="lbl_TaxItemTypeName" runat="server" Text='<%# Eval("TaxItemTypeName") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txt_name" runat="server" Text='<%# Eval("TaxItemTypeName") %>'></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_name" ErrorMessage="*" ForeColor="Red" />
                            </EditItemTemplate>
                        </telerik:GridTemplateColumn>

                        <telerik:GridButtonColumn ConfirmText="<%$Resources:Lang, confirm_delete %>" ButtonType="ImageButton"
                            CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                            <HeaderStyle Width="20px" />
                            <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                        </telerik:GridButtonColumn>

                    </Columns>

                    <EditFormSettings>
                        <EditColumn ButtonType="ImageButton"
                            InsertText="Insert Order" UpdateText="Update record" UniqueName="EditCommandColumn1" CancelText="Cancel edit">
                        </EditColumn>
                    </EditFormSettings>
                </MasterTableView>
            </telerik:RadGrid>
            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetAllTaxItemTypes" TypeName="BL.Setup.cls_Tax"></asp:ObjectDataSource>

            <div style="text-align:center"> <asp:Button ID="Button1" CssClass="buttonStyle" runat="server" Text="<%$Resources:Lang, Close %>" OnClick="Button1_Click" /></div>
           
        </div>
    </form>
</body>
</html>
