﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master.Master" AutoEventWireup="true" CodeBehind="general_setting.aspx.cs" Inherits="InvyLap.Configuration.general_setting" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


    <script type="text/javascript">
        $(document).ready(function () {
            var checkPassword = document.getElementById("<%= cb_passwordAsk.ClientID %>");
            ToggleValidator(checkPassword);

            var checkSales = document.getElementById("<%= cb_enableSalesTax.ClientID %>");
            ToggleSalesRBL(checkSales);
            alert(checkSales.checked);

            var checkIncome = document.getElementById("<%= cb_enableIncomeTax.ClientID %>");
            ToggleIncomeRBL(checkIncome);

            var txt1 = document.getElementById("<%=txt_inventoryAccount.ClientID%>");
            var txt2 = document.getElementById("<%=txt_costOfGoodSold.ClientID%>");
            var txt3 = document.getElementById("<%=txt_incomeAccount.ClientID%>");
            var txt4 = document.getElementById("<%=txt_vendorAccount.ClientID%>");
            var txt5 = document.getElementById("<%=txt_customerAccount.ClientID%>");


            $(txt1).keyup(function () {
                var comboBox = $find("<%= rcb_inventoryAccount.ClientID %>");

                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: '<%=ResolveUrl("~/cntries.asmx/GetAccount") %>',
                    data: '{"accountCode": "' + this.value + '"}',
                    processData: false,
                    dataType: "json",
                    success: function (response) {
                        var tree = $find('<%= rcb_inventoryAccount.Items[0].FindControl("rtv_inventoryAccount").ClientID %>');

                        var nodes = tree.get_allNodes();
                        for (var i = 0; i < nodes.length; i++) {
                            if (nodes[i].get_nodes() != null) {
                                nodes[i].collapse();
                            }
                        }

                        if (response.d == "0")
                        {
                            comboBox.clearSelection();
                        } else
                        {

                            var node = tree.findNodeByValue(response.d);

                            if (node.get_nodes().get_count() > 0) {
                                comboBox.set_value("<%= Resources.Lang.choose_account %>");
                                comboBox.set_text("");
                            } else {
                                node.expand();
                                node.set_expanded(true);
                                node.select();

                                comboBox.set_value(response.d);
                                comboBox.set_text(node.get_text());

                                comboBox.trackChanges();
                                comboBox.get_items().getItem(0).set_text(node.get_text());
                                comboBox.get_items().getItem(0).set_value(response.d);
                                comboBox.commitChanges();

                                comboBox.hideDropDown();

                                node2 = node.get_parent();

                                while (node2 != null) {
                                    if (node2.expand) {
                                        node2.expand();
                                    }

                                    node2 = node2.get_parent();
                                }
                            }
                        }
                    },
                    error: function (a, b, c) {
                        comboBox.set_value("<%= Resources.Lang.choose_account %>");
                        comboBox.set_text("");
                    }
                });
            });

            $(txt2).keyup(function () {
                var comboBox = $find("<%= rcb_costOfGoodSold.ClientID %>");

                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: '<%=ResolveUrl("~/cntries.asmx/GetAccount") %>',
                    data: '{"accountCode": "' + this.value + '"}',
                    processData: false,
                    dataType: "json",
                    success: function (response) {
                        var tree = $find('<%= rcb_costOfGoodSold.Items[0].FindControl("rtv_costOfGoodSold").ClientID %>');
                        var nodes = tree.get_allNodes();
                        for (var i = 0; i < nodes.length; i++) {
                            if (nodes[i].get_nodes() != null) {
                                nodes[i].collapse();
                            }
                        }

                        if (response.d == "0") {
                            comboBox.clearSelection();
                        } else {
                            var node = tree.findNodeByValue(response.d);
                            if (node.get_nodes().get_count() > 0) {
                                comboBox.set_value("<%= Resources.Lang.choose_account %>");
                                comboBox.set_text("");
                            } else {
                                node.expand();
                                node.set_expanded(true);
                                node.select();

                                comboBox.set_value(response.d);
                                comboBox.set_text(node.get_text());

                                comboBox.trackChanges();
                                comboBox.get_items().getItem(0).set_text(node.get_text());
                                comboBox.get_items().getItem(0).set_value(response.d);
                                comboBox.commitChanges();

                                node2 = node.get_parent();

                                while (node2 != null) {
                                    if (node2.expand) {
                                        node2.expand();
                                    }

                                    node2 = node2.get_parent();
                                }
                            }
                        }
                    },
                    error: function (a, b, c) {
                        comboBox.set_value("<%= Resources.Lang.choose_account %>");
                        comboBox.set_text("");
                    }
                });
            });

            $(txt3).keyup(function () {
                var comboBox = $find("<%= rcb_incomeAccount.ClientID %>");

                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: '<%=ResolveUrl("~/cntries.asmx/GetAccount") %>',
                    data: '{"accountCode": "' + this.value + '"}',
                    processData: false,
                    dataType: "json",
                    success: function (response) {
                        var tree = $find('<%= rcb_incomeAccount.Items[0].FindControl("rtv_incomeAccount").ClientID %>');
                        var nodes = tree.get_allNodes();
                        for (var i = 0; i < nodes.length; i++) {
                            if (nodes[i].get_nodes() != null) {
                                nodes[i].collapse();
                            }
                        }
                        if (response.d == "0") {
                            comboBox.clearSelection();
                        } else {
                            var node = tree.findNodeByValue(response.d);
                            if (node.get_nodes().get_count() > 0) {
                                comboBox.set_value("<%= Resources.Lang.choose_account %>");
                                comboBox.set_text("");
                            } else {
                                node.expand();
                                node.set_expanded(true);
                                node.select();

                                comboBox.set_value(response.d);
                                comboBox.set_text(node.get_text());

                                comboBox.trackChanges();
                                comboBox.get_items().getItem(0).set_text(node.get_text());
                                comboBox.get_items().getItem(0).set_value(response.d);
                                comboBox.commitChanges();

                                node2 = node.get_parent();

                                while (node2 != null) {
                                    if (node2.expand) {
                                        node2.expand();
                                    }

                                    node2 = node2.get_parent();
                                }
                            }
                        }
                    },
                    error: function (a, b, c) {
                        comboBox.set_value("<%= Resources.Lang.choose_account %>");
                        comboBox.set_text("");
                    }
                });
            });

            $(txt4).keyup(function () {
                var comboBox = $find("<%= rcb_vendorAccount.ClientID %>");

                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: '<%=ResolveUrl("~/cntries.asmx/GetAccount") %>',
                    data: '{"accountCode": "' + this.value + '"}',
                    processData: false,
                    dataType: "json",
                    success: function (response) {
                        var tree = $find('<%= rcb_vendorAccount.Items[0].FindControl("rtv_vendorAccount").ClientID %>');
                        var nodes = tree.get_allNodes();
                        for (var i = 0; i < nodes.length; i++) {
                            if (nodes[i].get_nodes() != null) {
                                nodes[i].collapse();
                            }
                        }
                        if (response.d == "0") {
                            comboBox.clearSelection();
                        } else {
                            var node = tree.findNodeByValue(response.d);
                            if (node.get_nodes().get_count() > 0) {
                                comboBox.set_value("<%= Resources.Lang.choose_account %>");
                                comboBox.set_text("");
                            } else {
                                node.expand();
                                node.set_expanded(true);
                                node.select();

                                comboBox.set_value(response.d);
                                comboBox.set_text(node.get_text());

                                comboBox.trackChanges();
                                comboBox.get_items().getItem(0).set_text(node.get_text());
                                comboBox.get_items().getItem(0).set_value(response.d);
                                comboBox.commitChanges();

                                node2 = node.get_parent();

                                while (node2 != null) {
                                    if (node2.expand) {
                                        node2.expand();
                                    }

                                    node2 = node2.get_parent();
                                }
                            }
                        }
                    },
                    error: function (a, b, c) {
                        comboBox.set_value("<%= Resources.Lang.choose_account %>");
                        comboBox.set_text("");
                    }
                });
            });

            $(txt5).keyup(function () {
                var comboBox = $find("<%= rcb_customerAccount.ClientID %>");

                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: '<%=ResolveUrl("~/cntries.asmx/GetAccount") %>',
                    data: '{"accountCode": "' + this.value + '"}',
                    processData: false,
                    dataType: "json",
                    success: function (response) {
                        var tree = $find('<%= rcb_customerAccount.Items[0].FindControl("rtv_customerAccount").ClientID %>');
                        var nodes = tree.get_allNodes();
                        for (var i = 0; i < nodes.length; i++) {
                            if (nodes[i].get_nodes() != null) {
                                nodes[i].collapse();
                            }
                        }
                        if (response.d == "0") {
                            comboBox.clearSelection();
                        } else {
                            var node = tree.findNodeByValue(response.d);
                            if (node.get_nodes().get_count() > 0) {
                                comboBox.set_value("<%= Resources.Lang.choose_account %>");
                                comboBox.set_text("");
                            } else {
                                node.expand();
                                node.set_expanded(true);
                                node.select();

                                comboBox.set_value(response.d);
                                comboBox.set_text(node.get_text());

                                comboBox.trackChanges();
                                comboBox.get_items().getItem(0).set_text(node.get_text());
                                comboBox.get_items().getItem(0).set_value(response.d);
                                comboBox.commitChanges();

                                node2 = node.get_parent();

                                while (node2 != null) {
                                    if (node2.expand) {
                                        node2.expand();
                                    }

                                    node2 = node2.get_parent();
                                }
                            }
                        }
                    },
                    error: function (a, b, c) {
                        comboBox.set_value("<%= Resources.Lang.choose_account %>");
                        comboBox.set_text("");
                    }
                });
            });


        });

            function ClientMouseOver(sender, eventArgs) {
                //var node = eventArgs.get_node();

                //if (node.get_nodes().get_count() > 0) {
                //    node.set_cssClass("ChangeStyle");
                //}
            }

            function nodeClicking(sender, args) {


                var comboBox = $find("<%= rcb_inventoryAccount.ClientID %>");

                var node = args.get_node()

                if (node.get_nodes().get_count() > 0) {
                    comboBox.set_value("<%= Resources.Lang.choose_account %>");
                    comboBox.set_text("");

                    var txt1 = document.getElementById("<%=txt_inventoryAccount.ClientID%>");
                    txt1.value = "";
                } else {
                    comboBox.set_text(node.get_text());
                    comboBox.set_value(node.get_value());

                    comboBox.trackChanges();
                    comboBox.get_items().getItem(0).set_text(node.get_text());
                    comboBox.get_items().getItem(0).set_value(node.get_value());
                    comboBox.commitChanges();

                    comboBox.hideDropDown();
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: '<%=ResolveUrl("~/cntries.asmx/GetAccountCode") %>',
                        data: '{"accountID": "' + node.get_value() + '"}',
                        processData: false,
                        dataType: "json",
                        success: function (response) {
                            //alert(response.d);
                            var txt1 = document.getElementById("<%=txt_inventoryAccount.ClientID%>");
                            txt1.value = response.d;
                        }
                    });
                }
                }


                function nodeClicking2(sender, args) {
                    var comboBox = $find("<%= rcb_costOfGoodSold.ClientID %>");

                    var node = args.get_node()

                    if (node.get_nodes().get_count() > 0) {
                        comboBox.set_value("<%= Resources.Lang.choose_account %>");
                        comboBox.set_text("");

                        var txt1 = document.getElementById("<%=txt_costOfGoodSold.ClientID%>");
                        txt1.value = "";
                    } else {
                        comboBox.set_text(node.get_text());
                        comboBox.set_value(node.get_value());

                        comboBox.trackChanges();
                        comboBox.get_items().getItem(0).set_text(node.get_text());
                        comboBox.get_items().getItem(0).set_value(node.get_value());
                        comboBox.commitChanges();

                        comboBox.hideDropDown();

                        $.ajax({
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            url: '<%=ResolveUrl("~/cntries.asmx/GetAccountCode") %>',
                            data: '{"accountID": "' + node.get_value() + '"}',
                            processData: false,
                            dataType: "json",
                            success: function (response) {
                                //alert(response.d);
                                var txt1 = document.getElementById("<%=txt_costOfGoodSold.ClientID%>");
                                txt1.value = response.d;
                            }
                        });
                        }
                    }

                    function nodeClicking3(sender, args) {
                        var comboBox = $find("<%= rcb_incomeAccount.ClientID %>");

                        var node = args.get_node()

                        if (node.get_nodes().get_count() > 0) {
                            comboBox.set_value("<%= Resources.Lang.choose_account %>");
                            comboBox.set_text("");

                            var txt1 = document.getElementById("<%=txt_incomeAccount.ClientID%>");
                    txt1.value = "";
                } else {
                    comboBox.set_text(node.get_text());
                    comboBox.set_value(node.get_value());

                    comboBox.trackChanges();
                    comboBox.get_items().getItem(0).set_text(node.get_text());
                    comboBox.get_items().getItem(0).set_value(node.get_value());
                    comboBox.commitChanges();

                    comboBox.hideDropDown();

                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: '<%=ResolveUrl("~/cntries.asmx/GetAccountCode") %>',
                        data: '{"accountID": "' + node.get_value() + '"}',
                        processData: false,
                        dataType: "json",
                        success: function (response) {
                            //alert(response.d);
                            var txt1 = document.getElementById("<%=txt_incomeAccount.ClientID%>");
                            txt1.value = response.d;
                        }
                    });
                    }
                }

                function nodeClicking4(sender, args) {
                    var comboBox = $find("<%= rcb_vendorAccount.ClientID %>");

                    var node = args.get_node()

                    if (node.get_nodes().get_count() > 0) {
                        comboBox.set_value("<%= Resources.Lang.choose_account %>");
                        comboBox.set_text("");

                        var txt1 = document.getElementById("<%=txt_vendorAccount.ClientID%>");
                    txt1.value = "";
                } else {
                    comboBox.set_text(node.get_text());
                    comboBox.set_value(node.get_value());

                    comboBox.trackChanges();
                    comboBox.get_items().getItem(0).set_text(node.get_text());
                    comboBox.get_items().getItem(0).set_value(node.get_value());
                    comboBox.commitChanges();

                    comboBox.hideDropDown();

                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: '<%=ResolveUrl("~/cntries.asmx/GetAccountCode") %>',
                        data: '{"accountID": "' + node.get_value() + '"}',
                        processData: false,
                        dataType: "json",
                        success: function (response) {
                            //alert(response.d);
                            var txt1 = document.getElementById("<%=txt_vendorAccount.ClientID%>");
                            txt1.value = response.d;
                        }
                    });
                    }
                }

                function nodeClicking5(sender, args) {
                    var comboBox = $find("<%= rcb_customerAccount.ClientID %>");

                    var node = args.get_node()

                    if (node.get_nodes().get_count() > 0) {
                        comboBox.set_value("<%= Resources.Lang.choose_account %>");
                        comboBox.set_text("");

                        var txt1 = document.getElementById("<%=txt_customerAccount.ClientID%>");
                    txt1.value = "";
                } else {
                    comboBox.set_text(node.get_text());
                    comboBox.set_value(node.get_value());

                    comboBox.trackChanges();
                    comboBox.get_items().getItem(0).set_text(node.get_text());
                    comboBox.get_items().getItem(0).set_value(node.get_value());
                    comboBox.commitChanges();

                    comboBox.hideDropDown();

                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: '<%=ResolveUrl("~/cntries.asmx/GetAccountCode") %>',
                        data: '{"accountID": "' + node.get_value() + '"}',
                        processData: false,
                        dataType: "json",
                        success: function (response) {
                            //alert(response.d);
                            var txt1 = document.getElementById("<%=txt_customerAccount.ClientID%>");
                            txt1.value = response.d;
                        }
                    });
                    }
                }




                function ToggleValidator(chk) {
                    var val1 = document.getElementById("<%=RequiredFieldValidator1.ClientID%>");
                    var val2 = document.getElementById("<%=RequiredFieldValidator2.ClientID%>");
                    var val3 = document.getElementById("<%=CompareValidator1.ClientID%>");
                    var txt1 = document.getElementById("<%=txt_password.ClientID%>");
                    var txt2 = document.getElementById("<%=txt_confirmPassword.ClientID%>");

                    if (chk.checked) {
                        txt1.disabled = false;
                        txt2.disabled = false;
                    } else {
                        txt1.value = "";
                        txt2.value = "";

                        txt1.disabled = true;
                        txt2.disabled = true;
                    }

                    ValidatorEnable(val1, chk.checked);
                    ValidatorEnable(val2, chk.checked);
                    ValidatorEnable(val3, chk.checked);
                }

                function ToggleIncomeRBL(chk) {
                    var val1 = document.getElementById("<%=rbl_incomeTaxBasedOn.ClientID%>");
                    var val2 = document.getElementById("<%=rbl_incomeTaxPaid.ClientID%>");

                    radio1 = val1.getElementsByTagName("input");
                    radio2 = val2.getElementsByTagName("input");

                    if (chk.checked) {
                        for (var i = 0; i < radio1.length; i++) {
                            radio1[i].disabled = false;
                        }

                        for (var i = 0; i < radio2.length; i++) {
                            radio2[i].disabled = false;
                        }
                    } else {
                        for (var i = 0; i < radio1.length; i++) {
                            radio1[i].disabled = true;
                        }

                        for (var i = 0; i < radio2.length; i++) {
                            radio2[i].disabled = true;
                        }
                    }
                }

        function ToggleSalesRBL(chk) {
            var val1 = document.getElementById("<%=rbl_salesTaxBasedOn.ClientID%>");
            var val2 = document.getElementById("<%=rbl_salesTaxPaid.ClientID%>");

            radio1 = val1.getElementsByTagName("input");
            radio2 = val2.getElementsByTagName("input");

            if (chk.checked) {
                for (var i = 0; i < radio1.length; i++) {
                    radio1[i].disabled = false;
                }

                for (var i = 0; i < radio2.length; i++) {
                    radio2[i].disabled = false;
                }
            } else {
                for (var i = 0; i < radio1.length; i++) {
                    radio1[i].disabled = true;
                }

                for (var i = 0; i < radio2.length; i++) {
                    radio2[i].disabled = true;
                }
            }
        }


            //$(function () {
            //    $('#notification').miniNotification();
            //});

            function pageLoad() {
                //getCheckedSalesTaxSystem();
            }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <%--<telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ddl_type" />
                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="RadGrid_Items">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid_Items" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>--%>
      <div class="box" >
    <div class="box-title">
        <h3>
            <i class="icon-reorder"></i>
            <asp:Label ID="Label31" runat="server" Text="<%$Resources:Lang, general_setting %>"></asp:Label>
        </h3>
    </div>
             </div>
    <div class="row-fluid">  
        <telerik:RadTabStrip ID="RadTabStrip1" runat="server" MultiPageID="RadMultiPage1"
            SelectedIndex="0" Align="<%$Resources:Lang, align %>" Skin="Metro">
            <Tabs>
                <telerik:RadTab Text="<%$Resources:Lang, inventory_system_setting %>" Selected="True">
                </telerik:RadTab>
                <telerik:RadTab Text="<%$Resources:Lang, account_setting %>">
                </telerik:RadTab>
                <telerik:RadTab Text="<%$Resources:Lang, tax_setting %>">
               
                </telerik:RadTab> <telerik:RadTab Text="<%$Resources:Lang, POS_Settings %>">
                </telerik:RadTab>
                   
            </Tabs>
        </telerik:RadTabStrip>
        <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0" CssClass="telerikTab">
            <telerik:RadPageView ID="RadPageView1" runat="server">
                <asp:Panel ID="Panel1" runat="server" GroupingText="<%$Resources:Lang, inventory_setting %>">
                    <table cellpadding="5">
                        <tr>
                            <td valign="top">
                                <asp:Label ID="Label2" runat="server" Text="<%$Resources:Lang, weighted_average_calc %>"></asp:Label>:
                            </td>
                            <td>
                                <asp:RadioButtonList ID="rbl_weightedAverage" runat="server">
                                    <asp:ListItem Text="<%$Resources:Lang, calculate_on_all_stores %>" Selected="True" Value="False"></asp:ListItem>
                                    <asp:ListItem Text="<%$Resources:Lang, calculate_on_each_store %>" Value="True"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <asp:Label ID="Label3" runat="server" Text="<%$Resources:Lang, item_allocation %>"></asp:Label>:
                            </td>
                            <td>
                                <asp:RadioButtonList ID="rbl_itemAllocation" runat="server">
                                    <asp:ListItem Text="<%$Resources:Lang, allocate_by_store %>" Selected="True" Value="False"></asp:ListItem>
                                    <asp:ListItem Text="<%$Resources:Lang, allocate_by_store_and_location %>" Value="True"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:CheckBoxList ID="cbl_" runat="server">
                                    <asp:ListItem Text="<%$Resources:Lang, allow_negative_stock %>"></asp:ListItem>
                                    <asp:ListItem Text="<%$Resources:Lang, use_reorder_point_in_purchasing_items %>"></asp:ListItem>
                                    <asp:ListItem Text="<%$Resources:Lang, use_of_gross_quantity %>"></asp:ListItem>
                                    <asp:ListItem Text="<%$Resources:Lang, enable_of_record_general_entries_with_zero_values %>"></asp:ListItem>
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                    </table>

                </asp:Panel>
                <asp:Panel ID="Panel2" runat="server" GroupingText="<%$Resources:Lang, print_barcode %>">
                    <table cellpadding="5">
                        <tr>
                            <td></td>
                            <td>
                                <asp:CheckBox ID="cb_passwordAsk" runat="server" Text="<%$Resources:Lang, ask_for_password_when_reprint_barcode %>" onclick="ToggleValidator(this);" /></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label4" runat="server" Text="<%$Resources:Lang, password %>"></asp:Label>:</td>
                            <td>
                                <asp:TextBox ID="txt_password" runat="server" TextMode="Password" Width="205" Enabled="false"></asp:TextBox>
                                <asp:RequiredFieldValidator Enabled="false" ForeColor="Red" ValidationGroup="save" ID="RequiredFieldValidator1" runat="server" Text="*" ErrorMessage="<%$Resources:Lang, please_insert_barcode_password %>" Display="Dynamic" ControlToValidate="txt_password" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label5" runat="server" Text="<%$Resources:Lang, confirm_password %>"></asp:Label>:</td>
                            <td>
                                <asp:TextBox ID="txt_confirmPassword" runat="server" TextMode="Password" Width="205" Enabled="false"></asp:TextBox>
                                <asp:RequiredFieldValidator Enabled="false" ForeColor="Red" ValidationGroup="save" ID="RequiredFieldValidator2" runat="server" Text="*" ErrorMessage="<%$Resources:Lang, please_insert_barcode_password_confirmation %>" Display="Dynamic" ControlToValidate="txt_confirmPassword" />
                                <asp:CompareValidator ValidationGroup="save" ID="CompareValidator1" runat="server" ErrorMessage="<%$Resources:Lang, not_match %>" Text="<%$Resources:Lang, not_match %>" Enabled="false" ForeColor="Red" Display="Dynamic" ControlToValidate="txt_confirmPassword" ControlToCompare="txt_password" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageView2" runat="server">
                <asp:Panel ID="Panel3" runat="server" GroupingText="<%$Resources:Lang, close_date_and_accounting_period %>">
                    <table cellpadding="5">
                        <tr>
                            <td>
                                <asp:Label ID="Label6" runat="server" Text="<%$Resources:Lang, next_repeated_close_date %>"></asp:Label>:
                            </td>
                            <td>
                                <telerik:RadDatePicker ID="txt_repeatedCloseDate" runat="server"></telerik:RadDatePicker>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*" Text="*" ControlToValidate="txt_repeatedCloseDate"
                                    ForeColor="Red" Display="Dynamic" ValidationGroup="save" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label7" runat="server" Text="<%$Resources:Lang, close_every %>"></asp:Label>:
                            </td>
                            <td>
                                <telerik:RadNumericTextBox
                                    Min="0"
                                    Max="999999999"
                                    ID="numeric_closeEvery"
                                    runat="server"
                                    ShowSpinButtons="true"
                                    IncrementSettings-InterceptArrowKeys="true"
                                    IncrementSettings-InterceptMouseWheel="true"
                                    Value="0">
                                    <NumberFormat GroupSeparator="" DecimalDigits="0" AllowRounding="true" KeepNotRoundedValue="false" />
                                </telerik:RadNumericTextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator88" runat="server" ErrorMessage="<%$Resources:Lang, please_select_next_repeated_close_date %>" Text="*" ControlToValidate="numeric_closeEvery"
                                    ForeColor="Red" Display="Dynamic" ValidationGroup="save" />
                                <asp:DropDownList AppendDataBoundItems="true" ID="ddl_units" runat="server" DataSourceID="ObjectDataSource3" DataTextField="Value" DataValueField="Key">
                                    <asp:ListItem Value="" Text="<%$Resources:Lang, select_unit %>"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator Display="Dynamic" ValidationGroup="save" ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddl_units" ErrorMessage="<%$Resources:Lang, please_select_time_unit %>" Text="*" ForeColor="Red" />
                                <asp:ObjectDataSource ID="ObjectDataSource3" runat="server" SelectMethod="GetTimeUnits" TypeName="BL.Static.cls_StaticData">
                                    <SelectParameters>
                                        <asp:SessionParameter DefaultValue="E" Name="selectedLanguage" SessionField="lang" Type="String" />
                                    </SelectParameters>
                                </asp:ObjectDataSource>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label8" runat="server" Text="<%$Resources:Lang, yearly_close_date %>"></asp:Label>:
                            </td>
                            <td>
                                <telerik:RadDatePicker ID="txt_yearlyCloseDate" DateInput-DateFormat="MM/dd" runat="server"></telerik:RadDatePicker>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="<%$Resources:Lang, please_select_yearly_close_date %>" ControlToValidate="txt_yearlyCloseDate"
                                    ForeColor="Red" Display="Dynamic" ValidationGroup="save" Text="*" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label9" runat="server" Text="<%$Resources:Lang, password %>"></asp:Label>:</td>
                            <td>
                                <asp:TextBox ID="txt_password2" runat="server" TextMode="Password" Width="205"></asp:TextBox>
                                <asp:RequiredFieldValidator ForeColor="Red" Text="*" ValidationGroup="save" ID="RequiredFieldValidator4" runat="server" ErrorMessage="<%$Resources:Lang, please_insert_password %>" Display="Dynamic" ControlToValidate="txt_password2" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label10" runat="server" Text="<%$Resources:Lang, confirm_password %>"></asp:Label>:</td>
                            <td>
                                <asp:TextBox ID="txt_confirmPassword2" runat="server" TextMode="Password" Width="205"></asp:TextBox>
                                <asp:RequiredFieldValidator ForeColor="Red" Text="*" ValidationGroup="save" ID="RequiredFieldValidator5" runat="server" ErrorMessage="<%$Resources:Lang, please_insert_password_confirmation %>" Display="Dynamic" ControlToValidate="txt_confirmPassword2" />
                                <asp:CompareValidator ValidationGroup="save" ID="CompareValidator2" runat="server" ErrorMessage="<%$Resources:Lang, not_match %>" Text="<%$Resources:Lang, not_match %>" ForeColor="Red" Display="Dynamic" ControlToValidate="txt_confirmPassword2" ControlToCompare="txt_password2" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="Panel4" runat="server" GroupingText="<%$Resources:Lang, default_accounts_for_inventory_item %>">
                    <table cellpadding="5">
                        <tr>
                            <td>
                                <asp:Label ID="Label11" runat="server" Text="<%$Resources:Lang, inventory_account %>"></asp:Label>:
                            </td>
                            <td>
                                <telerik:RadComboBox ID="rcb_inventoryAccount" runat="server" Width="250px" Style="vertical-align: middle;"
                                    EmptyMessage="<%$ Resources:Lang, choose_account %>" ExpandAnimation-Type="None" CollapseAnimation-Type="None" Skin="Metro">
                                    <ItemTemplate>
                                        <telerik:RadTreeView runat="server" ID="rtv_inventoryAccount" Width="100%" Height="200px" OnClientNodeClicking="nodeClicking" OnClientMouseOver="ClientMouseOver" DataFieldID="ID" DataFieldParentID="ParentID" DataTextField="Text" DataValueField="ID" Skin="Metro">
                                        </telerik:RadTreeView>
                                    </ItemTemplate>
                                    <Items>
                                        <telerik:RadComboBoxItem Text="" />
                                    </Items>
                                    <ExpandAnimation Type="None"></ExpandAnimation>
                                    <CollapseAnimation Type="None"></CollapseAnimation>
                                </telerik:RadComboBox>
                                <asp:RequiredFieldValidator ForeColor="Red" Text="*" ValidationGroup="save" ID="RequiredFieldValidator8" runat="server" ErrorMessage="<%$Resources:Lang, please_select_inventory_account %>" Display="Dynamic" ControlToValidate="rcb_inventoryAccount" />
                            </td>
                            <td>
                                <asp:TextBox ID="txt_inventoryAccount" runat="server"></asp:TextBox>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <asp:Label ID="Label12" runat="server" Text="<%$Resources:Lang, cost_of_good_sold %>"></asp:Label>:
                            </td>
                            <td>
                                <telerik:RadComboBox ID="rcb_costOfGoodSold" runat="server" Width="250px" Style="vertical-align: middle;"
                                    EmptyMessage="<%$ Resources:Lang, choose_account %>" ExpandAnimation-Type="None" CollapseAnimation-Type="None" Skin="Metro">
                                    <ItemTemplate>
                                        <telerik:RadTreeView runat="server" ID="rtv_costOfGoodSold" Width="100%" Height="200px" OnClientNodeClicking="nodeClicking2" DataFieldID="ID" DataFieldParentID="ParentID" DataTextField="Text" DataValueField="ID" Skin="Metro">
                                        </telerik:RadTreeView>
                                    </ItemTemplate>
                                    <Items>
                                        <telerik:RadComboBoxItem Text="" />
                                    </Items>
                                    <ExpandAnimation Type="None"></ExpandAnimation>
                                    <CollapseAnimation Type="None"></CollapseAnimation>
                                </telerik:RadComboBox>
                                <asp:RequiredFieldValidator ForeColor="Red" Text="*" ValidationGroup="save" ID="RequiredFieldValidator9" runat="server" ErrorMessage="<%$Resources:Lang, please_select_costofgoodsold %>" Display="Dynamic" ControlToValidate="rcb_costOfGoodSold" />
                            </td>
                            <td>
                                <asp:TextBox ID="txt_costOfGoodSold" runat="server"></asp:TextBox>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <asp:Label ID="Label13" runat="server" Text="<%$Resources:Lang, income_account %>"></asp:Label>:
                            </td>
                            <td>
                                <telerik:RadComboBox ID="rcb_incomeAccount" runat="server" Width="250px" Style="vertical-align: middle;"
                                    EmptyMessage="<%$ Resources:Lang, choose_account %>" ExpandAnimation-Type="None" CollapseAnimation-Type="None" Skin="Metro">
                                    <ItemTemplate>
                                        <telerik:RadTreeView runat="server" ID="rtv_incomeAccount" Width="100%" Height="200px" OnClientNodeClicking="nodeClicking3" DataFieldID="ID" DataFieldParentID="ParentID" DataTextField="Text" DataValueField="ID" Skin="Metro">
                                        </telerik:RadTreeView>
                                    </ItemTemplate>
                                    <Items>
                                        <telerik:RadComboBoxItem Text="" />
                                    </Items>
                                    <ExpandAnimation Type="None"></ExpandAnimation>
                                    <CollapseAnimation Type="None"></CollapseAnimation>
                                </telerik:RadComboBox>
                                <asp:RequiredFieldValidator ForeColor="Red" Text="*" ValidationGroup="save" ID="RequiredFieldValidator10" runat="server" ErrorMessage="<%$Resources:Lang, please_select_income_account %>" Display="Dynamic" ControlToValidate="rcb_incomeAccount" />
                            </td>
                            <td>
                                <asp:TextBox ID="txt_incomeAccount" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="Panel5" runat="server" GroupingText="<%$Resources:Lang, default_vendor_customer_account %>">
                    <table cellpadding="5">
                        <tr>
                            <td>
                                <asp:Label ID="Label14" runat="server" Text="<%$Resources:Lang, vendor_account %>"></asp:Label>:
                            </td>
                            <td>
                                <telerik:RadComboBox ID="rcb_vendorAccount" runat="server" Width="250px" Style="vertical-align: middle;"
                                    EmptyMessage="<%$ Resources:Lang, choose_account %>" ExpandAnimation-Type="None" CollapseAnimation-Type="None" Skin="Metro">
                                    <ItemTemplate>
                                        <telerik:RadTreeView runat="server" ID="rtv_vendorAccount" Width="100%" Height="200px" OnClientNodeClicking="nodeClicking4" DataFieldID="ID" DataFieldParentID="ParentID" DataTextField="Text" DataValueField="ID" Skin="Metro">
                                        </telerik:RadTreeView>
                                    </ItemTemplate>
                                    <Items>
                                        <telerik:RadComboBoxItem Text="" />
                                    </Items>
                                    <ExpandAnimation Type="None"></ExpandAnimation>
                                    <CollapseAnimation Type="None"></CollapseAnimation>
                                </telerik:RadComboBox>
                                <asp:RequiredFieldValidator ForeColor="Red" Text="*" ValidationGroup="save" ID="RequiredFieldValidator11" runat="server" ErrorMessage="<%$Resources:Lang, please_select_vendor_account %>" Display="Dynamic" ControlToValidate="rcb_vendorAccount" />
                            </td>
                            <td>
                                <asp:TextBox ID="txt_vendorAccount" runat="server"></asp:TextBox>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <asp:Label ID="Label15" runat="server" Text="<%$Resources:Lang, customer_account %>"></asp:Label>:
                            </td>
                            <td>
                                <telerik:RadComboBox ID="rcb_customerAccount" runat="server" Width="250px" Style="vertical-align: middle;"
                                    EmptyMessage="<%$ Resources:Lang, choose_account %>" ExpandAnimation-Type="None" CollapseAnimation-Type="None" Skin="Metro">
                                    <ItemTemplate>
                                        <telerik:RadTreeView runat="server" ID="rtv_customerAccount" Width="100%" Height="200px" OnClientNodeClicking="nodeClicking5" DataFieldID="ID" DataFieldParentID="ParentID" DataTextField="Text" DataValueField="ID" Skin="Metro">
                                        </telerik:RadTreeView>
                                    </ItemTemplate>
                                    <Items>
                                        <telerik:RadComboBoxItem Text="" />
                                    </Items>
                                    <ExpandAnimation Type="None"></ExpandAnimation>
                                    <CollapseAnimation Type="None"></CollapseAnimation>
                                </telerik:RadComboBox>
                                <asp:RequiredFieldValidator ForeColor="Red" Text="*" ValidationGroup="save" ID="RequiredFieldValidator12" runat="server" ErrorMessage="<%$Resources:Lang, please_select_customer_account %>" Display="Dynamic" ControlToValidate="rcb_customerAccount" />
                            </td>
                            <td>
                                <asp:TextBox ID="txt_customerAccount" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageView3" runat="server">
                <telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server" LoadingPanelID="RadAjaxLoadingPanel2">
                    <asp:Panel ID="Panel6" runat="server" GroupingText="<%$Resources:Lang, tax_setting %>">
                        <table cellpadding="5">
                            <tr>
                                <td>
                                    <asp:Label ID="Label16" runat="server" Text="<%$Resources:Lang, sales_tax_system %>"></asp:Label>:
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="rbl_salesTaxSystem" runat="server" RepeatDirection="Horizontal" AutoPostBack="True" OnSelectedIndexChanged="rbl_salesTaxSystem_SelectedIndexChanged"></asp:RadioButtonList>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>

                    <asp:Panel ID="Panel7" runat="server" GroupingText="<%$Resources:Lang, tax_rate %>">
                        <table cellpadding="5">
                            <tr>
                                <td>
                                    <asp:Label ID="Label17" runat="server" Text="<%$Resources:Lang, tax_name %>"></asp:Label>
                                    1:
                                </td>
                                <td>
                                    <asp:TextBox ID="txt_taxName1" runat="server" AutoPostBack="True" OnTextChanged="txt_taxName1_TextChanged"></asp:TextBox>
                                    <asp:Label ID="lbl_error1" runat="server" ForeColor="Red" Text="repeated name" Visible="false" />
                                    <asp:RequiredFieldValidator Text="*" ForeColor="Red" ValidationGroup="save" ID="RequiredFieldValidator13" runat="server" ErrorMessage="<%$Resources:Lang, please_insert_taxname_1 %>" Display="Dynamic" ControlToValidate="txt_taxName1" />
                                </td>
                                <td>
                                    <asp:Label ID="Label18" runat="server" Text="<%$Resources:Lang, tax_rate %>"></asp:Label>
                                    1
                                (<asp:Label ID="Label19" runat="server" Text="<%$Resources:Lang, precentage %>"></asp:Label>):
                                </td>
                                <td>
                                    <telerik:RadNumericTextBox ShowSpinButtons="true" IncrementSettings-InterceptArrowKeys="true"
                                        IncrementSettings-InterceptMouseWheel="true" runat="server"
                                        ID="numeric_taxName1" Width="200px" Value="0" Min="0" Max="100">
                                    </telerik:RadNumericTextBox>
                                    <asp:RequiredFieldValidator Text="*" ForeColor="Red" ValidationGroup="save" ID="RequiredFieldValidator14" runat="server" ErrorMessage="<%$Resources:Lang, please_insert_taxrate_1 %>" Display="Dynamic" ControlToValidate="numeric_taxName1" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label20" runat="server" Text="<%$Resources:Lang, tax_name %>"></asp:Label>
                                    2:
                                </td>
                                <td>
                                    <asp:TextBox ID="txt_taxName2" runat="server" Enabled="false" AutoPostBack="True" OnTextChanged="txt_taxName2_TextChanged"></asp:TextBox>
                                    <asp:Label ID="lbl_error2" runat="server" ForeColor="Red" Text="repeated name" Visible="false" />
                                    <asp:RequiredFieldValidator ForeColor="Red" Text="*" Enabled="false" ValidationGroup="save" ID="rfv_txtName2" runat="server" ErrorMessage="<%$Resources:Lang, please_insert_taxname_2 %>" Display="Dynamic" ControlToValidate="txt_taxName2" />
                                </td>
                                <td>
                                    <asp:Label ID="Label21" runat="server" Text="<%$Resources:Lang, tax_rate %>"></asp:Label>
                                    2
                                (<asp:Label ID="Label22" runat="server" Text="<%$Resources:Lang, precentage %>"></asp:Label>):
                                </td>
                                <td>
                                    <telerik:RadNumericTextBox ShowSpinButtons="true" IncrementSettings-InterceptArrowKeys="true"
                                        IncrementSettings-InterceptMouseWheel="true" runat="server" Enabled="false"
                                        ID="numeric_taxName2" Width="200px" Value="0" Min="0" Max="100">
                                    </telerik:RadNumericTextBox>
                                    <asp:RequiredFieldValidator ForeColor="Red" Text="*" Enabled="false" ValidationGroup="save" ID="rfv_txtNameNumeric2" runat="server" ErrorMessage="<%$Resources:Lang, please_insert_taxrate_2 %>" Display="Dynamic" ControlToValidate="numeric_taxName2" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label23" runat="server" Text="<%$Resources:Lang, tax_name %>"></asp:Label>
                                    3:
                                </td>
                                <td>
                                    <asp:TextBox ID="txt_taxName3" runat="server" Enabled="false" AutoPostBack="True" OnTextChanged="txt_taxName3_TextChanged"></asp:TextBox>
                                    <asp:Label ID="lbl_error3" runat="server" ForeColor="Red" Text="repeated name" Visible="false" />
                                    <asp:RequiredFieldValidator ForeColor="Red" Text="*" Enabled="false" ValidationGroup="save" ID="rfv_txtName3" runat="server" ErrorMessage="<%$Resources:Lang, please_insert_taxname_3 %>" Display="Dynamic" ControlToValidate="txt_taxName3" />
                                </td>
                                <td>
                                    <asp:Label ID="Label24" runat="server" Text="<%$Resources:Lang, tax_rate %>"></asp:Label>
                                    3
                                (<asp:Label ID="Label25" runat="server" Text="<%$Resources:Lang, precentage %>"></asp:Label>):
                                </td>
                                <td>
                                    <telerik:RadNumericTextBox ShowSpinButtons="true" IncrementSettings-InterceptArrowKeys="true"
                                        IncrementSettings-InterceptMouseWheel="true" runat="server" Enabled="false"
                                        ID="numeric_taxName3" Width="200px" Value="0" Min="0" Max="100">
                                    </telerik:RadNumericTextBox>
                                    <asp:RequiredFieldValidator ForeColor="Red" Text="*" Enabled="false" ValidationGroup="save" ID="rfv_txtNameNumeric3" runat="server" ErrorMessage="<%$Resources:Lang, please_insert_taxrate_3 %>" Display="Dynamic" ControlToValidate="numeric_taxName3" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>

                    <asp:Panel ID="Panel8" runat="server" GroupingText="<%$Resources:Lang, tax_option %>">
                        <table cellpadding="5">
                            <tr>
                                <td>
                                    <asp:Label ID="Label26" runat="server" Text="<%$Resources:Lang, by_default_item_has_this_tax_rate %>"></asp:Label>:
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="rbl_defaultTaxRate" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem Value="1" Text="<%$Resources:Lang, no_tax %>" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="<%$Resources:Lang, sales_tax1 %>"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="<%$Resources:Lang, sales_tax2 %>" Enabled="false"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="<%$Resources:Lang, sales_tax3 %>" Enabled="false"></asp:ListItem>
                                 
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <asp:CheckBox ID="cb_taxInclusive" Text="<%$Resources:Lang, item_price_is_tax_inclusive %>" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    </telerik:RadAjaxPanel>
                    <asp:Panel ID="Panel9" runat="server" GroupingText="<%$Resources:Lang, sales_tax %>">
                        <table cellpadding="5">
                            <tr>
                                <td></td>
                                <td>
                                    <asp:CheckBox ID="cb_enableSalesTax" Text="<%$Resources:Lang, enable_sales_tax %>" runat="server" onclick="ToggleSalesRBL(this);" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label27" runat="server" Text="<%$Resources:Lang, pay_sales_tax_based_on %>"></asp:Label>:
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="rbl_salesTaxBasedOn" runat="server" RepeatDirection="Horizontal" Enabled="false">
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label28" runat="server" Text="<%$Resources:Lang, tax_paid %>"></asp:Label>:
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="rbl_salesTaxPaid" runat="server" RepeatDirection="Horizontal" Enabled="false">
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>

                    <asp:Panel ID="Panel10" runat="server" GroupingText="<%$Resources:Lang, income_tax %>">
                        <table cellpadding="5">
                            <tr>
                                <td></td>
                                <td>
                                    <asp:CheckBox ID="cb_enableIncomeTax" Text="<%$Resources:Lang, enable_income_tax %>" runat="server" onclick="ToggleIncomeRBL(this);" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label29" runat="server" Text="<%$Resources:Lang, pay_income_tax_based_on %>"></asp:Label>:
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="rbl_incomeTaxBasedOn" runat="server" RepeatDirection="Horizontal" Enabled="false">
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label30" runat="server" Text="<%$Resources:Lang, tax_paid %>"></asp:Label>:
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="rbl_incomeTaxPaid" runat="server" RepeatDirection="Horizontal" Enabled="false">
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>

                
            </telerik:RadPageView>
                   <telerik:RadPageView ID="RadPageView4" runat="server">
                <asp:Panel ID="Panel11" runat="server" GroupingText="<%$Resources:Lang,  POS_Settings %>">
                      <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional"><ContentTemplate>
                          <asp:UpdateProgress ID="UpdateProgress3" runat="server" AssociatedUpdatePanelID="UpdatePanel4">
                            <ProgressTemplate>
                                <div class="loader">
                                    <img src="../App_Themes/Default/img/ajax-loader.gif" />
                                </div>
                            </ProgressTemplate></asp:UpdateProgress>
                    <table cellpadding="5"  >
                        <tr>
                            <td>
                                <asp:Label ID="Label1" runat="server" Text="<%$Resources:Lang, Minimum_Charge_Settings %>"></asp:Label>:
                            </td>
                            <td>
                                  </td>
                        </tr>
                        <tr>
                            <td>  </td>
                            <td>
                                <asp:CheckBox ID="chk_apply" runat="server" AutoPostBack="true"  Text="<%$Resources:Lang, Apply_Minimum_Charge %>" OnCheckedChanged="chk_apply_CheckedChanged" />
                                  </td>
                        </tr>
<tr> 
                            <td>     <asp:Label ID="L" runat="server" Text="<%$Resources:Lang, Saturday %>"></asp:Label>: </td>
                            <td>
                                         <telerik:RadNumericTextBox ID="Numeric_Saturday" Type="Number" Enabled="false" runat="server" Width="105px" ShowSpinButtons="True" Culture="en-US" DbValueFactor="1" LabelCssClass="" LabelWidth="64px" MinValue="0" Height="24px">
                                                                        <NumberFormat DecimalDigits="3" ZeroPattern="n" />
                                                                    </telerik:RadNumericTextBox>
                                  </td>
                        </tr>
<tr>
                            <td>     <asp:Label ID="La" runat="server" Text="<%$Resources:Lang,Sunday  %>"></asp:Label>: </td>
                            <td>
                                         <telerik:RadNumericTextBox ID="Numeric_Sunday" Type="Number"  Enabled="false" runat="server" Width="105px" ShowSpinButtons="True" Culture="en-US" DbValueFactor="1" LabelCssClass="" LabelWidth="64px" MinValue="0" Height="24px">
                                                                        <NumberFormat DecimalDigits="3" ZeroPattern="n" />
                                                                    </telerik:RadNumericTextBox>
                                  </td>
                        </tr>
<tr>
                            <td>     <asp:Label ID="Lab" runat="server" Text="<%$Resources:Lang,Monday  %>"></asp:Label>: </td>
                            <td>
                                         <telerik:RadNumericTextBox ID="Numeric_Monday" Type="Number"  Enabled="false" runat="server" Width="105px" ShowSpinButtons="True" Culture="en-US" DbValueFactor="1" LabelCssClass="" LabelWidth="64px" MinValue="0" Height="24px">
                                                                        <NumberFormat DecimalDigits="3" ZeroPattern="n" />
                                                                    </telerik:RadNumericTextBox>
                                  </td>
                        </tr>
<tr>
                            <td>     <asp:Label ID="be1" runat="server" Text="<%$Resources:Lang, Tuesday %>"></asp:Label>: </td>
                            <td>
                                         <telerik:RadNumericTextBox ID="Numeric_Tuesday"  Enabled="false" Type="Number" runat="server" Width="105px" ShowSpinButtons="True" Culture="en-US" DbValueFactor="1" LabelCssClass="" LabelWidth="64px" MinValue="0" Height="24px">
                                                                        <NumberFormat DecimalDigits="3" ZeroPattern="n" />
                                                                    </telerik:RadNumericTextBox>
                                  </td>
                        </tr>
<tr>
                            <td>     <asp:Label ID="b1" runat="server" Text="<%$Resources:Lang, Wendesday %>"></asp:Label>: </td>
                            <td>
                                         <telerik:RadNumericTextBox ID="Numeric_Wendesday"  Enabled="false" Type="Number" runat="server" Width="105px" ShowSpinButtons="True" Culture="en-US" DbValueFactor="1" LabelCssClass="" LabelWidth="64px" MinValue="0" Height="24px">
                                                                        <NumberFormat DecimalDigits="3" ZeroPattern="n" />
                                                                    </telerik:RadNumericTextBox>
                                  </td>
                        </tr>
<tr>
                            <td>     <asp:Label ID="Labe" runat="server" Text="<%$Resources:Lang,Thursday  %>"></asp:Label>: </td>
                            <td>
                                         <telerik:RadNumericTextBox ID="Numeric_Thursday" Type="Number" Enabled="false"  runat="server" Width="105px" ShowSpinButtons="True" Culture="en-US" DbValueFactor="1" LabelCssClass="" LabelWidth="64px" MinValue="0" Height="24px">
                                                                        <NumberFormat DecimalDigits="3" ZeroPattern="n" />
                                                                    </telerik:RadNumericTextBox>
                                  </td>
                        </tr>
<tr>
                            <td>     <asp:Label ID="Lbe1" runat="server" Text="<%$Resources:Lang, Friday %>"></asp:Label>: </td>
                            <td>
                                         <telerik:RadNumericTextBox ID="Numeric_Friday" Type="Number" Enabled="false"  runat="server" Width="105px" ShowSpinButtons="True" Culture="en-US" DbValueFactor="1" LabelCssClass="" LabelWidth="64px" MinValue="0" Height="24px">
                                                                        <NumberFormat DecimalDigits="3" ZeroPattern="n" />
                                                                    </telerik:RadNumericTextBox>
                                  </td>
                        </tr>

                        <tr><td colspan="2">
                            <hr style="width:100%;" />
                            </td></tr>
<tr>
                            <td>     <asp:Label ID="Lbe144" runat="server" Text="<%$Resources:Lang, currency %>"></asp:Label>: </td>
                            <td>
                                <asp:ObjectDataSource ID="O_ds_" runat="server" SelectMethod="GetCurDropDown" TypeName="BL.Setup.cls_currency">
                                       <SelectParameters>
                               <asp:SessionParameter DefaultValue="E" Name="userLanguage" SessionField="Lang" />
                              </SelectParameters>
                          </asp:ObjectDataSource>
    <telerik:RadComboBox ID="rcb_currency" runat="server"    EmptyMessage="<%$Resources:Lang, select_currency  %>"  Enabled="false"   Height="200"    Width="200" 
DataSourceID="O_ds_" DataKeyField="CurrencyId" DataTextField="cur" DataValueField="CurrencyId" Filter="Contains"  >    </telerik:RadComboBox>
                                
                                                                  </td>
                        </tr>
                        </table>
                          </ContentTemplate></asp:UpdatePanel>
                          </asp:Panel>
                        </telerik:RadPageView>
        </telerik:RadMultiPage>
        <br />
        <div dir="<%=Resources.Lang.dir %>">
            <asp:Button ID="btn_save" CssClass="buttonStyle" runat="server" ValidationGroup="save" Text="<%$Resources:Lang, btn_Save %>" OnClick="btn_save_Click" />
        </div>

    </div>

    <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="List" ValidationGroup="save" ShowMessageBox="true" ShowSummary="false" ForeColor="Red" />

    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel2" runat="server">
        <img alt="Loading" class="auto-style1" src="../App_Themes/Default/img/loading2.gif" />
    </telerik:RadAjaxLoadingPanel>
</asp:Content>
