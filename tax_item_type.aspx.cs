﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace InvyLap.Setup
{
    public partial class tax_item_type : System.Web.UI.Page
    {
        Boolean status;
        List<int> foundError = new List<int>();

        protected void Page_Load(object sender, EventArgs e)
        {
            lbl_msg.Visible = false;
        }

        protected override void InitializeCulture()
        {
            Settings.ChangeLang();

        }

        protected void RadGrid_Items_InsertCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            GridEditableItem newItem = e.Item as GridEditableItem;

            string name = ((TextBox)newItem.FindControl("txt_name")).Text;

            status = BL.Setup.cls_Tax.AddTaxItemType(name, int.Parse(Session["UserID"].ToString()), out foundError);

            ShowSuccessMessage(foundError, status);

            if (status == false)
            {
                e.Canceled = true;
            }
        }

        protected void RadGrid_Items_UpdateCommand(object sender, GridCommandEventArgs e)
        {
            GridEditableItem editedItem = e.Item as GridEditableItem;
            //Access the textbox from the edit form template and store the values in string variables.
            string ID = editedItem.OwnerTableView.DataKeyValues[editedItem.ItemIndex]["TaxItemTypeId"].ToString();
            string name = ((TextBox)editedItem.FindControl("txt_name")).Text;

            status = BL.Setup.cls_Tax.UpdateTaxItemType(int.Parse(ID), name, int.Parse(Session["UserID"].ToString()), out foundError);

            ShowSuccessMessage(foundError, status);

            if (status == false)
            {
                e.Canceled = true;
            }
        }

        protected void RadGrid_Items_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            GridDataItem item = (GridDataItem)e.Item;

            string ID = item.GetDataKeyValue("TaxItemTypeId").ToString();

            status = BL.Setup.cls_Tax.DeleteTaxItemType(int.Parse(ID), int.Parse(Session["UserID"].ToString()), out foundError);

            ShowSuccessMessage(foundError, status);
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CloseAndRebind();", true);
        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            lbl_msg.Visible = false;
            Timer1.Enabled = false;
        }

        protected void ShowSuccessMessage(List<int> foundError, Boolean status)
        {
            List<string> errorMessages = BL.Configuration.cls_AppConfiguration.GetErrors(HttpContext.Current.Session["lang"].ToString(), foundError);

            string success = "";
            foreach (string x in errorMessages)
            {
                success += x + "<br />";
            }

            lbl_msg.Text = success;

            if (status)
            {
                lbl_msg.BackColor = Color.FromArgb(0xe8f5e5);
            }
            else
            {
                lbl_msg.BackColor = Color.FromArgb(0xf39d77);
            }

            lbl_msg.Visible = true;
            Timer1.Enabled = true;
        }

    }
}