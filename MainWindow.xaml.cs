﻿using BL.Configuration;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Net;
using System.Printing;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Telerik.Reporting.Processing;
using Telerik.Windows.Controls;


namespace POS
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<DAL.Setup_ItemCategory> _newCategories;
        List<DAL.Data_ItemData> _newItems;
        List<DAL.POS_OrderItem> _selectedItems = new List<DAL.POS_OrderItem>();
        int selectedCategoryID = 0;

        //Private properties
        UserData _user;
        actionType? _actionType;
        DAL.POS_Cashier _cashier;
        List<DAL.POS_Cashier> _cashierList;
        Dictionary<string, decimal> _salesTaxes;
        bool _isOrderSaved = false;
        
        decimal _minChargeValue;
        bool _minChargeBeforeTax;
        decimal _minChargeDifference;
        

        //Public properties
        public DAL.POS_LocationTable _selectedTable { get; set; }
        public int? _orderID;
        public int? _discountID;
        public int? _discountRequestedByID;
        public decimal? _discountValue = 0;
        public DAL.POS_Order _order;
        public int? _paymentMethod;
        public DateTime? _paymentDateTime;
        public decimal? _paidAmount;
        public decimal? _remindAmount;
        public bool _isPaid = false;
        public bool _toPrint = false;
        public Discount _discountDetails = null;

        public MainWindow(UserData user, actionType? type, DAL.POS_Cashier cashier, List<DAL.POS_Cashier> cashierList)
        {
            InitializeComponent();

            _user = user;
            _cashier = cashier;
            _cashierList = cashierList;
            _actionType = type;

            this.KeyUp += Window_KeyUp_1;

            if (_cashier.TakeawayQueueId == null)
            {
                grid_takeAway.IsEnabled = false;
            }

            if (_actionType != null)
            {
                if (_actionType == actionType.Takaway)
                {
                    grid_categories.IsEnabled = true;
                    grid_items.IsEnabled = true;

                    grid_tables.Background = Brushes.White;
                    grid_takeAway.Background = Brushes.Orange;

                    grid_guestPhone.Visibility = Visibility.Visible;

                    tbk_takeAwayNumber.Text = BL.POS.cls_PosOrder.GetTakeawayNextTurn((int)_cashier.TakeawayQueueId).ToString();
                }
                else if (_actionType == actionType.Order)
                {
                    grid_tables.Background = Brushes.Orange;
                    grid_takeAway.Background = Brushes.White;

                    //MinimumChargeSetting();
                }
            }
            else
            {
                grid_categories.IsEnabled = false;
                grid_items.IsEnabled = false;
            }

            GetData();

            List<DAL.Setup_ItemCategory> mainCategories = _newCategories.Where(a => a.ParentCategoryId == null).ToList();

            CreateCategories(mainCategories);
            CreateTax();
        }

        private void CreateCategories(List<DAL.Setup_ItemCategory> mainCategories)
        {
            panel_category.Children.Clear();

            for (int i = 0; i < mainCategories.Count; i++)
            {
                Button btn = new Button();
                btn.Width = 140;
                btn.Height = 115;
                btn.Margin = new Thickness(5, 5, 5, 5);
                btn.Cursor = Cursors.Hand;

                StackPanel panel = new StackPanel();

                TextBlock txt = new TextBlock();
                txt.Text = mainCategories[i].ItemCategoryName;
                txt.Margin = new Thickness(0, 2, 0, 0);
                txt.FontSize = 15;
                txt.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                txt.TextWrapping = System.Windows.TextWrapping.Wrap;
                txt.TextAlignment = TextAlignment.Center;
                txt.Height = 34;

                Image img = new Image();
                if (mainCategories[i].Setup_ItemCategoryBinaryImage == null)
                {
                    img.Source = new BitmapImage(new Uri(@"/Resources/plate.png", UriKind.RelativeOrAbsolute));
                }
                else
                {
                    img.Source = Convert(mainCategories[i].Setup_ItemCategoryBinaryImage.ItemCategoryImage);
                }
                img.Width = 100;
                img.Height = 70;
                img.Stretch = Stretch.Fill;

                panel.Children.Add(txt);
                panel.Children.Add(img);              

                btn.Content = panel;
                btn.Name = "cat_" + mainCategories[i].ItemCategoryId;
                btn.Click += btn_Click;

                panel_category.Children.Add(btn); 
            }
        }

        private void CreateItems(List<DAL.Data_ItemData> items)
        {
            panel_items.Children.Clear();

            for (int i = 0; i < items.Count; i++)
            {
                Button btn = new Button();
                btn.Width = 140;
                btn.Height = 140;
                btn.Margin = new Thickness(5, 5, 5, 5);
                btn.Cursor = Cursors.Hand;

                StackPanel panel = new StackPanel();

                TextBlock txt = new TextBlock();
                txt.Text = items[i].ItemName;
                txt.Margin = new Thickness(0, 2, 0, 4);
                txt.FontSize = 15;
                txt.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;

                TextBlock txt_price = new TextBlock();
                txt_price.Text = String.Format("{0:0.00} L.E", items[i].SalesPrice) ;
                txt_price.Margin = new Thickness(0, 2, 0, 0);
                txt_price.FlowDirection = System.Windows.FlowDirection.LeftToRight;
                txt_price.FontSize = 14;
                txt_price.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                txt.TextWrapping = System.Windows.TextWrapping.Wrap;
                txt.TextAlignment = TextAlignment.Center;
                txt.Height = 30;

                Image img = new Image();
                if (items[i].Data_ItemPhoto == null)
                {
                    img.Source = new BitmapImage(new Uri(@"/Resources/plate.png", UriKind.RelativeOrAbsolute));
                }
                else
                {
                    img.Source = Convert(items[i].Data_ItemPhoto.ItemBinaryPhoro);
                }
                img.Width = 100;
                img.Height = 80;
                img.Stretch = Stretch.Fill;

                panel.Children.Add(txt);
                panel.Children.Add(img);
                panel.Children.Add(txt_price);

                btn.Content = panel;
                btn.Name = "itm_" + items[i].ItemDataId;
                btn.Click += btn_items_Click;

                panel_items.Children.Add(btn);
            }
        }

        void btn_Click(object sender, RoutedEventArgs e)
        {
            panel_items.Children.Clear();

            Button btn = (Button)sender;
            int id = int.Parse(btn.Name.Substring(4));

            List<DAL.Setup_ItemCategory> cat = _newCategories.Where(a => a.ParentCategoryId == id).ToList();

            if (cat.Count > 0)
            {
                selectedCategoryID = 0;
                CreateCategories(cat);
                CreateBreadcrumbItem(id);
            }
            else
            {
                CreateBreadcrumbItem(id);

                List<DAL.Data_ItemData> items = _newItems.Where(a => a.ItemCategoryId == id && a.ItemTypeId != 5).ToList();
                selectedCategoryID = id;
                CreateItems(items);
            }
        }

        private void CreateBreadcrumbItem(int id)
        {
            panel_breadcrumb.Children.Clear();

            CreateBreadcrumbParent(id);

            var bc = new BrushConverter();

            System.Windows.Controls.Label lbl_cat = new System.Windows.Controls.Label();
            lbl_cat.Content = _newCategories.Where(a => a.ItemCategoryId == id).SingleOrDefault().ItemCategoryName;
            lbl_cat.Cursor = System.Windows.Input.Cursors.Hand;
            lbl_cat.Background = Brushes.Orange;
            lbl_cat.Foreground = (Brush)bc.ConvertFrom("#FFffffff");
            lbl_cat.FontSize = 20;
            lbl_cat.Name = "lbl_" + id;
            lbl_cat.MouseLeftButtonUp += lbl_cat_MouseLeftButtonUp;

            System.Windows.Controls.Label lbl_separator = new System.Windows.Controls.Label();
            lbl_separator.Content = ">";
            lbl_separator.FontSize = 20;

            panel_breadcrumb.Children.Add(lbl_separator);
            panel_breadcrumb.Children.Add(lbl_cat);
        }

        private void CreateBreadcrumbParent(int? id)
        {
            int? parentID = _newCategories.Where(a => a.ItemCategoryId == id).SingleOrDefault().ParentCategoryId;

            if (parentID != null)
            {
                System.Windows.Controls.Label lbl_cat = new System.Windows.Controls.Label();
                lbl_cat.Content = _newCategories.Where(a => a.ItemCategoryId == parentID).SingleOrDefault().ItemCategoryName;
                lbl_cat.Cursor = System.Windows.Input.Cursors.Hand;
                lbl_cat.FontSize = 20;
                lbl_cat.Name = "lbl_" + parentID;
                lbl_cat.MouseLeftButtonUp += lbl_cat_MouseLeftButtonUp;

                System.Windows.Controls.Label lbl_separator = new System.Windows.Controls.Label();
                lbl_separator.Content = ">";
                lbl_separator.FontSize = 20;

                CreateBreadcrumbParent(parentID);

                panel_breadcrumb.Children.Add(lbl_separator);
                panel_breadcrumb.Children.Add(lbl_cat);
            }
            else
            {
                CreateFirstBreadcrumb();
            }
        }

        private void CreateFirstBreadcrumb()
        {
            System.Windows.Controls.Label lbl_cat = new System.Windows.Controls.Label();
            lbl_cat.Content = "الأصناف";
            lbl_cat.Cursor = System.Windows.Input.Cursors.Hand;
            lbl_cat.FontSize = 20;
            lbl_cat.MouseLeftButtonUp += Label_MouseLeftButtonUp_1;

            panel_breadcrumb.Children.Add(lbl_cat);    
        }

        void lbl_cat_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            System.Windows.Controls.Label lbl_cat = (System.Windows.Controls.Label)sender;
            int id = int.Parse(lbl_cat.Name.Substring(4));
            CreateBreadcrumbItem(id);
            List<DAL.Setup_ItemCategory> cat = _newCategories.Where(a => a.ParentCategoryId == id).ToList();

            selectedCategoryID = 0;

            CreateCategories(cat);

            panel_items.Children.Clear();
        }

        void btn_items_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            int id = int.Parse(btn.Name.Substring(4));

            DAL.POS_OrderItem selectedItem = _selectedItems.Where(a => a.ItemId == id && a.IsDeleted == false).SingleOrDefault();

            if (selectedItem != null)
            {
                selectedItem.ItemQuantity = selectedItem.ItemQuantity + 1;
            }
            else
            {
                DAL.Data_ItemData newItem = _newItems.Where(a => a.ItemDataId == id).SingleOrDefault();

                selectedItem = new DAL.POS_OrderItem();
                selectedItem.OrderItemId = -newItem.ItemDataId;
                selectedItem.ItemName = newItem.ItemName;
                selectedItem.ItemId = newItem.ItemDataId;
                selectedItem.ItemPrice = newItem.SalesPrice;
                selectedItem.ItemQuantity = 1;
                selectedItem.IsSalesTaxIncluded = newItem.IsSalesTaxIncluded;
                selectedItem.IsDeleted = false;
                selectedItem.CreatedUserId = _user.UserID;
                selectedItem.shortCut = newItem.ItemShortCut;

                _selectedItems.Add(selectedItem);
            }

            OrderItems();
            RadGridView1.ItemsSource = _selectedItems.Where(a => a.IsDeleted == false);
            RadGridView1.Rebind();

            CalculateTotalOrder();
        }

        public void GetData()
        {
            _newCategories = BL.POS.cls_Items.GetAllCategories();
            _newItems = BL.POS.cls_Items.GetAllItems();
        }

        private void btn_extra_Click_1(object sender, RoutedEventArgs e)
        {
            List<DAL.Data_ItemData> items = _newItems.Where(a => a.ItemCategoryId == selectedCategoryID && a.ItemTypeId == 5).ToList();
            CreateItems(items);
        }

        private void btn_dishes_Click_1(object sender, RoutedEventArgs e)
        {
            List<DAL.Data_ItemData> items = _newItems.Where(a => a.ItemCategoryId == selectedCategoryID && a.ItemTypeId != 5).ToList();
            CreateItems(items);
        }

        private void btnX_Click(object sender, RoutedEventArgs e)
        {
            if (_selectedItems != null)
            {
                DAL.POS_OrderItem deletedItem = (sender as FrameworkElement).DataContext as DAL.POS_OrderItem;

                if (deletedItem != null)
                {
                    if (deletedItem.OrderItemId > 0)
                    {
                        deletedItem.IsDeleted = true;
                    }
                    else
                    {
                        _selectedItems.Remove(deletedItem);
                    }
                    
                    OrderItems();
                    RadGridView1.ItemsSource = _selectedItems.Where(a => a.IsDeleted == false);
                    RadGridView1.Rebind();

                    CalculateTotalOrder();
                }
            }

        }

        private void RadGridView1_RowEditEnded(object sender, GridViewRowEditEndedEventArgs e)
        {
            RadGridView1.Items.Refresh();
            CalculateTotalOrder();
        }

        private decimal? CalculateTotalOrder()
        {
            decimal? total = 0;
            decimal? tax = 0;
            decimal? discount = 0;
            decimal? grandTotal = 0;
            decimal? minCharge = 0;

            if (_selectedItems.Where(a => a.IsDeleted == false).ToList().Count > 0)
            {
                total = _selectedItems.Where(a => a.IsDeleted == false).ToList().Sum(a => a.pos_total);
            }

            tax = CalculateTax();
            discount = this._discountValue;

            minCharge = _minChargeValue * (decimal)radNumeric_guestNumber.Value;

            if (_minChargeBeforeTax)
            {
                if (minCharge > total)
                {
                    grandTotal = minCharge + tax - discount;

                    _minChargeDifference = (decimal)minCharge - (decimal)total;
                }
                else
                {
                    grandTotal = total + tax - discount;
                }
            }
            else
            {
                if (minCharge > (total + tax))
                {
                    grandTotal = minCharge - discount;

                    _minChargeDifference = (decimal)minCharge - (decimal)(total + tax);
                }
                else
                {
                    grandTotal = total + tax - discount;
                }
            }

            tbk_total.Text = String.Format("{0:0.00}", total);
            tbk_discount.Text = String.Format("{0:0.00}", discount);
            tbk_grandTotal.Text = String.Format("{0:0.00}", grandTotal);

            return total + tax;
        }

        private decimal CalculateTax()
        {
            decimal tax1 = 0;
            decimal tax2 = 0;
            decimal tax3 = 0;

            if (_selectedItems.Where(a => a.IsDeleted == false).ToList().Count > 0)
            {

                foreach (var item in _selectedItems.Where(a => a.IsDeleted == false).ToList())
                {
                    if (!item.IsSalesTaxIncluded)
                    {
                        int order = 1;

                        foreach (var tax in _salesTaxes)
                        {
                            if (order == 1)
                            {
                                tax1 = tax1 + (((tax.Value * (decimal)item.ItemPrice) / 100) * (decimal)item.ItemQuantity);

                                order++;
                            }
                            else if (order == 2)
                            {
                                tax2 = tax2 + (((tax.Value * (decimal)item.ItemPrice) / 100) * (decimal)item.ItemQuantity);

                                order++;
                            }
                            else if (order == 3)
                            {
                                tax3 = tax3 + (((tax.Value * (decimal)item.ItemPrice) / 100) * (decimal)item.ItemQuantity);
                            }
                        }
                    }
                }
            }

            tbk_tax1.Text = string.Format("{0:0.00}", tax1);
            tbk_tax2.Text = string.Format("{0:0.00}", tax2);
            tbk_tax3.Text = string.Format("{0:0.00}", tax3);

            return (tax1 + tax2 + tax3);
        }

        private void Label_MouseLeftButtonUp_1(object sender, MouseButtonEventArgs e)
        {
            RecreateBreadcrumbAndCategoriesAndItems();
        }

        private void RecreateBreadcrumbAndCategoriesAndItems()
        {
            panel_breadcrumb.Children.Clear();

            CreateFirstBreadcrumb();

            List<DAL.Setup_ItemCategory> mainCategories = _newCategories.Where(a => a.ParentCategoryId == null).ToList();

            selectedCategoryID = 0;

            CreateCategories(mainCategories);

            panel_items.Children.Clear();
        }

        private void OrderItems()
        {
            List<DAL.POS_OrderItem> filtered = _selectedItems.Where(a => a.IsDeleted == false).ToList();

            for (int i = 0; i < filtered.Count; i++)
            {
                _selectedItems.Where(a => a.IsDeleted == false).ToList()[i].pos_number = i + 1;
            }
        }

        private void btn_home_Click_1(object sender, RoutedEventArgs e)
        {
            user_screen window = new user_screen(_user, _cashierList);
            window.Show();
            this.Close();
        }

        private void CreateTax()
        {
            _salesTaxes = BL.Configuration.cls_GeneralSetting.GetSalesTaxes();

            int order = 1;

            foreach (var tax in _salesTaxes)
            {
                if (order == 1)
                {
                    tbk_tax1Label.Visibility = Visibility.Visible;
                    tbk_tax1Label.Text = tax.Key + ":";

                    tbk_tax1.Visibility = Visibility.Visible;
                    tbk_tax1.Text = "0.00";

                    order++;
                }
                else if (order == 2)
                {
                    tbk_tax2Label.Visibility = Visibility.Visible;
                    tbk_tax2Label.Text = tax.Key + ":";

                    tbk_tax2.Visibility = Visibility.Visible;
                    tbk_tax2.Text = "0.00";

                    order++;
                }
                else if (order == 3)
                {
                    tbk_tax3Label.Visibility = Visibility.Visible;
                    tbk_tax3Label.Text = tax.Key + ":";

                    tbk_tax3.Visibility = Visibility.Visible;
                    tbk_tax3.Text = "0.00";
                }
            }
        }

        public static BitmapImage Convert(object value)
        {
            byte[] blob = (byte[])value;
            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            stream.Write(blob, 0, blob.Length);
            stream.Position = 0;

            System.Drawing.Image img = System.Drawing.Image.FromStream(stream);
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();

            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
            ms.Seek(0, System.IO.SeekOrigin.Begin);
            bi.StreamSource = ms;
            bi.EndInit();
            return bi;
        }

        private void btn_chooseTable_Click_1(object sender, RoutedEventArgs e)
        {
            Clear();

            Locations window = new Locations(true, _cashier, this);
            window.ShowDialog();

            if (_selectedTable != null)
            {
                tbk_tableNumber.Text = (_selectedTable == null) ? "----" : _selectedTable.LocationTableNo.ToString();
                tbk_locationName.Text = (_selectedTable == null) ? "----" : _selectedTable.POS_Location.LocationName;

                grid_tables.Background = Brushes.Orange;
                grid_takeAway.Background = Brushes.White;

                grid_guestPhone.Visibility = Visibility.Hidden;

                _actionType = actionType.Order;

                grid_categories.IsEnabled = true;
                grid_items.IsEnabled = true;

                tbk_takeAwayNumber.Text = "----";

                MinimumChargeSetting(_selectedTable.OpenDateTime);
            }
        }

        private void btn_openedTables_Click_1(object sender, RoutedEventArgs e)
        {
            Clear();

            Locations window = new Locations(false, _cashier, this);
            window.ShowDialog();

            if (_selectedTable != null)
            {
                grid_tables.Background = Brushes.Orange;
                grid_takeAway.Background = Brushes.White;
               
                grid_guestPhone.Visibility = Visibility.Hidden;

                _actionType = actionType.Order;

                grid_categories.IsEnabled = true;
                grid_items.IsEnabled = true;

                tbk_takeAwayNumber.Text = "----";

                _order = BL.POS.cls_PosOrder.GetOrder(_selectedTable.LocationTableId, _cashier.CashierId);
                btn_takeAway.IsEnabled = false;
                btn_chooseTable.IsEnabled = true;
                _isOrderSaved = true;

                _selectedItems = new List<DAL.POS_OrderItem>();

                foreach (var item in _order.POS_OrderItem.Where(a => a.IsDeleted == false))
                {
                    item.ItemName = item.Data_ItemData.ItemName;
                    item.IsSalesTaxIncluded = item.Data_ItemData.IsSalesTaxIncluded;
                    _selectedItems.Add(item);
                }

                txt_guestName.Text = _order.GuestName;
                txt_guestPhone.Text = "";

                _isPaid = _order.IsPaid;
                _discountID = _order.DiscountId;
                _discountRequestedByID = _order.DiscountRequestedById;
                _discountValue = _order.DiscountValue;
                radNumeric_guestNumber.Value = _order.GuestCount;
                _minChargeDifference = _order.MinimumChargeDiffValue;
                _paidAmount = _order.PaidAmount;
                _remindAmount = _order.RemindAmount;
                _paymentMethod = _order.PaymentMethodId;
                _paymentDateTime = _order.PaymentDateTime;

                RecreateBreadcrumbAndCategoriesAndItems();

                OrderItems();
                RadGridView1.ItemsSource = _selectedItems.Where(a => a.IsDeleted == false);
                RadGridView1.Rebind();

                MinimumChargeSetting(_selectedTable.OpenDateTime);
                
                CalculateTotalOrder();
            }

            tbk_tableNumber.Text = (_selectedTable == null) ? "----" : _selectedTable.LocationTableNo.ToString();
            tbk_locationName.Text = (_selectedTable == null) ? "----" : _selectedTable.POS_Location.LocationName;
        }

        private void btn_takeAway_Click_1(object sender, RoutedEventArgs e)
        {
            int nextTurn = BL.POS.cls_PosOrder.GetTakeawayNextTurn((int)_cashier.TakeawayQueueId);

            if (nextTurn == 0)
            {
                MessageBox.Show("ترتيب التيك اواي المربوط بالكاشير غير موجود");
            }
            else
            {
                Clear();

                tbk_takeAwayNumber.Text = nextTurn.ToString();

                if (_selectedTable != null)
                {
                    BL.POS.cls_Locations.CloseTable(_selectedTable.LocationTableId);
                    _selectedTable = null;
                }

                grid_tables.Background = Brushes.White;
                grid_takeAway.Background = Brushes.Orange;
                grid_guestPhone.Visibility = Visibility.Visible;
                _actionType = actionType.Takaway;

                tbk_tableNumber.Text = "----";

                grid_categories.IsEnabled = true;
                grid_items.IsEnabled = true;
            }
        }

        private void btn_waitingList_Click_1(object sender, RoutedEventArgs e)
        {
            Clear();

            W_takeawayList window = new W_takeawayList(_cashier, this, _user.UserID);
            window.ShowDialog();

            if (_orderID != null)
            {
                _selectedTable = null;

                tbk_tableNumber.Text = (_selectedTable == null) ? "----" : _selectedTable.LocationTableNo.ToString();
                tbk_locationName.Text = (_selectedTable == null) ? "----" : _selectedTable.POS_Location.LocationName;

                grid_tables.Background = Brushes.White;
                grid_takeAway.Background = Brushes.Orange;

                grid_guestPhone.Visibility = Visibility.Visible;

                _actionType = actionType.Takaway;

                grid_categories.IsEnabled = true;
                grid_items.IsEnabled = true;

                _order = BL.POS.cls_PosOrder.GetOrder((int)_orderID);
                btn_takeAway.IsEnabled = true;
                btn_chooseTable.IsEnabled = false;
                _isOrderSaved = true;

                _selectedItems = new List<DAL.POS_OrderItem>();

                foreach (var item in _order.POS_OrderItem.Where(a => a.IsDeleted == false))
                {
                    item.ItemName = item.Data_ItemData.ItemName;
                    item.IsSalesTaxIncluded = item.Data_ItemData.IsSalesTaxIncluded;
                    _selectedItems.Add(item);
                }

                txt_guestName.Text = _order.GuestName;
                txt_guestPhone.Text = _order.POS_TakeAwayOrder.OrderToPhone;

                tbk_takeAwayNumber.Text = _order.POS_TakeAwayOrder.OrderTurn.ToString();

                _isPaid = _order.IsPaid;
                _discountID = _order.DiscountId;
                _discountRequestedByID = _order.DiscountRequestedById;
                _discountValue = _order.DiscountValue;
                radNumeric_guestNumber.Value = _order.GuestCount;
                _minChargeDifference = _order.MinimumChargeDiffValue;
                _paidAmount = _order.PaidAmount;
                _remindAmount = _order.RemindAmount;
                _paymentMethod = _order.PaymentMethodId;
                _paymentDateTime = _order.PaymentDateTime;

                RecreateBreadcrumbAndCategoriesAndItems();
                OrderItems();
                RadGridView1.ItemsSource = _selectedItems.Where(a => a.IsDeleted == false);
                RadGridView1.Rebind();
                
                CalculateTotalOrder();
            }
        }

        private void btn_addDiscount_Click_1(object sender, RoutedEventArgs e)
        {
            decimal total = (CalculateTotalOrder() == null) ? 0 : (decimal)CalculateTotalOrder();

            W_addDiscount window = new W_addDiscount(total, this, _user);
            window.ShowDialog();

            CalculateTotalOrder();
        }

        private void btn_newOrder_Click_1(object sender, RoutedEventArgs e)
        {
            Clear();
        }

        private void Clear()
        {
            _actionType = null;
            _selectedItems = new List<DAL.POS_OrderItem>();

            if (!_isOrderSaved && _selectedTable != null)
            {
                BL.POS.cls_Locations.CloseTable(_selectedTable.LocationTableId);
            }

            _isOrderSaved = false;
            _selectedTable = null;
            _order = null;
            _orderID = null;

            RecreateBreadcrumbAndCategoriesAndItems();

            RadGridView1.ItemsSource = _selectedItems.Where(a => a.IsDeleted == false);
            RadGridView1.Rebind();

            tbk_tableNumber.Text = "----";
            tbk_takeAwayNumber.Text = "----";
            tbk_locationName.Text = "----";

            grid_tables.Background = Brushes.White;
            grid_takeAway.Background = Brushes.White;
            btn_takeAway.IsEnabled = true;
            btn_chooseTable.IsEnabled = true;

            grid_categories.IsEnabled = false;
            grid_items.IsEnabled = false;

            txt_guestName.Text = "";
            txt_guestPhone.Text = "";
            grid_guestPhone.Visibility = Visibility.Hidden;

            grid_guestNumber.Visibility = Visibility.Hidden;
            _minChargeValue = 0;
            _minChargeBeforeTax = false;
            _minChargeDifference = 0;
            radNumeric_guestNumber.Value = 0;

            _paymentMethod = null;
            _paidAmount = null;
            _remindAmount = null;
            _paymentDateTime = null;

            _discountID = 0;
            _discountRequestedByID = 0;
            _discountValue = 0;
            _toPrint = false;
            _discountDetails = null;

            CalculateTotalOrder();
        }

        private void Window_Closing_1(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Clear();
        }

        private void btn_save_Click_1(object sender, RoutedEventArgs e)
        {
            SaveAction();
        }

        private void SaveAction()
        {
            if (_actionType == null)
            {
                MessageBox.Show("من فضلك اختر نوع عملية البيع");
            }
            else if (_selectedItems.Where(a => a.IsDeleted == false).ToList().Count == 0)
            {
                MessageBox.Show("من فضلك قم باختيار منتج واحد على الاقل");
            }
            else if (txt_guestName.Text == "")
            {
                MessageBox.Show("من فضلك قم بإدخال اسم الضيف");
            }
            else if (_actionType == actionType.Takaway && txt_guestPhone.Text == "")
            {
                MessageBox.Show("من فضلك قم بإدخال رقم تليفون الضيف");
            }
            else if (_actionType == actionType.Order && _selectedTable == null)
            {
                MessageBox.Show("من فضلك قم باختيار مائدة");
            }
            else if (_selectedItems.Where(a => a.ItemPrice == 0 && a.IsDeleted == false).Any())
            {
                MessageBoxResult result = new MessageBoxResult();
                MessageBoxManager.Yes = "نعم";
                MessageBoxManager.No = "لا";
                MessageBoxManager.Register();
                result = MessageBox.Show("هناك بعض المنتجات قيمتها تساوي صفر، هل تريد حفظها بتلك القيمة؟", "", MessageBoxButton.YesNo, MessageBoxImage.Question, result, MessageBoxOptions.RtlReading);
                if (result == MessageBoxResult.Yes)
                {
                    if (_order == null)
                    {
                        SaveOrder(false);
                    }
                    else
                    {
                        UpdateOrder(false);
                    }
                }
                MessageBoxManager.Unregister();
            }
            else
            {
                if (_order == null)
                {
                    SaveOrder(false);
                }
                else
                {
                    UpdateOrder(false);
                }
            }

        }

        public void UpdateOrder(bool isFromPayment)
        {
            DAL.POS_Order order = new DAL.POS_Order()
            {
                OrderId = _order.OrderId,
                CashierId = _cashier.CashierId,
                EmployeeId = _user.UserID,
                LocationTableId = (_selectedTable == null) ? (int?)null : _selectedTable.LocationTableId,
                GuestName = txt_guestName.Text,
                OrderTotal = decimal.Parse(tbk_total.Text),
                TotalTaxValue = decimal.Parse(tbk_tax1.Text) + decimal.Parse(tbk_tax2.Text) + decimal.Parse(tbk_tax3.Text),
                GrandTotal = decimal.Parse(tbk_grandTotal.Text),
                IsPaid = _isPaid,
                DiscountId = (_discountID == 0) ? (int?)null : _discountID,
                DiscountRequestedById = (_discountRequestedByID == 0) ? (int?)null : _discountRequestedByID,
                DiscountValue = _discountValue,
                GuestCount = (int)radNumeric_guestNumber.Value,
                MinimumChargeDiffValue = _minChargeDifference,
                PaidAmount = _paidAmount,
                RemindAmount = _remindAmount,
                PaymentMethodId = _paymentMethod,
                PaymentDateTime = _paymentDateTime
            };

            DAL.POS_TakeAwayOrder takeAway = null;

            if (_actionType == actionType.Takaway)
            {
                takeAway = new DAL.POS_TakeAwayOrder()
                {
                    TakeAwayOrderId = _order.OrderId,
                    TakeAwayQueueId = _cashier.TakeawayQueueId,
                    OrderTurn = BL.POS.cls_PosOrder.GetTakeawayNextTurn((int)_cashier.TakeawayQueueId),
                    OrderToPhone = txt_guestPhone.Text,
                    IsDelivered = false,
                    IsDeleted = false,
                    CreatedUserId = _user.UserID,
                    CreationDateTime = DateTime.Now
                };
            }

            List<DAL.POS_OrderTax> taxList = new List<DAL.POS_OrderTax>();

            if (tbk_tax1Label.Text != "")
            {
                taxList.Add(new DAL.POS_OrderTax()
                {
                    TaxName = tbk_tax1Label.Text.Substring(0, tbk_tax1Label.Text.Length - 1),
                    OrderId = _orderID,
                    TaxValue = decimal.Parse(tbk_tax1.Text),
                    IsDeleted = false,
                    CreatedUserId = _user.UserID,
                    CreationDateTime = DateTime.Now
                });
            }

            if (tbk_tax2Label.Text != "")
            {
                taxList.Add(new DAL.POS_OrderTax()
                {
                    TaxName = tbk_tax2Label.Text.Substring(0, tbk_tax2Label.Text.Length - 1),
                    OrderId = _orderID,
                    TaxValue = decimal.Parse(tbk_tax2.Text),
                    IsDeleted = false,
                    CreatedUserId = _user.UserID,
                    CreationDateTime = DateTime.Now
                });
            }

            if (tbk_tax3Label.Text != "")
            {

                taxList.Add(new DAL.POS_OrderTax()
                {
                    OrderId = _orderID,
                    TaxName = tbk_tax3Label.Text.Substring(0, tbk_tax3Label.Text.Length - 1),
                    TaxValue = decimal.Parse(tbk_tax3.Text),
                    IsDeleted = false,
                    CreatedUserId = _user.UserID,
                    CreationDateTime = DateTime.Now
                });
            }

            bool isSaved = BL.POS.cls_PosOrder.UpdateOrder(order, _selectedItems, takeAway, taxList, _user.UserID);

            if (isSaved)
            {
                _isOrderSaved = true;

                if (isFromPayment)
                {
                    if (_actionType == actionType.Order)
                    {
                        BL.POS.cls_Locations.CloseTable(_selectedTable.LocationTableId);
                    }

                    List<int> foundError;

                    bool status = BL.Accounting.GL.cls_FlowCash_Transaction.AddNewFlowCashTransaction("", 50,
                          BL.Static.cls_StaticData.GetDefaultCurrencyId(), decimal.Parse(tbk_grandTotal.Text), (int)_cashier.CashFundId, "", DateTime.Now, (int?)null, _user.UserID, out foundError);

                    
                
                }

                if (_toPrint)
                {
                    Print(order.OrderId);
                }

                Clear();
            }
            else
            {
                MessageBox.Show("خطأ في الإدخال");
            }
        }

        public void SaveOrder(bool isFromPayment)
        {
            DAL.POS_Order order = new DAL.POS_Order(){
                CashierId = _cashier.CashierId,
                EmployeeId = _user.UserID,
                LocationTableId = (_selectedTable == null) ? (int?)null : _selectedTable.LocationTableId,
                GuestName = txt_guestName.Text,
                OrderTotal = decimal.Parse(tbk_total.Text),
                TotalTaxValue = decimal.Parse(tbk_tax1.Text) + decimal.Parse(tbk_tax2.Text) + decimal.Parse(tbk_tax3.Text),
                GrandTotal = decimal.Parse(tbk_grandTotal.Text),
                IsPaid = _isPaid,
                DiscountId = (_discountID == 0) ? (int?)null : _discountID,
                DiscountRequestedById = (_discountRequestedByID == 0) ? (int?)null : _discountRequestedByID,
                DiscountValue = _discountValue,
                GuestCount = (int)radNumeric_guestNumber.Value,
                MinimumChargeDiffValue = _minChargeDifference,
                PaidAmount = _paidAmount,
                RemindAmount = _remindAmount,
                PaymentMethodId = _paymentMethod,
                PaymentDateTime = _paymentDateTime,
                ReceiptNo = (_actionType == actionType.Order) ? BL.Setup.cls_Setup.GetEntityNextCode("POS Invoice") : BL.Setup.cls_Setup.GetEntityNextCode("Takeaway Invoice"),
                IsDeleted = false,
                CreatedUserId = _user.UserID,
                CreationDateTime = DateTime.Now
            };

            DAL.POS_TakeAwayOrder takeAway = null;

            if (_actionType == actionType.Takaway)
            {
                takeAway = new DAL.POS_TakeAwayOrder()
                {
                    TakeAwayQueueId = _cashier.TakeawayQueueId,
                    OrderTurn = BL.POS.cls_PosOrder.GetTakeawayNextTurn((int)_cashier.TakeawayQueueId),
                    OrderToPhone = txt_guestPhone.Text,
                    IsDelivered = false,
                    IsDeleted = false,
                    CreatedUserId = _user.UserID,
                    CreationDateTime = DateTime.Now
                };
            }

            List<DAL.POS_OrderTax> taxList = new List<DAL.POS_OrderTax>();

            if (tbk_tax1Label.Text != "")
            {
                taxList.Add(new DAL.POS_OrderTax()
                {
                    TaxName = tbk_tax1Label.Text.Substring(0, tbk_tax1Label.Text.Length - 1),
                    TaxValue = decimal.Parse(tbk_tax1.Text),
                    IsDeleted = false,
                    CreatedUserId = _user.UserID,
                    CreationDateTime = DateTime.Now
                });
            }

            if (tbk_tax2Label.Text != "")
            {
                taxList.Add(new DAL.POS_OrderTax()
                {
                    TaxName = tbk_tax2Label.Text.Substring(0, tbk_tax2Label.Text.Length - 1),
                    TaxValue = decimal.Parse(tbk_tax2.Text),
                    IsDeleted = false,
                    CreatedUserId = _user.UserID,
                    CreationDateTime = DateTime.Now
                });
            }

            if (tbk_tax3Label.Text != "")
            {
                
                taxList.Add(new DAL.POS_OrderTax()
                {
                    
                    TaxName = tbk_tax3Label.Text.Substring(0, tbk_tax3Label.Text.Length - 1),
                    TaxValue = decimal.Parse(tbk_tax3.Text),
                    IsDeleted = false,
                    CreatedUserId = _user.UserID,
                    CreationDateTime = DateTime.Now
                });
            }

            int newOrderID;

            bool isSaved = BL.POS.cls_PosOrder.InsertNewOrder(order, _selectedItems, takeAway, taxList,out newOrderID, _user.UserID);
            if(isSaved)
            {
                _isOrderSaved = true;

                if (isFromPayment)
                {
                    if (_actionType == actionType.Order)
                    {
                        BL.POS.cls_Locations.CloseTable(_selectedTable.LocationTableId);
                    }

                    List<int> foundError;

                    bool status = BL.Accounting.GL.cls_FlowCash_Transaction.AddNewFlowCashTransaction("", 50,
                          BL.Static.cls_StaticData.GetDefaultCurrencyId(), decimal.Parse(tbk_grandTotal.Text), (int)_cashier.CashFundId, "", DateTime.Now, (int?)null, _user.UserID, out foundError);

                }

                if (_toPrint)
                {
                    Print(newOrderID);
                }

                Clear();
            }
            else
            {
                MessageBox.Show("خطأ في الإدخال");
            }
            
        }

        private static void Print(int newOrderID)
        {
            R_receipt re = new R_receipt();

            re.ReportParameters["orderID"].Value = newOrderID.ToString();

            Telerik.Reporting.InstanceReportSource sourece = new Telerik.Reporting.InstanceReportSource();

            sourece.ReportDocument = re;
            try
            {

                PrinterSettings printerSettings = new PrinterSettings();
                printerSettings.PrinterName = Properties.Settings.Default.PrinterName;
                ReportProcessor reportProcessor = new ReportProcessor();
                reportProcessor.PrintReport(sourece, printerSettings);

                string hostName = Dns.GetHostName(); 
                string myIP = Dns.GetHostByName(hostName).AddressList[0].ToString();

                PrintServer server = new PrintServer(@"\\" + myIP);
                
                PrintQueue pq = new PrintQueue(server, Properties.Settings.Default.PrinterName);
                SpotTroubleUsingQueueAttributes(pq);

            }catch(Exception ex)
            {
                MessageBox.Show("من فضلك تأكد من توصيل الطابعة بصورة صحيحة");
            }
        }

        internal static void SpotTroubleUsingQueueAttributes(PrintQueue pq)
        {
            if ((pq.QueueStatus & PrintQueueStatus.PaperProblem) == PrintQueueStatus.PaperProblem)
            {
                MessageBox.Show("هناك مشكلة في الطابعة");
            }
            if ((pq.QueueStatus & PrintQueueStatus.NoToner) == PrintQueueStatus.NoToner)
            {
                MessageBox.Show("هناك مشكلة في الطابعة");
            }
            if ((pq.QueueStatus & PrintQueueStatus.DoorOpen) == PrintQueueStatus.DoorOpen)
            {
               // MessageBox.Show("Has an open door. ");
                MessageBox.Show("هناك مشكلة في الطابعة");
            }
            if ((pq.QueueStatus & PrintQueueStatus.Error) == PrintQueueStatus.Error)
            {
                //MessageBox.Show("Is in an error state. ");
                MessageBox.Show("هناك مشكلة في الطابعة");
            }
            if ((pq.QueueStatus & PrintQueueStatus.NotAvailable) == PrintQueueStatus.NotAvailable)
            {
                //MessageBox.Show("Is not available. ");
                MessageBox.Show("هناك مشكلة في الطابعة");
            }
            if ((pq.QueueStatus & PrintQueueStatus.Offline) == PrintQueueStatus.Offline)
            {
                //MessageBox.Show("Is off line. ");
                MessageBox.Show("هناك مشكلة في الطابعة");
            }
            if ((pq.QueueStatus & PrintQueueStatus.OutOfMemory) == PrintQueueStatus.OutOfMemory)
            {
                //MessageBox.Show("Is out of memory. ");
                MessageBox.Show("هناك مشكلة في الطابعة");
            }
            if ((pq.QueueStatus & PrintQueueStatus.PaperOut) == PrintQueueStatus.PaperOut)
            {
                //MessageBox.Show("Is out of paper. ");
                MessageBox.Show("هناك مشكلة في الطابعة");
            }
            if ((pq.QueueStatus & PrintQueueStatus.OutputBinFull) == PrintQueueStatus.OutputBinFull)
            {
                //MessageBox.Show("Has a full output bin. ");
                MessageBox.Show("هناك مشكلة في الطابعة");
            }
            if ((pq.QueueStatus & PrintQueueStatus.PaperJam) == PrintQueueStatus.PaperJam)
            {
                //MessageBox.Show("Has a paper jam. ");
                MessageBox.Show("هناك مشكلة في الطابعة");
            }
            if ((pq.QueueStatus & PrintQueueStatus.Paused) == PrintQueueStatus.Paused)
            {
               // MessageBox.Show("Is paused. ");
                MessageBox.Show("هناك مشكلة في الطابعة");
            }
            if ((pq.QueueStatus & PrintQueueStatus.TonerLow) == PrintQueueStatus.TonerLow)
            {
                //MessageBox.Show("Is low on toner.");
                MessageBox.Show("هناك مشكلة في الطابعة");
            }
            if ((pq.QueueStatus & PrintQueueStatus.UserIntervention) == PrintQueueStatus.UserIntervention)
            {
                //MessageBox.Show("Needs user intervention. ");
                MessageBox.Show("هناك مشكلة في الطابعة");
            }
        }

        private void MinimumChargeSetting(DateTime? openDate)
        {
            DAL.Configuration_GeneralSetting setting = BL.Configuration.cls_GeneralSetting.GetGeneralSettingPOS();

            if (setting != null)
            {
                if (setting.Configuration_POSMinmumChargeSetting != null)
                {
                    if (setting.Configuration_POSMinmumChargeSetting.IsMinmumChargeApplied)
                    {
                        decimal value = 0;

                        _minChargeBeforeTax = (setting.Configuration_POSMinmumChargeSetting.IsBeforeTax == null)? false : (bool)setting.Configuration_POSMinmumChargeSetting.IsBeforeTax;

                        DateTime date = (openDate == null) ? DateTime.Today : (DateTime)openDate;

                        if (date.DayOfWeek == DayOfWeek.Friday)
                        {
                            value = (decimal)setting.Configuration_POSMinmumChargeSetting.FridayValue;
                        }
                        else if (date.DayOfWeek == DayOfWeek.Saturday)
                        {
                            value = (decimal)setting.Configuration_POSMinmumChargeSetting.SaturdayValue;
                        }
                        else if (date.DayOfWeek == DayOfWeek.Sunday)
                        {
                            value = (decimal)setting.Configuration_POSMinmumChargeSetting.SundayValue;
                        }
                        else if (date.DayOfWeek == DayOfWeek.Monday)
                        {
                            value = (decimal)setting.Configuration_POSMinmumChargeSetting.MondayValue;
                        }
                        else if (date.DayOfWeek == DayOfWeek.Tuesday)
                        {
                            value = (decimal)setting.Configuration_POSMinmumChargeSetting.TuesdayValue;
                        }
                        else if (date.DayOfWeek == DayOfWeek.Wednesday)
                        {
                            value = (decimal)setting.Configuration_POSMinmumChargeSetting.WednesdayValue;
                        }
                        else if (date.DayOfWeek == DayOfWeek.Thursday)
                        {
                            value = (decimal)setting.Configuration_POSMinmumChargeSetting.ThursdayValue;
                        }

                        _minChargeValue = value;

                        grid_guestNumber.Visibility = Visibility.Visible;
                    }
                }
            }
        }

        private void radNumeric_guestNumber_ValueChanged_1(object sender, RadRangeBaseValueChangedEventArgs e)
        {
            CalculateTotalOrder();
        }

        private void btn_payment_Click_1(object sender, RoutedEventArgs e)
        {
            if (_actionType == null)
            {
                MessageBox.Show("من فضلك اختر نوع عملية البيع");
            }
            else if (_selectedItems.Where(a => a.IsDeleted == false).ToList().Count == 0)
            {
                MessageBox.Show("من فضلك قم باختيار منتج واحد على الاقل");
            }
            else if (txt_guestName.Text == "")
            {
                MessageBox.Show("من فضلك قم بإدخال اسم الضيف");
            }
            else if (_actionType == actionType.Takaway && txt_guestPhone.Text == "")
            {
                MessageBox.Show("من فضلك قم بإدخال رقم تليفون الضيف");
            }
            else if (_actionType == actionType.Order && _selectedTable == null)
            {
                MessageBox.Show("من فضلك قم باختيار مائدة");
            }
            else if (_selectedItems.Where(a => a.ItemPrice == 0 && a.IsDeleted == false).Any())
            {
                MessageBoxResult result = new MessageBoxResult();
                MessageBoxManager.Yes = "نعم";
                MessageBoxManager.No = "لا";
                MessageBoxManager.Register();
                result = MessageBox.Show("هناك بعض المنتجات قيمتها تساوي صفر، هل تريد حفظها بتلك القيمة؟", "", MessageBoxButton.YesNo, MessageBoxImage.Question, result, MessageBoxOptions.RtlReading);
                if (result == MessageBoxResult.Yes)
                {
                    decimal grandTotal = decimal.Parse(tbk_grandTotal.Text);

                    W_payment window = new W_payment(grandTotal, this);
                    window.ShowDialog();
                }
                MessageBoxManager.Unregister();
            }
            else
            {
                decimal grandTotal = decimal.Parse(tbk_grandTotal.Text);

                W_payment window = new W_payment(grandTotal, this);
                window.ShowDialog();
            }
        }

        private void btn_cancelDiscount_Click_1(object sender, RoutedEventArgs e)
        {
            _discountID = 0;
            _discountRequestedByID = 0;
            _discountValue = 0;
            _discountDetails = null;

            CalculateTotalOrder();
        }

        private void btn_bills_Click_1(object sender, RoutedEventArgs e)
        {
            W_invoices window = new W_invoices(_cashier, _user.UserID);
            window.ShowDialog();
        }

        private void btn_print_Click_1(object sender, RoutedEventArgs e)
        {
            _toPrint = true;
            SaveAction();
        }

        private void Window_KeyUp_1(object sender, KeyEventArgs e)
        {
            //if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
            //{
                string key = e.Key.ToString();

                if (_actionType == actionType.Order && _selectedTable == null)
                {
                    MessageBox.Show("من فضلك قم باختيار مائدة");
                }
                else if (_actionType == null)
                {
                    MessageBox.Show("من فضلك اختر نوع عملية البيع");
                }
                else if (key == Key.Enter.ToString())
                {
                    btn_payment_Click_1(sender, e);
                }
                else
                {
                    DAL.Setup_ItemCategory cat = _newCategories.Where(a => a.ItemCategoryShortCut == key).SingleOrDefault();
                    panel_items.Children.Clear();

                    if (cat != null)
                    {
                        List<DAL.Setup_ItemCategory> list = _newCategories.Where(a => a.ParentCategoryId == cat.ItemCategoryId).ToList();

                        if (list.Count > 0)
                        {
                            CreateCategories(list);
                            selectedCategoryID = 0;
                            CreateBreadcrumbItem(cat.ItemCategoryId);
                        }
                        else
                        {
                            List<DAL.Setup_ItemCategory> list2 = _newCategories.Where(a => a.ParentCategoryId == cat.ParentCategoryId).ToList();
                            CreateCategories(list2);
                            if (cat.ParentCategoryId != null)
                            {
                                CreateBreadcrumbItem((int)cat.ParentCategoryId);
                            }
                            else
                            {
                                CreateBreadcrumbItem(cat.ItemCategoryId);
                            }

                            List<DAL.Data_ItemData> items = _newItems.Where(a => a.ItemCategoryId == cat.ItemCategoryId && a.ItemTypeId != 5).ToList();
                            selectedCategoryID = cat.ItemCategoryId;
                            CreateItems(items);
                        }
                    }
                    else
                    {
                        DAL.POS_OrderItem selectedItem = _selectedItems.Where(a => a.shortCut == key && a.IsDeleted == false).SingleOrDefault();
                        DAL.Data_ItemData newItem = _newItems.Where(a => a.ItemShortCut == key).SingleOrDefault();

                        if (newItem != null)
                        {
                            DAL.Setup_ItemCategory itemCategory = _newCategories.Where(a => a.ItemCategoryId == newItem.ItemCategoryId).SingleOrDefault();

                            List<DAL.Setup_ItemCategory> list2 = _newCategories.Where(a => a.ParentCategoryId == itemCategory.ParentCategoryId).ToList();
                            CreateCategories(list2);

                            if (itemCategory.ParentCategoryId != null)
                            {
                                CreateBreadcrumbItem((int)itemCategory.ParentCategoryId);
                            }
                            else
                            {
                                CreateBreadcrumbItem(itemCategory.ItemCategoryId);
                            }

                            List<DAL.Data_ItemData> items = _newItems.Where(a => a.ItemCategoryId == itemCategory.ItemCategoryId && ((newItem.ItemTypeId == 5) ? a.ItemTypeId == newItem.ItemTypeId : a.ItemTypeId != 5)).ToList();
                            selectedCategoryID = itemCategory.ItemCategoryId;
                            CreateItems(items);
                        }

                        if (selectedItem != null)
                        {
                            selectedItem.ItemQuantity = selectedItem.ItemQuantity + 1;
                        }
                        else
                        {
                            if (newItem != null)
                            {
                                selectedItem = new DAL.POS_OrderItem();
                                selectedItem.OrderItemId = -newItem.ItemDataId;
                                selectedItem.ItemName = newItem.ItemName;
                                selectedItem.ItemId = newItem.ItemDataId;
                                selectedItem.ItemPrice = newItem.SalesPrice;
                                selectedItem.ItemQuantity = 1;
                                selectedItem.IsSalesTaxIncluded = newItem.IsSalesTaxIncluded;
                                selectedItem.IsDeleted = false;
                                selectedItem.CreatedUserId = _user.UserID;
                                selectedItem.shortCut = newItem.ItemShortCut;

                                _selectedItems.Add(selectedItem);
                            }
                        }

                        OrderItems();
                        RadGridView1.ItemsSource = _selectedItems.Where(a => a.IsDeleted == false);
                        RadGridView1.Rebind();

                        CalculateTotalOrder();
                    }

                }
            //}
        }

        private void txt_guestName_GotFocus_1(object sender, RoutedEventArgs e)
        {
            this.KeyUp -= Window_KeyUp_1;
        }

        private void txt_guestName_LostFocus_1(object sender, RoutedEventArgs e)
        {
            this.KeyUp += Window_KeyUp_1;
        }

        private void btn_resetTakaway_Click_1(object sender, RoutedEventArgs e)
        {
            if (_cashier.TakeawayQueueId != null)
            {
                MessageBoxResult result = new MessageBoxResult();
                MessageBoxManager.Yes = "نعم";
                MessageBoxManager.No = "لا";
                MessageBoxManager.Register();
                result = MessageBox.Show("هل انت متاكد من تصفير القائمة؟", "", MessageBoxButton.YesNo, MessageBoxImage.Question, result, MessageBoxOptions.RtlReading);
                if (result == MessageBoxResult.Yes)
                {
                    BL.POS.cls_PosOrder.RestetTakeawayNextTurn((int)_cashier.TakeawayQueueId, _user.UserID);

                    tbk_takeAwayNumber.Text = BL.POS.cls_PosOrder.GetTakeawayNextTurn((int)_cashier.TakeawayQueueId).ToString();
                }

                MessageBoxManager.Unregister();
            }
        }

        
    }

    public class Discount
    {
        public string discountTypeID { set; get; }
        public string discountID { set; get; }
        public decimal value { set; get; }
        public string requestedByID { set; get; }
    }
}
