﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master.Master" AutoEventWireup="true" CodeBehind="tax_item.aspx.cs" Inherits="InvyLap.Setup.tax_item" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function showfilter() {
            //    document.getElementById("ctl00_ContentPlaceHolder1_RadGrid_Department_ctl00_ctl02_ctl00_InitInsertButton").style.display = '';

            $find('<%=RadGrid_Items.ClientID %>').get_masterTableView().showFilterItem();
            $find('<%=RadGrid_Items.ClientID %>').get_masterTableView().showColumn(0);
            $find('<%=RadGrid_Items.ClientID %>').get_masterTableView().showColumn(5);
        }
        function hidefilter() {
            //    document.getElementById("ctl00_ContentPlaceHolder1_RadGrid_Department_ctl00_ctl02_ctl00_InitInsertButton").style.display = 'none';

            $find('<%=RadGrid_Items.ClientID %>').get_masterTableView().hideFilterItem();
            $find('<%=RadGrid_Items.ClientID %>').get_masterTableView().hideColumn(0);
            $find('<%=RadGrid_Items.ClientID %>').get_masterTableView().hideColumn(8);
        }

        function PrintRadGrid() {

            hidefilter();
            var RadGrid1 = $find("<%=RadGrid_Items.ClientID %>");
            $('.rgPager').attr("style", "display: none;");
            var previewWnd = window.open('', '', '', false);
            var sh = '<%= ClientScript.GetWebResourceUrl(RadGrid_Items.GetType(),String.Format("Telerik.Web.UI.Skins.{0}.Grid.{0}.css",RadGrid_Items.Skin)) %>';
            var shBase = '<%= ClientScript.GetWebResourceUrl(RadGrid_Items.GetType(),"Telerik.Web.UI.Skins.Grid.css") %>';
            var styleStr = "<html><head><link href = '" + sh + "' rel='stylesheet' type='text/css'></link>";
            styleStr += "<link href = '" + shBase + "' rel='stylesheet' type='text/css'></link></head>";

            var htmlcontent = styleStr + "<body>" + $find('<%= RadGrid_Items.ClientID %>').get_element().outerHTML + "</body></html>";
            previewWnd.document.open();
            previewWnd.document.write(htmlcontent);
            previewWnd.document.close();
            previewWnd.print();
            previewWnd.close();

            showfilter();
            var RadGrid1 = $find("<%=RadGrid_Items.ClientID %>");
            $('.rgPager').attr("style", "display:'' ;");

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ddl_type" UpdatePanelRenderMode="Inline" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <div class="row-fluid">
        <div class="span6" style="width: 100%">
            <div class="box box-color box-bordered">
                <div class="box-title">
                    <h3 style='float: <%# Resources.Lang.style_float %>'>
                        <i class="icon-bar-chart"></i>
                        <asp:Label ID="Label1" runat="server" Text="<%$Resources:Lang, tax_item %>"></asp:Label></h4>

                    </h3>
                    <div class="actions" style='float: <%# Resources.Lang.style_float_reverse %>'>
                        <a href="#" class="btn btn-mini content-slideUp" style="display: block"><i class="icon-angle-down"></i></a>
                    </div>
                </div>
                <div class="box-content">


                    <div class="row-fluid">
                        <telerik:RadTabStrip ID="RadTabStrip1" runat="server" MultiPageID="RadMultiPage1"
                            SelectedIndex="0" Align="<%$Resources:Lang, align %>" Skin="Metro">
                            <Tabs>
                                <telerik:RadTab Text="<%$Resources:Lang, tax_item_list %>" Selected="True">
                                </telerik:RadTab>
                                <telerik:RadTab Text="<%$Resources:Lang, tax_item %>">
                                </telerik:RadTab>

                            </Tabs>
                        </telerik:RadTabStrip>
                        <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0" CssClass="telerikTab">

                            <telerik:RadPageView ID="RadPageView2" runat="server">
                                <table style='float: <%# Resources.Lang.style_float_reverse %>'>
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="ImageButton2" Height="35px" Width="30px" runat="server" ImageUrl="~/App_Themes/Default/img/excel.png"
                                                OnClick="ImageButton2_Click" AlternateText="ExcelML" /></td>
                                        <td>
                                            <asp:ImageButton ID="ImageButton3" runat="server" Height="35px" Width="30px" ImageUrl="~/App_Themes/Default/img/pdf-icon.png"
                                                OnClick="ImageButton3_Click" AlternateText="pdf" />
                                        </td>
                                        <td>
                                            <img onclick="javascript:PrintRadGrid()" src="../App_Themes/Default/img/print-icon.png" height="35" width="30" style="cursor: pointer;" />
                                        </td>
                                    </tr>
                                </table>

                                <div>
                                    <asp:LinkButton ID="lb_add" runat="server" OnClick="lb_add_Click">
                                        <asp:Label ID="Label9" runat="server" Text="<%$Resources:Lang, add %>"></asp:Label>
                                    </asp:LinkButton>
                                </div>
                                <telerik:RadGrid ID="RadGrid_Items" runat="server" AllowFilteringByColumn="True" AllowPaging="True" AllowSorting="True" GridLines="None" Skin="Metro" DataSourceID="ObjectDataSource4" AutoGenerateColumns="False" CellSpacing="0" OnDeleteCommand="RadGrid_Items_DeleteCommand" OnItemCreated="RadGrid_Items_ItemCreated" OnSelectedIndexChanged="RadGrid_Items_SelectedIndexChanged">
                                    <MasterTableView OverrideDataSourceControlSorting="true" CommandItemDisplay="None" DataKeyNames="TaxItemId" DataSourceID="ObjectDataSource4" ShowFooter="True" Dir="<%$Resources:Lang, grid_direction %>">
                                        <Columns>
                                            <telerik:GridTemplateColumn AllowFiltering="false" UniqueName="Edit">

                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../App_Themes/Default/img/edit.gif" CommandName="Select" />
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>

                                            <telerik:GridBoundColumn Visible="False" DataField="TaxItemId" DataType="System.Int32" HeaderText="TaxItemId" ReadOnly="True" SortExpression="TaxItemId" UniqueName="TaxItemId"></telerik:GridBoundColumn>

                                            <telerik:GridTemplateColumn DataField="TaxItemcode" HeaderText="<%$Resources:Lang, code %>" SortExpression="TaxItemcode" UniqueName="TaxItemcode">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_TaxItemcodeLabel" runat="server" Text='<%# Eval("TaxItemcode") %>'></asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>


                                            <telerik:GridTemplateColumn DataField="TaxItemTypeName" HeaderText="<%$Resources:Lang, type %>" SortExpression="TaxItemTypeName" UniqueName="TaxItemTypeName">
                                                <ItemTemplate>
                                                    <asp:Label ID="TaxItemTypeNameLabel" runat="server" Text='<%# Eval("TaxItemTypeName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>



                                            <telerik:GridTemplateColumn DataField="TaxItemTitle" HeaderText="<%$Resources:Lang, name %>" SortExpression="TaxItemTitle" UniqueName="TaxItemTitle">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_TaxItemTitle" runat="server" Text='<%# Eval("TaxItemTitle") %>'></asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>

                                            <telerik:GridTemplateColumn DataField="Description" HeaderText="<%$Resources:Lang, description %>" SortExpression="Description" UniqueName="Description">
                                                <ItemTemplate>
                                                    <asp:Label ID="DescriptionLabel" runat="server" Text='<%# Eval("Description")!= null ? Eval("Description").ToString().Length > 50 ? Eval("Description").ToString().Substring(0, 50) + " ....." :  Eval("Description") :  Eval("Description")%>'></asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>



                                            <%--<telerik:GridTemplateColumn DataField="IsPrecentage" HeaderText="<%$Resources:Lang, is_precentage %>" SortExpression="IsPrecentage" UniqueName="IsPrecentage">
                                <ItemTemplate>
                                    <asp:CheckBox ID="cb_IsPrecentage" runat="server" Checked='<%# Eval("IsPrecentage") %>' Enabled="false" />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>

                            <telerik:GridTemplateColumn DataField="Precentage" HeaderText="<%$Resources:Lang, precentage %>" SortExpression="Precentage" UniqueName="Precentage">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Precentage" runat="server" Text='<%# Eval("Precentage") %>'></asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>

                            <telerik:GridTemplateColumn DataField="FixedAmount" HeaderText="<%$Resources:Lang, fixed_amount %>" SortExpression="FixedAmount" UniqueName="FixedAmount">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_FixedAmount" runat="server" Text='<%# Eval("FixedAmount") %>'></asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>

                            <telerik:GridTemplateColumn DataField="CurrencyEnglishName" HeaderText="<%$Resources:Lang, currency %>" SortExpression="CurrencyEnglishName" UniqueName="CurrencyEnglishName">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_CurrencyEnglishName" runat="server" Text='<%# Eval("CurrencyEnglishName") %>'></asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>--%>

                                            <telerik:GridTemplateColumn DataField="commission" HeaderText="<%$Resources:Lang, deduction %>" SortExpression="commission" UniqueName="commission">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Commission" runat="server" Text='<%# Eval("commission") %>'></asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>

                                            <telerik:GridTemplateColumn DataField="AccountName" HeaderText="<%$Resources:Lang, account %>" SortExpression="AccountName" UniqueName="AccountName">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_AccountName" runat="server" Text='<%# Eval("AccountName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>

                                            <telerik:GridTemplateColumn DataField="IsActive" HeaderText="<%$Resources:Lang, isActive %>" SortExpression="IsActive" UniqueName="IsActive">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="cb_isActive_view" Checked='<%# Eval("IsActive") %>' runat="server" Enabled="false" />
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>

                                            <telerik:GridButtonColumn ConfirmText="<%$Resources:Lang, confirm_delete %>" ButtonType="ImageButton"
                                                CommandName="Delete" Text="Delete" UniqueName="DeleteColumn">
                                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                                            </telerik:GridButtonColumn>
                                        </Columns>

                                    </MasterTableView>
                                </telerik:RadGrid>
                                <asp:ObjectDataSource ID="ObjectDataSource4" runat="server" SelectMethod="GetAllTaxItem" TypeName="BL.Setup.cls_Tax"></asp:ObjectDataSource>
                            </telerik:RadPageView>

                            <telerik:RadPageView ID="RadPageView1" runat="server">
                                <table cellpadding="5">
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label2" runat="server" Text="<%$Resources:Lang, type %>"></asp:Label>:</td>
                                        <td>

                                            <asp:DropDownList ID="ddl_type" runat="server" DataTextField="Value" DataValueField="Key" AppendDataBoundItems="true">
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator Display="Dynamic" ValidationGroup="save" ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddl_type" ErrorMessage="*" ForeColor="Red" />
                                            <a href="#" onclick="openWin(); return false;">
                                                <asp:Label ID="Label8" runat="server" Text="<%$Resources:Lang, add %>"></asp:Label></a>

                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label3" runat="server" Text="<%$Resources:Lang, code %>"></asp:Label>:</td>
                                        <td colspan="2">
                                            <asp:Label ID="lbl_code" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label4" runat="server" Text="<%$Resources:Lang, name %>"></asp:Label>:</td>
                                        <td colspan="2">
                                            <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="RadAjaxLoadingPanel1">
                                                <asp:TextBox ID="txt_name" runat="server" Width="205" AutoPostBack="True" OnTextChanged="txt_name_TextChanged"></asp:TextBox>
                                                <asp:RequiredFieldValidator Display="Dynamic" ValidationGroup="save" ID="RequiredFieldValidator2" runat="server" ControlToValidate="txt_name" ErrorMessage="*" ForeColor="Red" />
                                                <asp:Label ID="lbl_error" runat="server" ForeColor="Red"></asp:Label>
                                            </telerik:RadAjaxPanel>
                                            <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
                                                <img alt="Loading" class="auto-style1" src="../App_Themes/Default/img/loading2.gif" />
                                            </telerik:RadAjaxLoadingPanel>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <asp:Label ID="Label5" runat="server" Text="<%$Resources:Lang, description %>"></asp:Label>:</td>
                                        <td colspan="2">
                                            <asp:TextBox ID="txt_description" runat="server" TextMode="MultiLine" Width="205px" Height="100px" onkeypress="return reflect(this);" onkeyup="return reflect(this);" onchange="return reflect(this);"></asp:TextBox>
                                            <span id="lbl_maxError"></span>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label6" runat="server" Text="<%$Resources:Lang, isActive %>"></asp:Label>:</td>
                                        <td colspan="2">
                                            <asp:CheckBox ID="cb_isActive" runat="server" Checked="true" /></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <hr style="background-color: black" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <asp:Label ID="Label10" runat="server" Text="<%$Resources:Lang, deduction %>"></asp:Label>:</td>
                                        <td colspan="2">
                                            <telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server" LoadingPanelID="RadAjaxLoadingPanel2">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:RadioButton ID="rb_percentage" Checked="true" runat="server" GroupName="way" AutoPostBack="True" OnCheckedChanged="rb_percentage_CheckedChanged" /></td>
                                                        <td colspan="2">
                                                            <telerik:RadNumericTextBox ShowSpinButtons="true" IncrementSettings-InterceptArrowKeys="true"
                                                                IncrementSettings-InterceptMouseWheel="true" Label="<%$Resources:Lang, precentage %>" runat="server"
                                                                ID="Numeric1" Width="200px" Value="0" Min="0" Max="100">
                                                            </telerik:RadNumericTextBox></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:RadioButton ID="rb_amount" runat="server" GroupName="way" OnCheckedChanged="rb_amount_CheckedChanged" AutoPostBack="True" /></td>
                                                        <td>
                                                            <telerik:RadNumericTextBox ShowSpinButtons="true" IncrementSettings-InterceptArrowKeys="true"
                                                                IncrementSettings-InterceptMouseWheel="true" Label="<%$Resources:Lang, fixed_amount %>" runat="server"
                                                                ID="Numeric2" Width="200px" Enabled="false" Value="0" Min="0">
                                                            </telerik:RadNumericTextBox></td>
                                                        <td>
                                                            <asp:DropDownList ID="ddl_currency" runat="server" Enabled="False" DataSourceID="ObjectDataSource3" DataTextField="Value" DataValueField="Key"></asp:DropDownList><asp:ObjectDataSource ID="ObjectDataSource3" runat="server" SelectMethod="GetCurrencyList" TypeName="BL.Static.cls_StaticData">
                                                                <SelectParameters>
                                                                    <asp:SessionParameter DefaultValue="E" Name="selectedLanguage" SessionField="lang" Type="String" />
                                                                </SelectParameters>
                                                            </asp:ObjectDataSource>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </telerik:RadAjaxPanel>
                                            <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel2" runat="server">
                                                <img alt="Loading" class="auto-style1" src="../App_Themes/Default/img/loading2.gif" />
                                            </telerik:RadAjaxLoadingPanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <hr style="background-color: black" />
                                        </td>
                                    </tr>
                                    <tr id="tr_account" runat="server">
                                        <td>
                                            <asp:Label ID="Label7" runat="server" Text="<%$Resources:Lang, account %>"></asp:Label>:</td>
                                        <td colspan="2">
                                            <telerik:RadComboBox ID="RadComboBox1" runat="server" Width="250px" Style="vertical-align: middle;"
                                                EmptyMessage="<%$ Resources:Lang, choose_account %>" ExpandAnimation-Type="None" CollapseAnimation-Type="None" Skin="Metro">
                                                <ItemTemplate>
                                                    <telerik:RadTreeView runat="server" ID="RadTreeView1" Width="100%" Height="200px" OnClientNodeClicking="nodeClicking" DataFieldID="ID" DataFieldParentID="ParentID" DataTextField="Text" DataValueField="ID" Skin="Metro" OnNodeDataBound="RadTreeView1_NodeDataBound"></telerik:RadTreeView>
                                                </ItemTemplate>
                                                <Items>
                                                    <telerik:RadComboBoxItem Text="" />
                                                </Items>
                                                <ExpandAnimation Type="None"></ExpandAnimation>
                                                <CollapseAnimation Type="None"></CollapseAnimation>
                                            </telerik:RadComboBox>
                                            <asp:HiddenField ID="hd" runat="server" />
                                            <asp:RequiredFieldValidator ValidationGroup="save" ID="RequiredFieldValidator3" runat="server" ControlToValidate="RadComboBox1" ErrorMessage="*" ForeColor="Red" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="2">
                                            <asp:Button ID="btn_save" CssClass="buttonStyle" ValidationGroup="save" runat="server" Text="<%$Resources:Lang, btn_save %>" OnClick="btn_save_Click" />
                                            &nbsp;&nbsp;
                            <asp:Button ID="btn_cancel" CssClass="buttonStyle" runat="server" Text="<%$Resources:Lang, btn_cancel %>" OnClick="btn_cancel_Click" /></td>
                                    </tr>
                                </table>
                            </telerik:RadPageView>
                        </telerik:RadMultiPage>

                        <script type="text/javascript">
                            function openWin() {
                                window.radopen("tax_item_type.aspx", "UserListDialog");
                            }

                            function refreshGrid(arg) {
                                $find("<%= RadAjaxManager1.ClientID %>").ajaxRequest("Rebind");
                            }

                            function nodeClicking(sender, args) {
                                var comboBox = $find("<%= RadComboBox1.ClientID %>");
                                var hd = $find("<%= hd.ClientID %>");

                                var node = args.get_node()

                                if (node.get_nodes().get_count() > 0) {
                                    comboBox.set_value("<%= Resources.Lang.choose_account %>");
                    comboBox.set_text("");
                } else {
                    comboBox.set_text(node.get_text());
                    comboBox.set_value(node.get_value());

                    comboBox.trackChanges();
                    comboBox.get_items().getItem(0).set_text(node.get_text());
                    comboBox.get_items().getItem(0).set_value(node.get_value());
                    comboBox.commitChanges();

                    comboBox.hideDropDown();
                }

                                // Call comboBox.attachDropDown if:
                                // 1) The RadComboBox is inside an AJAX panel.
                                // 2) The RadTreeView has a server-side event handler for the NodeClick event, i.e. it initiates a postback when clicking on a Node.
                                // Otherwise the AJAX postback becomes a normal postback regardless of the outer AJAX panel.

                                //comboBox.attachDropDown();
            }


                        </script>

                        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true" Skin="Metro">
                            <Windows>
                                <telerik:RadWindow ID="UserListDialog" runat="server" Title="<%$Resources:Lang, add_tax_type %>" Height="530px" OnClientClose="refreshGrid"
                                    Width="500px" Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false"
                                    Modal="true" />
                            </Windows>
                        </telerik:RadWindowManager>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
