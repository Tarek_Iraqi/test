﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Resources;
namespace InvyLap.Configuration
{
    public partial class general_setting : System.Web.UI.Page
    {
        Boolean status;
        List<int> foundError = new List<int>();

        protected void Page_Load(object sender, EventArgs e)
        {

            txt_repeatedCloseDate.MinDate = DateTime.Today.Date;

            if (!IsPostBack)
            {
                Privilage();
         
                List<BL.Setup.Tree> accounts = BL.Setup.cls_AccountType.AccountTree();
                RadTreeView tree1 = (RadTreeView)rcb_inventoryAccount.Items[0].FindControl("rtv_inventoryAccount");
                tree1.DataSource = accounts;
                tree1.DataBind();
              if( BL.Setup.cls_ItemCategory.CheckModule(1)==false )  
                RadTabStrip1.Tabs[3].Visible = false;

                RadTreeView tree2 = (RadTreeView)rcb_costOfGoodSold.Items[0].FindControl("rtv_costOfGoodSold");
                tree2.DataSource = accounts;
                tree2.DataBind();

                RadTreeView tree3 = (RadTreeView)rcb_incomeAccount.Items[0].FindControl("rtv_incomeAccount");
                tree3.DataSource = accounts;
                tree3.DataBind();

                RadTreeView tree4 = (RadTreeView)rcb_vendorAccount.Items[0].FindControl("rtv_vendorAccount");
                tree4.DataSource = accounts;
                tree4.DataBind();

                RadTreeView tree5 = (RadTreeView)rcb_customerAccount.Items[0].FindControl("rtv_customerAccount");
                tree5.DataSource = accounts;
                tree5.DataBind();

                Dictionary<int, string> taxSystemList = BL.Static.cls_StaticData.GetSalesTaxSystem(Session["lang"].ToString());
                rbl_salesTaxSystem.DataSource = taxSystemList;
                rbl_salesTaxSystem.DataTextField = "Value";
                rbl_salesTaxSystem.DataValueField = "Key";
                rbl_salesTaxSystem.DataBind();

                rbl_salesTaxSystem.SelectedIndex = 0;

                Dictionary<int, string> taxPaymentBasedList = BL.Static.cls_StaticData.GetTaxPaymentBased(Session["lang"].ToString());
                rbl_salesTaxBasedOn.DataSource = taxPaymentBasedList;
                rbl_salesTaxBasedOn.DataTextField = "Value";
                rbl_salesTaxBasedOn.DataValueField = "Key";
                rbl_salesTaxBasedOn.DataBind();
                rbl_salesTaxBasedOn.SelectedIndex = 0;

                rbl_incomeTaxBasedOn.DataSource = taxPaymentBasedList;
                rbl_incomeTaxBasedOn.DataTextField = "Value";
                rbl_incomeTaxBasedOn.DataValueField = "Key";
                rbl_incomeTaxBasedOn.DataBind();
                rbl_incomeTaxBasedOn.SelectedIndex = 0;

                Dictionary<int, string> taxPaymentPeriodList = BL.Static.cls_StaticData.GetTaxPaymentPeriod(Session["lang"].ToString());
                rbl_salesTaxPaid.DataSource = taxPaymentPeriodList;
                rbl_salesTaxPaid.DataTextField = "Value";
                rbl_salesTaxPaid.DataValueField = "Key";
                rbl_salesTaxPaid.DataBind();
                rbl_salesTaxPaid.SelectedIndex = 0;

                rbl_incomeTaxPaid.DataSource = taxPaymentPeriodList;
                rbl_incomeTaxPaid.DataTextField = "Value";
                rbl_incomeTaxPaid.DataValueField = "Key";
                rbl_incomeTaxPaid.DataBind();
                rbl_incomeTaxPaid.SelectedIndex = 0;

                LoadData();
            }
        }
        public void Privilage()
        {
            List<KeyValuePair<int, bool>> functions = new List<KeyValuePair<int, bool>>();
            BL.Administration.cls_groups.GetPagePrivilage(1, out functions, int.Parse(Session["UserID"].ToString()));
            if (functions.Count > 0)
            {
                try
                {
                    if (functions.Where(a => a.Key == 2).SingleOrDefault().Value == false)
                    {
                        Response.Redirect("../Welcome/index.aspx");
                    }
                    //RadGrid_Items.MasterTableView.GetColumn("Edit").Display = functions.Where(a => a.Key == 2).SingleOrDefault().Value;
                }
                catch { }
            }
            else
            {
                Response.Redirect("../Welcome/index.aspx");
            }
        }
        private void LoadData()
        {

            BL.Configuration.AppGeneralSetting gs = BL.Configuration.cls_GeneralSetting.GetGeneralSetting();

            if (gs != null)
            {
                //************************* First Screen ***************************
                rbl_weightedAverage.SelectedValue = gs.isWACalculatedPerStore.ToString();
                rbl_itemAllocation.SelectedValue = gs.isItemAllocatedByLocationOnStore.ToString();
                cbl_.Items[0].Selected = gs.isNegativeStockAllowed;
                cbl_.Items[1].Selected = gs.isReorderPointUsedInPurchasingItems;
                cbl_.Items[2].Selected = gs.isGrossQuantityUsed;
                cbl_.Items[3].Selected = gs.enableRecordGeneralEntriesWithZero;
                cb_passwordAsk.Checked = gs.ispasswordRequiredToReprintBarcode;
                //txt_password.Text = gs.reprintBarcodeHashedPassword;
                txt_password.Attributes.Add("value", gs.reprintBarcodeHashedPassword);
                txt_confirmPassword.Attributes.Add("value", gs.reprintBarcodeHashedPassword);
                //txt_confirmPassword.Text = gs.reprintBarcodeHashedPassword;

                //************************* Second Screen ***************************
                txt_repeatedCloseDate.SelectedDate = gs.nextRepeatedCloseDate;
                numeric_closeEvery.Text = gs.closeEvery.ToString();
                ddl_units.SelectedValue = gs.closeEveryTimeUnitId.ToString();
                txt_yearlyCloseDate.SelectedDate = gs.yearlyCloseDate;
                //txt_password2.Text = gs.addToClosedPeriodHashedPassword;
                txt_password2.Attributes.Add("value", gs.addToClosedPeriodHashedPassword);
                txt_confirmPassword2.Attributes.Add("value", gs.addToClosedPeriodHashedPassword);
                //txt_confirmPassword2.Text = gs.addToClosedPeriodHashedPassword;

                RadTreeView tree = (RadTreeView)rcb_inventoryAccount.Items[0].FindControl("rtv_inventoryAccount");
                RadTreeNode tn = new RadTreeNode();
                tn = tree.FindNodeByValue(gs.inventoryAccountId.ToString());
                tn.Expanded = true;
                tn.ExpandParentNodes();
                tn.Selected = true;
                rcb_inventoryAccount.SelectedValue = tn.Value;
                rcb_inventoryAccount.Text = tn.Text;

                DAL.Setup_Account acc1 = BL.Setup.cls_Account.GetAccountByID(int.Parse(gs.inventoryAccountId.ToString()));
                txt_inventoryAccount.Text = acc1.AccountCode;

                RadTreeView tree_2 = (RadTreeView)rcb_costOfGoodSold.Items[0].FindControl("rtv_costOfGoodSold");
                RadTreeNode tn2 = new RadTreeNode();
                tn2 = tree_2.FindNodeByValue(gs.costOfGoodSoldAccountId.ToString());
                tn2.Expanded = true;
                tn2.ExpandParentNodes();
                tn2.Selected = true;
                rcb_costOfGoodSold.SelectedValue = tn2.Value;
                rcb_costOfGoodSold.Text = tn2.Text;

                DAL.Setup_Account acc2 = BL.Setup.cls_Account.GetAccountByID(int.Parse(gs.costOfGoodSoldAccountId.ToString()));
                txt_costOfGoodSold.Text = acc2.AccountCode;

                RadTreeView tree_3 = (RadTreeView)rcb_incomeAccount.Items[0].FindControl("rtv_incomeAccount");
                RadTreeNode tn3 = new RadTreeNode();
                tn3 = tree_3.FindNodeByValue(gs.incomeAccountId.ToString());
                tn3.Expanded = true;
                tn3.ExpandParentNodes();
                tn3.Selected = true;
                rcb_incomeAccount.SelectedValue = tn3.Value;
                rcb_incomeAccount.Text = tn3.Text;

                DAL.Setup_Account acc3 = BL.Setup.cls_Account.GetAccountByID(int.Parse(gs.incomeAccountId.ToString()));
                txt_incomeAccount.Text = acc3.AccountCode;

                RadTreeView tree_4 = (RadTreeView)rcb_vendorAccount.Items[0].FindControl("rtv_vendorAccount");
                RadTreeNode tn4 = new RadTreeNode();
                tn4 = tree_4.FindNodeByValue(gs.vendorAccountId.ToString());
                tn4.Expanded = true;
                tn4.ExpandParentNodes();
                tn4.Selected = true;
                rcb_vendorAccount.SelectedValue = tn4.Value;
                rcb_vendorAccount.Text = tn4.Text;

                DAL.Setup_Account acc4 = BL.Setup.cls_Account.GetAccountByID(int.Parse(gs.vendorAccountId.ToString()));
                txt_vendorAccount.Text = acc4.AccountCode;

                RadTreeView tree_5 = (RadTreeView)rcb_customerAccount.Items[0].FindControl("rtv_customerAccount");
                RadTreeNode tn5 = new RadTreeNode();
                tn5 = tree_5.FindNodeByValue(gs.customerAccountId.ToString());
                tn5.Expanded = true;
                tn5.ExpandParentNodes();
                tn5.Selected = true;
                rcb_customerAccount.SelectedValue = tn5.Value;
                rcb_customerAccount.Text = tn5.Text;

                DAL.Setup_Account acc5 = BL.Setup.cls_Account.GetAccountByID(int.Parse(gs.customerAccountId.ToString()));
                txt_customerAccount.Text = acc5.AccountCode;

                //************************* Third Screen **************************
                rbl_salesTaxSystem.SelectedValue = gs.salesTaxSystemId.ToString();
                LoadRadioButtonList();

                txt_taxName1.Text = gs.taxNameOneName;
                numeric_taxName1.Text = gs.taxRateOnePersentage.ToString();
                if (gs.salesTaxSystemId == 2)
                {
                    txt_taxName2.Text = gs.taxNameTwoName;
                    numeric_taxName2.Text = gs.taxRateTwoPersentage.ToString();
                }

                if (gs.salesTaxSystemId == 3)
                {
                    txt_taxName2.Text = gs.taxNameTwoName;
                    numeric_taxName2.Text = gs.taxRateTwoPersentage.ToString();

                    txt_taxName3.Text = gs.taxNameThreeName;
                    numeric_taxName3.Text = gs.taxRateThreePersentage.ToString();
                }

                if (gs.isItemHasSalesTaxOne)
                {
                    rbl_defaultTaxRate.SelectedIndex = 1;
                }
                else if (gs.isItemHasSalesTaxTwo)
                {
                    rbl_defaultTaxRate.SelectedIndex = 2;
                }
                else if (gs.isItemHasSalesTaxThree)
                {
                    rbl_defaultTaxRate.SelectedIndex = 3;
                }
                else
                {
                    rbl_defaultTaxRate.SelectedIndex = 0;
                }

                cb_taxInclusive.Checked = gs.isItemPriceAreTaxInclusive;
                cb_enableSalesTax.Checked = gs.enableSalesTax;
                if (gs.enableSalesTax)
                {
                    rbl_salesTaxBasedOn.SelectedValue = gs.SalesTaxPaymentBasedId.ToString();
                    rbl_salesTaxPaid.SelectedValue = gs.SalesTaxPaymentPeriodId.ToString();
                }
                cb_enableIncomeTax.Checked = gs.EnableIncomeTax;
                if (gs.EnableIncomeTax)
                {
                    rbl_incomeTaxBasedOn.SelectedValue = gs.IncomeTaxPaymentBasedId.ToString();
                    rbl_incomeTaxPaid.SelectedValue = gs.IncomeTaxPaymentPeriodId.ToString();
                }
                chk_apply.Checked = gs.isMinmumChargeApplied;
                if (gs.isMinmumChargeApplied == true)
                {
                    rcb_currency.SelectedValue = gs.currencyId.ToString();
                    Numeric_Friday.Text = gs.fridayValue.ToString();
                    Numeric_Monday.Text = gs.mondayValue.ToString()  ;
      Numeric_Saturday.Text = gs.saturdayValue .ToString()  ;
              Numeric_Sunday.Text = gs.sundayValue .ToString()  ;
          Numeric_Thursday.Text =  gs.thursdayValue  .ToString()  ;
      Numeric_Tuesday. Text=   gs.tuesdayValue  .ToString();
              Numeric_Wendesday. Text= gs.wednesdayValue   .ToString(); ;
              Numeric_Friday.Enabled = true;
              Numeric_Monday.Enabled = true;
              Numeric_Saturday.Enabled = true;
              Numeric_Sunday.Enabled = true;
              Numeric_Thursday.Enabled = true;
              Numeric_Tuesday.Enabled = true;
              Numeric_Wendesday.Enabled = true;
              rcb_currency.Enabled = true;
                }
                
            }
        }

        private void LoadRadioButtonList()
        {
            if (rbl_salesTaxSystem.SelectedIndex == 0)
            {
                txt_taxName2.Enabled = false;
                txt_taxName2.Text = "";
                rfv_txtName2.Enabled = false;
                numeric_taxName2.Enabled = false;
                numeric_taxName2.Text = "0.00";
                rfv_txtNameNumeric2.Enabled = false;

                txt_taxName3.Enabled = false;
                txt_taxName3.Text = "";
                rfv_txtName3.Enabled = false;
                numeric_taxName3.Enabled = false;
                numeric_taxName3.Text = "0.00";
                rfv_txtNameNumeric3.Enabled = false;

                rbl_defaultTaxRate.SelectedIndex = 0;
                rbl_defaultTaxRate.Items[2].Enabled = false;
                rbl_defaultTaxRate.Items[3].Enabled = false;
            }
            else if (rbl_salesTaxSystem.SelectedIndex == 1)
            {
                txt_taxName2.Enabled = true;
                rfv_txtName2.Enabled = true;
                numeric_taxName2.Enabled = true;
                rfv_txtNameNumeric2.Enabled = true;

                txt_taxName3.Enabled = false;
                txt_taxName3.Text = "";
                rfv_txtName3.Enabled = false;
                numeric_taxName3.Enabled = false;
                numeric_taxName3.Text = "0.00";
                rfv_txtNameNumeric3.Enabled = false;

                rbl_defaultTaxRate.SelectedIndex = 0;
                rbl_defaultTaxRate.Items[2].Enabled = true;
                rbl_defaultTaxRate.Items[3].Enabled = false;
            }
            else
            {
                txt_taxName2.Enabled = true;
                rfv_txtName2.Enabled = true;
                numeric_taxName2.Enabled = true;
                rfv_txtNameNumeric2.Enabled = true;

                txt_taxName3.Enabled = true;
                rfv_txtName3.Enabled = true;
                numeric_taxName3.Enabled = true;
                rfv_txtNameNumeric3.Enabled = true;

                rbl_defaultTaxRate.SelectedIndex = 0;
                rbl_defaultTaxRate.Items[2].Enabled = true;
                rbl_defaultTaxRate.Items[3].Enabled = true;
            }
        }

        protected override void InitializeCulture()
        {
            Settings.ChangeLang();

        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            if (CheckTaxNames())
            {

                BL.Configuration.AppGeneralSetting gs = BL.Configuration.cls_GeneralSetting.GetGeneralSetting();


                //*********************** First Screen ***************************//
                bool isWACalculatedPerStore = bool.Parse(rbl_weightedAverage.SelectedValue);
                bool isItemAllocatedByLocationOnStore = bool.Parse(rbl_itemAllocation.SelectedValue);
                bool isNegativeStockAllowed = cbl_.Items[0].Selected;
                bool isReorderPointUsedInPurchasingItems = cbl_.Items[1].Selected;
                bool isGrossQuantityUsed = cbl_.Items[2].Selected;
                bool enableRecordGeneralEntriesWithZero = cbl_.Items[3].Selected;
                bool ispasswordRequiredToReprintBarcode = cb_passwordAsk.Checked;
                string reprintBarcodeHashedPassword = null;
                if (cb_passwordAsk.Checked)
                {
                    if (gs != null)
                    {
                        if (txt_password.Text != gs.reprintBarcodeHashedPassword)
                        {
                            reprintBarcodeHashedPassword = BL.Configuration.Password.Encryption(txt_password.Text);
                        }
                        else
                        {
                            reprintBarcodeHashedPassword = gs.reprintBarcodeHashedPassword;
                        }
                    }
                    else
                    {
                        reprintBarcodeHashedPassword = BL.Configuration.Password.Encryption(txt_password.Text);
                    }
                }


                //************************ Second Screen *************************//
                DateTime? nextRepeatedCloseDate = txt_repeatedCloseDate.SelectedDate;
                int closeEvery = int.Parse(numeric_closeEvery.Text);
                int closeEveryTimeUnitId = int.Parse(ddl_units.SelectedValue);
                DateTime? yearlyCloseDate = txt_yearlyCloseDate.SelectedDate;
                string addToClosedPeriodHashedPassword = "";
                if (gs != null)
                {
                    if (txt_password2.Text != gs.addToClosedPeriodHashedPassword)
                    {
                        addToClosedPeriodHashedPassword = BL.Configuration.Password.Encryption(txt_password2.Text);
                    }
                    else
                    {
                        addToClosedPeriodHashedPassword = gs.addToClosedPeriodHashedPassword;
                    }
                }
                else
                {
                    addToClosedPeriodHashedPassword = BL.Configuration.Password.Encryption(txt_password2.Text);
                }
                int inventoryAccountId = int.Parse(rcb_inventoryAccount.SelectedValue);
                int costOfGoodSoldAccountId = int.Parse(rcb_costOfGoodSold.SelectedValue);
                int incomeAccountId = int.Parse(rcb_incomeAccount.SelectedValue);
                int vendorAccountId = int.Parse(rcb_vendorAccount.SelectedValue);
                int customerAccountId = int.Parse(rcb_customerAccount.SelectedValue);

                //*********************** Third Screen ****************************//
                int salesTaxSystemId = int.Parse(rbl_salesTaxSystem.SelectedValue);
                string taxNameOneName = txt_taxName1.Text;
                decimal taxRateOnePersentage = decimal.Parse(numeric_taxName1.Text);
                string taxNameTwoName = null;
                decimal taxRateTwoPersentage = 0;

                if (txt_taxName2.Text != "")
                {
                    taxNameTwoName = txt_taxName2.Text;
                    taxRateTwoPersentage = decimal.Parse(numeric_taxName2.Text);
                }

                string taxNameThreeName = null;
                decimal taxRateThreePersentage = 0;

                if (txt_taxName3.Text != "")
                {
                    taxNameThreeName = txt_taxName3.Text;
                    taxRateThreePersentage = decimal.Parse(numeric_taxName3.Text);
                }

                bool isItemHasSalesTaxOne = false;
                bool isItemHasSalesTaxTwo = false;
                bool isItemHasSalesTaxThree = false;

                if (rbl_defaultTaxRate.SelectedIndex == 1)
                {
                    isItemHasSalesTaxOne = true;
                }

                if (rbl_defaultTaxRate.SelectedIndex == 2)
                {
                    isItemHasSalesTaxTwo = true;
                }

                if (rbl_defaultTaxRate.SelectedIndex == 3)
                {
                    isItemHasSalesTaxThree = true;
                }

                bool isItemPriceAreTaxInclusive = cb_taxInclusive.Checked;
                bool enableSalesTax = cb_enableSalesTax.Checked;
                int? SalesTaxPaymentBasedId = null;
                int? SalesTaxPaymentPeriodId = null;

                if (cb_enableSalesTax.Checked)
                {
                    SalesTaxPaymentBasedId = int.Parse(rbl_salesTaxBasedOn.SelectedValue);
                    SalesTaxPaymentPeriodId = int.Parse(rbl_salesTaxPaid.SelectedValue);
                }

                bool EnableIncomeTax = cb_enableIncomeTax.Checked;
                int? IncomeTaxPaymentBasedId = null;
                int? IncomeTaxPaymentPeriodId = null;

                if (cb_enableIncomeTax.Checked)
                {
                    IncomeTaxPaymentBasedId = int.Parse(rbl_incomeTaxBasedOn.SelectedValue);
                    IncomeTaxPaymentPeriodId = int.Parse(rbl_incomeTaxPaid.SelectedValue);
                }
                int? empty=null;
                status = BL.Configuration.cls_GeneralSetting.UpdateGeneralSetting(isWACalculatedPerStore, isItemAllocatedByLocationOnStore,
                    isNegativeStockAllowed, isReorderPointUsedInPurchasingItems, isGrossQuantityUsed, enableRecordGeneralEntriesWithZero,
                    ispasswordRequiredToReprintBarcode, reprintBarcodeHashedPassword, nextRepeatedCloseDate, closeEvery, closeEveryTimeUnitId,
                    yearlyCloseDate, addToClosedPeriodHashedPassword, inventoryAccountId, costOfGoodSoldAccountId, incomeAccountId, vendorAccountId,
                    customerAccountId, salesTaxSystemId, taxNameOneName, taxRateOnePersentage, taxNameTwoName, taxRateTwoPersentage, taxNameThreeName,
                    taxRateThreePersentage, isItemHasSalesTaxOne, isItemHasSalesTaxTwo, isItemHasSalesTaxThree, isItemPriceAreTaxInclusive, enableSalesTax,
                    SalesTaxPaymentBasedId, SalesTaxPaymentPeriodId, EnableIncomeTax, IncomeTaxPaymentBasedId, IncomeTaxPaymentPeriodId,chk_apply.Checked ,
                    Numeric_Saturday.Text==""?  0: decimal.Parse(  Numeric_Saturday.Text ), Numeric_Sunday.Text==""?  0: decimal.Parse(Numeric_Sunday.Text ),
                     Numeric_Monday.Text==""?  0: decimal.Parse(Numeric_Monday.Text ), Numeric_Tuesday.Text==""?  0: decimal.Parse(Numeric_Tuesday.Text ),
                    Numeric_Wendesday.Text==""?  0: decimal.Parse(Numeric_Wendesday.Text ) ,  Numeric_Thursday.Text==""?  0: decimal.Parse(Numeric_Thursday.Text ) 
                    , Numeric_Friday .Text==""?  0: decimal.Parse(Numeric_Friday.Text ) ,rcb_currency.SelectedValue==""? empty:int.Parse(rcb_currency.SelectedValue),int.Parse(Session["UserID"].ToString()), out foundError);

                LoadData();

                UI.ShowSuccessMessage1(this, foundError, status);

            }
            else
            {
                foundError.Add(1);
                UI.ShowSuccessMessage1(this, foundError, false);
            }
        }

        protected void rbl_salesTaxSystem_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadRadioButtonList();
        }

        protected void txt_taxName1_TextChanged(object sender, EventArgs e)
        {
            CheckTaxNames();
        }

        

        protected void txt_taxName2_TextChanged(object sender, EventArgs e)
        {
            CheckTaxNames();
        }

        protected void txt_taxName3_TextChanged(object sender, EventArgs e)
        {
            CheckTaxNames();
        }

        private bool CheckTaxNames()
        {
            bool valid = true;

            if (txt_taxName1.Text != "" && (txt_taxName1.Text == txt_taxName2.Text || txt_taxName1.Text == txt_taxName3.Text))
            {
                lbl_error1.Visible = true;
                valid = false;
            }
            else
            {
                lbl_error1.Visible = false;
            }

            if (txt_taxName2.Text != "" && (txt_taxName2.Text == txt_taxName1.Text || txt_taxName2.Text == txt_taxName3.Text))
            {
                lbl_error2.Visible = true;
                valid = false;
            }
            else
            {
                lbl_error2.Visible = false;
            }

            if (txt_taxName3.Text != "" && (txt_taxName3.Text == txt_taxName1.Text || txt_taxName3.Text == txt_taxName2.Text))
            {
                lbl_error3.Visible = true;
                valid = false;
            }
            else
            {
                lbl_error3.Visible = false;
            }

            return valid;
        }

        protected void chk_apply_CheckedChanged(object sender, EventArgs e)
        {
            if (chk_apply.Checked)
            {
                Numeric_Friday.Enabled = true;
              Numeric_Monday.Enabled = true;
      Numeric_Saturday.Enabled = true;
              Numeric_Sunday.Enabled = true;
          Numeric_Thursday.Enabled = true;
      Numeric_Tuesday.Enabled = true;
              Numeric_Wendesday.Enabled = true;
              rcb_currency.Enabled = true;
            }

            else { 
               Numeric_Friday.Enabled = false  ;
              Numeric_Monday.Enabled = false ;
      Numeric_Saturday.Enabled =  false;
              Numeric_Sunday.Enabled =false  ;
          Numeric_Thursday.Enabled =  false;
      Numeric_Tuesday.Enabled =false  ;
              Numeric_Wendesday.Enabled =false  ; rcb_currency.Enabled = false ;
   Numeric_Friday.Text = ""  ;
              Numeric_Monday.Text = "" ;
      Numeric_Saturday. Text=  "";
              Numeric_Sunday.Text =""  ;
          Numeric_Thursday.Text =  "";
      Numeric_Tuesday. Text=""  ;
              Numeric_Wendesday.Text =""  ; rcb_currency.SelectedValue  = "" ;
              rcb_currency.Text = Lang.select_currency;


            }
        }

    }
}