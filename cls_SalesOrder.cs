﻿using BL.Purchasing;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Data.Entity;

namespace BL.Sales
{
    public class cls_SalesOrder
    {
        public static DAL.Flow_OrderTemplate SelectSalesOrder(int? orderTemplateID, int typeID)
        {
            DAL.Flow_OrderTemplate order;

            using (DAL.inventoryEntities db = new DAL.inventoryEntities())
            {
                order = db.Flow_OrderTemplate.Include("Flow_OrderTemplateItem")
                                             .Include("Flow_OrderTemplateAttachment")
                                             .Include("Flow_OrderTemplateItem.Data_ItemData")
                                             .Include("Setup_Address")
                                             .Include("Setup_Address2")
                                             .Include("Flow_OrderTemplateCustomField")
                                             .Include("Flow_OrderTemplateCustomField.Setup_CustomField")
                                             .Include("Flow_OrderTemplateCharg")
                                             .Include("Flow_OrderTemplateCharg.Setup_Charge")
                                             .Include("Flow_OrderTemplateTax")
                                             .Include("Flow_OrderTemplateTax.Setup_TaxItem")
                                             .Include("Flow_OrderTemplateDiscount")
                                             .Include("Flow_OrderTemplateDiscount.Setup_Discount")
                                             .Include("Flow_OrderTemplateSalesPerson")
                                             .Include("Flow_OrderTemplateSalesPerson.Setup_SalesPerson")
                                             .Include("Setup_MessageTitle")
                                             .Include("Setup_Currency")
                                             .Include("Flow_OrderTemplateItem.Setup_UOM")
                                             .Include("Flow_OrderTemplateItem.Static_Country")

                                             .Where(a => a.OrderTemplateId == orderTemplateID && a.TemplateTypeId == typeID && a.IsDeleted == false).SingleOrDefault();
            }

            return order;
        }

        public static Boolean AddSalesOrder(int templateTypeID,
                                            string templateCode,
                                            string paperTemplateCode,
                                            bool isHold,
                                            int? requestedBy,
                                            DateTime? requestDate,
                                            int? departmentID,
                                            int? vendorID,
                                            int? vendorAddressID,
                                            int? customerID,
                                            int? shipmentAddressID,
                                            int? billingAddressID,
                                            int? customerAddressID,
                                            int? shipmentMethodID,
                                            int? currencyID,
                                            bool isCurrencyPerItem,
                                            decimal deduction,
                                            bool isDeductionPercentage,
                                            int? taxItemID,
                                            decimal noneInventoryCost,
                                            bool isPercentageNonInventoryCost,
                                            int? paymentMethodID,
                                            int? paymentTermID,
                                            DateTime? dateOfDelivery,
                                            bool isDeliveryDatePerItem,
                                            DateTime? paymentDate,
                                            int? messageTitleID,
                                            string message,
                                            string notes,
                                            List<DAL.Flow_OrderTemplateAttachment> attachment,
                                            List<OrderedItems> orderedItems,
                                            List<OrderedItems> chargesList,
                                            List<OrderedItems> taxList,
                                            List<OrderedItems> discountList,
                                            List<DAL.Setup_SalesPerson> salesPersonList,
                                            List<BL.Data.ItemCustomFieldData> customFields,
                                            DAL.Setup_Address newShipAddress,
                                            DAL.Setup_Address newBillAddress,
                                            int userID,
                                            out List<int> errorList,
                                            out int orderID)
        {
            Boolean success = false;
            List<int> founderrorList = new List<int>();

            using (DAL.inventoryEntities db = new DAL.inventoryEntities())
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    //Checking for vendor and customer addresses and add new if not exist
                    int? _vendorAddressID = null;
                    int? _customerAddressID = null;

                    int? _shipAddressID = null;
                    int? _billAddressID = null;

                    if (shipmentAddressID == null && newShipAddress != null)
                    {
                        DAL.Setup_Address Address = new DAL.Setup_Address()
                        {
                            CountryId = newShipAddress.CountryId,
                            CityId = newShipAddress.CityId,
                            GovernorateId = newShipAddress.GovernorateId,
                            NeighborhoodId = newShipAddress.NeighborhoodId,
                            AddressLine = newShipAddress.AddressLine,
                            BuildingNumber = newShipAddress.BuildingNumber,
                            ZipCode = newShipAddress.ZipCode,
                        };

                        db.Setup_Address.Add(Address);
                        db.SaveChanges();

                        _shipAddressID = Address.AddressId;
                    }
                    else
                    {
                        _shipAddressID = shipmentAddressID;
                    }

                    if (billingAddressID == null && newBillAddress != null)
                    {
                        DAL.Setup_Address Address = new DAL.Setup_Address()
                        {
                            CountryId = newBillAddress.CountryId,
                            CityId = newBillAddress.CityId,
                            GovernorateId = newBillAddress.GovernorateId,
                            NeighborhoodId = newBillAddress.NeighborhoodId,
                            AddressLine = newBillAddress.AddressLine,
                            BuildingNumber = newBillAddress.BuildingNumber,
                            ZipCode = newBillAddress.ZipCode,
                        };

                        db.Setup_Address.Add(Address);
                        db.SaveChanges();

                        _billAddressID = Address.AddressId;
                    }
                    else
                    {
                        _billAddressID = billingAddressID;
                    }

                    //Adding new order template
                    DAL.Flow_OrderTemplate Order = new DAL.Flow_OrderTemplate()
                    {
                        TemplateTypeId = templateTypeID,
                        TemplateCode = templateCode,
                        PaperTemplateCode = paperTemplateCode,
                        IsHold = isHold,
                        RequestedByEmployeeId = requestedBy,
                        RequestDateTime = requestDate,
                        DepartmentId = departmentID,
                        VendorId = vendorID,
                        VendorAddressId = _vendorAddressID,
                        CustomerId = customerID,
                        CustomerAddressId = _customerAddressID,
                        ShipmentMethodId = shipmentMethodID,
                        CurrencyId = currencyID,
                        TaxItemId = taxItemID,
                        NoneInventoryCost = noneInventoryCost,
                        IsPersentageNonInventoryCost = isPercentageNonInventoryCost,
                        PaymentMethodId = paymentMethodID,
                        PaymentTearmId = paymentTermID,
                        DateOfDelivery = (isDeliveryDatePerItem) ? null : dateOfDelivery,
                        IsDeliveryDatePerItem = isDeliveryDatePerItem,
                        PaymentDate = paymentDate,
                        MessageTitleId = messageTitleID,
                        Message = message,
                        Notes = notes,
                        ShipmentAddressId = shipmentAddressID,
                        BillingAddressId = billingAddressID,
                        IsCurrencyPerItem = isCurrencyPerItem,
                        Deduction = deduction,
                        IsDeductionPercentage = isDeductionPercentage,
                        CreatedUserId = userID,
                        CreationDateTime = DateTime.Now,
                        IsDeleted = false
                    };

                    BL.Setup.cls_Setup.UpdateCode("Sales Order", userID);

                    db.Flow_OrderTemplate.Add(Order);
                    db.SaveChanges();

                    //Adding order template status
                    DAL.Flow_OrderTemplateStatus newOrderStatus = new DAL.Flow_OrderTemplateStatus
                    {
                        OrderTemplateId = Order.OrderTemplateId,
                        TemplateStatusId = 1,
                        LastUpdatedBy = userID,
                        LastUpdatedDateTime = DateTime.Now
                    };

                    db.Flow_OrderTemplateStatus.Add(newOrderStatus);
                    db.SaveChanges();

                    //Adding order items
                    if (orderedItems != null)
                    {
                        foreach (OrderedItems item in orderedItems)
                        {
                            DAL.Flow_OrderTemplateItem newOrderItem = new DAL.Flow_OrderTemplateItem
                            {
                                OrderTemplateId = Order.OrderTemplateId,
                                ItemId = item.itemID,
                                ItemUOMId = item.salesUOMID,
                                ItemQuantity = decimal.Parse(item.quantity.ToString()),
                                UnitPrice = decimal.Parse(item.unitPrice.ToString()),
                                DeliveryDate = (isDeliveryDatePerItem) ? item.deliveryDate : dateOfDelivery,
                                CustomerId = item.customerID,
                                CountryOfOrignID = item.classID,
                                Description = item.Description,
                                Discount = (decimal)item.discountPerItem,
                                IsDeleted = item.isDeleted,
                                CreatedUserId = userID,
                                CreationDateTime = DateTime.Now,
                                CurrencyId = item.currency
                            };

                            db.Flow_OrderTemplateItem.Add(newOrderItem);
                            db.SaveChanges();

                            //Adding order items status
                            DAL.Flow_OrderItemStatus newOrderItemStatus = new DAL.Flow_OrderItemStatus
                            {
                                OrderTemplateItemId = newOrderItem.OrderTemplateItemId,
                                ItemStatusId = 1,
                                LastUpdatedBy = userID,
                                LastUpdatedDateTime = DateTime.Now
                            };
                            db.Flow_OrderItemStatus.Add(newOrderItemStatus);
                            db.SaveChanges();
                        }
                    }

                    //Adding charges list
                    if (chargesList != null)
                    {
                        foreach (OrderedItems item in chargesList)
                        {
                            DAL.Flow_OrderTemplateCharg newOrderItem = new DAL.Flow_OrderTemplateCharg
                            {
                                OrderTemplateId = Order.OrderTemplateId,
                                ChargeId = item.itemID,
                                Description = item.Description,
                                IsDeleted = item.isDeleted,
                                CreatedUserId = userID,
                                CreationDateTime = DateTime.Now
                            };

                            db.Flow_OrderTemplateCharg.Add(newOrderItem);
                            db.SaveChanges();
                        }
                    }

                    //Adding tax list
                    if (taxList != null)
                    {
                        foreach (OrderedItems item in taxList)
                        {
                            DAL.Flow_OrderTemplateTax newOrderItem = new DAL.Flow_OrderTemplateTax
                            {
                                OrderTemplateId = Order.OrderTemplateId,
                                TaxId = item.itemID,
                                Description = item.Description,
                                IsDeleted = item.isDeleted,
                                CreatedUserId = userID,
                                CreationDateTime = DateTime.Now
                            };

                            db.Flow_OrderTemplateTax.Add(newOrderItem);
                            db.SaveChanges();
                        }
                    }

                    //Adding discount list
                    if (discountList != null)
                    {
                        foreach (OrderedItems item in discountList)
                        {
                            DAL.Flow_OrderTemplateDiscount newOrderItem = new DAL.Flow_OrderTemplateDiscount
                            {
                                OrderTemplateId = Order.OrderTemplateId,
                                DiscountId = item.itemID,
                                Description = item.Description,
                                IsDeleted = item.isDeleted,
                                CreatedUserId = userID,
                                CreationDateTime = DateTime.Now
                            };

                            db.Flow_OrderTemplateDiscount.Add(newOrderItem);
                            db.SaveChanges();
                        }
                    }

                    //Adding Sales person list
                    if (salesPersonList != null)
                    {
                        foreach (DAL.Setup_SalesPerson item in salesPersonList)
                        {
                            DAL.Flow_OrderTemplateSalesPerson newSalesPerson = new DAL.Flow_OrderTemplateSalesPerson
                            {
                                OrderTemplateId = Order.OrderTemplateId,
                                SalesPersonId = item.SalesPersonId,
                                IsDeleted = false,
                                CreatedUserId = userID,
                                CreationDateTime = DateTime.Now
                            };

                            db.Flow_OrderTemplateSalesPerson.Add(newSalesPerson);
                            db.SaveChanges();
                        }
                    }

                    //Adding custom fields
                    if (customFields != null)
                    {
                        foreach (BL.Data.ItemCustomFieldData c in customFields)
                        {
                            DAL.Flow_OrderTemplateCustomField OTCF = new DAL.Flow_OrderTemplateCustomField
                            {
                                OrderTemplateId = Order.OrderTemplateId,
                                CustomFieldId = c.CustomFieldId,
                                CustomFieldValue = c.CustomFieldValue,
                                IsDeleted = false,
                                CreatedUserId = userID,
                                CreationDateTime = DateTime.Now
                            };

                            db.Flow_OrderTemplateCustomField.Add(OTCF);
                            db.SaveChanges();
                        }
                    }

                    //Adding order attachments
                    if (attachment.Count() > 0)
                    {
                        foreach (var file in attachment)
                        {
                            DAL.Flow_OrderTemplateAttachment newAttachment = new DAL.Flow_OrderTemplateAttachment()
                            {
                                OrderTemplateId = Order.OrderTemplateId,
                                AttachmentName = file.AttachmentName,
                                AttachmentPath = file.AttachmentPath,
                                CreatedUserId = userID,
                                CreationDateTime = DateTime.Now,
                                IsDeleted = false
                            };

                            db.Flow_OrderTemplateAttachment.Add(newAttachment);
                            db.SaveChanges();
                        }
                    }

                    scope.Complete();
                    success = true;
                    founderrorList.Add(2);
                    orderID = Order.OrderTemplateId;
                }
            }


            errorList = founderrorList;
            return success;
        }


        public static Boolean UpdateSalesOrder(int? orderID,
                                                    string paperTemplateCode,
                                                    bool isHold,
                                                    int? requestedBy,
                                                    DateTime? requestDate,
                                                    int? departmentID,
                                                    int? vendorID,
                                                    int? vendorAddressID,
                                                    int? customerID,
                                                    int? shipmentAddressID,
                                                    int? billingAddressID,
                                                    int? customerAddressID,
                                                    int? shipmentMethodID,
                                                    int? currencyID,
                                                    bool isCurrencyPerItem,
                                                    decimal deduction,
                                                    bool isDeductionPercentage,
                                                    int? taxItemID,
                                                    decimal noneInventoryCost,
                                                    bool isPercentageNonInventoryCost,
                                                    int? paymentMethodID,
                                                    int? paymentTermID,
                                                    DateTime? dateOfDelivery,
                                                    bool isDeliveryDatePerItem,
                                                    DateTime? paymentDate,
                                                    int? messageTitleID,
                                                    string message,
                                                    string notes,
                                                    List<DAL.Flow_OrderTemplateAttachment> attachment,
                                                    List<OrderedItems> orderedItems,
                                                    List<OrderedItems> chargesList,
                                                    List<OrderedItems> taxList,
                                                    List<OrderedItems> discountList,
                                                    List<DAL.Setup_SalesPerson> salesPersonList,
                                                    List<BL.Data.ItemCustomFieldData> customFields,
                                                    DAL.Setup_Address newShipAddress,
                                                    DAL.Setup_Address newBillAddress,
                                                    int userID,
                                                    out List<int> errorList)
        {
            Boolean success = false;
            List<int> founderrorList = new List<int>();
            bool isChanged = false;

            using (DAL.inventoryEntities db = new DAL.inventoryEntities())
            {
                DAL.Flow_OrderTemplate databaseOrder = db.Flow_OrderTemplate.Where(a => a.OrderTemplateId == orderID && a.IsDeleted == false).SingleOrDefault();

                using (TransactionScope scope = new TransactionScope())
                {
                    //Checking for vendor and customer addresses and add new if not exist
                    int? _vendorAddressID = null;
                    int? _customerAddressID = null;

                    int? _shipAddressID = null;
                    int? _billAddressID = null;

                    if (shipmentAddressID == null && newShipAddress != null)
                    {
                        DAL.Setup_Address Address = new DAL.Setup_Address()
                        {
                            CountryId = newShipAddress.CountryId,
                            CityId = newShipAddress.CityId,
                            GovernorateId = newShipAddress.GovernorateId,
                            NeighborhoodId = newShipAddress.NeighborhoodId,
                            AddressLine = newShipAddress.AddressLine,
                            BuildingNumber = newShipAddress.BuildingNumber,
                            ZipCode = newShipAddress.ZipCode,
                        };

                        db.Setup_Address.Add(Address);
                        db.SaveChanges();

                        _shipAddressID = Address.AddressId;
                    }
                    else
                    {
                        _shipAddressID = shipmentAddressID;
                    }

                    if (billingAddressID == null && newBillAddress != null)
                    {
                        DAL.Setup_Address Address = new DAL.Setup_Address()
                        {
                            CountryId = newBillAddress.CountryId,
                            CityId = newBillAddress.CityId,
                            GovernorateId = newBillAddress.GovernorateId,
                            NeighborhoodId = newBillAddress.NeighborhoodId,
                            AddressLine = newBillAddress.AddressLine,
                            BuildingNumber = newBillAddress.BuildingNumber,
                            ZipCode = newBillAddress.ZipCode,
                        };

                        db.Setup_Address.Add(Address);
                        db.SaveChanges();

                        _billAddressID = Address.AddressId;
                    }
                    else
                    {
                        _billAddressID = billingAddressID;
                    }

                    if (databaseOrder.PaperTemplateCode != paperTemplateCode)
                    {
                        BL.Setup.cls_Setup.Log(orderID, "Flow_OrderTemplate.PaperTemplateCode", databaseOrder.PaperTemplateCode, paperTemplateCode, userID);
                        databaseOrder.PaperTemplateCode = paperTemplateCode;
                        isChanged = true;
                    }

                    if (databaseOrder.IsHold != isHold)
                    {
                        BL.Setup.cls_Setup.Log(orderID, "Flow_OrderTemplate.IsHold", databaseOrder.IsHold.ToString(), isHold.ToString(), userID);
                        databaseOrder.IsHold = isHold;
                        isChanged = true;
                    }

                    if (databaseOrder.RequestedByEmployeeId != requestedBy)
                    {
                        BL.Setup.cls_Setup.Log(orderID, "Flow_OrderTemplate.RequestedByEmployeeId", databaseOrder.RequestedByEmployeeId.ToString(), requestedBy.ToString(), userID);
                        databaseOrder.RequestedByEmployeeId = requestedBy;
                        isChanged = true;
                    }

                    if (databaseOrder.RequestDateTime != requestDate)
                    {
                        BL.Setup.cls_Setup.Log(orderID, "Flow_OrderTemplate.RequestDateTime", databaseOrder.RequestDateTime.ToString(), requestDate.ToString(), userID);
                        databaseOrder.RequestDateTime = requestDate;
                        isChanged = true;
                    }

                    if (databaseOrder.DepartmentId != departmentID)
                    {
                        BL.Setup.cls_Setup.Log(orderID, "Flow_OrderTemplate.DepartmentId", databaseOrder.DepartmentId.ToString(), departmentID.ToString(), userID);
                        databaseOrder.DepartmentId = departmentID;
                        isChanged = true;
                    }

                    if (databaseOrder.VendorId != vendorID)
                    {
                        BL.Setup.cls_Setup.Log(orderID, "Flow_OrderTemplate.VendorId", databaseOrder.VendorId.ToString(), vendorID.ToString(), userID);
                        databaseOrder.VendorId = vendorID;
                        isChanged = true;
                    }

                    if (databaseOrder.VendorAddressId != _vendorAddressID)
                    {
                        BL.Setup.cls_Setup.Log(orderID, "Flow_OrderTemplate.VendorAddressId", databaseOrder.VendorAddressId.ToString(), _vendorAddressID.ToString(), userID);
                        databaseOrder.VendorAddressId = _vendorAddressID;
                        isChanged = true;
                    }

                    if (databaseOrder.CustomerId != customerID)
                    {
                        BL.Setup.cls_Setup.Log(orderID, "Flow_OrderTemplate.CustomerId", databaseOrder.CustomerId.ToString(), customerID.ToString(), userID);
                        databaseOrder.CustomerId = customerID;
                        isChanged = true;
                    }

                    if (databaseOrder.CustomerAddressId != _customerAddressID)
                    {
                        BL.Setup.cls_Setup.Log(orderID, "Flow_OrderTemplate.CustomerAddressId", databaseOrder.CustomerAddressId.ToString(), _customerAddressID.ToString(), userID);
                        databaseOrder.CustomerAddressId = _customerAddressID;
                        isChanged = true;
                    }

                    if (databaseOrder.ShipmentMethodId != shipmentMethodID)
                    {
                        BL.Setup.cls_Setup.Log(orderID, "Flow_OrderTemplate.ShipmentMethodId", databaseOrder.ShipmentMethodId.ToString(), shipmentMethodID.ToString(), userID);
                        databaseOrder.ShipmentMethodId = shipmentMethodID;
                        isChanged = true;
                    }

                    if (databaseOrder.CurrencyId != currencyID)
                    {
                        BL.Setup.cls_Setup.Log(orderID, "Flow_OrderTemplate.CurrencyId", databaseOrder.CurrencyId.ToString(), currencyID.ToString(), userID);
                        databaseOrder.CurrencyId = currencyID;
                        isChanged = true;
                    }

                    if (databaseOrder.TaxItemId != taxItemID)
                    {
                        BL.Setup.cls_Setup.Log(orderID, "Flow_OrderTemplate.TaxItemId", databaseOrder.TaxItemId.ToString(), taxItemID.ToString(), userID);
                        databaseOrder.TaxItemId = taxItemID;
                        isChanged = true;
                    }

                    if (databaseOrder.NoneInventoryCost != noneInventoryCost)
                    {
                        BL.Setup.cls_Setup.Log(orderID, "Flow_OrderTemplate.NoneInventoryCost", databaseOrder.NoneInventoryCost.ToString(), noneInventoryCost.ToString(), userID);
                        databaseOrder.NoneInventoryCost = noneInventoryCost;
                        isChanged = true;
                    }

                    if (databaseOrder.IsPersentageNonInventoryCost != isPercentageNonInventoryCost)
                    {
                        BL.Setup.cls_Setup.Log(orderID, "Flow_OrderTemplate.IsPersentageNonInventoryCost", databaseOrder.IsPersentageNonInventoryCost.ToString(), isPercentageNonInventoryCost.ToString(), userID);
                        databaseOrder.IsPersentageNonInventoryCost = isPercentageNonInventoryCost;
                        isChanged = true;
                    }

                    if (databaseOrder.PaymentMethodId != paymentMethodID)
                    {
                        BL.Setup.cls_Setup.Log(orderID, "Flow_OrderTemplate.PaymentMethodId", databaseOrder.PaymentMethodId.ToString(), paymentMethodID.ToString(), userID);
                        databaseOrder.PaymentMethodId = paymentMethodID;
                        isChanged = true;
                    }

                    if (databaseOrder.PaymentTearmId != paymentTermID)
                    {
                        BL.Setup.cls_Setup.Log(orderID, "Flow_OrderTemplate.PaymentTearmId", databaseOrder.PaymentTearmId.ToString(), paymentTermID.ToString(), userID);
                        databaseOrder.PaymentTearmId = paymentTermID;
                        isChanged = true;
                    }

                    if (!isDeliveryDatePerItem)
                    {
                        if (databaseOrder.DateOfDelivery != dateOfDelivery)
                        {
                            BL.Setup.cls_Setup.Log(orderID, "Flow_OrderTemplate.DateOfDelivery", databaseOrder.DateOfDelivery.ToString(), dateOfDelivery.ToString(), userID);
                            databaseOrder.DateOfDelivery = dateOfDelivery;
                            isChanged = true;
                        }
                    }

                    if (databaseOrder.IsDeliveryDatePerItem != isDeliveryDatePerItem)
                    {
                        BL.Setup.cls_Setup.Log(orderID, "Flow_OrderTemplate.IsDeliveryDatePerItem", databaseOrder.IsDeliveryDatePerItem.ToString(), isDeliveryDatePerItem.ToString(), userID);
                        databaseOrder.IsDeliveryDatePerItem = isDeliveryDatePerItem;
                        isChanged = true;
                    }

                    if (databaseOrder.PaymentDate != paymentDate)
                    {
                        BL.Setup.cls_Setup.Log(orderID, "Flow_OrderTemplate.PaymentDate", databaseOrder.PaymentDate.ToString(), paymentDate.ToString(), userID);
                        databaseOrder.PaymentDate = paymentDate;
                        isChanged = true;
                    }

                    if (databaseOrder.MessageTitleId != messageTitleID)
                    {
                        BL.Setup.cls_Setup.Log(orderID, "Flow_OrderTemplate.MessageTitleId", databaseOrder.MessageTitleId.ToString(), messageTitleID.ToString(), userID);
                        databaseOrder.MessageTitleId = messageTitleID;
                        isChanged = true;
                    }

                    if (databaseOrder.Message != message)
                    {
                        BL.Setup.cls_Setup.Log(orderID, "Flow_OrderTemplate.Message", databaseOrder.Message, message, userID);
                        databaseOrder.Message = message;
                        isChanged = true;
                    }

                    if (databaseOrder.Notes != notes)
                    {
                        BL.Setup.cls_Setup.Log(orderID, "Flow_OrderTemplate.Notes", databaseOrder.Notes, notes, userID);
                        databaseOrder.Notes = notes;
                        isChanged = true;
                    }

                    if (databaseOrder.ShipmentAddressId != _shipAddressID)
                    {
                        BL.Setup.cls_Setup.Log(orderID, "Flow_OrderTemplate.ShipmentAddressId", databaseOrder.ShipmentAddressId.ToString(), _shipAddressID.ToString(), userID);
                        databaseOrder.ShipmentAddressId = _shipAddressID;
                        isChanged = true;
                    }

                    if (databaseOrder.BillingAddressId != _billAddressID)
                    {
                        BL.Setup.cls_Setup.Log(orderID, "Flow_OrderTemplate.BillingAddressId", databaseOrder.BillingAddressId.ToString(), _billAddressID.ToString(), userID);
                        databaseOrder.BillingAddressId = _billAddressID;
                        isChanged = true;
                    }

                    if (databaseOrder.IsCurrencyPerItem != isCurrencyPerItem)
                    {
                        BL.Setup.cls_Setup.Log(orderID, "Flow_OrderTemplate.IsCurrencyPerItem", databaseOrder.IsCurrencyPerItem.ToString(), isCurrencyPerItem.ToString(), userID);
                        databaseOrder.IsCurrencyPerItem = isCurrencyPerItem;
                        isChanged = true;
                    }

                    if (databaseOrder.Deduction != deduction)
                    {
                        BL.Setup.cls_Setup.Log(orderID, "Flow_OrderTemplate.Deduction", databaseOrder.Deduction.ToString(), deduction.ToString(), userID);
                        databaseOrder.Deduction = deduction;
                        isChanged = true;
                    }

                    if (databaseOrder.IsDeductionPercentage != isDeductionPercentage)
                    {
                        BL.Setup.cls_Setup.Log(orderID, "Flow_OrderTemplate.IsDeductionPercentage", databaseOrder.IsDeductionPercentage.ToString(), isDeductionPercentage.ToString(), userID);
                        databaseOrder.IsDeductionPercentage = isDeductionPercentage;
                        isChanged = true;
                    }

                    //Editing order template
                    if (isChanged)
                    {
                        databaseOrder.ModifiedUserId = userID;
                        databaseOrder.ModificationDateTime = DateTime.Now;
                        db.SaveChanges();
                    };

                    //Editing order items
                    if (orderedItems != null)
                    {
                        foreach (OrderedItems item in orderedItems)
                        {
                            if (item.itemOrderID > 0)
                            {
                                DAL.Flow_OrderTemplateItem orderItem = db.Flow_OrderTemplateItem.Where(a => a.OrderTemplateItemId == item.itemOrderID).SingleOrDefault();

                                if (item.isDeleted != orderItem.IsDeleted)
                                {
                                    orderItem.IsDeleted = item.isDeleted;
                                    orderItem.DeletedUserId = userID;
                                    orderItem.DeleteDateTime = DateTime.Now;

                                    db.SaveChanges();
                                }
                                else
                                {
                                    bool isOrderItemChanged = false;

                                    if (orderItem.ItemId != item.itemID)
                                    {
                                        BL.Setup.cls_Setup.Log(item.itemOrderID, "Flow_OrderTemplateItem.ItemId", orderItem.ItemId.ToString(), item.itemID.ToString(), userID);
                                        orderItem.ItemId = item.itemID;
                                        isOrderItemChanged = true;
                                    }

                                    if (orderItem.ItemUOMId != item.salesUOMID)
                                    {
                                        BL.Setup.cls_Setup.Log(item.itemOrderID, "Flow_OrderTemplateItem.ItemUOMId", orderItem.ItemUOMId.ToString(), item.salesUOMID.ToString(), userID);
                                        orderItem.ItemUOMId = item.salesUOMID;
                                        isOrderItemChanged = true;
                                    }

                                    if (orderItem.ItemQuantity != decimal.Parse(item.quantity.ToString()))
                                    {
                                        BL.Setup.cls_Setup.Log(item.itemOrderID, "Flow_OrderTemplateItem.ItemQuantity", orderItem.ItemQuantity.ToString(), item.quantity.ToString(), userID);
                                        orderItem.ItemQuantity = decimal.Parse(item.quantity.ToString());
                                        isOrderItemChanged = true;
                                    }

                                    if (orderItem.UnitPrice != decimal.Parse(item.unitPrice.ToString()))
                                    {
                                        BL.Setup.cls_Setup.Log(item.itemOrderID, "Flow_OrderTemplateItem.UnitPrice", orderItem.UnitPrice.ToString(), item.unitPrice.ToString(), userID);
                                        orderItem.UnitPrice = decimal.Parse(item.unitPrice.ToString());
                                        isOrderItemChanged = true;
                                    }

                                    if (isDeliveryDatePerItem)
                                    {
                                        if (orderItem.DeliveryDate != item.deliveryDate)
                                        {
                                            BL.Setup.cls_Setup.Log(item.itemOrderID, "Flow_OrderTemplateItem.DeliveryDate", orderItem.DeliveryDate.ToString(), item.deliveryDate.ToString(), userID);
                                            orderItem.DeliveryDate = item.deliveryDate;
                                            isOrderItemChanged = true;
                                        }
                                    }
                                    else
                                    {
                                        if (orderItem.DeliveryDate != dateOfDelivery)
                                        {
                                            BL.Setup.cls_Setup.Log(item.itemOrderID, "Flow_OrderTemplateItem.DeliveryDate", orderItem.DeliveryDate.ToString(), dateOfDelivery.ToString(), userID);
                                            orderItem.DeliveryDate = dateOfDelivery;
                                            isOrderItemChanged = true;
                                        }
                                    }

                                    if (orderItem.CustomerId != item.customerID)
                                    {
                                        BL.Setup.cls_Setup.Log(item.itemOrderID, "Flow_OrderTemplateItem.CustomerId", orderItem.CustomerId.ToString(), item.customerID.ToString(), userID);
                                        orderItem.CustomerId = item.customerID;
                                        isOrderItemChanged = true;
                                    }

                                    if (orderItem.ClassId != item.classID)
                                    {
                                        BL.Setup.cls_Setup.Log(item.itemOrderID, "Flow_OrderTemplateItem.ClassId", orderItem.ClassId.ToString(), item.classID.ToString(), userID);
                                        orderItem.ClassId = item.classID;
                                        isOrderItemChanged = true;
                                    }

                                    if (orderItem.CurrencyId != item.currency)
                                    {
                                        BL.Setup.cls_Setup.Log(item.itemOrderID, "Flow_OrderTemplateItem.CurrencyId", orderItem.CurrencyId.ToString(), item.currency.ToString(), userID);
                                        orderItem.CurrencyId = item.currency;
                                        isOrderItemChanged = true;
                                    }

                                    if (isOrderItemChanged)
                                    {
                                        orderItem.ModificationDateTime = DateTime.Now;
                                        orderItem.ModifiedUserId = userID;
                                        db.SaveChanges();
                                    }
                                }
                            }
                            else
                            {
                                DAL.Flow_OrderTemplateItem newOrderItem = new DAL.Flow_OrderTemplateItem
                                {
                                    OrderTemplateId = orderID,
                                    ItemId = item.itemID,
                                    ItemUOMId = item.salesUOMID,
                                    ItemQuantity = decimal.Parse(item.quantity.ToString()),
                                    UnitPrice = decimal.Parse(item.unitPrice.ToString()),
                                    DeliveryDate = (isDeliveryDatePerItem) ? item.deliveryDate : dateOfDelivery,
                                    CustomerId = item.customerID,
                                    ClassId = item.classID,
                                    CurrencyId = item.currency,
                                    IsDeleted = item.isDeleted,
                                    CreatedUserId = userID,
                                    CreationDateTime = DateTime.Now
                                };

                                db.Flow_OrderTemplateItem.Add(newOrderItem);
                                db.SaveChanges();

                                //Adding order items status
                                DAL.Flow_OrderItemStatus newOrderItemStatus = new DAL.Flow_OrderItemStatus
                                {
                                    OrderTemplateItemId = newOrderItem.OrderTemplateItemId,
                                    ItemStatusId = 1,
                                    LastUpdatedBy = userID,
                                    LastUpdatedDateTime = DateTime.Now
                                };
                                db.Flow_OrderItemStatus.Add(newOrderItemStatus);
                                db.SaveChanges();
                            }

                        }
                    }


                    //updating charges list
                    if (chargesList != null)
                    {
                        foreach (OrderedItems item in chargesList)
                        {
                            if (item.itemOrderID > 0)
                            {
                                DAL.Flow_OrderTemplateCharg charge = db.Flow_OrderTemplateCharg.Where(a => a.OrderTemplateChargId == item.itemOrderID).SingleOrDefault();

                                if (charge.IsDeleted != item.isDeleted)
                                {
                                    charge.IsDeleted = item.isDeleted;
                                    charge.DeletedUserId = userID;
                                    charge.DeleteDateTime = DateTime.Now;

                                    db.SaveChanges();
                                }
                                else
                                {
                                    bool ischargeChanged = false;

                                    if (charge.ChargeId != item.itemID)
                                    {
                                        BL.Setup.cls_Setup.Log(item.itemOrderID, "Flow_OrderTemplateCharg.ChargeId", charge.ChargeId.ToString(), item.itemID.ToString(), userID);
                                        charge.ChargeId = item.itemID;
                                        ischargeChanged = true;
                                    }

                                    if (charge.Description != item.Description)
                                    {
                                        BL.Setup.cls_Setup.Log(item.itemOrderID, "Flow_OrderTemplateCharg.Description", charge.Description, item.Description, userID);
                                        charge.Description = item.Description;
                                        ischargeChanged = true;
                                    }

                                    if (ischargeChanged)
                                    {
                                        charge.ModificationDateTime = DateTime.Now;
                                        charge.ModifiedUserId = userID;

                                        db.SaveChanges();
                                    }
                                }
                            }
                            else
                            {
                                DAL.Flow_OrderTemplateCharg newOrderItem = new DAL.Flow_OrderTemplateCharg
                                {
                                    OrderTemplateId = orderID,
                                    ChargeId = item.itemID,
                                    Description = item.Description,
                                    IsDeleted = item.isDeleted,
                                    CreatedUserId = userID,
                                    CreationDateTime = DateTime.Now
                                };

                                db.Flow_OrderTemplateCharg.Add(newOrderItem);
                                db.SaveChanges();
                            }
                        }
                    }


                    //updating tax list
                    if (taxList != null)
                    {
                        foreach (OrderedItems item in taxList)
                        {
                            if (item.itemOrderID > 0)
                            {
                                DAL.Flow_OrderTemplateTax tax = db.Flow_OrderTemplateTax.Where(a => a.OrderTemplateTaxId == item.itemOrderID).SingleOrDefault();

                                if (tax.IsDeleted != item.isDeleted)
                                {
                                    tax.IsDeleted = item.isDeleted;
                                    tax.DeletedUserId = userID;
                                    tax.DeleteDateTime = DateTime.Now;

                                    db.SaveChanges();
                                }
                                else
                                {
                                    bool istaxChanged = false;

                                    if (tax.TaxId != item.itemID)
                                    {
                                        BL.Setup.cls_Setup.Log(item.itemOrderID, "Flow_OrderTemplateTax.TaxId", tax.TaxId.ToString(), item.itemID.ToString(), userID);
                                        tax.TaxId = item.itemID;
                                        istaxChanged = true;
                                    }

                                    if (tax.Description != item.Description)
                                    {
                                        BL.Setup.cls_Setup.Log(item.itemOrderID, "Flow_OrderTemplateTax.Description", tax.Description, item.Description, userID);
                                        tax.Description = item.Description;
                                        istaxChanged = true;
                                    }

                                    if (istaxChanged)
                                    {
                                        tax.ModificationDateTime = DateTime.Now;
                                        tax.ModifiedUserId = userID;

                                        db.SaveChanges();
                                    }
                                }
                            }
                            else
                            {
                                DAL.Flow_OrderTemplateTax newOrderItem = new DAL.Flow_OrderTemplateTax
                                {
                                    OrderTemplateId = orderID,
                                    TaxId = item.itemID,
                                    Description = item.Description,
                                    IsDeleted = item.isDeleted,
                                    CreatedUserId = userID,
                                    CreationDateTime = DateTime.Now
                                };

                                db.Flow_OrderTemplateTax.Add(newOrderItem);
                                db.SaveChanges();
                            }
                        }
                    }

                    //updating discount list
                    if (discountList != null)
                    {
                        foreach (OrderedItems item in discountList)
                        {
                            if (item.itemOrderID > 0)
                            {
                                DAL.Flow_OrderTemplateDiscount discount = db.Flow_OrderTemplateDiscount.Where(a => a.OrderTemplateDiscountId == item.itemOrderID).SingleOrDefault();

                                if (discount.IsDeleted != item.isDeleted)
                                {
                                    discount.IsDeleted = item.isDeleted;
                                    discount.DeletedUserId = userID;
                                    discount.DeleteDateTime = DateTime.Now;

                                    db.SaveChanges();
                                }
                                else
                                {
                                    bool isdiscountChanged = false;

                                    if (discount.DiscountId != item.itemID)
                                    {
                                        BL.Setup.cls_Setup.Log(item.itemOrderID, "Flow_OrderTemplateDiscount.DiscountId", discount.DiscountId.ToString(), item.itemID.ToString(), userID);
                                        discount.DiscountId = item.itemID;
                                        isdiscountChanged = true;
                                    }

                                    if (discount.Description != item.Description)
                                    {
                                        BL.Setup.cls_Setup.Log(item.itemOrderID, "Flow_OrderTemplateDiscount.Description", discount.Description, item.Description, userID);
                                        discount.Description = item.Description;
                                        isdiscountChanged = true;
                                    }

                                    if (isdiscountChanged)
                                    {
                                        discount.ModificationDateTime = DateTime.Now;
                                        discount.ModifiedUserId = userID;

                                        db.SaveChanges();
                                    }
                                }
                            }
                            else
                            {
                                DAL.Flow_OrderTemplateDiscount newOrderItem = new DAL.Flow_OrderTemplateDiscount
                                {
                                    OrderTemplateId = orderID,
                                    DiscountId = item.itemID,
                                    Description = item.Description,
                                    IsDeleted = item.isDeleted,
                                    CreatedUserId = userID,
                                    CreationDateTime = DateTime.Now
                                };

                                db.Flow_OrderTemplateDiscount.Add(newOrderItem);
                                db.SaveChanges();
                            }
                        }
                    }

                    //updating sales person list
                    if (salesPersonList != null)
                    {
                        foreach (DAL.Setup_SalesPerson item in salesPersonList)
                        {
                            if (item.ModifiedUserId > 0)
                            {
                                DAL.Flow_OrderTemplateSalesPerson salesPerson = db.Flow_OrderTemplateSalesPerson.Where(a => a.OrderTemplateSalesPersonId == item.ModifiedUserId).SingleOrDefault();

                                if (salesPerson.IsDeleted != item.IsDeleted)
                                {
                                    salesPerson.IsDeleted = (bool)item.IsDeleted;
                                    salesPerson.DeleteUserId = userID;
                                    salesPerson.DeleteDateTime = DateTime.Now;

                                    db.SaveChanges();
                                }
                                else
                                {
                                    bool issalesPersonChanged = false;

                                    if (salesPerson.SalesPersonId != item.SalesPersonId)
                                    {
                                        BL.Setup.cls_Setup.Log(item.ModifiedUserId, "Flow_OrderTemplateSalesPerson.SalesPersonId", salesPerson.SalesPersonId.ToString(), item.SalesPersonId.ToString(), userID);
                                        salesPerson.SalesPersonId = item.SalesPersonId;
                                        issalesPersonChanged = true;
                                    }

                                    if (issalesPersonChanged)
                                    {
                                        salesPerson.ModificationDateTime = DateTime.Now;
                                        salesPerson.ModifiedUserId = userID;

                                        db.SaveChanges();
                                    }
                                }
                            }
                            else
                            {
                                DAL.Flow_OrderTemplateSalesPerson newSalesPerson = new DAL.Flow_OrderTemplateSalesPerson
                                {
                                    OrderTemplateId = orderID,
                                    SalesPersonId = item.SalesPersonId,
                                    IsDeleted = (bool)item.IsDeleted,
                                    CreatedUserId = userID,
                                    CreationDateTime = DateTime.Now
                                };

                                db.Flow_OrderTemplateSalesPerson.Add(newSalesPerson);
                                db.SaveChanges();
                            }
                        }
                    }

                    // update or add Custom Field Data
                    if (customFields != null)
                    {
                        foreach (var customField in customFields)
                        {
                            if (customField.ItemCustomFieldId > 0)
                            {
                                DAL.Flow_OrderTemplateCustomField databaseCustomField = db.Flow_OrderTemplateCustomField.Where(a => a.OrderTemplateCustomFieldId == customField.ItemCustomFieldId && a.IsDeleted == false).SingleOrDefault();

                                if (customField.CustomFieldValue != databaseCustomField.CustomFieldValue)
                                {
                                    BL.Setup.cls_Setup.Log(customField.ItemCustomFieldId, "Flow_OrderTemplateCustomField.CustomFieldValue", databaseCustomField.CustomFieldValue, customField.CustomFieldValue, userID);
                                    databaseCustomField.CustomFieldValue = customField.CustomFieldValue;

                                    databaseCustomField.ModificationDateTime = DateTime.Now;
                                    databaseCustomField.ModifiedUserId = userID;
                                    db.SaveChanges();
                                }
                            }
                            else
                            {
                                DAL.Flow_OrderTemplateCustomField OTCF = new DAL.Flow_OrderTemplateCustomField
                                {
                                    OrderTemplateId = orderID,
                                    CustomFieldId = customField.CustomFieldId,
                                    CustomFieldValue = customField.CustomFieldValue,
                                    CreatedUserId = userID,
                                    CreationDateTime = DateTime.Now
                                };

                                db.Flow_OrderTemplateCustomField.Add(OTCF);
                                db.SaveChanges();
                            }
                        }
                    }

                    //Adding order attachments
                    if (attachment.Count() > 0)
                    {
                        foreach (var file in attachment)
                        {
                            if (file.OrderTemplateAttachmentId > 0)
                            {
                                DAL.Flow_OrderTemplateAttachment orderAttachment = db.Flow_OrderTemplateAttachment.Where(a => a.OrderTemplateAttachmentId == file.OrderTemplateAttachmentId && a.IsDeleted == false).SingleOrDefault();

                                if (orderAttachment.IsDeleted != file.IsDeleted)
                                {
                                    orderAttachment.IsDeleted = file.IsDeleted;
                                    orderAttachment.DeleteDateTime = DateTime.Now;
                                    orderAttachment.DeletedUserId = userID;

                                    db.SaveChanges();
                                }
                            }
                            else
                            {
                                DAL.Flow_OrderTemplateAttachment newAttachment = new DAL.Flow_OrderTemplateAttachment()
                                {
                                    OrderTemplateId = orderID,
                                    AttachmentName = file.AttachmentName,
                                    AttachmentPath = file.AttachmentPath,
                                    CreatedUserId = userID,
                                    CreationDateTime = DateTime.Now,
                                    IsDeleted = false
                                };

                                db.Flow_OrderTemplateAttachment.Add(newAttachment);
                                db.SaveChanges();
                            }
                        }
                    }

                    scope.Complete();
                    success = true;
                    founderrorList.Add(2);
                }
            }

            errorList = founderrorList;
            return success;
        }


        public static ArrayList GetItemCustomUnitPrice(int customerID, int itemID, decimal selectedQuantity, int selectedUOMID, int? storeID)
        {
            ArrayList dataList = new ArrayList();

            using (DAL.inventoryEntities db = new DAL.inventoryEntities())
            {
                var customerData = db.Data_CustomerVendorData.Include("Data_PriceList")
                                                             .Include("Data_PriceList.Data_PriceListItem")
                                                             .Where(a => a.CustomerVendorDataId == customerID &&
                                                                         a.IsDeleted == false &&
                                                                         a.Data_PriceList.ValidFrom <= DateTime.Today &&
                                                                         a.Data_PriceList.ValidTo >= DateTime.Today &&
                                                                         a.Data_PriceList.IsDeleted == false).SingleOrDefault();

                if (customerData != null)
                {
                    var item = customerData.Data_PriceList.Data_PriceListItem.Where(a => a.ItemId == itemID && a.IsDeleted == false).SingleOrDefault();

                    if (item != null)
                    {
                        //var itemData = db.Data_ItemData.Where(a => a.ItemDataId == itemID && a.IsDeleted == false).SingleOrDefault();

                        decimal customPrice = 0;

                        if (item.UOMId == selectedUOMID)
                        {
                            dataList.Add((item.CustomPrice / item.Quantity));
                        }
                        else
                        {
                            BL.General.cls_UOMEquivelent.UOMEQ UOMEqObject = BL.General.cls_UOMEquivelent.GETUOMEquivelent(selectedUOMID, item.UOMId);

                            if (UOMEqObject != null)
                            {
                                decimal oneUnitCost = item.CustomPrice / item.Quantity;

                                if (UOMEqObject.BaseUnitId == selectedUOMID)
                                {
                                    customPrice = oneUnitCost * (decimal)UOMEqObject.Value;
                                    dataList.Add(customPrice);
                                }
                                else
                                {
                                    customPrice = oneUnitCost / (decimal)UOMEqObject.Value;
                                    dataList.Add(customPrice);
                                }
                            }
                        }

                        dataList.Add(item.CurrencyId);
                        dataList.Add(selectedUOMID);
                    }
                    else
                    {
                        dataList = GetItemUnitPriceByEvaluationMethod(itemID, dataList, selectedQuantity, selectedUOMID, storeID);
                    }
                }
                else
                {
                    dataList = GetItemUnitPriceByEvaluationMethod(itemID, dataList, selectedQuantity, selectedUOMID, storeID);
                }

            }

            return dataList;
        }

        public static ArrayList GetItemUnitPriceByEvaluationMethod(int itemID, ArrayList dataList, decimal selectedQuantity, int selectedUOMID, int? storeID)
        {
            using (DAL.inventoryEntities db = new DAL.inventoryEntities())
            {
                int? evaluationmethodID = db.Data_ItemData.Where(a => a.ItemDataId == itemID && a.IsDeleted == false).SingleOrDefault().EvaluationMethodId;

                List<DAL.Flow_StoreItemTransaction> storeItemTransaction;

                if (storeID == null)
                {
                    storeItemTransaction = db.Flow_StoreItemTransaction.Where(a => a.ItemId == itemID && a.IsDeleted == false && a.IsAddition == true).ToList();
                }
                else
                {
                    storeItemTransaction = db.Flow_StoreItemTransaction.Where(a => a.ItemId == itemID && a.IsDeleted == false && a.IsAddition == true && a.Setup_Store.StoreId == storeID && a.Setup_Store.IsDeleted == false && a.Setup_Store.IsActive == true).ToList();
                }



                var itemData = db.Data_ItemData.Where(a => a.ItemDataId == itemID && a.IsDeleted == false).SingleOrDefault();

                if (evaluationmethodID != null && storeItemTransaction != null)
                {
                    decimal CustomPrice = 0;
                    decimal quantity_X_price = 0;
                    decimal quantity = 0;

                    decimal itemUnitPrice = 0;
                    decimal itemQuantity = 0;

                    decimal orginalQuantity = selectedQuantity;

                    if (evaluationmethodID == 1)
                    {
                        foreach (var i in storeItemTransaction.OrderBy(a => a.CreationDateTime))
                        {
                            calculationForeEachTransaction(selectedUOMID, itemData, ref itemUnitPrice, ref itemQuantity, i);

                            if (itemQuantity > 0 && itemQuantity <= orginalQuantity)
                            {
                                decimal remindar = orginalQuantity - itemQuantity;

                                decimal taken = orginalQuantity - remindar;

                                quantity_X_price += taken * itemUnitPrice;

                                if (remindar == 0)
                                {
                                    break;
                                }
                                else
                                {
                                    orginalQuantity = remindar;
                                }
                            }
                            else
                            {
                                quantity_X_price += orginalQuantity * itemUnitPrice;
                                break;
                            }
                        }

                        CustomPrice = quantity_X_price / selectedQuantity;

                        dataList.Add(CustomPrice);
                        dataList.Add(itemData.SalesPriceCurrencyId);
                        dataList.Add(selectedUOMID);

                    }
                    else if (evaluationmethodID == 2)
                    {
                        foreach (var i in storeItemTransaction.OrderByDescending(a => a.CreationDateTime))
                        {
                            calculationForeEachTransaction(selectedUOMID, itemData, ref itemUnitPrice, ref itemQuantity, i);

                            if (itemQuantity > 0 && itemQuantity <= orginalQuantity)
                            {
                                decimal remindar = orginalQuantity - itemQuantity;

                                decimal taken = orginalQuantity - remindar;

                                quantity_X_price += taken * itemUnitPrice;

                                if (remindar == 0)
                                {
                                    break;
                                }
                                else
                                {
                                    orginalQuantity = remindar;
                                }
                            }
                            else
                            {
                                quantity_X_price += orginalQuantity * itemUnitPrice;
                                break;
                            }
                        }

                        CustomPrice = quantity_X_price / selectedQuantity;

                        dataList.Add(CustomPrice);
                        dataList.Add(itemData.SalesPriceCurrencyId);
                        dataList.Add(selectedUOMID);
                    }
                    else if (evaluationmethodID == 3)
                    {
                        foreach (var i in storeItemTransaction)
                        {
                            calculationForeEachTransaction(selectedUOMID, itemData, ref itemUnitPrice, ref itemQuantity, i);

                            quantity_X_price += itemUnitPrice * itemQuantity;
                            quantity += itemQuantity;
                        }

                        CustomPrice = quantity_X_price / quantity;

                        dataList.Add(CustomPrice);
                        dataList.Add(itemData.SalesPriceCurrencyId);
                        dataList.Add(selectedUOMID);
                    }

                }
            }
            return dataList;
        }

        private static void calculationForeEachTransaction(int selectedUOMID, DAL.Data_ItemData itemData, ref decimal itemUnitPrice, ref decimal itemQuantity, DAL.Flow_StoreItemTransaction i)
        {
            if (itemData.SalesPriceCurrencyId == i.CurrencyId)
            {
                itemUnitPrice = i.ItemPrice;
            }
            else if (i.CurrencyId == null)
            {
                itemUnitPrice = 0;
            }
            else
            {
                decimal? convertedValue = GetItemUnitPriceByDifferentCurrency(i.CurrencyId, itemData.SalesPriceCurrencyId, i.ItemPrice);
                itemUnitPrice = (decimal)convertedValue;
            }


            if (selectedUOMID == i.UOMId)
            {
                itemQuantity = i.RemindQuantity;
            }
            else
            {
                BL.General.cls_UOMEquivelent.UOMEQ UOMEqObject = BL.General.cls_UOMEquivelent.GETUOMEquivelent(selectedUOMID, i.UOMId);

                if (UOMEqObject != null)
                {
                    if (UOMEqObject.BaseUnitId == selectedUOMID)
                    {
                        itemQuantity = i.RemindQuantity / (decimal)UOMEqObject.Value;
                    }
                    else
                    {
                        itemQuantity = i.RemindQuantity * (decimal)UOMEqObject.Value;
                    }
                }
            }
        }

        public static decimal? GetItemUnitPriceByDifferentCurrency(int? fromCurrency, int? toCurrency, decimal? value)
        {
            decimal? convertedValue = 0;

            using (DAL.inventoryEntities db = new DAL.inventoryEntities())
            {
                int defaultCurrency = BL.Static.cls_StaticData.GetDefaultCurrencyId();


                if (fromCurrency == defaultCurrency)
                {

                    var result = db.Setup_CurrencyExchangeRate.Where(a => a.BaseCurrencyId == fromCurrency && a.EquivalentCurrencyId == toCurrency && a.EffectiveDate <= DateTime.Today)
                        .OrderByDescending(a => a.EffectiveDate).FirstOrDefault();

                    convertedValue = value * result.EquivalentQuantity;
                }
                else
                {
                    var result = db.Setup_CurrencyExchangeRate.Where(a => a.BaseCurrencyId == toCurrency && a.EquivalentCurrencyId == fromCurrency && a.EffectiveDate <= DateTime.Today)
                        .OrderByDescending(a => a.EffectiveDate).FirstOrDefault();

                    convertedValue = value / result.EquivalentQuantity;
                }
            }

            return convertedValue;


        }

        //Charges
        public static List<BL.Quotation.CutomerQuotationItems> Sales_Order_Charg__ForReport(int OrderID, string Lang)
        {
            //List<DAL.Flow_OrderTemplateItem> OrderItems = new List<DAL.Flow_OrderTemplateItem>();
            List<BL.Quotation.CutomerQuotationItems> SalesOrderItems = new List<BL.Quotation.CutomerQuotationItems>();
            //DAL.Flow_OrderTemplate Order = SelectSalesOrder(OrderID, 3);
            //List<DAL.Flow_OrderTemplateCharg> Charges = Order.Flow_OrderTemplateCharg.ToList();

            //using (DAL.inventoryEntities db = new DAL.inventoryEntities())
            //{
            //    try
            //    {
            //        foreach (var Charg in Charges)
            //        {
            //            SalesOrderItems.Add(new Quotation.CutomerQuotationItems()
            //            {
            //                chargeName = db.Setup_Charge.Where(ch => ch.ChargeId == Charg.OrderTemplateChargId).Select(a => a.ChargesTitle).SingleOrDefault(),
            //                chargeDescription = Charg.Description.ToString(),
            //                chargeAmount = Charg.ChargeValue,
            //            });
            //        }
            //    }
            //    catch
            //    { }
            //}
            return SalesOrderItems;
        }

        //Taxes
        public static List<BL.Quotation.CutomerQuotationItems> Sales_Order_Taxes__ForReport(int OrderID, string Lang)
        {
            //List<DAL.Flow_OrderTemplateItem> OrderItems = new List<DAL.Flow_OrderTemplateItem>();
            List<BL.Quotation.CutomerQuotationItems> SalesOrderItems = new List<BL.Quotation.CutomerQuotationItems>();
            //DAL.Flow_OrderTemplate Order = SelectSalesOrder(OrderID, 3);
            //List<DAL.Flow_OrderTemplateTax> Taxes = Order.Flow_OrderTemplateTax.ToList();

            //using (DAL.inventoryEntities db = new DAL.inventoryEntities())
            //{
            //    try
            //    {
            //        foreach (var Tax in Taxes)
            //        {
            //            SalesOrderItems.Add(new Quotation.CutomerQuotationItems()
            //            {
            //                taxName = db.Setup_TaxItem.Where(t => t.TaxItemId == Tax.TaxId).Select(a => a.TaxItemTitle).SingleOrDefault(),
            //                taxDescription = Tax.Description.ToString(),
            //                taxAmount = Tax.TaxValue,
            //            });
            //        }
            //    }
            //    catch
            //    { }
            //}
            return SalesOrderItems;
        }


        //Discounts
        public static List<BL.Quotation.CutomerQuotationItems> Sales_Order_Descounds__ForReport(int OrderID, string Lang)
        {
            //List<DAL.Flow_OrderTemplateItem> OrderItems = new List<DAL.Flow_OrderTemplateItem>();
            List<BL.Quotation.CutomerQuotationItems> SalesOrderItems = new List<BL.Quotation.CutomerQuotationItems>();
            //DAL.Flow_OrderTemplate Order = SelectSalesOrder(OrderID, 3);
            //List<DAL.Flow_OrderTemplateDiscount> Descounts = Order.Flow_OrderTemplateDiscount.ToList();

            //using (DAL.inventoryEntities db = new DAL.inventoryEntities())
            //{
            //    try
            //    {
            //        foreach (var Desc in Descounts)
            //        {
            //            SalesOrderItems.Add(new Quotation.CutomerQuotationItems()
            //            {
            //                DescName = db.Setup_Discount.Where(t => t.DiscountId == Desc.OrderTemplateDiscountId).Select(a => a.DiscountTitle).SingleOrDefault(),
            //                DescDescription = Desc.Description.ToString(),
            //                DescAmount = Desc.DiscountValue,
            //            });
            //        }
            //    }
            //    catch
            //    { }
            //}
            return SalesOrderItems;
        }


        //Items
        public static List<BL.Quotation.CutomerQuotationItems> Sales_Order_ByOrderID_Lang__ForReport(int OrderID, string Lang)
        {
            List<DAL.Flow_OrderTemplateItem> OrderItems = new List<DAL.Flow_OrderTemplateItem>();
            List<BL.Quotation.CutomerQuotationItems> SalesOrderItems = new List<BL.Quotation.CutomerQuotationItems>();


            DAL.Flow_OrderTemplate Order = SelectSalesOrder(OrderID, 3);
          List<DAL.Flow_OrderTemplateItem> items = Order.Flow_OrderTemplateItem.ToList();
          //List<DAL.Flow_OrderTemplateCharg> Charges = Order.Flow_OrderTemplateCharg.ToList();
          //List<DAL.Flow_OrderTemplateTax> Taxes = Order.Flow_OrderTemplateTax.ToList();
          //List<DAL.Flow_OrderTemplateDiscount> Descounts = Order.Flow_OrderTemplateDiscount.ToList();


          using (DAL.inventoryEntities db = new DAL.inventoryEntities())
          {
              try
              {
                  foreach (var item in items)
                  {
                      string dsk = item.Description;
                      var shipadress = db.Setup_Address.Where(a => a.AddressId == item.Flow_OrderTemplate.ShipmentAddressId).SingleOrDefault();
                      string addresss = BL.Data.cls_CustomerVendorAddress.BuildAddress(Lang, "", shipadress, 2);


                      double qty = (double)item.ItemQuantity;
                      Math.Round(qty, 2);
                      decimal uprc = (decimal)item.UnitPrice;
                      uprc = Math.Round(uprc, 2);


                      string oc = "";

                    //  oc = item.Static_Country.CountryEnglishName == null ?  oc = "" :oc = item.Static_Country.CountryEnglishName ;
                     
                         
                      DateTime? ddt = item.Flow_OrderTemplate.DateOfDelivery;
                      string dlvrydt = "";
                      if (ddt != null)
                      {
                          dlvrydt = DateTime.Parse(ddt.ToString()).ToString("dd-MM-yyyy");
                          ddt.ToString();
                      }

                      if (ddt == null )
                      {
                          dsk = dsk + " " + "\r\n" + "Delivery Date: " + " " + dlvrydt + "\r\n";// +oc;
                      }
                      else
                      {
                          dsk = dsk + "";
                      }
                      SalesOrderItems.Add(new Quotation.CutomerQuotationItems()
                      {
                          //Header
                          qoutationCode = item.Flow_OrderTemplate.TemplateCode,
                          paperQoutationNo = item.Flow_OrderTemplate.PaperTemplateCode,
                          deliveryDate = item.Flow_OrderTemplate.DateOfDelivery,

                          //Details

                          itemName = item.Data_ItemData.ItemName,
                          itemCode = item.Data_ItemData.ItemCode,
                          description = dsk,
                          qunitName = item.Setup_UOM.UnitName.ToString() + " " + qty.ToString(),
                          unitName = Order.Setup_Currency.CurrencyEnglishName + " " + uprc.ToString(),
                          discount = item.Discount,
                          totalAmmount = (item.ItemQuantity * item.UnitPrice) - item.Discount,
                          isSalesTax = db.Data_ItemData.Where(i => i.ItemDataId == item.Data_ItemData.ItemDataId).SingleOrDefault().IsSalesTaxIncluded,
                          discountPerItem = item.Discount,
                          itemUnitPrice = item.UnitPrice,
                          itemqunity = item.ItemQuantity,
                          //Foter
                          paymentMethod = db.Setup_PaymentMethod.Where(m => m.PaymentMethodId == item.Flow_OrderTemplate.PaymentMethodId).Select(a => a.PaymentMethodName).SingleOrDefault(),

                          paymentTerm = db.Setup_PaymentTerm.Where(m => m.PaymentTermId == item.Flow_OrderTemplate.PaymentMethodId).Select(a => a.PaymentTermName).SingleOrDefault(),

                          paymentDate = item.Flow_OrderTemplate.PaymentDate,

                          currency = db.Setup_Currency.Where(m => m.CurrencyId == item.Flow_OrderTemplate.CurrencyId).Select(a => a.CurrencyEnglishName).SingleOrDefault(),

                          message = item.Flow_OrderTemplate.Message,
                          messageTitle = item.Flow_OrderTemplate.Setup_MessageTitle.MessageTitleInEnglish,

                          //Customer Data
                          customerName = db.Data_CustomerVendorData.Where(m => m.CustomerVendorDataId == item.Flow_OrderTemplate.PaymentMethodId).Select(a => a.CustomerVendorName).SingleOrDefault(),

                          customerAddress = addresss,
                          deduction = item.Flow_OrderTemplate.Deduction,
                          TotalCharg = item.Flow_OrderTemplate.Flow_OrderTemplateCharg.Where(m => m.OrderTemplateId == OrderID).Sum(m => m.ChargeValue),
                          TotalDiscount = item.Flow_OrderTemplate.Flow_OrderTemplateDiscount.Where(m => m.OrderTemplateId == OrderID).Sum(m => m.DiscountValue),
                          TotalTax = item.Flow_OrderTemplate.Flow_OrderTemplateTax.Where(m => m.OrderTemplateId == OrderID).Sum(m => m.TaxValue)
                          

                      });
                  }

                

                  
              }





              catch
              { }
          }

          return SalesOrderItems;
        }


     
        


    }
}
